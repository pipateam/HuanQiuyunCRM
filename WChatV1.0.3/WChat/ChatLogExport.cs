﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WChat;
namespace WChat {
   public   class ChatLogExport {
        //private string _fil_base64 = "data:img/jpg;base64,";
        //private string _Voi_base64 = "data:img/jpg;base64,";
        //private string _gro_base64 = "data:img/jpg;base64,";
        //private string _friend_base64 = "data:img/jpg;base64,";
        //private string _me_base64 = "data:img/jpg;base64,";
        //private string _locicon_base64 = "data:img/jpg;base64,";
        //private string sendpic_base64 = "data:img/jpg;base64,";
        //private string recpic_base64 = "data:img/jpg;base64,";

        public void ChatLog_Export( string FileName, string AccNam,string Fri) {
            SqlHandle sql = new SqlHandle();
            List<WXMsg> limsg = new List<WXMsg>();
            
                string msg = @"<!DOCTYPE html>
                               <html>  
                               <head> 
                               <meta http-equiv=""X-UA-Compatible"" content=""IE=9""/>
                               <meta http-equiv=""Content-Type""content=""text/html; charset=utf-8""/> 
                               <script type=""text/javascript""> window.scrollTo(0,document.body.scrollHeight); </script >
                               <style type = ""text/css"" >
                               body{ font-size:12px; line-height:22px; margin:2px; }
                               td{ font-size:12px; line - height:22px; }
                               </style >
                               </head >
                               <body > 
                               <table width=100% cellspacing=0><tr><td><div style=padding-left:10px;><br><b>消息记录</b></div></td></tr>
                               <tr><td><div style=padding-left:10px;>当前客服:"+ AccNam + @"</div></td></tr>
                               <tr><td><div style=padding-left:10px;>消息对象:" + Fri + @"</div></td></tr>
                               <tr><td><div style=padding-left:10px;>&nbsp;</div></td></tr>";

                limsg = sql.GetChatLogs(AccNam, Fri);
                for (int i = limsg.Count - 1; i >= 0; i--) {
                    if (limsg[i].IsReceive == 1) {
                        msg += ShowSend(limsg[i]);
                    }
                    else {
                        msg += ShowReceive(limsg[i]);
                    }
                    msg += @"</table></body></html>";
                }

                FileStream fs = new FileStream(FileName, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(msg);
                sw.Close();
     }

        public  string ShowSend( WXMsg msg ) {
            string _msg = "";
            switch (msg.Type) {
            case "1":
            _msg = showsendmsg(msg);
            break;
            //case "3":
            //_msg = showsendpic(msg);
            //break;
            //case "42":
            //_msg = showsendcard(msg);   //发送名片
            //break;
            //case "34":
            //_msg = showsendvoice(msg); //语音消息
            //break;
            //case "47":
            //_msg = showsendmsg(msg);  //不支持的表情
            //break;
            //case "99":
            //_msg = showlocpic(msg);
            //break;
            //case "49":
            //if (msg.FileName == "微信转账") {
            //    _msg = showreceivemsg(msg);
            //}
            //else if (msg.TxtUrl.Trim() == "" && msg.TxtUrl.Length == 0) { //文件
            //    _msg = showsendfilemsg(msg);
            //}
            //else {
            //    _msg = showsendurlmsg(msg);
            //}
            //break;
            //case "10000":
            //msg.Msg = "发出红包，请在手机上查看";
            //_msg = showsendmsg(msg);
            //break;
            //default:
            //_msg = showsendmsg(msg);
            //break;
            }
            return _msg;
        }

        public string ShowReceive( WXMsg msg ) {
            string _msg = "";
            switch (msg.Type) {
            case "1":
                _msg = showreceivemsg(msg);
            break;
            //case "3":
            //_msg = showreceivepic(msg);
            //break;
            //case "34":
            //_msg = showreceivevoice(msg);     //接受语音
            //break;
            //case "42":
            //_msg = showreceivecard(msg);   //接受名片
            //break;
            //case "47":
            //_msg = showreceivemsg(msg);
            //break;
            //case "37":
            //_msg = showreceiveverifymsg(msg);
            //break;
            //case "49":
            //if (msg.FileName == "微信转账") {
            //    _msg = showreceivemsg(msg);
            //}
            //else if (msg.TxtUrl.Trim() == "" && msg.TxtUrl.Length == 0) { //文件
            //    _msg = showreceivefilemsg(msg);  //先屏蔽了，否则会卡死
            //}
            //else {
            //    _msg = showreceiveurlmsg(msg); //链接
            //}
            //break;
            //case "10000":
            //_msg = showreceivemsg(msg);
            //break;
            //default:
            //_msg = showreceivemsg(msg);
            //break;
            }
            return _msg;
        }

        private string showsendmsg( WXMsg msg ) {
            string str = @"  
            <tr><td><div style=color:#42B475;padding-left:10px;><div style=float:left;margin-right:6px;>"+msg.Fr_NickName+ @"</div>" + msg.Time.ToString("f") + @"</div>
            <div style=padding-left:20px;>
            <font style=""font -size:10pt; font - family:'宋体','MS Sans Serif',sans - serif; "" color='000000'>" + msg.Msg + @"</font>
            </div></td></tr>";

            return str;
        }
        private string showreceivemsg( WXMsg msg ) {
            string str = @"  
           <tr><td><div style=color:#006EFE;padding-left:10px;><div style=float:left;margin-right:6px;>" + msg.To_NickName + @"</div>" + msg.Time.ToString("f") + @"</div>
           <div style=padding-left:20px;>
           <font style=""font - size:10pt; font - family:'宋体','MS Sans Serif',sans - serif;"" color='000000'>"+ msg.Msg + @"</font>
           </div></td></tr>";

            return str;
        }

        #region 记录

        //private string showsendpic( WXMsg msg ) {
        //    try {
        //        string path = WChat.path.paths;
        //        string picpath = path + "\\" + msg.MsgId + "small.jpg";
        //        if (File.Exists(picpath)) {
        //            using (MemoryStream ms = new MemoryStream()) {
        //                Bitmap b = new Bitmap(picpath);
        //                b.Save(ms, ImageFormat.Png);
        //              //  sendpic_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                b.Dispose();
        //            }
        //        }
        //    }
        //    catch (Exception e) {
        //        WriteLog.Writelog("showsendpic", e.ToString());
        //    }

        //    string str = @"
        //     <div style=""clear:both; "">
        //     <li style=""  margin-bottom:13px;"" > <div style=""color:white;margin-top:6px; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //         <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#fff #fff #fff #32cd32; border-bottom-color:#fff;""></div>  
        //         <div  class= ""ss""   onclick=""go();"" style=""float:right; color:white; padding-top: 10px; padding-bottom: 10px; border-radius:5px; padding-left: 10px;padding-right: 10px; font-family:微软雅黑; height: auto; min-height:10px; width:auto; max-width:300px; background:#32cd32;""><img src=""" + sendpic_base64 + @""" /></div>     
        //         </div> ";
        //    return str;
        //}
        //private string showlocpic( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) { return null; }
        //    if (msg != null)  //图片
        //      {
        //        try {
        //            using (MemoryStream ms = new MemoryStream()) {
        //                string picpath = msg.FileName;
        //                if (File.Exists(picpath)) {
        //                    Bitmap b = new Bitmap(picpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    _locicon_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                    b.Dispose();
        //                }
        //            }
        //        }
        //        catch (Exception e) {
        //            WriteLog.Writelog("showlocpic", e.ToString());
        //        }
        //    }
        //    string str = @"
        //    <div style=""clear:both; "">
        //    <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //    <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:right;"">
        //    <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + _me_base64 + @""" style="" height: 40px; width: 40px;"">  
        //    </div>
        //    <div style=""float:right; top:5px; magin-right:-45px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#fff #fff #fff #32cd32; border-bottom-color:#fff;""></div>  
        //    <div  class= ""ss""   onclick=""go();"" style=""float:right;color:white; padding-top: 10px; padding-bottom: 10px; border-radius:5px; padding-left: 10px;padding-right: 10px; font-family:微软雅黑; height: auto; min-height:10px; width:auto; max-width:300px; background:#32cd32;"">
        //    <img src=""" + _locicon_base64 + @""" width =""80px""  />
        //    </div>    
        //    </div> ";
        //    return str;
        //}
        //private string showsendfilemsg( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) {
        //    //    return null;
        //    //}
        //    using (MemoryStream ms = new MemoryStream()) {
        //        string picpath = msg.FileName;
        //        Bitmap backImage = new Bitmap(this.GetType(), "file.jpg");
        //        Bitmap b = new Bitmap(backImage);
        //        b.Save(ms, ImageFormat.Png);
        //        _fil_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //    }
        //    string str = @"
        //      <div style=""clear:both; "">
        //      <li style=""  margin-bottom:25px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //       <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:right;"">
        //       <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + _me_base64 + @""" style="" height: 40px; width: 40px;"">  
        //       </div>
        //       <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#fff #fff #fff #AAAAAA; border-bottom-color:#fff;""></div>  

        //        <div onclick=""go();""  class= ""ss"" style=""float:right; border-radius:5px;  margin-top:-13px; padding: 10px; font-family:微软雅黑; margin-left:0px;height: 80px; min-height:10px; width:auto; background:white; border:1px solid #AAAAAA;"">                    
        //        <div style=""width:140px;height:50px;text-overflow: ellipsis;overflow:hidden;margin-top:-10px; float:left;line-height:13px;"">
        //        <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.Msg + @"</p>
        //        <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileSize + "B" + @"</p>
        //        </div>
        //        <div style""width:140;float:right;"">
        //        <img src=""" + _fil_base64 + @""" width =""30px""/>
        //        </div>
        //        <hr color=""#DDDDDD"" /> 
        //        <p style=""margin:0px;font-size:10px;color:#AAAAAA;"">环球云控</p>                          
        //        </div>     
        //        </div> ";
        //    return str;
        //}
        //private string showsendurlmsg( WXMsg msg ) {
        //    string _url_base64 = "data:img/jpg;base64,";
        //    string filena = null;
        //    //if (_meUser == null || _friendUser == null) { return null; }
        //    try {
        //        string path = WChat.path.paths;
        //        if (!Directory.Exists(path))
        //            Directory.CreateDirectory(path);
        //        string picpath = path + "\\" + msg.MsgId + "small.jpg";
        //        if (File.Exists(picpath)) {
        //            filena = picpath;
        //            using (MemoryStream ms = new MemoryStream()) {
        //                Bitmap url = new Bitmap(filena);
        //                url.Save(ms, ImageFormat.Png);
        //                _url_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //            }
        //        }
        //    }
        //    catch (Exception e) {
        //        WriteLog.Writelog("showsendurlmsg", e.ToString());
        //    }

        //    string str = @"
        //     <div style=""clear:both; "">
        //     <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //     <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:right;"">
        //     <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + _me_base64 + @""" style="" height: 40px; width: 40px;"">  
        //     </div>
        //     <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#fff #fff #fff #AAAAAA; border-bottom-color:#fff;""></div>  
        //     <div onclick=""go();""  class= ""ss"" style=""float:right; border-radius:5px;  margin-top:-10px;padding:10px; font-family:微软雅黑;height: 100px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">
        //     <div style=""width:180px;height:50px;text-overflow: ellipsis;overflow:hidden;margin-top:-15px;"">
        //     <p style=""text-overflow: ellipsis;overflow:hidden;font-size:13px;"">" + msg.FileName + @"</p>                       
        //     </div>
        //     <div style=""clear:both;height:50px"">
        //     <div style=""width:140px;height:60px;text-overflow: ellipsis;overflow:hidden;margin-top:-10px; float:left;line-height:10px;"">
        //     <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileName + @"</p>
        //     </div>
        //     <div style""width:140;float:right;"">
        //     <img src=""" + _url_base64 + @""" width=""38px"" height=""38px"" />
        //     </div>
        //     </div> 

        //     </div>     
        //     </div> ";
        //    return str;
        //}
        //private string showsendcard( WXMsg w ) {
        //    //if (w == null || _friendUser == null)
        //    //    return string.Empty;
        //    //局部变量声明
        //    string path = WChat.path.paths;
        //    string str = null;
        //    string tempName = null;

        //    //名字长度取舍
        //    if (w.Msg.Length > 10)
        //        tempName = w.Msg.Substring(0, 10);
        //    else
        //        tempName = w.Msg;

        //    //名片格式
        //    str = @"<script type=""text/javascript"">window.location.hash = ""#ok"";</script>
        //                    <div style=""clear:both;""><li style=""margin-bottom:13px; list-style:none;"">
        //                        <div style=""margin-top:6px; color:white; background:#808080; width:60px; border-radius:5px; position:absolute; right:50%; text-align:center;"">" + w.Time.ToLongTimeString() + @"</div>
        //                    </li>
        //                    <br />
        //                    <div style=""clear:both; margin:15px 0 0 0;"">
        //                       <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:right;"">
        //                       <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + _me_base64 + @""" style="" height: 40px; width: 40px;"">
        //                       </div>
        //                        <div onclick=""go();"" class=""ss"" style = ""border:1px solid Gray; height:100px; width:300px; float:right; margin:0 5px 0 0; border-radius:5px; cursor:pointer;"">
        //                            <div style = ""height:20px; border-bottom:1px #33CC66 solid; padding:8px 5px 2px 5px; width:90%; margin:0 0 0 3%; text-align:left;"">名片</div>
        //                            <div style=""height:70px; line-height:70px;"">
        //                                <div style = ""float:left; margin:5px 0 5px 3%;""><img src = """ + w.MsgId + @""" style=""border-radius:60px;"" height=""60"" width=""60""/></div>
        //                                <div style = ""float:left; margin-left:10px;"">&nbsp;" + tempName + @"</div>
        //                            </div>
        //                        </div>
        //                    </div>
        //                    </div></div>";
        //    return str;
        //}    //名片
        //private string showsendvoice( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) { return null; }
        //    try {
        //        using (MemoryStream ms = new MemoryStream()) {
        //            Bitmap backImage = new Bitmap(this.GetType(), "voice_right.png");
        //            Bitmap b = new Bitmap(backImage);
        //            b.Save(ms, ImageFormat.Png);
        //            _Voi_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //        }
        //    }
        //    catch (Exception e) {
        //        WriteLog.Writelog("showsendvoice", e.ToString());
        //    }

        //    string str = @"<script type=""text/javascript"">window.location.hash = ""#ok"";</script> 
        //     <div style=""clear:both; "">
        //     <li style=""  margin-bottom:25px;"" > <div style=""color:white;margin-top:6px; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  
        //    <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:right;"">

        //    <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + _me_base64 + @""" style="" height: 40px; width: 40px;"">
        //    </div> 

        //     <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#fff #fff #fff #32cd32; border-bottom-color:#fff;""></div>  
        //     <div  class= ""ss""  onclick=""go();"" style=""float:right; color:white; padding-top: 10px; padding-bottom: 5px; border-radius:5px;  font-family:微软雅黑; height:25px;  min-width:70px; max-width:300px; background:#32cd32;"">&nbsp &nbsp &nbsp &nbsp &nbsp <img src=""" + _Voi_base64 + @""" /></div>     
        //     </div> ";

        //    return str;

        //}





        //private string showreceivepic( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) { return null; }
        //    string headimg = "";
        //    string nickname = "";
        //    try {
        //        if (msg.IsGroup) {
        //            nickname = msg.CrewNickName;
        //            string path = WChat.path.paths;
        //            if (!Directory.Exists(path))
        //                Directory.CreateDirectory(path);
        //            string headpath = path + "\\" + msg.CrewNickName.Trim() + ".jpg";
        //            if (File.Exists(headpath)) {
        //                using (MemoryStream ms = new MemoryStream()) {
        //                    Bitmap b = new Bitmap(headpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    _gro_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                }
        //                headimg = _gro_base64;
        //            }
        //            else {
        //                headimg = _friend_base64;
        //            }
        //            /////////////////////////////////////////////////////////接受图片
        //            string picpath = path + "\\" + msg.MsgId + "small.jpg";
        //            if (File.Exists(picpath)) {
        //                using (MemoryStream ms = new MemoryStream()) {
        //                    Bitmap b = new Bitmap(picpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    recpic_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                }
        //            }
        //        }
        //        else {
        //            headimg = _friend_base64;
        //            nickname = _friendUser.ShowName;
        //        }
        //    }
        //    catch (Exception e) {
        //        WriteLog.Writelog("showreceivepic", e.ToString());
        //    }

        //    string str = @"
        //   <div style=""clear:both; width: 100%;"" >
        //   <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //   <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:left;"">
        //    <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + headimg + @""" style=""height: 40px; width: 40px;"">
        //   </div>
        //    <p style=""margin-left:50px; color:gray; "" class="" chat_nick"">" + nickname + @"</p>  
        //   <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#fff #e6e6fa #fff #fff; border-bottom-color:#fff;""></div>  
        //   <div onclick=""go();"" class= ""ss""  style=""float:left; border-radius:5px;  margin-top:-8px; padding: 10px; font-family:微软雅黑; margin-left:0px;height: auto; min-height:10px; width:auto; max-width:300px; background:#e6e6fa; ""><img src=""" + recpic_base64 + @""" /></div>    
        //   </div> ";
        //    return str;
        //}
        //private string showreceivefilemsg( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) { return null; }

        //    string headimg = "";
        //    string nickname = "";
        //    if (msg.IsGroup) {
        //        nickname = msg.CrewNickName;
        //        try {
        //            string path = WChat.path.paths;
        //            if (!Directory.Exists(path))
        //                Directory.CreateDirectory(path);
        //            string picpath = path + "\\" + msg.CrewNickName.Trim() + ".jpg";
        //            if (File.Exists(picpath)) {
        //                using (MemoryStream ms = new MemoryStream()) {
        //                    Bitmap b = new Bitmap(picpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    _gro_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                }
        //                headimg = _gro_base64;
        //            }
        //            else {
        //                headimg = _friend_base64;
        //            }
        //        }
        //        catch (Exception e) {
        //            WriteLog.Writelog("showreceivefilemsg", e.ToString());
        //        }
        //    }
        //    else {
        //        headimg = _friend_base64;
        //        nickname = _friendUser.ShowName;
        //    }

        //    using (MemoryStream ms = new MemoryStream()) {
        //        string picpath = msg.FileName;
        //        Bitmap backImage = new Bitmap(this.GetType(), "file.jpg");
        //        Bitmap b = new Bitmap(backImage);
        //        b.Save(ms, ImageFormat.Png);
        //        _fil_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //    }
        //    string str = @"
        //   <div style=""clear:both; width: 100%;"" >
        //   <li style=""  margin-bottom:25px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  
        //   <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:left;"">
        //        <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + headimg + @""" style=""height: 40px; width: 40px; "">
        //   </div>
        //   <p style=""margin-left:50px; color:gray; "" class="" chat_nick"">" + nickname + @"</p>  
        //   <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#fff #AAAAAA #fff #fff; border-bottom-color:#fff;""></div> 
        //   <div onclick=""go();""  class= ""ss"" style=""float:left; border-radius:5px; padding:10px;  margin-top:-10px; font-family:微软雅黑;height: 80px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">                     
        //            <div style=""width:140px;height:50px;text-overflow: ellipsis;overflow:hidden;margin-top:-10px; float:left;line-height:13px;"">
        //               <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.FileName + @"</p>
        //               <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileSize + "B" + @"</p>
        //            </div>
        //            <div style""width:140;float:right;"">
        //               <img src=""" + _fil_base64 + @""" width =""30px"" />
        //            </div>
        //            <hr color=""#DDDDDD"" /> 
        //            <p style=""margin:0px;font-size:10px;color:#AAAAAA;"">环球云控</p>    
        //   </div>
        //   </div> ";

        //    return str;
        //}
        //private string showreceiveurlmsg( WXMsg msg ) {
        //    string _url_base64 = "data:img/jpg;base64,";
        //    string path = WChat.path.paths;
        //    string filena = null;

        //    string headimg = "";
        //    string nickname = "";
        //    if (msg.IsGroup) {
        //        nickname = msg.CrewNickName;
        //        try {
        //            string picpath = path + "\\" + msg.CrewNickName.Trim() + ".jpg";
        //            if (File.Exists(picpath)) {
        //                using (MemoryStream ms = new MemoryStream()) {
        //                    Bitmap b = new Bitmap(picpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    _gro_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                }
        //                headimg = _gro_base64;
        //            }
        //            else {
        //                headimg = _friend_base64;
        //            }
        //        }
        //        catch (Exception e) {
        //            WriteLog.Writelog("showreceiveurlmsg", e.ToString());
        //        }
        //    }
        //    else {
        //        headimg = _friend_base64;
        //        nickname = _friendUser.ShowName;
        //    }
        //    try {
        //        string picpath = path + @"\\" + msg.MsgId + "small.jpg";
        //        if (File.Exists(picpath)) {
        //            filena = picpath;
        //            using (MemoryStream ms = new MemoryStream()) {
        //                Bitmap url = new Bitmap(filena);
        //                url.Save(ms, ImageFormat.Png);
        //                _url_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //            }
        //        }
        //    }
        //    catch (Exception e) {
        //        WriteLog.Writelog("showreceiveurlmsg", e.ToString());
        //    }

        //    string str = @"
        //   <div style=""clear:both; width: 100%;"" >
        //   <li style=""  margin-bottom:25px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>            
        //   <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:left;"">
        //        <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + headimg + @""" style=""height: 40px; width: 40px;"">
        //   </div>
        //   <p style=""margin-left:50px; color:gray; "" class="" chat_nick"">" + nickname + @"</p>  
        //   <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#fff #AAAAAA #fff #fff; border-bottom-color:#fff;""></div> 
        //   <div onclick=""go();""  class= ""ss"" style=""float:left; border-radius:5px;  margin-top:-10px;padding:10px; font-family:微软雅黑;height: 100px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">                     
        //            <div style=""width:180px;height:50px;text-overflow: ellipsis;overflow:hidden;margin-top:-15px"">
        //               <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.FileName + @"</p>                       
        //            </div>
        //            <div style=""clear:both;"">
        //                <div style=""width:140px;height:60px;text-overflow: ellipsis;overflow:hidden;margin-top:-10px; float:left;line-height:10px;"">
        //                    <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileName + @"</p>
        //                </div>
        //                <div style""width:140;float:right;"">
        //                    <img src=""" + _url_base64 + @""" width=""38px"" height=""38px"" />
        //                </div>
        //            </div> 

        //   </div>
        //   </div>  ";

        //    return str;
        //}
        //private string showreceiveverifymsg( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) {
        //    //    return null;
        //    //}
        //    string str = @" 
        //   <div style=""clear:both; width: 100%;"">
        //   <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //   <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:left;"">
        //    <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + _friend_base64 + @""" style=""height: 40px; width: 40px;"">
        //    </div>
        //   <p style=""margin-left:50px; color:gray; "" class="" chat_nick"">" + _friendUser.ShowName + @"</p>  
        //   <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#fff #AAAAAA #fff #fff; border-bottom-color:#fff;""></div> 
        //   <div onclick=""go();"" class= ""ss"" style=""float:left; border-radius:5px;  margin-top:-10px;padding:10px; font-family:微软雅黑;height: 100px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">                     
        //            <div style=""width:140px;height:40px;margin-top:-15px"">
        //               <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.Msg + @"</p>                       
        //            </div>
        //            <div style=""clear:both;"">
        //                <div style=""width:140px;height:60px;margin-top:-10px; float:left;line-height:10px;"">
        //                    <p style=""color:#AAAAAA;font-size:10px;text-overflow: ellipsis;overflow:hidden;white-space:nowrap;"">" + msg.V_Content + @"</p>
        //                </div>
        //                <div style""width:140;float:right;"">
        //                   <input type=""button"" value=""通过验证"" style=""color=#808080;""></input>
        //                </div>
        //            </div> 
        //            <hr color=""#DDDDDD"" /> 
        //            <p style=""margin:0px;font-size:10px;color:#AAAAAA;margin-top:-5px;"">环球云控</p>
        //   </div>
        //   </div> ";

        //    return str;
        //}    //名片
        //private string showreceivecard( WXMsg msg ) {
        //    //if (msg == null || _friendUser == null)
        //    //    return string.Empty;
        //    //局部变量声明
        //    string path = WChat.path.paths;
        //    string str = null;
        //    string tempName = null;
        //    string headimg = "";
        //    string nickname = "";
        //    if (!Directory.Exists(path))
        //        Directory.CreateDirectory(path);
        //    if (msg.IsGroup) {
        //        nickname = msg.CrewNickName;
        //        try {
        //            string picpath = path + "\\" + msg.CrewNickName.Trim() + ".jpg";
        //            if (File.Exists(picpath)) {
        //                using (MemoryStream ms = new MemoryStream()) {
        //                    Bitmap b = new Bitmap(picpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    _gro_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                }
        //                headimg = _gro_base64;
        //            }
        //            else {
        //                headimg = _friend_base64;
        //            }
        //        }
        //        catch (Exception e) {
        //            WriteLog.Writelog("showreceivecard", e.ToString());
        //        }
        //    }
        //    else {
        //        headimg = _friend_base64;
        //        nickname = _friendUser.ShowName;
        //    }

        //    //名字长度取舍
        //    if (msg.Msg.Length > 10)
        //        tempName = msg.Msg.Substring(0, 10);
        //    else
        //        tempName = msg.Msg;

        //    //名片格式
        //    str = @"<script type=""text/javascript"">window.location.hash = ""#ok"";</script>
        //                    <div style=""clear:both;""><li style=""margin-bottom:13px; list-style:none;"">
        //                        <div style=""margin-top:6px; color:white; background:#808080; width:60px; border-radius:5px; position:absolute; right:50%; text-align:center;"">" + msg.Time.ToLongTimeString() + @"</div>
        //                    </li>
        //                    <br />
        //                    <div style=""clear:both; margin:15px 0 0 0;"">
        //                        <div style=""float:left;""><img style=""height:40px; width:40px; border-radius:40px; border:solid 2px #ADFF2F"" src = """ + headimg + @"""/></div>
        //                        <div onclick=""go();"" class=""ss"" style = ""border:1px solid Gray; height:100px; width:300px; float:left; margin:0 0 0 5px; border-radius:5px; cursor:pointer;"">
        //                            <div style = ""height:20px; border-bottom:1px #33CC66 solid; padding:8px 5px 2px 5px; width:90%; margin:0 0 0 3%; text-align:left;"">名片</div>
        //                            <div style=""height:70px; line-height:70px;"">
        //                                <div style = ""float:left; margin:5px 0 5px 3%;""><img src = """ + msg.MsgId + @""" style=""border-radius:60px;"" height=""60"" width=""60""/></div>
        //                                <div style = ""float:left; margin-left:10px;"">&nbsp;" + tempName + @"</div>
        //                            </div>
        //                        </div>
        //                    </div>
        //                    </div></div>";
        //    return str;
        //}
        //private string showreceivevoice( WXMsg msg ) {
        //    //if (_meUser == null || _friendUser == null) {
        //    //    return null;
        //    //}
        //    string path = WChat.path.paths;
        //    string headimg = "";
        //    string nickname = "";
        //    if (!Directory.Exists(path))
        //        Directory.CreateDirectory(path);
        //    if (msg.IsGroup) {
        //        nickname = msg.CrewNickName;
        //        try {
        //            string picpath = path + "\\" + msg.CrewNickName + ".jpg";
        //            if (File.Exists(picpath)) {
        //                using (MemoryStream ms = new MemoryStream()) {
        //                    Bitmap b = new Bitmap(picpath);
        //                    b.Save(ms, ImageFormat.Png);
        //                    _gro_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //                }
        //                headimg = _gro_base64;
        //            }
        //            else {
        //                headimg = _friend_base64;
        //            }
        //        }
        //        catch (Exception e) {
        //            WriteLog.Writelog("showreceivevoice", e.ToString());
        //        }
        //    }
        //    else {
        //        headimg = _friend_base64;
        //        nickname = _friendUser.ShowName;
        //    }

        //    using (MemoryStream ms = new MemoryStream()) {
        //        string picpath = msg.FileName;
        //        Bitmap backImage = new Bitmap(this.GetType(), "voice_left.png");
        //        Bitmap b = new Bitmap(backImage);
        //        b.Save(ms, ImageFormat.Png);
        //        _Voi_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
        //    }
        //    string str = @"
        //   <script type=""text/javascript"">window.location.hash = ""#ok"";</script>
        //   <div style=""clear:both; width: 100%;"" >
        //   <li style=""  margin-bottom:25px;"" > <div style=""margin-top:6px;color:white; background:#808080; width:60px; border-radius:5px; position: absolute; right: 50%;text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("T") + @"</div></li>  

        //   <div style=""width:40px; height:40px; border-radius:20px;border:2px solid #ADFF2F; overflow:hidden;float:left;"">
        //    <img  id = ""imgPre"" class=""chat_content_avatar"" src=""" + headimg + @""" style=""height: 40px; width: 40px;"">
        //   </div>
        //    <p style=""margin-left:50px; color:gray; "" class="" chat_nick"">" + nickname + @"</p>  
        //   <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#fff #e6e6fa #fff #fff; border-bottom-color:#fff;""></div>  
        //   <div onclick=""go();"" class= ""ss""  style=""float:left; border-radius:5px; padding-top: 10px; padding-left: 10px;  margin-top:-8px;  font-family:微软雅黑; margin-left:0px; height: 30px; width:60px; max-width:300px; background:#e6e6fa; ""><img src=""" + _Voi_base64 + @""" /></div>    
        //   </div>";

        //    return str;
        //}

        #endregion

        /// <summary>
        /// 表情转换
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string ConvertToGif( string text ) {
            string ctent = "";
            if (text.IndexOf("[") > -1) {
                StringToUrl getUrl = new WChat.StringToUrl();
                Regex reg = new Regex(@"\[(.+?)]");
                List<string> asd_string = new List<string>();
                foreach (Match asd in reg.Matches(text)) {
                    asd_string.Add(asd.Groups[1].ToString());
                    for (int i = 0; i < asd_string.Count; i++) {
                        ctent = text.Replace(asd_string[i], "");
                    }
                    text = ctent.Replace("[]", getUrl.GetEmUrl(asd.Groups[1].ToString()));
                }
                return text;
            }
            return text;
        }
    }
}
