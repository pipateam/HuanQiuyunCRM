﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat {
    /// <summary>
    /// By：李大亮 2017.3.17
    /// 为了多线程创参数的一个下载文件的方法
    /// </summary>
    class DowloadFile {
        public WXUser user;
        public WXUser from_user_name;
        public WXUser to;
        public string mediaid;
        public string filenam;
        /// <summary>
        /// 下载文件
        /// </summary>
        public void downloadfiles() {
           // MessageBox.Show("kaishi");
            user.Download_File(from_user_name.UserName, mediaid, filenam);
        }
        /// <summary>
        /// 上传文件
        /// </summary>
        public void uploadfiles() { 
            user.SendFiles(from_user_name, to, filenam);
        }
    }
}
