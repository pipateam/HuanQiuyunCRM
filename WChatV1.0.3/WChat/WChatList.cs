﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat {
    public partial class WChatList :  ListBox {
        private Timer timer1;
  
        private int _mouse_over = -1;
        public event StartChatEventHandler StartChat;
        public WChatList() {
            InitializeComponent();   //控件初始化
            DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
            BorderStyle = System.Windows.Forms.BorderStyle.None;
            Cursor = Cursors.Hand;
        }

        /// <summary>
        /// 重绘
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Rectangle bound;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;  //绘制的类型
            for (int i = 0; i < Items.Count; ++i) {
                WXUser user = Items[i] as WXUser;
                if (user != null) {
                    bound = GetItemRectangle(i);
                    if (!ClientRectangle.IntersectsWith(bound)) {
                        continue;
                    }
                    if (_mouse_over == i) {   //如果当前鼠标落在该行上
                        e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(110, 139, 196, 31)), bound);    //则绘制颜色
                    }
                    if (SelectedIndex == i) {
                        e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 139, 196, 31)), bound);
                    }
                    e.Graphics.DrawLine(new Pen(Color.FromArgb(50, Color.Black)), new Point(bound.X, bound.Y + 60), new Point(bound.X + Width, bound.Y + 60));
                    using (Font f = new System.Drawing.Font("微软雅黑", 11)) {
                        if (user.Icon != null) {

                            Bitmap b = DrewEllipse(user.Icon);
                            //绘制圆形图片
                           // e.Graphics.DrawImage(b, bound.X + (bound.Width - user.Icon.Width) / 2, (bound.Y + (bound.Height - user.Icon.Height) / 2) - 6);
                            //绘制圆形圈
                            e.Graphics.DrawImage(b, bound.X + 10, bound.Y + 10, 38, 38);  //头像
                            using (Pen p = new Pen(Color.FromArgb(255, 139, 196, 31), 2)) {
                                e.Graphics.DrawEllipse(p, bound.X + 10, bound.Y + 10, 38, 38);
                            }
                        }
                        string us_na = user.ShowName.Length < 9 ? user.ShowName : user.ShowName.Substring(0, 9) + "...";
                        e.Graphics.DrawString(us_na, f, Brushes.White, new PointF(bound.X + 70, bound.Y + 9)); //昵称
                    }
                    List<WXMsg> list_unread = user.GetUnReadMsg();
                    WXMsg latest = user.GetLatestMsg();
                    if (list_unread != null)  //有未读消息
                    {
                        using (Font f2 = new Font("微软雅黑", 10)) {
                            string time_str = list_unread[list_unread.Count - 1].Time.ToShortTimeString();
                            Size time_size = TextRenderer.MeasureText(time_str, f2);
                            e.Graphics.DrawString(time_str, f2, new SolidBrush(Color.FromArgb(220, 224, 223)), new PointF(bound.X + Width - time_size.Width - 13, bound.Y + 9)); //最后一条未读消息时间

                            string latest_msg = list_unread[list_unread.Count - 1].Msg.ToString();  //最后一条未读消息
                            latest_msg = latest_msg.Length <= 10 ? latest_msg : latest_msg.Substring(0, 10) + "...";
                            latest_msg = "[" + list_unread.Count + "条] " + latest_msg;
                            Size latest_msg_size = TextRenderer.MeasureText(latest_msg, f2);
                            e.Graphics.DrawString(latest_msg, f2, new SolidBrush(Color.FromArgb(220, 224, 223)), new PointF(bound.X + 70, bound.Y + 35));
                        }
                        using (Font f3 = new Font("微软雅黑", 8))  //未读消息条数  小红圆点
                        {
                            e.Graphics.FillEllipse(Brushes.Red, new Rectangle(bound.X + 10 + 50 - 16, bound.Y + 10 - 3, 18, 18));
                            e.Graphics.DrawString((list_unread.Count < 10 ? list_unread.Count.ToString() : "*"), f3, Brushes.White, new PointF(bound.X + 10 + 50 - 12, bound.Y + 10 - 1));
                        }
                    }
                    else //否则 显示最新的一条消息
                    {
                        if (latest != null) {
                            using (Font f2 = new Font("微软雅黑", 10)) {
                                string time_str = latest.Time.ToShortTimeString();
                                Size time_size = TextRenderer.MeasureText(time_str, f2);
                                e.Graphics.DrawString(time_str, f2, new SolidBrush(Color.FromArgb(220, 224, 223)), new PointF(bound.X + Width - time_size.Width - 13, bound.Y + 8)); //最后一条未读消息时间

                                string latest_msg = latest.Msg.ToString();  //最新一条消息
                                latest_msg = latest_msg.Length <= 10 ? latest_msg : latest_msg.Substring(0, 10) + "...";
                                Size latest_msg_size = TextRenderer.MeasureText(latest_msg, f2);
                                e.Graphics.DrawString(latest_msg, f2, new SolidBrush(Color.FromArgb(220, 224, 223)), new PointF(bound.X + 70, bound.Y + 35));
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 设置每项高度
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMeasureItem(MeasureItemEventArgs e) {
            base.OnMeasureItem(e);
            e.ItemHeight = 60;
        }

        private Bitmap DrewEllipse(Image img) {
            using (Image i = new Bitmap(img)) {
                Bitmap b = new Bitmap(i.Width, i.Height);
                using (Graphics g = Graphics.FromImage(b)) {
                    g.FillEllipse(new TextureBrush(i), -1, 0, i.Width, i.Height);
                }
                return b;
            }
        }
        /// <summary>
        /// 选择变化
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSelectedIndexChanged(EventArgs e) {
            base.OnSelectedIndexChanged(e);
            Invalidate();
        }
        /// <summary>
        /// 鼠标双击
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDoubleClick(EventArgs e) {   
        }

        /// <summary>
        /// 鼠标单击
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClick(EventArgs e) {
            base.OnDoubleClick(e);
            Invalidate();
            if (SelectedIndex != -1) {
               
                //开启聊天
                StartChat?.Invoke(Items[SelectedIndex] as WXUser);
            }
        }
        /// <summary>
        /// 鼠标移动
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e) {
            base.OnMouseMove(e);
            for (int i = 0; i < Items.Count; ++i) {
                if (GetItemRectangle(i).Contains(e.Location)) {
                    _mouse_over = i;
                    Invalidate();
                    return;
                }
            }
            _mouse_over = -1;
        }
        /// <summary>
        /// 鼠标移出
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e) {
            base.OnMouseLeave(e);
            _mouse_over = -1;
            Invalidate();
        }

        private void InitializeComponent() {            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.deleteiteam = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.查看更多 = new System.Windows.Forms.ToolStripMenuItem();
            this.删除项目 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteiteam.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // deleteiteam
            // 
            this.deleteiteam.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除项目,
            this.查看更多});
            this.deleteiteam.Name = "deleteiteam";
            this.deleteiteam.Size = new System.Drawing.Size(193, 48);
            this.deleteiteam.Text = "删除";
            this.deleteiteam.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.deleteiteam_ItemClicked);
            // 
            // 查看更多
            // 
            this.查看更多.Name = "查看更多";
            this.查看更多.Size = new System.Drawing.Size(192, 22);
            this.查看更多.Text = "toolStripMenuItem3";
            // 
            // 删除项目
            // 
            this.删除项目.Name = "删除项目";
            this.删除项目.Size = new System.Drawing.Size(192, 22);
            this.删除项目.Text = "删除项目";
            // 
            // WChatList
            // 
            this.ContextMenuStrip = this.deleteiteam;
            this.ItemHeight = 12;
            this.Size = new System.Drawing.Size(120, 88);
            this.deleteiteam.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e) {
            Invalidate();
        }

        private void deleteiteam_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
           
        }

       
    }
    /// <summary>
    /// 表示处理开启聊天事件的方法
    /// </summary>
    /// <param name="user"></param>

    public delegate void StartChatEventHandler(WXUser user);
}
