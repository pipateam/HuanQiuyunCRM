﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace WChat {
    public partial class Card : Form {
        public Card() {
            InitializeComponent();
        }

        private Point mouseoffset;
        private bool isMouseDown = false;

        public WXMsg w;
        public WXUser meUser;
        public string savePathName = null;

        private void Card_Load( object sender, EventArgs e ) {
            label3.BackColor = Color.Transparent;
            label3.Text = string.Empty;

            if (w.Msg.Length > 7)
                label1.Text = w.Msg.Substring(0, 7)+"...";
            else
                label1.Text = w.Msg;

            label4.Text = w.Alias;

            if (w.Sex == "1")
                label7.Text = "男";
            else if (w.Sex ==  "2")
                label7.Text = "女";
            else
                label7.Text = "";

            label6.Text = w.Province + " " + w.City;

            pictureBox1.Image = Image.FromFile(savePathName);
        }

        private void label3_MouseMove( object sender, MouseEventArgs e ) {
            label3.BackColor = Color.Red;
            label3.Text = "×";
        }

        private void label3_MouseLeave( object sender, EventArgs e ) {
            label3.BackColor = Color.Transparent;
            label3.Text = string.Empty;
        }

        public void PicDown( string url, string savePathandName ) {
            HttpWebRequest webreq = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse webres = (HttpWebResponse)webreq.GetResponse();
            webreq.Method = "POST";
            Stream stream = webres.GetResponseStream();
            Stream stro = new FileStream(savePathandName, FileMode.Create);
            byte[] b = new byte[1024];

            int start = stream.Read(b, 0, 1024);
            while (start > 0) {
                stro.Write(b, 0, start);
                start = stream.Read(b, 0, 1024);
            }
            stro.Close();
            stream.Close();
        }

        private void label3_Click( object sender, EventArgs e ) {
            this.Close();
        }

        private void Card_MouseUp( object sender, MouseEventArgs e ) {
            if (e.Button == MouseButtons.Left)
                isMouseDown = false;
        }

        private void Card_MouseMove( object sender, MouseEventArgs e ) {
            if (isMouseDown) {
                Point mousepos = Control.MousePosition;
                mousepos.Offset(mouseoffset.X, mouseoffset.Y);
                Location = mousepos;
            }
        }

        private void Card_MouseDown( object sender, MouseEventArgs e ) {
            int xoffset;
            int yoffset;
            if (e.Button == MouseButtons.Left) {
                xoffset = -e.X;
                yoffset = -e.Y;
                mouseoffset = new Point(xoffset, yoffset);
                isMouseDown = true;
            }
        }

        private void skinButton1_Click( object sender, EventArgs e ) {
            pa_message.Visible = true;
            tbx_content.Text = "我是" + meUser.NickName;
        }

        private void btn_send_Click( object sender, EventArgs e ) {
            if (tbx_content.Text != string.Empty) {
                if (w.V_Value != string.Empty) {
                    if (meUser.AddFriend(tbx_content.Text, w.V_Value)) {
                        MessageBox.Show("验证发送成功!");
                    }
                    else {
                        MessageBox.Show("发送失败!");
                    }
                }
                pa_message.Visible = false;
            }
        }

        private void Card_Click( object sender, EventArgs e ) {
            pa_message.Visible = false;
        }
    }
}
