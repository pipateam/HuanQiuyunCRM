﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;

namespace WChat {
    public partial class AddLabels : Form {

        /// <summary>
        ///微信好友名称 
        /// </summary>
        private string ChildName;
        private string UserName;
        public WinXinBox wxb;
        LabSer ls = new LabSer();

        public AddLabels(string UserName, string ChildName ) {
            this.ChildName = ChildName;
            this.UserName = UserName;
            InitializeComponent();
        }

        private void AddLabels_Load( object sender, EventArgs e ) {
            textBox1.GotFocus += TextBox1_GotFocus;
            textBox1.Focus();
        }

        private void TextBox1_GotFocus( object sender, EventArgs e ) {
            textBox1.Text = string.Empty;
            textBox1.ForeColor = Color.Black;
        }

        private void textBox1_KeyDown( object sender, KeyEventArgs e ) {
            if (e.KeyCode == Keys.Enter) {
                if (Encoding.Default.GetByteCount(textBox1.Text.Trim()) > 24) {
                    textBox1.ForeColor = Color.Silver;
                    textBox1.Text = "最大支持12个字";
                    textBox1.Enabled = false;
                    textBox1.Enabled = true;
                }
                else {
                    if (ls.SearchTable(UserName) == -1) //判断是否有表
                        ls.CreateLable(UserName);

                    if (ls.Search(UserName, ChildName) > 0)
                        ls.Set(UserName, ChildName, textBox1.Text.Trim());
                    else
                        ls.Insert(UserName, ChildName, textBox1.Text.Trim());
                    wxb.LabelsTextShow(UserName, ChildName);
                    this.Close();
                }
            }
            else if (e.KeyCode == Keys.Escape) {
                this.Close();
            }
        }
    }
}
