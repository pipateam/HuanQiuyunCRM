﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat
{
    public class Auto_Reply
    {
        HttpWebResponse Response = null;
        Auto_Reply_EventHandler Areply = new Auto_Reply_EventHandler();
        private bool RAreply = false;
        #region 图灵机器人
        /// <summary>
        /// 对话图灵机器人
        /// </summary>
        /// <param name="p_strMessage"></param>
        /// <returns></returns>
        public string ConnectTuLing(string p_strMessage)
        {
            string result = null;
            try
            {
                //注册码自己到网上注册去
                String APIKEY = "a9a4f7f233824534a1733b1f9d11075c";
                String _strMessage = p_strMessage;
                String INFO = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(_strMessage));
                String getURL = "http://www.tuling123.com/openapi/api?key=" + APIKEY + "&info=" + INFO;
                HttpWebRequest MyRequest = (HttpWebRequest)HttpWebRequest.Create(getURL);
                HttpWebResponse MyResponse = (HttpWebResponse)MyRequest.GetResponse();
                Response = MyResponse;
                using (Stream MyStream = MyResponse.GetResponseStream())
                {
                    long ProgMaximum = MyResponse.ContentLength;
                    long totalDownloadedByte = 0;
                    byte[] by = new byte[1024];
                    int osize = MyStream.Read(by, 0, by.Length);
                    Encoding encoding = Encoding.UTF8;
                    while (osize > 0)
                    {
                        totalDownloadedByte = osize + totalDownloadedByte;
                        result += encoding.GetString(by, 0, osize);
                        long ProgValue = totalDownloadedByte;
                        osize = MyStream.Read(by, 0, by.Length);
                    }
                }
                //解析json
                JsonReader reader = new JsonTextReader(new StringReader(result));
                while (reader.Read())
                {
                    //text中的内容才是你需要的
                    if (reader.Path == "text")
                    {
                        //结果赋值
                        result = reader.Value.ToString();
                    }
                    Console.WriteLine(reader.TokenType + "\t\t" + reader.ValueType + "\t\t" + reader.Value);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public void Rebot_Reply(WXUser user, string reci_mess, WXUser from_user, bool Areply)
        {
            if (Areply) {
                string msg = ConnectTuLing(reci_mess);
                WXMsg AMsg = new WXMsg();
                AMsg.Msg = msg;
                AMsg.From = from_user.UserName;
                AMsg.Fr_NickName = from_user.NickName;
                AMsg.Readed = false;
                AMsg.Type = "1";
                AMsg.IsReceive = 1;
                AMsg.Time = DateTime.Now;
                AMsg.To = user.UserName;
                AMsg.To_NickName = user.NickName;
                user.Syc_SendMsg(from_user.NickName,AMsg); //显示机器人发送的消息
                user.SendMsgToServer( AMsg);  //向微信服务器发送的消息
            }
        }

        public void Automatic_Reply(WXUser user, string reci_mess, WXUser from_user, bool Areply) {

            if (Areply) {
                WXMsg AMsg = new WXMsg();
                AMsg.Msg = reci_mess;
                AMsg.From = from_user.UserName;
                AMsg.Fr_NickName = from_user.NickName;
                AMsg.Readed = false;
                AMsg.Type = "1";
                AMsg.IsReceive = 1;
                AMsg.Time = DateTime.Now;
                AMsg.To = user.UserName;
                AMsg.To_NickName = user.NickName;
                user.Syc_SendMsg(from_user.NickName, AMsg);
                user.SendMsgToServer( AMsg);  //向微信服务器发送的消息
            }
        }

        #endregion
    }
}
