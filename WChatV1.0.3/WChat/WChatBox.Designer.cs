﻿namespace WChat {
    partial class WChatBox {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WChatBox));
            this.plTop = new System.Windows.Forms.Panel();
            this.btnInfo = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.cmstrRichbox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.粘贴ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.brower_righ_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除该条消息ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.复制ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.tbx_new_reply = new System.Windows.Forms.TextBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_new_reply = new System.Windows.Forms.Button();
            this.btn_send = new CCWin.SkinControl.SkinButton();
            this.chatGroupBox = new CCWin.SkinControl.SkinGroupBox();
            this.btn_pic_reply = new CCWin.SkinControl.SkinButton();
            this.btn_reply = new CCWin.SkinControl.SkinButton();
            this.btn_sendfile = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_cutter = new System.Windows.Forms.Button();
            this.topGroupBox = new CCWin.SkinControl.SkinGroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn_del_img = new System.Windows.Forms.Button();
            this.btn_add_img = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.emojiPanel1 = new WChat.EmojiPanel();
            this.plTop.SuspendLayout();
            this.cmstrRichbox.SuspendLayout();
            this.brower_righ_menu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.chatGroupBox.SuspendLayout();
            this.topGroupBox.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // plTop
            // 
            this.plTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plTop.BackColor = System.Drawing.Color.White;
            this.plTop.Controls.Add(this.btnInfo);
            this.plTop.Controls.Add(this.lblInfo);
            this.plTop.Controls.Add(this.btnBack);
            this.plTop.Location = new System.Drawing.Point(0, 0);
            this.plTop.Name = "plTop";
            this.plTop.Size = new System.Drawing.Size(1267, 33);
            this.plTop.TabIndex = 15;
            this.plTop.Resize += new System.EventHandler(this.plTop_Resize);
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInfo.FlatAppearance.BorderSize = 0;
            this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfo.Location = new System.Drawing.Point(1235, 1);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(29, 30);
            this.btnInfo.TabIndex = 2;
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click_1);
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblInfo.ForeColor = System.Drawing.Color.Black;
            this.lblInfo.Location = new System.Drawing.Point(47, 4);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(101, 20);
            this.lblInfo.TabIndex = 1;
            this.lblInfo.Text = "与 张三 聊天中";
            this.lblInfo.Click += new System.EventHandler(this.lblInfo_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackgroundImage = global::WChat.Properties.Resources.back;
            this.btnBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(0, 1);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(29, 29);
            this.btnBack.TabIndex = 0;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click_1);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.ContextMenuStrip = this.cmstrRichbox;
            this.richTextBox1.DetectUrls = false;
            this.richTextBox1.EnableAutoDragDrop = true;
            this.richTextBox1.Location = new System.Drawing.Point(24, 48);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1201, 68);
            this.richTextBox1.TabIndex = 22;
            this.richTextBox1.Text = "";
            this.richTextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyDown);
            // 
            // cmstrRichbox
            // 
            this.cmstrRichbox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.粘贴ToolStripMenuItem});
            this.cmstrRichbox.Name = "contextMenuStrip1";
            this.cmstrRichbox.Size = new System.Drawing.Size(101, 26);
            // 
            // 粘贴ToolStripMenuItem
            // 
            this.粘贴ToolStripMenuItem.Name = "粘贴ToolStripMenuItem";
            this.粘贴ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.粘贴ToolStripMenuItem.Text = "粘贴";
            this.粘贴ToolStripMenuItem.Click += new System.EventHandler(this.粘贴ToolStripMenuItem_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Location = new System.Drawing.Point(7, 19);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(1228, 377);
            this.webBrowser1.TabIndex = 24;
            // 
            // brower_righ_menu
            // 
            this.brower_righ_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除该条消息ToolStripMenuItem,
            this.复制ToolStripMenuItem});
            this.brower_righ_menu.Name = "brower_righ_menu";
            this.brower_righ_menu.Size = new System.Drawing.Size(149, 48);
            // 
            // 删除该条消息ToolStripMenuItem
            // 
            this.删除该条消息ToolStripMenuItem.Name = "删除该条消息ToolStripMenuItem";
            this.删除该条消息ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.删除该条消息ToolStripMenuItem.Text = "删除该条消息";
            this.删除该条消息ToolStripMenuItem.Click += new System.EventHandler(this.删除该条消息ToolStripMenuItem_Click);
            // 
            // 复制ToolStripMenuItem
            // 
            this.复制ToolStripMenuItem.Name = "复制ToolStripMenuItem";
            this.复制ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.复制ToolStripMenuItem.Text = "复制";
            this.复制ToolStripMenuItem.Click += new System.EventHandler(this.复制ToolStripMenuItem_Click_1);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.checkedListBox1);
            this.panel1.Controls.Add(this.tbx_new_reply);
            this.panel1.Controls.Add(this.btn_delete);
            this.panel1.Controls.Add(this.btn_new_reply);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(24, 201);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 201);
            this.panel1.TabIndex = 28;
            this.panel1.Visible = false;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // btnClose
            // 
            this.btnClose.ForeColor = System.Drawing.Color.Transparent;
            this.btnClose.Image = global::WChat.Properties.Resources.ico_close;
            this.btnClose.Location = new System.Drawing.Point(460, 169);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(48, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.HorizontalScrollbar = true;
            this.checkedListBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.checkedListBox1.Items.AddRange(new object[] {
            "快捷回复列表"});
            this.checkedListBox1.Location = new System.Drawing.Point(7, 3);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(353, 196);
            this.checkedListBox1.TabIndex = 25;
            this.checkedListBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.checkedListBox1_MouseDoubleClick);
            // 
            // tbx_new_reply
            // 
            this.tbx_new_reply.Location = new System.Drawing.Point(366, 7);
            this.tbx_new_reply.Multiline = true;
            this.tbx_new_reply.Name = "tbx_new_reply";
            this.tbx_new_reply.Size = new System.Drawing.Size(142, 119);
            this.tbx_new_reply.TabIndex = 3;
            // 
            // btn_delete
            // 
            this.btn_delete.ForeColor = System.Drawing.Color.Black;
            this.btn_delete.Location = new System.Drawing.Point(365, 170);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(89, 23);
            this.btn_delete.TabIndex = 2;
            this.btn_delete.Text = "删除回复";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_new_reply
            // 
            this.btn_new_reply.ForeColor = System.Drawing.Color.Black;
            this.btn_new_reply.Location = new System.Drawing.Point(366, 136);
            this.btn_new_reply.Name = "btn_new_reply";
            this.btn_new_reply.Size = new System.Drawing.Size(88, 23);
            this.btn_new_reply.TabIndex = 1;
            this.btn_new_reply.Text = "添加回复";
            this.btn_new_reply.UseVisualStyleBackColor = true;
            this.btn_new_reply.Click += new System.EventHandler(this.btn_new_reply_Click);
            // 
            // btn_send
            // 
            this.btn_send.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_send.BackColor = System.Drawing.Color.Transparent;
            this.btn_send.BaseColor = System.Drawing.Color.Transparent;
            this.btn_send.BorderColor = System.Drawing.Color.Silver;
            this.btn_send.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_send.DownBack = null;
            this.btn_send.DownBaseColor = System.Drawing.Color.LimeGreen;
            this.btn_send.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_send.ForeColor = System.Drawing.Color.Gray;
            this.btn_send.InnerBorderColor = System.Drawing.Color.White;
            this.btn_send.IsDrawGlass = false;
            this.btn_send.IsEnabledDraw = false;
            this.btn_send.Location = new System.Drawing.Point(1150, 119);
            this.btn_send.MouseBack = null;
            this.btn_send.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_send.Name = "btn_send";
            this.btn_send.NormlBack = null;
            this.btn_send.Size = new System.Drawing.Size(75, 27);
            this.btn_send.TabIndex = 30;
            this.btn_send.Text = "发送(S)";
            this.btn_send.UseVisualStyleBackColor = false;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // chatGroupBox
            // 
            this.chatGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chatGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.chatGroupBox.BorderColor = System.Drawing.Color.Silver;
            this.chatGroupBox.Controls.Add(this.btn_pic_reply);
            this.chatGroupBox.Controls.Add(this.btn_reply);
            this.chatGroupBox.Controls.Add(this.richTextBox1);
            this.chatGroupBox.Controls.Add(this.btn_send);
            this.chatGroupBox.Controls.Add(this.btn_sendfile);
            this.chatGroupBox.Controls.Add(this.button1);
            this.chatGroupBox.Controls.Add(this.btn_cutter);
            this.chatGroupBox.ForeColor = System.Drawing.Color.Blue;
            this.chatGroupBox.Location = new System.Drawing.Point(12, 443);
            this.chatGroupBox.Name = "chatGroupBox";
            this.chatGroupBox.RectBackColor = System.Drawing.Color.White;
            this.chatGroupBox.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.chatGroupBox.Size = new System.Drawing.Size(1241, 152);
            this.chatGroupBox.TabIndex = 31;
            this.chatGroupBox.TabStop = false;
            this.chatGroupBox.TitleBorderColor = System.Drawing.Color.DimGray;
            this.chatGroupBox.TitleRectBackColor = System.Drawing.Color.White;
            this.chatGroupBox.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // btn_pic_reply
            // 
            this.btn_pic_reply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_pic_reply.BackColor = System.Drawing.Color.Transparent;
            this.btn_pic_reply.BaseColor = System.Drawing.Color.Transparent;
            this.btn_pic_reply.BorderColor = System.Drawing.Color.Transparent;
            this.btn_pic_reply.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_pic_reply.DownBack = null;
            this.btn_pic_reply.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_pic_reply.GlowColor = System.Drawing.Color.Transparent;
            this.btn_pic_reply.Image = global::WChat.Properties.Resources.replay_pic;
            this.btn_pic_reply.ImageSize = new System.Drawing.Size(26, 25);
            this.btn_pic_reply.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_pic_reply.IsDrawBorder = false;
            this.btn_pic_reply.IsDrawGlass = false;
            this.btn_pic_reply.Location = new System.Drawing.Point(176, 18);
            this.btn_pic_reply.MouseBack = null;
            this.btn_pic_reply.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_pic_reply.Name = "btn_pic_reply";
            this.btn_pic_reply.NormlBack = null;
            this.btn_pic_reply.Size = new System.Drawing.Size(27, 26);
            this.btn_pic_reply.TabIndex = 33;
            this.btn_pic_reply.UseVisualStyleBackColor = false;
            this.btn_pic_reply.Click += new System.EventHandler(this.btn_pic_reply_Click);
            // 
            // btn_reply
            // 
            this.btn_reply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_reply.BackColor = System.Drawing.Color.Transparent;
            this.btn_reply.BaseColor = System.Drawing.Color.Transparent;
            this.btn_reply.BorderColor = System.Drawing.Color.Transparent;
            this.btn_reply.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_reply.DownBack = null;
            this.btn_reply.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_reply.GlowColor = System.Drawing.Color.Transparent;
            this.btn_reply.Image = global::WChat.Properties.Resources.s;
            this.btn_reply.ImageSize = new System.Drawing.Size(26, 25);
            this.btn_reply.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_reply.IsDrawBorder = false;
            this.btn_reply.IsDrawGlass = false;
            this.btn_reply.Location = new System.Drawing.Point(138, 18);
            this.btn_reply.MouseBack = null;
            this.btn_reply.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_reply.Name = "btn_reply";
            this.btn_reply.NormlBack = null;
            this.btn_reply.Size = new System.Drawing.Size(27, 26);
            this.btn_reply.TabIndex = 32;
            this.btn_reply.UseVisualStyleBackColor = false;
            this.btn_reply.Click += new System.EventHandler(this.btn_reply_Click_1);
            // 
            // btn_sendfile
            // 
            this.btn_sendfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_sendfile.BackgroundImage = global::WChat.Properties.Resources.file1;
            this.btn_sendfile.FlatAppearance.BorderSize = 0;
            this.btn_sendfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sendfile.Location = new System.Drawing.Point(100, 17);
            this.btn_sendfile.Name = "btn_sendfile";
            this.btn_sendfile.Size = new System.Drawing.Size(27, 26);
            this.btn_sendfile.TabIndex = 23;
            this.btn_sendfile.UseVisualStyleBackColor = true;
            this.btn_sendfile.Click += new System.EventHandler(this.btn_sendfile_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.BackgroundImage = global::WChat.Properties.Resources.表情;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(24, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 25);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btn_cutter
            // 
            this.btn_cutter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_cutter.BackgroundImage = global::WChat.Properties.Resources.截图;
            this.btn_cutter.FlatAppearance.BorderSize = 0;
            this.btn_cutter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cutter.Location = new System.Drawing.Point(62, 17);
            this.btn_cutter.Name = "btn_cutter";
            this.btn_cutter.Size = new System.Drawing.Size(27, 26);
            this.btn_cutter.TabIndex = 2;
            this.btn_cutter.UseVisualStyleBackColor = true;
            this.btn_cutter.Click += new System.EventHandler(this.btn_cutter_Click);
            this.btn_cutter.MouseEnter += new System.EventHandler(this.btn_cutter_MouseEnter);
            // 
            // topGroupBox
            // 
            this.topGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.topGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.topGroupBox.BorderColor = System.Drawing.Color.DarkGray;
            this.topGroupBox.Controls.Add(this.panel2);
            this.topGroupBox.Controls.Add(this.panel1);
            this.topGroupBox.Controls.Add(this.emojiPanel1);
            this.topGroupBox.Controls.Add(this.webBrowser1);
            this.topGroupBox.ForeColor = System.Drawing.Color.Blue;
            this.topGroupBox.Location = new System.Drawing.Point(12, 35);
            this.topGroupBox.Name = "topGroupBox";
            this.topGroupBox.RectBackColor = System.Drawing.Color.White;
            this.topGroupBox.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.topGroupBox.Size = new System.Drawing.Size(1240, 403);
            this.topGroupBox.TabIndex = 32;
            this.topGroupBox.TabStop = false;
            this.topGroupBox.TitleBorderColor = System.Drawing.Color.Red;
            this.topGroupBox.TitleRectBackColor = System.Drawing.Color.White;
            this.topGroupBox.TitleRoundStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.flowLayoutPanel1);
            this.panel2.Controls.Add(this.btn_del_img);
            this.panel2.Controls.Add(this.btn_add_img);
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(27, 150);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(348, 251);
            this.panel2.TabIndex = 30;
            this.panel2.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(340, 200);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.Visible = false;
            // 
            // btn_del_img
            // 
            this.btn_del_img.ForeColor = System.Drawing.Color.Black;
            this.btn_del_img.Location = new System.Drawing.Point(196, 215);
            this.btn_del_img.Name = "btn_del_img";
            this.btn_del_img.Size = new System.Drawing.Size(75, 23);
            this.btn_del_img.TabIndex = 2;
            this.btn_del_img.Text = "删除图片";
            this.btn_del_img.UseVisualStyleBackColor = true;
            this.btn_del_img.Click += new System.EventHandler(this.btn_del_img_Click);
            // 
            // btn_add_img
            // 
            this.btn_add_img.ForeColor = System.Drawing.Color.Black;
            this.btn_add_img.Location = new System.Drawing.Point(57, 215);
            this.btn_add_img.Name = "btn_add_img";
            this.btn_add_img.Size = new System.Drawing.Size(75, 23);
            this.btn_add_img.TabIndex = 1;
            this.btn_add_img.Text = "添加图片";
            this.btn_add_img.UseVisualStyleBackColor = true;
            this.btn_add_img.Click += new System.EventHandler(this.btn_add_img_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "temp (1).ico");
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(460, 136);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(48, 23);
            this.button2.TabIndex = 27;
            this.button2.Text = "导入";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // emojiPanel1
            // 
            this.emojiPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emojiPanel1.Location = new System.Drawing.Point(24, 224);
            this.emojiPanel1.Name = "emojiPanel1";
            this.emojiPanel1.Size = new System.Drawing.Size(406, 173);
            this.emojiPanel1.TabIndex = 29;
            this.emojiPanel1.Visible = false;
            // 
            // WChatBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.topGroupBox);
            this.Controls.Add(this.chatGroupBox);
            this.Controls.Add(this.plTop);
            this.Name = "WChatBox";
            this.Size = new System.Drawing.Size(1267, 608);
            this.Load += new System.EventHandler(this.WChatBox_Load_1);
            this.Leave += new System.EventHandler(this.WChatBox_Leave);
            this.plTop.ResumeLayout(false);
            this.plTop.PerformLayout();
            this.cmstrRichbox.ResumeLayout(false);
            this.brower_righ_menu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.chatGroupBox.ResumeLayout(false);
            this.topGroupBox.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel plTop;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btn_cutter;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_sendfile;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.TextBox tbx_new_reply;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_new_reply;
        private EmojiPanel emojiPanel1;
        private CCWin.SkinControl.SkinButton btn_send;
        private CCWin.SkinControl.SkinGroupBox chatGroupBox;
        private CCWin.SkinControl.SkinGroupBox topGroupBox;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ContextMenuStrip brower_righ_menu;
        private System.Windows.Forms.ToolStripMenuItem 删除该条消息ToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_del_img;
        private System.Windows.Forms.Button btn_add_img;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private CCWin.SkinControl.SkinButton btn_reply;
        private CCWin.SkinControl.SkinButton btn_pic_reply;
        private System.Windows.Forms.ToolStripMenuItem 复制ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmstrRichbox;
        private System.Windows.Forms.ToolStripMenuItem 粘贴ToolStripMenuItem;
        private System.Windows.Forms.Button button2;
    }
}
