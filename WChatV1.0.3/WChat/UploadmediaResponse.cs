﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WChat {
  public  class UploadmediaResponse {
        public BaseResponse BaseResponse;
        public string MediaId;
        public int CDNThumbImgHeight;
        public int CDNThumbImgWidth;
    }
}
