﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat {
    public partial class picbox : Form {
        public picbox() {
            InitializeComponent();
        }

    public void showpic(string  picname) {
            if (picname != "") {
                pictureBox1.Image = Image.FromFile(picname);
            }
        }
    }
}
