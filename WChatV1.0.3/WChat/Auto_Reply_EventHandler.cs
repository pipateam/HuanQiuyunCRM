﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat
{
    public class Auto_Reply_EventHandler
    {
        //委托 机器人开关
        public delegate void Ask_replyEventHandler(bool open,List<string> arr);
        public event Ask_replyEventHandler Ask_reply;
        public delegate void open_closeEventHandler(bool aotu,List<string> arr);
        public event open_closeEventHandler open_close;

        public void Open_Close(bool asd,List<string> arr)
        {
            open_close?.Invoke(asd, arr);
        }

        public void Aotu_reply(bool asd,List<string> arr) {
            Ask_reply?.Invoke(asd, arr);
        }
    }
}
