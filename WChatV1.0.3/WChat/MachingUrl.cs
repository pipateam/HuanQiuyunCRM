﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WChat {
    /// <summary>
    /// 李大亮，获取url&lang=zh_CN
    /// </summary>
    class MachingUrl {
        private string _wurl = "https://wx.qq.com/";
        //微信初始化url
        private string _init_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxinit?r=1377482058764";
        //获取好友头像
        private string _geticon_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgeticon?username=";
        //获取群聊（组）头像
        private string _getheadimg_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetheadimg?username=";
        //获取好友列表
        private string _getcontact_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetcontact?r={0}&seq={1}&skey={2}";
        //同步消息url
        private string _sync_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsync?sid=";
        //发送消息url
        private string _sendmsg_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsg?sid=";
        //接收图片url
        private string _getpic_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetmsgimg?&MsgID=";
        //接收文件url
        private string _getfile_url = "https://file.wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetmedia?sender={0}&mediaid={1}&filename={2}&fromuser={3}&pass_ticket={4}&webwx_data_ticket={5}";
        //发送媒体url
        private string _upload_media_url = "https://file.wx.qq.com/cgi-bin/mmwebwx-bin/webwxuploadmedia?f=json";
        //发送图片url
        private string _upload_pic_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsgimg?fun=async&f=json&pass_ticket={0}";
        //发送文件url 
        private string _upload_file_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxsendappmsg?fun=async&f=json&lang=zh_CN&pass_ticket={0}";
        //同意好友请求
        private string _verify_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}&lang=zh_CN&pass_ticket={1}";
        //添加好友请求
        private string wx_add_friend = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}";
        //批量获取好友
        private string _batchgetcontact_url="https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxbatchgetcontact?type=ex";
        //获取语音消息
        private string _getvoicemsg = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxgetvoice?msgid={0}&skey={1}";
        //加好友
        private string _add_friend_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}&lang=zh_CN&pass_ticket={1}";
        //更新群聊
        string _updata_chat_room_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxupdatechatroom?fun=addmember&pass_ticket="; 
        //建群
        string _creata_room_url = "https://wx.qq.com/cgi-bin/mmwebwx-bin/webwxcreatechatroom?r={0}&pass_ticket={1}"; 



        //发送数据url
        private string wurl = "https://wx2.qq.com/";
        //初始化url
        private string init_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxinit?r=1377482058764";
        //获取好友头像
        private string geticon_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxgeticon?username=";
        //获取群聊（组）头像
        private string getheadimg_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxgetheadimg?username=";
        //获取好友列表
        private string getcontact_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxgetcontact?r={0}&seq={1}&skey={2}";
        //同步消息url
        private string sync_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxsync?sid=";
        //发送消息url
        private string sendmsg_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsg?sid=";
        //获取图片url
        private string getpic_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxgetmsgimg?&MsgID=";
        //接收文件url
        private string getfile_url = "https://file.wx2.qq.com/cgi-bin/mmwebwx-bin/webwxgetmedia?sender={0}&mediaid={1}&filename={2}&fromuser={3}&pass_ticket={4}&webwx_data_ticket={5}";
        //发送媒体url
        private string upload_media_url = "https://file.wx2.qq.com/cgi-bin/mmwebwx-bin/webwxuploadmedia?f=json";
        //发送图片url
        private string upload_pic_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxsendmsgimg?fun=async&f=json&pass_ticket={0}";
        //发送文件url 
        private string upload_file_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxsendappmsg?fun=async&f=json&lang=zh_CN&pass_ticket={0}";
        //同意好友请求
        private string verify_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}&lang=zh_CN&pass_ticket={1}";
        //添加好友请求
        private string wx2_add_friend = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}";
        //批量获取好友
        private string batchgetcontact_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxbatchgetcontact?type=ex";
        //获取语音消息
        private string getvoicemsg = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxgetvoice?msgid={0}&skey={1}";
        //加好友
        private string add_friend_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}&lang=zh_CN&pass_ticket={1}";
        //更新群聊
        string updata_chat_room_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxupdatechatroom?fun=addmember&pass_ticket=";
        //建群
        string creata_room_url = "https://wx2.qq.com/cgi-bin/mmwebwx-bin/webwxcreatechatroom?r={0}&pass_ticket={1}";





        //发送数据url
        private string weburl = "https://web.wechat.com/";
        //初始化url
        private string webinit_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxinit?r=-335314589";
        //获取好友头像
        private string webgeticon_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxgeticon?username=";
        //获取群聊（组）头像
        private string webgetheadimg_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxgetheadimg?username=";
        //获取好友列表
        private string webgetcontact_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxgetcontact?r={0}&seq={1}&skey={2}";
        //同步消息url
        private string websync_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxsync?sid=";
        //发送消息url
        private string websendmsg_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxsendmsg?sid=";
        //获取图片url
        private string webgetpic_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxgetmsgimg?&MsgID=";
        //接收文件url
        private string webgetfile_url = "https://file.web.wechat.com/cgi-bin/mmwebwx-bin/webwxgetmedia?sender={0}&mediaid={1}&filename={2}&fromuser={3}&pass_ticket={4}&webwx_data_ticket={5}";
        //发送媒体url
        private string webupload_media_url = "https://file.web.wechat.com/cgi-bin/mmwebwx-bin/webwxuploadmedia?f=json";
        //发送图片url
        private string webupload_pic_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxsendmsgimg?fun=async&f=json&pass_ticket={0}";
        //发送文件url 
        private string webupload_file_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxsendappmsg?fun=async&f=json&lang=zh_CN&pass_ticket={0}";
        //同意好友请求
        private string webverify_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}&lang=zh_CN&pass_ticket={1}";
        //添加好友请求
        private string web_add_friend = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}";
        //批量获取好友
        private string web_batchgetcontact_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxbatchgetcontact?type=ex";
        //获取语音消息
        private string web_getvoicemsg = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxgetvoice?msgid={0}&skey={1}";
        //加好友
        private string web_add_friend_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxverifyuser?r={0}&lang=zh_CN&pass_ticket={1}";
        //更新群聊
        string web_updata_chat_room_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxupdatechatroom?fun=addmember&pass_ticket=";
        //建群
        string web_creata_room_url = "https://web.wechat.com/cgi-bin/mmwebwx-bin/webwxcreatechatroom?r={0}&pass_ticket={1}"; 
        /// <summary>
        /// 判断添加好友的发送地址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string Add_friend(int url)
        {
            string addfriend = string.Empty;
            switch (url)
            {
                case 1:
                    addfriend = wx_add_friend;
                    break;
                case 2:
                    addfriend = wx2_add_friend;
                    break;
                case 3:
                    addfriend = web_add_friend;
                    break;
            }
            return addfriend;
        }


        /// <summary>
        /// 设置参数
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string WUrl(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _wurl;
            break;
            case 2:
            t_url = wurl;
            break;
            case 3:
            t_url = weburl;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        public string Init_Url(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _init_url;
            break;
            case 2:
            t_url = init_url;
            break;
            case 3:
            t_url = webinit_url;
            break;
            }
            return t_url;
        }
        /// <summary>
        /// 图标
        /// </summary>
        /// <returns></returns>
        public string GetIcons(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _geticon_url;
            break;
            case 2:
            t_url = geticon_url;
            break;
            case 3:
            t_url = webgeticon_url;
            break;
            }
            return t_url;
        }
        /// <summary>
        /// 图像
        /// </summary>
        /// <returns></returns>
        public string GetHead_img(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _getheadimg_url;
            break;
            case 2:
            t_url = getheadimg_url;
            break;
            case 3:
            t_url = webgetheadimg_url;
            break;
            }
            return t_url;
        }
        /// <summary>
        /// 联系人
        /// </summary>
        /// <returns></returns>
        public string GetContact(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _getcontact_url;
            break;
            case 2:
            t_url = getcontact_url;
            break;
            case 3:
            t_url = webgetcontact_url;
            break;
            }
            return t_url;
        }
        /// <summary>
        /// 同步
        /// </summary>
        /// <returns></returns>
        public string Sync(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _sync_url;
            break;
            case 2:
            t_url = sync_url;
            break;
            case 3:
            t_url = websync_url;
            break;
            }
            return t_url;
        }
        /// <summary>
        /// 发消息
        /// </summary>
        /// <returns></returns>
        public string SendMsg(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _sendmsg_url;
            break;
            case 2:
            t_url = sendmsg_url;
            break;
            case 3:
            t_url = websendmsg_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 获取图片
        /// </summary>
        /// <returns></returns>
        public string GetPic(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _getpic_url;
            break;
            case 2:
            t_url = getpic_url;
            break;
            case 3:
            t_url = webgetpic_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 上传媒体
        /// </summary>
        /// <returns></returns>
        public string UploadMediaUrl(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _upload_media_url;
            break;
            case 2:
            t_url = upload_media_url;
            break;
            case 3:
            t_url = webupload_media_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <returns></returns>
        public string UploadPicUrl(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _upload_pic_url;
            break;
            case 2:
            t_url = upload_pic_url;
            break;
            case 3:
            t_url = webupload_pic_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        public string UploadFileUrl(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _upload_file_url;
            break;
            case 2:
            t_url = upload_file_url;
            break;
            case 3:
            t_url = webupload_file_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <returns></returns>
        public string GetFileUrl(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _getfile_url;
            break;
            case 2:
            t_url = getfile_url;
            break;
            case 3:
            t_url = webgetfile_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 同意好友
        /// </summary>
        /// <returns></returns>
        public string VeryfyUser(int url) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _verify_url;
            break;
            case 2:
            t_url = verify_url;
            break;
            case 3:
            t_url = webverify_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 批量获取好友
        /// </summary>
        /// <returns></returns>
        public string BatchGetContactUrl( int url ) { 
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _batchgetcontact_url;
            break;
            case 2:
            t_url = batchgetcontact_url;
            break;
            case 3:
            t_url = web_batchgetcontact_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 获取语音消息
        /// </summary>
        /// <returns></returns>
        public string GetVoiceUrl( int url ) { 
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _getvoicemsg;
            break;
            case 2:
            t_url = getvoicemsg;
            break;
            case 3:
            t_url = web_getvoicemsg;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 更新群聊（邀请好友进群)
        /// </summary>
        /// <returns></returns>
        public string UpdChRoUrl( int url ) { 
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _updata_chat_room_url;
            break;
            case 2:
            t_url = updata_chat_room_url;
            break;
            case 3:
            t_url = web_updata_chat_room_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 加好友
        /// </summary>
        /// <returns></returns>
        public string AddFrieUrl( int url ) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _add_friend_url;
            break;
            case 2:
            t_url = add_friend_url;
            break;
            case 3:
            t_url = web_add_friend_url;
            break;
            }
            return t_url;
        }

        /// <summary>
        /// 建群
        /// </summary>
        /// <returns></returns>
        public string CreChaRoUrl( int url ) {
            string t_url = null;
            switch (url) {
            case 1:
            t_url = _creata_room_url;
            break;
            case 2:
            t_url = creata_room_url;
            break;
            case 3:
            t_url = web_creata_room_url;
            break;
            }
            return t_url;
        }
    }
}
