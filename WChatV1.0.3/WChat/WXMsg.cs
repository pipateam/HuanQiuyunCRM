﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace WChat {
    /// <summary>
    /// 微信消息
    /// </summary>
    public class WXMsg {
        /// <summary>
        /// 消息发送方
        /// </summary>
        public string From {
            get;
            set;
        }

        public string Fr_NickName {  
            get;
            set;
        }
        /// <summary>
        /// 是否接受消息，0为接受，1发送
        /// </summary>
        public int IsReceive { 
            get;
            set;
        }

        public string To_NickName { 
            get;
            set;
        }
        /// <summary>
        /// 消息接收方
        /// </summary>
        public string To {
            set;
            get;
        }
        /// <summary>
        /// 消息发送时间
        /// </summary>
        public DateTime Time {
            get;
            set;
        }
        /// <summary>
        /// 是否已读
        /// </summary>
        public bool Readed {
            get;
            set;
        }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Msg {
            get;
            set;
        }
        /// <summary>
        /// 消息类型
        /// </summary>
        public string Type {
            get;
            set;
        }

        //图片id
        private string _msgid;
        public string MsgId {
            get {
                return _msgid;
            }
            set {
                _msgid = value;
            }
        }

        //媒体id
        public string MediaId {
            get;
            set;
        }

        //媒体url
        public string TxtUrl {
            get;
            set;
        }

        //文件名
        public string FileName {
            get;
            set;
        }

        //文件大小
        public string FileSize {
            get;
            set;
        }

        //通关文牒
        private string _direct;
        public string Direct {
            get {
                return _direct;
            }
            set {
                _direct = value;
            }
        }

        //被添加人的留言
        public string V_Content { 
            get;
            set;
        }

        //被添加人的呢称
        public string V_NickName {
            get; 
            set;
        }

        //被添加人的用户名
        public string V_Value { 
            get;
            set;
        }

        //被添加人的ticket
        public string V_UserTicket { 
            get;
            set;
        }

        ///微信号
        public string Alias { get; set; }
        ///省份
        public string Province { get; set; }
        ///QQ号
        public string QQNum { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 小头像
        /// </summary>
        public string smallheadimgurl { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// 大头像
        /// </summary>
        public string bigheadimgurl { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }
        /// <summary>
        /// 好友串号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 是否群聊
        /// </summary>
        public bool IsGroup { get; set; }
        /// <summary>
        /// 群聊好友名
        /// </summary>
        public string CrewUserName { get; set; }
        /// <summary>
        /// 群聊好友昵称
        /// </summary>
        public string CrewNickName { get; set; }

        public string CrewImgPath  { get; set; } 


    }
}
