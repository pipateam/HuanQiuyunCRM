﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WChat;
using CCWin;
using System.Threading;

namespace WChat {
    public partial class ChatInfo : Skin_Color {
        private WXUser C_FriendUser;
        private WXUser C_me_user;
        private int friend_num = 0;
        private bool is_create_room;
        SqlHandle sql = new SqlHandle();
        List<string> arr = new List<string>();
        List<string> UserList = new List<string>();
        private List<string> MyFriList = new List<string>();
        private List<string> TempUserList = new List<string>();
        public ChatInfo() {
            InitializeComponent();
        }
        public void GetUserName(WXUser user,WXUser me, bool iscreatroom) {
            C_FriendUser = user;
            C_me_user = me;
            is_create_room = iscreatroom;
        }
      
        private void ChatInfo_Load( object sender, EventArgs e ) {
            if (WinXinBox._setting == string.Empty) {
                MessageBox.Show("请重启软件再进行操作!");
            }
            if (is_create_room) {
                Text = "请选择要群聊的好友，并输入群名";
            } else {
                Text = "请选择要邀请进群的好友(单选)";
                label1.Visible = false;
                tbx_topic.Visible = false;
            }
           
            arr = sql.Select_send_man(C_me_user.NickName, WinXinBox._setting);   //获取群发好友
            string[] Wx_Name = arr.Distinct().ToArray();
            int friend_num = 0;
            for (int i = 0; i < Wx_Name.Length; i++) {
                listBox1.Items.Add(Wx_Name[i].Trim());   //列表添加群发好友
                friend_num = friend_num + 1;
                MyFriList.Add(Wx_Name[i].Trim());
            }
        }

        private void InvitationFriend() {
            string usernam = sql.Select_Send_User(C_me_user.NickName, TempUserList[0], WinXinBox._setting);
            WriteLog.Writelog("InvitationFriend", "将要邀请好友的UserNam"+usernam);
            if (C_me_user.Updata_ChatRoom(usernam, C_FriendUser.UserName)) {
                MessageBox.Show("添加好友成功!");
            }
            else {
                MessageBox.Show("添加好友失败!");
            }
        }
        /// <summary>
        /// 完成事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_done_Click( object sender, EventArgs e ) {
            string user_Name;
                if (!is_create_room) {  //更新群
                    if (TempUserList != null) {
                        if (TempUserList.Count == 1) {
                            WriteLog.Writelog("btn_done_Click", "开始启动邀请好友线程");
                            WriteLog.Writelog("btn_done_Click", "将要邀请好友的名称" + TempUserList[0]);
                            Thread invitation_fri = new Thread(new ThreadStart(InvitationFriend));
                            invitation_fri.IsBackground = true;
                            invitation_fri.Start();
                            this.Close();
                        }
                        else MessageBox.Show("一次只能邀请一位好友!");
                    }
                }  else {      //建群
                    for (int i = 0; i < TempUserList.Count; i++) {
                        user_Name = sql.Select_Send_User(C_me_user.NickName, TempUserList[i], WinXinBox._setting);
                       WriteLog.Writelog("btn_done_Click", user_Name);
                       WriteLog.Writelog("btn_done_Click", TempUserList[i]);
                      UserList.Add(user_Name);
                    }
                    if (tbx_topic.Text==string.Empty) {
                        MessageBox.Show("请输入群名!");
                    }else {
                        if (C_me_user.Create_ChatRoom(UserList.Count, UserList, tbx_topic.Text)) {
                            MessageBox.Show("建群成功!");
                        }  else {
                            MessageBox.Show("建群失败!");
                        }
                     this.Close();
                    }
                }
            }
              
        /// <summary>
        /// 搜索按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_search_Click( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            foreach (var item in MyFriList) {
                if (item.Contains(tbx_search.Text)) {
                    listBox1.Items.Add(item);
                }
            }
            tbx_search.Text = "";
        }

        private void listBox1_SelectedIndexChanged( object sender, EventArgs e ) {
            lbx_seled.Items.Clear();
            if (listBox1.SelectedItem != null) {
                string seledfris = listBox1.SelectedItem.ToString();
                if (!TempUserList.Contains(seledfris)) {  //没有了在进行添加，防止重复
                    TempUserList.Add(seledfris);
                }
            }
            foreach (var item in TempUserList) {
                friend_num = friend_num + 1;
                lbx_seled.Items.Add(item);
            }
            friend_num = 0;
            friend_num = TempUserList.Count();
            lb_fri_num.Text = friend_num.ToString();
            tbx_search.Text = "";
        }

        private void tbx_search_TextChanged( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            foreach (var item in MyFriList) {
                if (item.Contains(tbx_search.Text)) {
                    listBox1.Items.Add(item);
                }
            }
        }
        /// <summary>
        /// 单击移除选择项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbx_seled_SelectedIndexChanged( object sender, EventArgs e ) {
            if (lbx_seled.SelectedItem != null) {
                string fri = lbx_seled.SelectedItem.ToString();
                lbx_seled.Items.Remove(fri);
                TempUserList.Remove(fri);
                friend_num = 0;
                friend_num = TempUserList.Count();
                lb_fri_num.Text = friend_num.ToString();
            }
        }
    }
}
