﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Windows.Forms;

namespace WChat {
    public class SqlTest {
       
        private SQLiteConnection sqlcon;
        public SqlTest(){
            sqlcon = new SQLiteConnection();
            sqlcon.ConnectionString = @"Data Source=" + WChat.path.paths + @"\hq_chatlog.db;";
            sqlcon.Open();
        }

        public void Open() {
            if (sqlcon.State == System.Data.ConnectionState.Closed) {
                sqlcon.Open();
            }
        }
        /// <summary>
        ///创建与微信号对应的表
        /// </summary>
        /// <param name="tabName"></param>
        public void CreateNewChatTab( string tabName ) {
            try {
                Open();
                string tbna = "ChatLog" + tabName;
                string sql = "CREATE TABLE " + tbna + "(Time datetime not null ,FromUser char(100) ,ToUser char(100) ,CONT char(600) ,Received int,Readed int,Type int,FileName char(400),FileSize char(20),V_Content char(200),V_NickName char(60) ,V_Value char(600),V_UserTicket char(200),_msgid char(1000),MediaId char(1000),TxtUrl char(400),"
                    + " IsGroup int,"
                    + " CrewNickName char(200),"
                    + " CrewImgPath char(100),"
                    + " Alias char(100),"
                    + " Province char(100),"
                    + " QQNum char(100),"
                    + " NickName char(100), "
                    + " smallheadimgurl char(100), "
                    + " RemarkName char(100), "
                    + " bigheadimgurl char(100), "
                    + " City char(100), "
                    + " Sex char(50) "
                    + ")";
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("CreateNewChatTab-sqllite", e.ToString());
            }
        }

        /// <summary>
        /// 存储聊天记录数据
        /// </summary>
        /// <param name="msg"></param>
        public void insertChatLog( string tabName, WXMsg msg ) {
            string tbna = "ChatLog" + tabName;
            int read,isgroup;
            if (msg.Readed) {
                read = 1;
            }
            else {
                read = 0;
            }
            if (msg.IsGroup) {
                isgroup = 1;
            }else {
                isgroup = 0;
            }
            Open();
            try {
                string sql = "insert into " + tbna + "(Time,"
                   + " FromUser ,"
                   + " ToUser,CONT ,"
                   + " Received,Readed,"
                   + " Type,FileName,"
                   + " FileSize,"
                   + " V_Content ,"
                   + " V_NickName,"
                   + " V_Value,"
                   + " V_UserTicket,"
                   + " _msgid ,"
                   + " MediaId,"
                   + " TxtUrl,"
                   + " IsGroup,"
                   + " CrewNickName,"
                   + " CrewImgPath,"
                   + " Alias,"
                   + " Province,"
                   + " QQNum,"
                   + " NickName,"
                   + " smallheadimgurl,"
                   + " RemarkName,"
                   + " bigheadimgurl,"
                   + " City,"
                   + " Sex"
                   + " )VALUES ('"
                   + msg.Time.ToString("s") + "','" 
                   + msg.Fr_NickName + "','" 
                   + msg.To_NickName + "','" 
                   + msg.Msg + "','" 
                   + msg.IsReceive + "','" 
                   + read + "','" 
                   + msg.Type + "','" 
                   + msg.FileName + "','" 
                   + msg.FileSize + "','" 
                   + msg.V_Content + "','" 
                   + msg.V_NickName + "','" 
                   + msg.V_Value + "','" 
                   + msg.V_UserTicket + "','" 
                   + msg.MsgId + "','"
                   + msg.MediaId + "','"
                   + msg.TxtUrl + "','"
                   + isgroup + "','"
                   + msg.CrewNickName + "','"
                   + msg.CrewImgPath + "','"
                   + msg.Alias + "','"
                   + msg.Province + "','"
                   + msg.QQNum + "','"
                   + msg.NickName + "','"
                   + msg.smallheadimgurl + "','"
                   + msg.RemarkName + "','"
                   + msg.bigheadimgurl + "','"
                   + msg.City + "','"
                   + msg.Sex 
                   + "')";
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                cmd.ExecuteNonQuery();  //连接关闭必须在这个的后面，负责会出现sql语句无法执行的问题
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("存储聊天记录数据-sqllite", e.ToString());
            }
        }
        /// <summary>
        /// 从特定表获取聊天记录，按时间排序
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="to_user"></param>
        /// <returns></returns>
        public List<WXMsg> GetChatLog( string tabName, string to_user ) {
            List<WXMsg> list = new List<WXMsg>();
            string tbna = "ChatLog" + tabName;
            try {
               Open();
                string sql = "select * from " + tbna + " where ToUser ='" + to_user + "'order by Time DESC LIMIT 0, 10";
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                SQLiteDataReader reader = cmd.ExecuteReader();
                //将结果集信息添加到返回向量中
                while (reader.Read()) {
                    WXMsg msg = new WXMsg();  //就是这个啊，气死我了，因为放到外面，每次添加的数据都一样，
                    msg.Time = Convert.ToDateTime(reader[0]);
                    msg.Fr_NickName = reader[1].ToString().Trim();
                    msg.To_NickName = reader[2].ToString().Trim();
                    msg.Msg = reader[3].ToString().Trim();
                    msg.IsReceive = int.Parse(reader[4].ToString().Trim());
                    msg.Readed = reader[5].ToString().Trim() == "1" ? true : false;
                    msg.Type = reader[6].ToString().Trim();
                    msg.FileName = reader[7].ToString().Trim();
                    msg.FileSize = reader[8].ToString().Trim();
                    msg.V_Content = reader[9].ToString().Trim();
                    msg.V_NickName = reader[10].ToString().Trim();
                    msg.V_Value = reader[11].ToString().Trim();
                    msg.V_UserTicket = reader[12].ToString().Trim();
                    msg.MsgId = reader[13].ToString().Trim();
                    msg.MediaId = reader[14].ToString().Trim();
                    msg.TxtUrl = reader[15].ToString().Trim();

                    msg.IsGroup = reader[16].ToString().Trim() == "1" ? true : false;
                    msg.CrewNickName = reader[17].ToString().Trim();
                    msg.CrewImgPath = reader[18].ToString().Trim();
                    msg.Alias = reader[19].ToString().Trim();
                    msg.Province = reader[20].ToString().Trim();
                    msg.QQNum = reader[21].ToString().Trim();
                    msg.NickName = reader[22].ToString().Trim();
                    msg.smallheadimgurl = reader[23].ToString().Trim();
                    msg.RemarkName = reader[24].ToString().Trim();
                    msg.bigheadimgurl = reader[25].ToString().Trim();
                    msg.City = reader[26].ToString().Trim();
                    msg.Sex = reader[27].ToString().Trim();

                    list.Add(msg);
                }
                reader.Close();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("GetChatLog:获取记录-sqllite", e.ToString());
            }
            return list;
        }
        /// <summary>
        /// 判断表的存在
        /// </summary>
        /// <param name="tabna"></param>
        /// <returns></returns>
        public bool HaveTab( string tabna ) {
            bool havtab = false;
            Open();
            try {
                string sql = "select * from " + tabna;
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read()) {
                    havtab = true;
                }
                reader.Close();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("HaveTab-sqllite", e.ToString());
            }
            return havtab;
        }
        /// <summary>
        /// 从时间删除记录
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="time"></param>
        public void DeleteChatLog( string tabName, string time ) {
            Open();
            try {
                string tbna = "ChatLog" + tabName;
                string sql = "delete  from " + tbna + " where Time='" + time + "'";
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("DeleteChatLog:删除聊天记录-sqllite", e.ToString());
            }
        }

        #region 敏感词操作
        /// <summary>
        ///创建与子账号对应的敏感词表
        /// </summary>
        /// <param name="tabName"></param>
        public void CreateNewSensitiveTab( string tabName ) { 
            try {
                Open();
                string tbna = "Sensitive" + tabName;
                string sql = "CREATE TABLE " + tbna + "(Iden int identity(1,1),Time datetime not null ,SensitiveWords char(300) "
                    + ")";
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("CreateNewSensitiveTab-sqllite", e.ToString());
            }
        }

        /// <summary>
        /// 存储用户铭感词
        /// </summary>
        /// <param name="msg"></param>
        public void insertSensitive( string tabName,DateTime time, string sensitive ) { 
            string tbna = "Sensitive" + tabName;
         
            Open();
            try {
                string sql = "insert into " + tbna + "( Time,SensitiveWords) VALUES ('"+ time.ToString("s") + "','"+ sensitive + "'" + ")";

                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                cmd.ExecuteNonQuery();  //连接关闭必须在这个的后面，负责会出现sql语句无法执行的问题
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("insertSensitive-sqllite", e.ToString());
            }
        }

        /// <summary>
        /// 从特定表获取用户铭感词
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="to_user"></param>
        /// <returns></returns>
        public List<string> GetSensitive( string tabName) { 
            List<string> Senlist = new List<string>();
            string tbna = "Sensitive" + tabName;
            string sensitive;
            try {
                Open();
                string sql = "select SensitiveWords from " + tbna ;
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                SQLiteDataReader reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    MessageBox.Show(reader["SensitiveWords"].ToString());
                    sensitive = reader["SensitiveWords"].ToString();
                    Senlist.Add(sensitive);
                }
                reader.Close();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("GetSensitive:获取记录-sqllite", e.ToString());
            }
            return Senlist;
        }

        /// <summary>
        /// 删除用户敏感词
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="sensitive"></param>
        public void DeleteSensitive( string tabName, string sensitive ) {
            Open();
            try {
                string tbna = "Sensitive" + tabName;
                string sql = "delete  from " + tbna + " where SensitiveWords='" + sensitive + "'";
                SQLiteCommand cmd = new SQLiteCommand(sql, sqlcon);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                sqlcon.Close();
            }
            catch (Exception e) {
                WriteLog.Writelog("DeleteChatLog:删除聊天记录-sqllite", e.ToString());
            }
        }
        #endregion
    }
}
