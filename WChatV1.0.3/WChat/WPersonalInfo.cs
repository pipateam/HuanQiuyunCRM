﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat {
    public partial class WPersonalInfo : UserControl {
        public event StartChatEventHandler StartChat;

        private bool _showTopPanel = true;
        /// <summary>
        /// 是否显示最上端栏
        /// </summary>
        public bool ShowTopPanel {
            set {
                _showTopPanel = value;
                if (_showTopPanel) {
                    plTop.Visible = true;
                    btnSendMsg.Visible = true;
                }
                else {
                    plTop.Visible = false;
                    btnSendMsg.Visible = false;
                }
            }
            get {
                return _showTopPanel;
            }
        }

        /// <summary>
        /// 好友
        /// </summary>
        private WXUser _friendUser;
        public WXUser FriendUser {
            get {
                return _friendUser;
            }
            set {
                _friendUser = value;
                if (_friendUser != null) {
                    picImage.Image = _friendUser.Icon;
                    lblNick.Text = _friendUser.NickName;
                    lblArea.Text = _friendUser.City + "，" + _friendUser.Province;
                    lblSignature.Text = _friendUser.Signature;
                    picSexImage.Image = _friendUser.Sex == "1" ? Properties.Resources.male : Properties.Resources.female;
                    picSexImage.Location = new Point(lblNick.Location.X + lblNick.Width + 4, picSexImage.Location.Y);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public WPersonalInfo() {
            InitializeComponent();
        }

        /// <summary>
        /// 界面大小改变  调整显示位置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WPersonalInfo_Resize(object sender, EventArgs e) {
           // 

        }
        /// <summary>
        /// 返回
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
      
        /// <summary>
        /// 发送消息 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSendMsg_Click(object sender, EventArgs e) {
            if (StartChat != null) {
                StartChat(_friendUser);
            }
        }

        private void button1_Click_1(object sender, EventArgs e) {
            Visible = false;
        }

        private void WPersonalInfo_Resize_1(object sender, EventArgs e) {
            picImage.Location = new Point((Width - picImage.Width) / 2, 84);
            lblNick.Location = new Point((Width - lblNick.Width) / 2, 339);
            picSexImage.Location = new Point(((Width - picSexImage.Width) / 2)+20,343);
            lblSignature.Location = new Point((Width - lblSignature.Width) / 2,367);
            lblArea.Location = new Point((Width - lblArea.Width) / 2, 404);
            btnSendMsg.Location = new Point((Width - btnSendMsg.Width) / 2, 440);
        }

        private void WPersonalInfo_Load(object sender, EventArgs e) {
            picImage.Location = new Point((Width - picImage.Width) / 2, 84);
            lblNick.Location = new Point((Width - lblNick.Width) / 2, 339);
            picSexImage.Location = new Point(((Width - picSexImage.Width) / 2) + 20, 343);
            lblSignature.Location = new Point((Width - lblSignature.Width) / 2, 367);
            lblArea.Location = new Point((Width - lblArea.Width) / 2, 404);
            btnSendMsg.Location = new Point((Width - btnSendMsg.Width) / 2, 440);
        }
    }
}
