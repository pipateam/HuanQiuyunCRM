﻿namespace WChat {
    partial class FriendDetails {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FriendDetails));
            this.btn_UpdChRoo = new CCWin.SkinControl.SkinButton();
            this.pbox_icon = new CCWin.SkinControl.SkinPictureBox();
            this.btn_CreaChatRoo = new CCWin.SkinControl.SkinButton();
            this.lb_nickname = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbox_icon)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_UpdChRoo
            // 
            this.btn_UpdChRoo.BackColor = System.Drawing.Color.Transparent;
            this.btn_UpdChRoo.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_UpdChRoo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_UpdChRoo.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_UpdChRoo.DownBack = null;
            this.btn_UpdChRoo.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_UpdChRoo.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_UpdChRoo.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_UpdChRoo.Location = new System.Drawing.Point(45, 256);
            this.btn_UpdChRoo.MouseBack = null;
            this.btn_UpdChRoo.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_UpdChRoo.Name = "btn_UpdChRoo";
            this.btn_UpdChRoo.NormlBack = null;
            this.btn_UpdChRoo.Size = new System.Drawing.Size(136, 27);
            this.btn_UpdChRoo.TabIndex = 4;
            this.btn_UpdChRoo.Text = "邀请好友进群";
            this.btn_UpdChRoo.UseVisualStyleBackColor = false;
            this.btn_UpdChRoo.Click += new System.EventHandler(this.btn_UpdChRoo_Click);
            // 
            // pbox_icon
            // 
            this.pbox_icon.BackColor = System.Drawing.Color.Transparent;
            this.pbox_icon.Location = new System.Drawing.Point(45, 54);
            this.pbox_icon.Name = "pbox_icon";
            this.pbox_icon.Size = new System.Drawing.Size(136, 119);
            this.pbox_icon.TabIndex = 5;
            this.pbox_icon.TabStop = false;
            // 
            // btn_CreaChatRoo
            // 
            this.btn_CreaChatRoo.BackColor = System.Drawing.Color.Transparent;
            this.btn_CreaChatRoo.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_CreaChatRoo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_CreaChatRoo.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_CreaChatRoo.DownBack = null;
            this.btn_CreaChatRoo.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_CreaChatRoo.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_CreaChatRoo.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_CreaChatRoo.Location = new System.Drawing.Point(45, 214);
            this.btn_CreaChatRoo.MouseBack = null;
            this.btn_CreaChatRoo.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_CreaChatRoo.Name = "btn_CreaChatRoo";
            this.btn_CreaChatRoo.NormlBack = null;
            this.btn_CreaChatRoo.Size = new System.Drawing.Size(136, 27);
            this.btn_CreaChatRoo.TabIndex = 6;
            this.btn_CreaChatRoo.Text = "新建群聊";
            this.btn_CreaChatRoo.UseVisualStyleBackColor = false;
            this.btn_CreaChatRoo.Click += new System.EventHandler(this.btn_CreaChatRoo_Click);
            // 
            // lb_nickname
            // 
            this.lb_nickname.AutoSize = true;
            this.lb_nickname.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_nickname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.lb_nickname.Location = new System.Drawing.Point(45, 190);
            this.lb_nickname.Name = "lb_nickname";
            this.lb_nickname.Size = new System.Drawing.Size(135, 12);
            this.lb_nickname.TabIndex = 41;
            this.lb_nickname.Text = "总有一天会遇见你总有";
            // 
            // FriendDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(230, 303);
            this.Controls.Add(this.lb_nickname);
            this.Controls.Add(this.btn_CreaChatRoo);
            this.Controls.Add(this.pbox_icon);
            this.Controls.Add(this.btn_UpdChRoo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.Name = "FriendDetails";
            this.Shadow = true;
            this.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.ShadowWidth = 9;
            this.Text = "好友详情";
            this.Load += new System.EventHandler(this.FriendDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbox_icon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinButton btn_UpdChRoo;
        private CCWin.SkinControl.SkinPictureBox pbox_icon;
        private CCWin.SkinControl.SkinButton btn_CreaChatRoo;
        private System.Windows.Forms.Label lb_nickname;
    }
}