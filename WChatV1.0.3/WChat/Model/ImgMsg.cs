﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WChat
{
    public class ImgMsg
    {
        public string FromUserName {
            get;
            set;
        }
        public string ToUserName {
            get;
            set;
        }
        public string MediaId { 
            get;
            set;
        }
        public int ClientMsgId { 
            get;
            set;
        }
       
        public int LocalID { 
            get;
            set;
        }

        public int Type {
            get;
            set;
        }

    }
}
