﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WChat {
   public class ChiAcc {
        public string UseAcct {
            get;
            set;
        }

        public string  UsePass {
            get;
            set;
        }

        public DateTime RegiTime {
            get;
            set;
        }

        public int CanChat {
            get;
            set;
        }

        public int CanViNots {
            get;
            set;
        }
        public int CanMass {
            get;
            set;
        }

        public int CanRob {
            get;
            set;
        }

        public int CanAddWx {
            get;
            set;
        }

        public int CanLooChaLog {
            get;
            set;
        }

        public int CanQuickReply {
            get;
            set;
        }

    }

}
