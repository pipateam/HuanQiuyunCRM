﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
namespace WChat {
    public partial class FriendDetails :Skin_Color {
        private WXUser _friend, _me;
        public FriendDetails() {
            InitializeComponent();
        }

        public void GetExtra( WXUser friend, WXUser me ) {
            _friend = friend;
            _me = me;
        }
     
        private void FriendDetails_Load( object sender, EventArgs e ) {
            lb_nickname.Text = _friend.NickName.Length > 10 ? _friend.NickName.Substring(0, 8) + "...": _friend.NickName ;
            if (_friend.Icon!=null) {
                pbox_icon.Image = _friend.Icon;
            }
        }
        /// <summary>
        /// 邀请好友进群
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_UpdChRoo_Click( object sender, EventArgs e ) {
            ChatInfo ci = new ChatInfo();
            ci.GetUserName(_friend, _me,false);
            ci.Show();
        }
        /// <summary>
        /// 建群
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_CreaChatRoo_Click( object sender, EventArgs e ) {
            ChatInfo ci = new ChatInfo();
            ci.GetUserName(_friend, _me, true);
            ci.Show();
        }

    }
}
