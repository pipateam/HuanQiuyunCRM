﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;

namespace WChat {
    public partial class ChatLogBox : UserControl {
        private string _fil_base64 = "data:img/jpg;base64,";
        private string _locicon_base64 = "data:img/jpg;base64,";

        public ChatLogBox() {
            InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;
        }
        public void RecoveryChatLog( WXMsg me ) {

            if (1 == me.IsReceive) {  //如果touser为我的话说明是受到的消息
                ShowSend(me);
            }
            else {
                ShowReceive(me);
            }
        }

        public void ShowSend( WXMsg msg ) {
            switch (msg.Type) {
            case "1":
            ShowSendMsg(msg);
            break;
            case "3":
            ShowSendPIC(msg);
            break;
            case "47":
            ShowSendMsg(msg);  //不支持的表情
            break;
            case "99":
            ShowLocPIC(msg);
            break;
            case "49":
            if (msg.TxtUrl.Trim() == "" && msg.TxtUrl.Length == 0) { //文件
                                                                     // MessageBox.Show("49，文件");
                ShowSendfileMsg(msg);
            }
            else {
                ShowSendUrlMsg(msg);
            }
            break;
            default:
            ShowSendMsg(msg);
            break;
            }
        }
        public void ShowReceive( WXMsg msg ) {
            switch (msg.Type) {
            case "1":
            ShowReceiveMsg(msg);
            break;
            case "3":
            ShowReceivePIC(msg);
            break;
            case "47":
            ShowReceiveMsg(msg);  //不支持的表情
            break;
            case "37":
            ShowReceiveVerifyMsg(msg);
            break;
            case "49":
            if (msg.TxtUrl.Trim() == "" && msg.TxtUrl.Length == 0) { //文件
                ShowReceiveFileMsg(msg);  //先屏蔽了，否则会卡死
            }
            else {                       //链接
                ShowReceiveUrlMsg(msg);
            }
            break;
            default:
            ShowReceiveMsg(msg);
            break;
            }
        }

        #region  消息框操作相关

        #region 显示发送消息
        /// <summary>
        /// UI界面显示发送消息  
        /// </summary>
        private void ShowSendMsg( WXMsg msg ) {

            #region 表情转换GIF格式
            //如果发送的消息中包含表情则转换表情成为GIF格式
            string text = msg.Msg;
            string ctent = "";
            if (text.IndexOf("[") > -1) {
                StringToUrl getUrl = new WChat.StringToUrl();
                Regex reg = new Regex(@"\[(.+?)]");
                List<string> asd_string = new List<string>();
                foreach (Match asd in reg.Matches(text)) {
                    asd_string.Add(asd.Groups[1].ToString());
                    for (int i = 0; i < asd_string.Count; i++) {
                        ctent = text.Replace(asd_string[i], "");
                    }
                    text = ctent.Replace("[]", getUrl.GetEmUrl(asd.Groups[1].ToString()));
                }
                msg.Msg = text;
            }
            #endregion

            string str = @"  <script type=""text/javascript"">

             window.location.hash = ""#ok"";</script>
             <div style=""clear:both; "">
             <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  
           
            <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#F5F5F5 #F5F5F5 #F5F5F5 #9ACD32; border-bottom-color:#F5F5F5;""></div>  
             <div  class= ""ss""  onclick=""go();"" style=""float:right; color:white; padding-top: 10px; padding-bottom: 10px; border-radius:5px; padding-left: 10px;padding-right: 10px; font-family:微软雅黑; height: auto; min-height:10px; width:auto; max-width:300px; background:#9ACD32;""> <p class=""div"" style=""margin:0px auto;"">" + msg.Msg + @"</p >  </div>    
             </div> 
             <a id='ok'></ a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            try {
                webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;
            }
            catch (Exception) {

               // throw;
            }
         

        }
        /// <summary>
        /// UI界面显示接收消息
        /// </summary>
        private void ShowSendPIC( WXMsg msg ) {
            string filena = null;
            try {
                string picpath = "D:/Documents/HuanQiuYunKong/" + msg.MsgId + "small.jpg";
                if (File.Exists(picpath)) {
                    filena = picpath;
                }
            }
            catch { }

            string str = @"<script type=""text/javascript"">window.location.hash = ""#ok"";</script> 
             <div style=""clear:both; "">

             <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

             <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#F5F5F5 #F5F5F5 #F5F5F5 #9ACD32; border-bottom-color:#F5F5F5;""></div>  
             <div  class= ""ss""   onclick=""go();"" style=""float:right; color:white; padding-top: 10px; padding-bottom: 10px; border-radius:5px; padding-left: 10px;padding-right: 10px; font-family:微软雅黑; height: auto; min-height:10px; width:auto; max-width:300px; background:#9ACD32;""><img src=" + filena + @" /></div>     
             </div> 
             <a id='ok'></a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;
        }
        /// <summary>
        ///客户端发送图片
        /// </summary>
        private bool ShowLocPIC( WXMsg msg ) {

            if (msg != null)  //图片
              {
                try {
                    using (MemoryStream ms = new MemoryStream()) {
                        string picpath = msg.FileName;
                        Bitmap b = new Bitmap(picpath);
                        b.Save(ms, ImageFormat.Png);
                        _locicon_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
                    }
                }
                catch { }
            }
            string str = @"
             <script type=""text/javascript"">window.location.hash = ""#ok"";</script> 
             <div style=""clear:both; "">

            <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

            <div style=""float:right; top:5px; magin-right:-45px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#F5F5F5 #F5F5F5 #F5F5F5 #9ACD32; border-bottom-color:#F5F5F5;""></div>  
            <div  class= ""ss""   onclick=""go();"" style=""float:right;color:white; padding-top: 10px; padding-bottom: 10px; border-radius:5px; padding-left: 10px;padding-right: 10px; font-family:微软雅黑; height: auto; min-height:10px; width:auto; max-width:300px; background:#9ACD32;"">
            <img src=""" + _locicon_base64 + @""" width =""80px""  />
            </div>    
            </div> 
            <a id='ok'></a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;
            return true;
        }

        /// <summary>
        ///客户端发送文件
        /// </summary>
        private void ShowSendfileMsg( WXMsg msg ) {
            using (MemoryStream ms = new MemoryStream()) {
                string picpath = msg.FileName;
                Bitmap backImage = new Bitmap(this.GetType(), "file.jpg");
                Bitmap b = new Bitmap(backImage);
                b.Save(ms, ImageFormat.Png);
                _fil_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
            }
            string str = @"
              <script type=""text/javascript"">window.location.hash = ""#ok"";</script> 
             <div style=""clear:both; "">

           <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

            <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px;border-color:#F5F5F5 #F5F5F5 #F5F5F5 #9ACD32; border-bottom-color:#F5F5F5;""></div>  
               
             <div onclick=""go();""  class= ""ss"" style=""float:right; border-radius:5px;  margin-top:-13px; padding: 10px; font-family:微软雅黑; margin-left:0px;height: 80px; min-height:10px; width:auto; background:white; border:1px solid #AAAAAA;"">                    
                <div style=""width:140px;height:60px;margin-top:-10px; float:left;line-height:13px;"">
                    <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.Msg + @"</p>
                    <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileSize + "B" + @"</p>
                </div>
                <div style""width:140;float:right;"">
                    <img src=""" + _fil_base64 + @""" width =""30px""/>
                </div>
                <hr color=""#DDDDDD"" /> 
                <p style=""margin:0px;font-size:10px;color:#AAAAAA;"">环球云控</p>                          
                
              </div>     
             </div> 
             <a id='ok'></a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;
        }
        /// <summary>
        ///客户端发送链接
        /// </summary>
        private void ShowSendUrlMsg( WXMsg msg ) {
            string _url_base64 = "data:img/jpg;base64,";
            string filena = null;

            try {
                string picpath = "D:/Documents/HuanQiuYunKong/" + msg.MsgId + "small.jpg";
                if (File.Exists(picpath)) {
                    filena = picpath;
                }
                using (MemoryStream ms = new MemoryStream()) {
                    Bitmap url = new Bitmap(filena);
                    url.Save(ms, ImageFormat.Png);
                    _url_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
                }
            }
            catch { }
            string str = @"
              <script type=""text/javascript"">window.location.hash = ""#ok"";</script> 
             <div style=""clear:both; "">

             <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 47%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

              <div style=""float:right; top:5px; magin-right:-33px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:9px; border-color:#fff #fff #fff #AAAAAA; border-bottom-color:#fff;""></div>  
              <div onclick=""go();""  class= ""ss"" style=""float:right; border-radius:5px;  margin-top:-10px;padding:10px; font-family:微软雅黑;height: 100px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">
                   <div style=""width:180px;margin-top:-15px;"">
                       <p style=""text-overflow: ellipsis;overflow:hidden;font-size:13px;"">" + msg.FileName + @"</p>                       
                    </div>
                    <div style=""clear:both;height:50px"">
                        <div style=""width:140px;height:60px;margin-top:-10px; float:left;line-height:10px;"">
                            <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileName + @"</p>
                        </div>
                        <div style""width:140;float:right;"">
                            <img src=""" + _url_base64 + @""" width=""38px"" height=""38px"" />
                        </div>
                    </div> 
                   
               </div>     
             </div> 
             <a id='ok'></a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;
        }
        #endregion

        #region 显示接受消息

        private void ShowReceivePIC( WXMsg msg ) {
            string filena = null;
            try {
                string picpath = "D:/Documents/HuanQiuYunKong/" + msg.MsgId + "small.jpg";
                if (File.Exists(picpath)) {
                    filena = picpath;
                }
            }
            catch { }
            string str = @"
           <script type=""text/javascript"">window.location.hash = ""#ok"";</script>
           <div style=""clear:both; width: 100%;"" >

           <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

           <p style=""margin-left:10px; color:gray; "" class="" chat_nick"">" + msg.To_NickName + @"</p>          
           <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#F5F5F5 #fff #F5F5F5 #F5F5F5; border-bottom-color:#F5F5F5;""></div>  
           <div onclick=""go();"" class= ""ss""  style=""float:left; border-radius:5px;  margin-top:-8px; padding: 10px; font-family:微软雅黑; margin-left:0px;height: auto; min-height:10px; width:auto; max-width:300px; background:#fff; ""><img src=" + filena + @" /></div>    
           </div>    
           <a id='ok'></a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;
        }
        /// <summary>
        /// UI界面显示接收消息
        /// </summary>
        private void ShowReceiveMsg( WXMsg msg ) {
            string gr_use_na = "";
            try {
                if (msg.Msg.Contains("@")) {
                    string[] sArray = msg.Msg.Split('>');
                    gr_use_na = sArray[1];
                }
                else {
                    gr_use_na = msg.Msg;
                }
            }
            catch { }

            string str = @"
           <script type=""text/javascript"">window.location.hash = ""#ok"";</script>
           <div style=""clear:both; width: 100%;"" >

           <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

           <p style=""margin-left:10px; color:gray; "" class="" chat_nick"">" + msg.To_NickName + @"</p>  
           <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#F5F5F5 #fff #F5F5F5 #F5F5F5; border-bottom-color:#F5F5F5;""></div>  
           <div class= ""ss""  onclick=""go();"" style=""float:left;  margin-top:-8px;  padding-top: 10px; padding-bottom: 10px; border-radius:5px; padding-left: 10px; padding-right: 10px; font-family:微软雅黑; height: auto; min-height:10px; width:auto; max-width:300px; background:#fff;""> <p class=""div"" style=""margin:0px auto;"">" + gr_use_na + @"</p>  </div>    
           </div>    
           <a id='ok'></a>";


            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;

        }
        /// <summary>
        /// UI界面显示接收文件
        /// </summary>
        private void ShowReceiveFileMsg( WXMsg msg ) {
            using (MemoryStream ms = new MemoryStream()) {
                string picpath = msg.FileName;
                Bitmap backImage = new Bitmap(this.GetType(), "file.jpg");
                Bitmap b = new Bitmap(backImage);
                b.Save(ms, ImageFormat.Png);
                _fil_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
            }
            string str = @"
           <script type=""text/javascript"">window.location.hash = ""#ok"";</script>
           <div style=""clear:both; width: 100%;"" >

           <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

           <p style=""margin-left:10px; color:gray; "" class="" chat_nick"">" + msg.To_NickName + @"</p>  
           <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#F5F5F5 #AAAAAA #F5F5F5 #F5F5F5; border-bottom-color:#F5F5F5;""></div> 
           <div onclick=""go();""  class= ""ss"" style=""float:left; border-radius:5px; padding:10px;  margin-top:-10px; font-family:微软雅黑;height: 80px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">                     
           <div style=""width:140px;height:60px;margin-top:-10px; float:left;line-height:13px;"">
           <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.FileName + @"</p>
           <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileSize + "B" + @"</p>
           </div>
           <div style""width:140;float:right;"">
           <img src=""" + _fil_base64 + @""" width =""30px"" />
           </div>
           <hr color=""#DDDDDD"" /> 
           <p style=""margin:0px;font-size:10px;color:#AAAAAA;"">环球云控</p>    
           </div>
           </div>    
           <a id='ok'></a>";


            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;

        }
        /// <summary>
        /// UI界面显示接收链接
        /// </summary>
        private void ShowReceiveUrlMsg( WXMsg msg ) {
            string _url_base64 = "data:img/jpg;base64,";
            string filena = null;
            try {
                string picpath = "D:/Documents/HuanQiuYunKong/" + msg.MsgId + "small.jpg";
                if (File.Exists(picpath)) {
                    filena = picpath;
                }
                using (MemoryStream ms = new MemoryStream()) {
                    Bitmap url = new Bitmap(filena);
                    url.Save(ms, ImageFormat.Png);
                    _url_base64 = "data:img/jpg;base64," + Convert.ToBase64String(ms.ToArray());
                }

            }
            catch { }
            string str = @"
           <script type=""text/javascript"">window.location.hash = ""#ok"";</script>
           <div style=""clear:both; width: 100%;"" >

           <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

           <p style=""margin-left:10px; color:gray; "" class="" chat_nick"">" + msg.To_NickName + @"</p>  
           <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#F5F5F5 #AAAAAA #F5F5F5 #F5F5F5; border-bottom-color:#F5F5F5;""></div> 
           <div onclick=""go();""  class= ""ss"" style=""float:left; border-radius:5px;  margin-top:-10px;padding:10px; font-family:微软雅黑;height: 100px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">                     
           <div style=""width:180px;margin-top:-15px"">
           <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.FileName + @"</p>                       
           </div>
           <div style=""clear:both;"">
           <div style=""width:140px;height:60px;margin-top:-10px; float:left;line-height:10px;"">
           <p style=""color:#AAAAAA;font-size:10px;"">" + msg.FileName + @"</p>
           </div>
           <div style""width:140;float:right;"">
           <img src=""" + _url_base64 + @""" width=""38px"" height=""38px"" />
           </div>
           </div>                   
           </div>
           </div>    
           <a id='ok'></a>";


            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;

        }
        /// <summary>
        /// UI界面显示通过好友验证
        /// </summary>
        private void ShowReceiveVerifyMsg( WXMsg msg ) {
            string str = @" 
           <script type=""text/javascript"">window.location.hash = ""#ok"";</script>
           <div style=""clear:both; width: 100%;"">

           <li style=""  margin-bottom:13px;"" > <div style=""margin-top:6px;color:white; background:#D9D9D9; font-size:13px; width:120px; border-radius:2px; position: absolute; right: 45%; text-align: center; "" class="" chat_nick"">" + msg.Time.ToString("f") + @"</div></li>  

           <p style=""margin-left:10px; color:gray; "" class="" chat_nick"">" + msg.To_NickName + @"</p>  
           <div style=""float:left;  top:5px; right:-16px;  width:0; height:0; font-size:0; border:solid 8px;  margin-top:-2px; border-color:#F5F5F5 #AAAAAA #F5F5F5 #F5F5F5; border-bottom-color:#F5F5F5;""></div> 
           <div onclick=""go();"" class= ""ss"" style=""float:left; border-radius:5px;  margin-top:-10px;padding:10px; font-family:微软雅黑;height: 100px; min-height:10px; width:auto; background:white;border:1px solid #AAAAAA; "">                     
           <div style=""width:140px;height:40px;margin-top:-15px"">
           <p style=""text-overflow: ellipsis;overflow:hidden;font-size:14px;"">" + msg.Msg + @"</p>                       
           </div>
           <div style=""clear:both;"">
           <div style=""width:140px;height:60px;margin-top:-10px; float:left;line-height:10px;"">
           <p style=""color:#AAAAAA;font-size:10px;text-overflow: ellipsis;overflow:hidden;white-space:nowrap;"">" + msg.V_Content + @"</p>
           </div>
           <div style""width:140;float:right;"">
           <input type=""button"",value=""通过验证"" style=""color=#808080;""></input>
           </div>
           </div> 
           <hr color=""#DDDDDD"" /> 
           <p style=""margin:0px;font-size:10px;color:#AAAAAA;margin-top:-5px;"">环球云控</p>
           </div>
           </div>    
           <a id='ok'></a>";

            if (_totalHtml == "") {
                _totalHtml = _baseHtml;
            }
            msg.Readed = true;
            webBrowser1.DocumentText = _totalHtml = _totalHtml.Replace("<a id='ok'></a>", "") + str;

        }
        #endregion

        #region 界面显示初始化

        private string _totalHtml = "";
        private string _baseHtml = @"<html><head>
       <meta http-equiv=""X-UA-Compatible"" content=""IE=9""/>
        <script type=""text/javascript"">
              function go()
         {
                  var oli=document.getElementsByClassName('ss');  
              for(var i=0; i<oli.length;i++)  
             {     
                  oli[i].index=i;  
                  oli[i].onclick=function(){  
                 // alert('你点击的列表的下标是'+this.index);
                  var x=document.getElementsByClassName('ss')[this.index].innerText;
                   window.external.getContext(this.index);
                      // alert(x);
                  };  
             }                 
          }
         window.location.hash = ""#ok"";
        </script>
        </head><body style=""background:#F5F5F5;""> </body>";

        #endregion

        #endregion
        /// <summary>
        /// 这里自定义的load事件防止webbrower第一次加载数据不显示
        /// </summary>
        public void load() {
            webBrowser1.DocumentText = _baseHtml;
        }

        public void Clear() {
            _totalHtml = "";
            webBrowser1.DocumentText = "";

        }
    }
}
