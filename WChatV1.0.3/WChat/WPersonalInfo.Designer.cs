﻿namespace WChat {
    partial class WPersonalInfo {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.picSexImage = new System.Windows.Forms.PictureBox();
            this.btnSendMsg = new System.Windows.Forms.Button();
            this.lblArea = new System.Windows.Forms.Label();
            this.lblSignature = new System.Windows.Forms.Label();
            this.lblNick = new System.Windows.Forms.Label();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.plTop = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picSexImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.plTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // picSexImage
            // 
            this.picSexImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.picSexImage.Location = new System.Drawing.Point(387, 419);
            this.picSexImage.Name = "picSexImage";
            this.picSexImage.Size = new System.Drawing.Size(20, 20);
            this.picSexImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picSexImage.TabIndex = 11;
            this.picSexImage.TabStop = false;
            // 
            // btnSendMsg
            // 
            this.btnSendMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSendMsg.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSendMsg.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSendMsg.Location = new System.Drawing.Point(354, 516);
            this.btnSendMsg.Name = "btnSendMsg";
            this.btnSendMsg.Size = new System.Drawing.Size(121, 35);
            this.btnSendMsg.TabIndex = 10;
            this.btnSendMsg.Text = "发送消息";
            this.btnSendMsg.UseVisualStyleBackColor = true;
            this.btnSendMsg.Click += new System.EventHandler(this.btnSendMsg_Click);
            // 
            // lblArea
            // 
            this.lblArea.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblArea.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblArea.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblArea.Location = new System.Drawing.Point(325, 480);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(201, 20);
            this.lblArea.TabIndex = 9;
            this.lblArea.Text = "地区：天津，中国";
            // 
            // lblSignature
            // 
            this.lblSignature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSignature.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblSignature.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblSignature.Location = new System.Drawing.Point(325, 443);
            this.lblSignature.Name = "lblSignature";
            this.lblSignature.Size = new System.Drawing.Size(201, 20);
            this.lblSignature.TabIndex = 8;
            this.lblSignature.Text = "行到水穷处，坐看云起时";
            // 
            // lblNick
            // 
            this.lblNick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNick.AutoSize = true;
            this.lblNick.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblNick.Location = new System.Drawing.Point(324, 415);
            this.lblNick.Name = "lblNick";
            this.lblNick.Size = new System.Drawing.Size(50, 25);
            this.lblNick.TabIndex = 7;
            this.lblNick.Text = "张三";
            // 
            // picImage
            // 
            this.picImage.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.picImage.Location = new System.Drawing.Point(315, 133);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(188, 183);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picImage.TabIndex = 6;
            this.picImage.TabStop = false;
            // 
            // btnBack
            // 
            this.btnBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBack.Location = new System.Drawing.Point(3, 4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(36, 29);
            this.btnBack.TabIndex = 0;
            this.btnBack.UseVisualStyleBackColor = true;
            // 
            // plTop
            // 
            this.plTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(49)))), ((int)(((byte)(56)))));
            this.plTop.Controls.Add(this.button1);
            this.plTop.Controls.Add(this.btnBack);
            this.plTop.Location = new System.Drawing.Point(0, 0);
            this.plTop.Name = "plTop";
            this.plTop.Size = new System.Drawing.Size(850, 40);
            this.plTop.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::WChat.Properties.Resources.back;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(10, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 29);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            // 
            // WPersonalInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.picSexImage);
            this.Controls.Add(this.btnSendMsg);
            this.Controls.Add(this.plTop);
            this.Controls.Add(this.lblArea);
            this.Controls.Add(this.picImage);
            this.Controls.Add(this.lblSignature);
            this.Controls.Add(this.lblNick);
            this.Name = "WPersonalInfo";
            this.Size = new System.Drawing.Size(850, 616);
            this.Load += new System.EventHandler(this.WPersonalInfo_Load);
            this.Resize += new System.EventHandler(this.WPersonalInfo_Resize_1);
            ((System.ComponentModel.ISupportInitialize)(this.picSexImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.plTop.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox picSexImage;
        private System.Windows.Forms.Button btnSendMsg;
        private System.Windows.Forms.Label lblArea;
        private System.Windows.Forms.Label lblSignature;
        private System.Windows.Forms.Label lblNick;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Panel plTop;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnBack_Click;
        private System.Windows.Forms.Button button1;
    }
}
