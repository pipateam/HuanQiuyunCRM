﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WChat {
    public partial class mychatbox : ListBox {
        private Timer timer1;
        WXMsg msg;

        private int _mouse_over = -1;
        public mychatbox() {
            InitializeComponent();
            DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            UpdateStyles();
            BorderStyle = System.Windows.Forms.BorderStyle.None;
            Cursor = Cursors.Hand;

        }

        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Rectangle bound;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;  //绘制的类型
            for (int i = 0; i < Items.Count; ++i) {
                msg = Items[i] as WXMsg;
                if (msg != null) {
                    bound = GetItemRectangle(i);

                    if (!ClientRectangle.IntersectsWith(bound)) {
                        continue;
                    }
                
                    e.Graphics.DrawLine(new Pen(Color.FromArgb(50, Color.Black)), new Point(bound.X, bound.Y + 70), new Point(bound.X + Width, bound.Y + 70));   //中间的黑色的线
                    using (Font f = new System.Drawing.Font("微软雅黑", 11)) {
                        if (msg.FileName != null) {

                            //   e.Graphics.DrawImage(user.Icon, new Rectangle(bound.X + Width -  60, bound.Y + 10, 50, 50));  //头像

                            //  e.Graphics.DrawImage(user.Icon, new Rectangle(bound.X + 10, bound.Y + 10, 50, 50));  //头像   
                            e.Graphics.DrawString(msg.FileName, f, Brushes.Black, new PointF(bound.X + 70, bound.Y + 8)); //昵称
                        }
                       
                    }
              
                      //  MessageBox.Show("msg事件！");
                      //  using (Font f2 = new Font("微软雅黑", 10)) {
                          //  string time_str = user.Time.ToShortTimeString();
                         //   Size time_size = TextRenderer.MeasureText(time_str, f2);
                         //   e.Graphics.DrawString(time_str, f2, new SolidBrush(Color.FromArgb(37, 37, 37)), new PointF((bound.X + Width)/2, bound.Y + 8)); //最后一条未读消息时间

                         //   string latest_msg = user.Msg.ToString();  //最后一条未读消息
                       
                           // e.Graphics.DrawString(latest_msg, f2, new SolidBrush(Color.FromArgb(37, 37, 37)), new PointF(bound.X + 70, bound.Y + 40));
                      //  }
                  
                }
            }
        }

        /// <summary>
        /// 设置每项高度
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMeasureItem(MeasureItemEventArgs e) {
            base.OnMeasureItem(e);
            e.ItemHeight= 70;
        }
        /// <summary>
        /// 选择变化
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSelectedIndexChanged(EventArgs e) {
            base.OnSelectedIndexChanged(e);
            Invalidate();
        }
        /// <summary>
        /// 鼠标双击
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDoubleClick(EventArgs e) {
            base.OnDoubleClick(e);
            Invalidate();
            if (SelectedIndex != -1) {
                //开启聊天
               //  StartChat?.Invoke(Items[SelectedIndex] as WXUser);
                MessageBox.Show("双击事件！");
            }
        }
        /// <summary>
        /// 鼠标移动
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e) {
            base.OnMouseMove(e);
            for (int i = 0; i < Items.Count; ++i) {
                if (GetItemRectangle(i).Contains(e.Location)) {
                    _mouse_over = i;
                    Invalidate();
                    return;
                }
            }
            _mouse_over = -1;
        }
        /// <summary>
        /// 鼠标移出
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e) {
            base.OnMouseLeave(e);
            _mouse_over = -1;
            Invalidate();
        }

        private void InitializeComponent() {     //覆写初始化方法
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            this.ResumeLayout(false);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e) {
            Invalidate();
        }
    }
}

