﻿namespace WChat {
    partial class ChatInfo {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatInfo));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_done = new CCWin.SkinControl.SkinButton();
            this.tbx_topic = new System.Windows.Forms.TextBox();
            this.btn_search = new CCWin.SkinControl.SkinButton();
            this.lbx_seled = new System.Windows.Forms.ListBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.lb_fri_num = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbx_search = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 14;
            this.listBox1.Location = new System.Drawing.Point(14, 89);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(277, 312);
            this.listBox1.Sorted = true;
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // btn_done
            // 
            this.btn_done.BackColor = System.Drawing.Color.Transparent;
            this.btn_done.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_done.DownBack = null;
            this.btn_done.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_done.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_done.Location = new System.Drawing.Point(508, 48);
            this.btn_done.MouseBack = null;
            this.btn_done.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.Name = "btn_done";
            this.btn_done.NormlBack = null;
            this.btn_done.Size = new System.Drawing.Size(74, 23);
            this.btn_done.TabIndex = 3;
            this.btn_done.Text = "完成";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // tbx_topic
            // 
            this.tbx_topic.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_topic.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.tbx_topic.Location = new System.Drawing.Point(384, 408);
            this.tbx_topic.Name = "tbx_topic";
            this.tbx_topic.Size = new System.Drawing.Size(196, 22);
            this.tbx_topic.TabIndex = 45;
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.Transparent;
            this.btn_search.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_search.DownBack = null;
            this.btn_search.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.ForeColor = System.Drawing.Color.Black;
            this.btn_search.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_search.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_search.Location = new System.Drawing.Point(228, 48);
            this.btn_search.MouseBack = null;
            this.btn_search.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.Name = "btn_search";
            this.btn_search.NormlBack = null;
            this.btn_search.Size = new System.Drawing.Size(61, 24);
            this.btn_search.TabIndex = 44;
            this.btn_search.Text = "搜索";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // lbx_seled
            // 
            this.lbx_seled.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbx_seled.FormattingEnabled = true;
            this.lbx_seled.ItemHeight = 14;
            this.lbx_seled.Location = new System.Drawing.Point(317, 89);
            this.lbx_seled.Name = "lbx_seled";
            this.lbx_seled.Size = new System.Drawing.Size(265, 312);
            this.lbx_seled.Sorted = true;
            this.lbx_seled.TabIndex = 46;
            this.lbx_seled.SelectedIndexChanged += new System.EventHandler(this.lbx_seled_SelectedIndexChanged);
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.skinLabel1.Location = new System.Drawing.Point(315, 51);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(107, 20);
            this.skinLabel1.TabIndex = 47;
            this.skinLabel1.Text = "已选择联系人：";
            // 
            // lb_fri_num
            // 
            this.lb_fri_num.AutoSize = true;
            this.lb_fri_num.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.lb_fri_num.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.lb_fri_num.Location = new System.Drawing.Point(127, 413);
            this.lb_fri_num.Name = "lb_fri_num";
            this.lb_fri_num.Size = new System.Drawing.Size(12, 12);
            this.lb_fri_num.TabIndex = 49;
            this.lb_fri_num.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.label5.Location = new System.Drawing.Point(12, 413);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 12);
            this.label5.TabIndex = 48;
            this.label5.Text = "共选择好友个数：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.label1.Location = new System.Drawing.Point(314, 413);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 12);
            this.label1.TabIndex = 51;
            this.label1.Text = "输入群名：";
            // 
            // tbx_search
            // 
            this.tbx_search.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_search.Font = new System.Drawing.Font("微软雅黑", 13.6F);
            this.tbx_search.Location = new System.Drawing.Point(20, 47);
            this.tbx_search.Name = "tbx_search";
            this.tbx_search.Size = new System.Drawing.Size(196, 24);
            this.tbx_search.TabIndex = 52;
            this.tbx_search.TextChanged += new System.EventHandler(this.tbx_search_TextChanged);
            // 
            // ChatInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CanResize = false;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(596, 438);
            this.Controls.Add(this.tbx_search);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lb_fri_num);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.lbx_seled);
            this.Controls.Add(this.tbx_topic);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.listBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.MdiBorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Name = "ChatInfo";
            this.Shadow = true;
            this.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.ShadowWidth = 15;
            this.Text = "选择联系人";
            this.Load += new System.EventHandler(this.ChatInfo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private CCWin.SkinControl.SkinButton btn_done;
        private System.Windows.Forms.TextBox tbx_topic;
        private CCWin.SkinControl.SkinButton btn_search;
        private System.Windows.Forms.ListBox lbx_seled;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private System.Windows.Forms.Label lb_fri_num;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx_search;
    }
}