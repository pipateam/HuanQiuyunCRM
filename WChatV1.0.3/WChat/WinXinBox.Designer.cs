﻿namespace WChat {
    partial class WinXinBox {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WinXinBox));
            this.wchatlist_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除项目ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbxFriendSearch = new System.Windows.Forms.ListBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加标签ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbc_right = new CCWin.SkinControl.SkinTabControl();
            this.basic_info = new CCWin.SkinControl.SkinTabPage();
            this.basic_pal = new System.Windows.Forms.Panel();
            this.tbx_creatime = new CCWin.SkinControl.SkinTextBox();
            this.tbx_creater = new CCWin.SkinControl.SkinTextBox();
            this.tb_belongkf = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel19 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel17 = new CCWin.SkinControl.SkinLabel();
            this.cb_classification = new System.Windows.Forms.ComboBox();
            this.cbo_age = new System.Windows.Forms.ComboBox();
            this.cb_gender = new System.Windows.Forms.ComboBox();
            this.daTiPicker_brith = new System.Windows.Forms.DateTimePicker();
            this.tbn_basic_save = new CCWin.SkinControl.SkinButton();
            this.skinLabel11 = new CCWin.SkinControl.SkinLabel();
            this.tb_remarks = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel16 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel15 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.tbx_remark = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel14 = new CCWin.SkinControl.SkinLabel();
            this.tbx_nickname = new CCWin.SkinControl.SkinTextBox();
            this.tb_address = new CCWin.SkinControl.SkinTextBox();
            this.tbx_wxaccount = new CCWin.SkinControl.SkinTextBox();
            this.lb_address = new CCWin.SkinControl.SkinLabel();
            this.tbx_belong_wx = new CCWin.SkinControl.SkinTextBox();
            this.tb_dgedu = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.tb_qq = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel12 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.tb_phone = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel7 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel8 = new CCWin.SkinControl.SkinLabel();
            this.tb_incoleve = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel9 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel10 = new CCWin.SkinControl.SkinLabel();
            this.tb_occupation = new CCWin.SkinControl.SkinTextBox();
            this.pa_detail_info = new CCWin.SkinControl.SkinTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pic_sex = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblSignature = new System.Windows.Forms.Label();
            this.lblNick = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblArea = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_remarknam = new System.Windows.Forms.Label();
            this.Costom_analysls = new CCWin.SkinControl.SkinTabPage();
            this.tbc_right_analysis = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_time_lin_save = new CCWin.SkinControl.SkinButton();
            this.skinLabel25 = new CCWin.SkinControl.SkinLabel();
            this.tbx_tim_li_cost1 = new CCWin.SkinControl.SkinTextBox();
            this.tbx_tim_li_cost3 = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel24 = new CCWin.SkinControl.SkinLabel();
            this.tbx_tim_li_cost2 = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel23 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel22 = new CCWin.SkinControl.SkinLabel();
            this.tbx_tim_li_persanaly = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel21 = new CCWin.SkinControl.SkinLabel();
            this.tbx_tim_li_acti_tim = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel13 = new CCWin.SkinControl.SkinLabel();
            this.tbx_tim_li_like = new CCWin.SkinControl.SkinTextBox();
            this.skinTabPage2 = new CCWin.SkinControl.SkinTabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_requirement_save = new CCWin.SkinControl.SkinButton();
            this.tbx_custom2 = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel26 = new CCWin.SkinControl.SkinLabel();
            this.tbx_custom3 = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel27 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel28 = new CCWin.SkinControl.SkinLabel();
            this.tbx_custom1 = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel29 = new CCWin.SkinControl.SkinLabel();
            this.tbx_potential_demand = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel30 = new CCWin.SkinControl.SkinLabel();
            this.tbx_req_now = new CCWin.SkinControl.SkinTextBox();
            this.skinTabPage3 = new CCWin.SkinControl.SkinTabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_consumption_save = new CCWin.SkinControl.SkinButton();
            this.lbzh = new CCWin.SkinControl.SkinLabel();
            this.lb_b = new CCWin.SkinControl.SkinLabel();
            this.tbx_comprehensive_analy = new CCWin.SkinControl.SkinTextBox();
            this.tbx_buy_cosmetic_now = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel39 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel40 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel41 = new CCWin.SkinControl.SkinLabel();
            this.tbx_Ornaments_tendency = new CCWin.SkinControl.SkinTextBox();
            this.tbx_buy_ornaments_now = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel35 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel36 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel37 = new CCWin.SkinControl.SkinLabel();
            this.lb_cloth_tendency = new CCWin.SkinControl.SkinLabel();
            this.tbx_sk_care_brand_tenden = new CCWin.SkinControl.SkinTextBox();
            this.tbx_buy_skinca_now = new CCWin.SkinControl.SkinTextBox();
            this.tbx_cloth_tendency = new CCWin.SkinControl.SkinTextBox();
            this.tbx_buy_clothes_now = new CCWin.SkinControl.SkinTextBox();
            this.tgh = new CCWin.SkinControl.SkinLabel();
            this.skinLabel32 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel33 = new CCWin.SkinControl.SkinLabel();
            this.tbx_standard_consump = new CCWin.SkinControl.SkinTextBox();
            this.tbx_con_hab = new CCWin.SkinControl.SkinTextBox();
            this.tbx_Cosmetic_tendency = new CCWin.SkinControl.SkinTextBox();
            this.skinTabPage4 = new CCWin.SkinControl.SkinTabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.pa_chatlog = new CCWin.SkinControl.SkinTabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabc_left = new CCWin.SkinControl.SkinTabControl();
            this.chat = new CCWin.SkinControl.SkinTabPage();
            this.friends = new CCWin.SkinControl.SkinTabPage();
            this.chatVScrollBar2 = new CustomScrollBar.ListBoxScrollBar.ListBoxVScrollBar();
            this.wChatList1 = new WChat.WChatList();
            this.friendscrool = new CustomScrollBar.ListBoxScrollBar.ListBoxVScrollBar();
            this.wFriendsList1 = new WChat.WFriendsList();
            this.wchatlist_menu.SuspendLayout();
            this.panel6.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tbc_right.SuspendLayout();
            this.basic_info.SuspendLayout();
            this.basic_pal.SuspendLayout();
            this.pa_detail_info.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.Costom_analysls.SuspendLayout();
            this.tbc_right_analysis.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.skinTabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.skinTabPage3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.skinTabPage4.SuspendLayout();
            this.pa_chatlog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabc_left.SuspendLayout();
            this.chat.SuspendLayout();
            this.friends.SuspendLayout();
            this.SuspendLayout();
            // 
            // wchatlist_menu
            // 
            this.wchatlist_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除项目ToolStripMenuItem});
            this.wchatlist_menu.Name = "wchatlist_menu";
            this.wchatlist_menu.Size = new System.Drawing.Size(125, 26);
            // 
            // 删除项目ToolStripMenuItem
            // 
            this.删除项目ToolStripMenuItem.Name = "删除项目ToolStripMenuItem";
            this.删除项目ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除项目ToolStripMenuItem.Text = "删除项目";
            this.删除项目ToolStripMenuItem.Click += new System.EventHandler(this.删除项目ToolStripMenuItem_Click);
            // 
            // lbxFriendSearch
            // 
            this.lbxFriendSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbxFriendSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.lbxFriendSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbxFriendSearch.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lbxFriendSearch.ForeColor = System.Drawing.Color.GreenYellow;
            this.lbxFriendSearch.IntegralHeight = false;
            this.lbxFriendSearch.ItemHeight = 21;
            this.lbxFriendSearch.Location = new System.Drawing.Point(-1, 53);
            this.lbxFriendSearch.Name = "lbxFriendSearch";
            this.lbxFriendSearch.Size = new System.Drawing.Size(282, 551);
            this.lbxFriendSearch.TabIndex = 0;
            this.lbxFriendSearch.DoubleClick += new System.EventHandler(this.lbxFriendSearch_DoubleClick);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSearch.Font = new System.Drawing.Font("宋体", 14F);
            this.txtSearch.ForeColor = System.Drawing.Color.GreenYellow;
            this.txtSearch.Location = new System.Drawing.Point(49, 16);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(215, 22);
            this.txtSearch.TabIndex = 7;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.panel6.Controls.Add(this.btnSearch);
            this.panel6.Controls.Add(this.txtSearch);
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(282, 53);
            this.panel6.TabIndex = 1;
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSearch.BackgroundImage")));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSearch.Enabled = false;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSearch.Location = new System.Drawing.Point(16, 14);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(32, 26);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(382, 588);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "0";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(288, 588);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "同步消息次数：";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加标签ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            // 
            // 添加标签ToolStripMenuItem
            // 
            this.添加标签ToolStripMenuItem.Name = "添加标签ToolStripMenuItem";
            this.添加标签ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.添加标签ToolStripMenuItem.Text = "添加标签";
            this.添加标签ToolStripMenuItem.Click += new System.EventHandler(this.添加标签ToolStripMenuItem_Click);
            // 
            // tbc_right
            // 
            this.tbc_right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbc_right.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.tbc_right.BackColor = System.Drawing.Color.Gainsboro;
            this.tbc_right.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.tbc_right.Controls.Add(this.basic_info);
            this.tbc_right.Controls.Add(this.pa_detail_info);
            this.tbc_right.Controls.Add(this.Costom_analysls);
            this.tbc_right.Controls.Add(this.pa_chatlog);
            this.tbc_right.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbc_right.HeadBack = null;
            this.tbc_right.IcoStateSwitch = true;
            this.tbc_right.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.tbc_right.ItemSize = new System.Drawing.Size(70, 36);
            this.tbc_right.Location = new System.Drawing.Point(943, 3);
            this.tbc_right.Name = "tbc_right";
            this.tbc_right.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("tbc_right.PageArrowDown")));
            this.tbc_right.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("tbc_right.PageArrowHover")));
            this.tbc_right.PageBaseColor = System.Drawing.Color.Teal;
            this.tbc_right.PageBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.tbc_right.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("tbc_right.PageCloseHover")));
            this.tbc_right.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("tbc_right.PageCloseNormal")));
            this.tbc_right.PageDown = ((System.Drawing.Image)(resources.GetObject("tbc_right.PageDown")));
            this.tbc_right.PageDownTxtColor = System.Drawing.Color.White;
            this.tbc_right.PageHover = ((System.Drawing.Image)(resources.GetObject("tbc_right.PageHover")));
            this.tbc_right.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.tbc_right.PageNorml = null;
            this.tbc_right.Radius = 60;
            this.tbc_right.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbc_right.SelectedIndex = 1;
            this.tbc_right.Size = new System.Drawing.Size(290, 597);
            this.tbc_right.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbc_right.TabIndex = 1;
            this.tbc_right.SelectedIndexChanged += new System.EventHandler(this.skinTabControl1_SelectedIndexChanged);
            // 
            // basic_info
            // 
            this.basic_info.BackColor = System.Drawing.Color.White;
            this.basic_info.BorderColor = System.Drawing.Color.DarkGray;
            this.basic_info.Controls.Add(this.basic_pal);
            this.basic_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.basic_info.Location = new System.Drawing.Point(0, 36);
            this.basic_info.Name = "basic_info";
            this.basic_info.Size = new System.Drawing.Size(290, 561);
            this.basic_info.TabIndex = 0;
            this.basic_info.TabItemImage = null;
            this.basic_info.Text = "基本信息";
            // 
            // basic_pal
            // 
            this.basic_pal.AutoScroll = true;
            this.basic_pal.AutoSize = true;
            this.basic_pal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.basic_pal.Controls.Add(this.tbx_creatime);
            this.basic_pal.Controls.Add(this.tbx_creater);
            this.basic_pal.Controls.Add(this.tb_belongkf);
            this.basic_pal.Controls.Add(this.skinLabel19);
            this.basic_pal.Controls.Add(this.skinLabel17);
            this.basic_pal.Controls.Add(this.cb_classification);
            this.basic_pal.Controls.Add(this.cbo_age);
            this.basic_pal.Controls.Add(this.cb_gender);
            this.basic_pal.Controls.Add(this.daTiPicker_brith);
            this.basic_pal.Controls.Add(this.tbn_basic_save);
            this.basic_pal.Controls.Add(this.skinLabel11);
            this.basic_pal.Controls.Add(this.tb_remarks);
            this.basic_pal.Controls.Add(this.skinLabel1);
            this.basic_pal.Controls.Add(this.skinLabel16);
            this.basic_pal.Controls.Add(this.skinLabel2);
            this.basic_pal.Controls.Add(this.skinLabel15);
            this.basic_pal.Controls.Add(this.skinLabel3);
            this.basic_pal.Controls.Add(this.skinLabel4);
            this.basic_pal.Controls.Add(this.tbx_remark);
            this.basic_pal.Controls.Add(this.skinLabel14);
            this.basic_pal.Controls.Add(this.tbx_nickname);
            this.basic_pal.Controls.Add(this.tb_address);
            this.basic_pal.Controls.Add(this.tbx_wxaccount);
            this.basic_pal.Controls.Add(this.lb_address);
            this.basic_pal.Controls.Add(this.tbx_belong_wx);
            this.basic_pal.Controls.Add(this.tb_dgedu);
            this.basic_pal.Controls.Add(this.skinLabel5);
            this.basic_pal.Controls.Add(this.tb_qq);
            this.basic_pal.Controls.Add(this.skinLabel12);
            this.basic_pal.Controls.Add(this.skinLabel6);
            this.basic_pal.Controls.Add(this.tb_phone);
            this.basic_pal.Controls.Add(this.skinLabel7);
            this.basic_pal.Controls.Add(this.skinLabel8);
            this.basic_pal.Controls.Add(this.tb_incoleve);
            this.basic_pal.Controls.Add(this.skinLabel9);
            this.basic_pal.Controls.Add(this.skinLabel10);
            this.basic_pal.Controls.Add(this.tb_occupation);
            this.basic_pal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.basic_pal.Location = new System.Drawing.Point(0, 0);
            this.basic_pal.Margin = new System.Windows.Forms.Padding(20);
            this.basic_pal.Name = "basic_pal";
            this.basic_pal.Size = new System.Drawing.Size(290, 561);
            this.basic_pal.TabIndex = 5;
            // 
            // tbx_creatime
            // 
            this.tbx_creatime.BackColor = System.Drawing.Color.Transparent;
            this.tbx_creatime.DownBack = null;
            this.tbx_creatime.Icon = null;
            this.tbx_creatime.IconIsButton = false;
            this.tbx_creatime.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_creatime.IsPasswordChat = '\0';
            this.tbx_creatime.IsSystemPasswordChar = false;
            this.tbx_creatime.Lines = new string[0];
            this.tbx_creatime.Location = new System.Drawing.Point(101, 762);
            this.tbx_creatime.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_creatime.MaxLength = 32767;
            this.tbx_creatime.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_creatime.MouseBack = null;
            this.tbx_creatime.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_creatime.Multiline = false;
            this.tbx_creatime.Name = "tbx_creatime";
            this.tbx_creatime.NormlBack = null;
            this.tbx_creatime.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_creatime.ReadOnly = false;
            this.tbx_creatime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_creatime.Size = new System.Drawing.Size(162, 28);
            // 
            // 
            // 
            this.tbx_creatime.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_creatime.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_creatime.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_creatime.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_creatime.SkinTxt.Name = "BaseText";
            this.tbx_creatime.SkinTxt.Size = new System.Drawing.Size(152, 18);
            this.tbx_creatime.SkinTxt.TabIndex = 0;
            this.tbx_creatime.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_creatime.SkinTxt.WaterText = "";
            this.tbx_creatime.TabIndex = 41;
            this.tbx_creatime.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_creatime.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_creatime.WaterText = "";
            this.tbx_creatime.WordWrap = true;
            // 
            // tbx_creater
            // 
            this.tbx_creater.BackColor = System.Drawing.Color.Transparent;
            this.tbx_creater.DownBack = null;
            this.tbx_creater.Icon = null;
            this.tbx_creater.IconIsButton = false;
            this.tbx_creater.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_creater.IsPasswordChat = '\0';
            this.tbx_creater.IsSystemPasswordChar = false;
            this.tbx_creater.Lines = new string[0];
            this.tbx_creater.Location = new System.Drawing.Point(100, 717);
            this.tbx_creater.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_creater.MaxLength = 32767;
            this.tbx_creater.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_creater.MouseBack = null;
            this.tbx_creater.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_creater.Multiline = false;
            this.tbx_creater.Name = "tbx_creater";
            this.tbx_creater.NormlBack = null;
            this.tbx_creater.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_creater.ReadOnly = false;
            this.tbx_creater.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_creater.Size = new System.Drawing.Size(162, 28);
            // 
            // 
            // 
            this.tbx_creater.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_creater.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_creater.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_creater.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_creater.SkinTxt.Name = "BaseText";
            this.tbx_creater.SkinTxt.Size = new System.Drawing.Size(152, 18);
            this.tbx_creater.SkinTxt.TabIndex = 0;
            this.tbx_creater.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_creater.SkinTxt.WaterText = "";
            this.tbx_creater.TabIndex = 22;
            this.tbx_creater.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_creater.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_creater.WaterText = "";
            this.tbx_creater.WordWrap = true;
            // 
            // tb_belongkf
            // 
            this.tb_belongkf.BackColor = System.Drawing.Color.Transparent;
            this.tb_belongkf.DownBack = null;
            this.tb_belongkf.Icon = null;
            this.tb_belongkf.IconIsButton = false;
            this.tb_belongkf.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_belongkf.IsPasswordChat = '\0';
            this.tb_belongkf.IsSystemPasswordChar = false;
            this.tb_belongkf.Lines = new string[0];
            this.tb_belongkf.Location = new System.Drawing.Point(97, 519);
            this.tb_belongkf.Margin = new System.Windows.Forms.Padding(0);
            this.tb_belongkf.MaxLength = 32767;
            this.tb_belongkf.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_belongkf.MouseBack = null;
            this.tb_belongkf.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_belongkf.Multiline = false;
            this.tb_belongkf.Name = "tb_belongkf";
            this.tb_belongkf.NormlBack = null;
            this.tb_belongkf.Padding = new System.Windows.Forms.Padding(5);
            this.tb_belongkf.ReadOnly = false;
            this.tb_belongkf.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_belongkf.Size = new System.Drawing.Size(162, 28);
            // 
            // 
            // 
            this.tb_belongkf.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_belongkf.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_belongkf.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_belongkf.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_belongkf.SkinTxt.Name = "BaseText";
            this.tb_belongkf.SkinTxt.Size = new System.Drawing.Size(152, 18);
            this.tb_belongkf.SkinTxt.TabIndex = 0;
            this.tb_belongkf.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_belongkf.SkinTxt.WaterText = "";
            this.tb_belongkf.TabIndex = 21;
            this.tb_belongkf.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_belongkf.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_belongkf.WaterText = "";
            this.tb_belongkf.WordWrap = true;
            // 
            // skinLabel19
            // 
            this.skinLabel19.AutoSize = true;
            this.skinLabel19.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel19.BorderColor = System.Drawing.Color.White;
            this.skinLabel19.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel19.Location = new System.Drawing.Point(18, 767);
            this.skinLabel19.Name = "skinLabel19";
            this.skinLabel19.Size = new System.Drawing.Size(73, 20);
            this.skinLabel19.TabIndex = 39;
            this.skinLabel19.Text = "创建时间:";
            // 
            // skinLabel17
            // 
            this.skinLabel17.AutoSize = true;
            this.skinLabel17.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel17.BorderColor = System.Drawing.Color.White;
            this.skinLabel17.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel17.Location = new System.Drawing.Point(21, 722);
            this.skinLabel17.Name = "skinLabel17";
            this.skinLabel17.Size = new System.Drawing.Size(58, 20);
            this.skinLabel17.TabIndex = 37;
            this.skinLabel17.Text = "创建人:";
            // 
            // cb_classification
            // 
            this.cb_classification.FormattingEnabled = true;
            this.cb_classification.Items.AddRange(new object[] {
            "即将成交",
            "有意向",
            "无意向"});
            this.cb_classification.Location = new System.Drawing.Point(97, 559);
            this.cb_classification.Name = "cb_classification";
            this.cb_classification.Size = new System.Drawing.Size(160, 28);
            this.cb_classification.TabIndex = 36;
            // 
            // cbo_age
            // 
            this.cbo_age.FormattingEnabled = true;
            this.cbo_age.Items.AddRange(new object[] {
            "20",
            "30",
            "40",
            "50",
            "60"});
            this.cbo_age.Location = new System.Drawing.Point(193, 178);
            this.cbo_age.Name = "cbo_age";
            this.cbo_age.Size = new System.Drawing.Size(65, 28);
            this.cbo_age.TabIndex = 34;
            // 
            // cb_gender
            // 
            this.cb_gender.FormattingEnabled = true;
            this.cb_gender.Items.AddRange(new object[] {
            "男",
            "女"});
            this.cb_gender.Location = new System.Drawing.Point(69, 178);
            this.cb_gender.Name = "cb_gender";
            this.cb_gender.Size = new System.Drawing.Size(51, 28);
            this.cb_gender.TabIndex = 5;
            // 
            // daTiPicker_brith
            // 
            this.daTiPicker_brith.Location = new System.Drawing.Point(95, 218);
            this.daTiPicker_brith.Name = "daTiPicker_brith";
            this.daTiPicker_brith.Size = new System.Drawing.Size(163, 26);
            this.daTiPicker_brith.TabIndex = 33;
            // 
            // tbn_basic_save
            // 
            this.tbn_basic_save.BackColor = System.Drawing.Color.Transparent;
            this.tbn_basic_save.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tbn_basic_save.BackgroundImage")));
            this.tbn_basic_save.BaseColor = System.Drawing.Color.Transparent;
            this.tbn_basic_save.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.tbn_basic_save.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.tbn_basic_save.DownBack = null;
            this.tbn_basic_save.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.tbn_basic_save.IsDrawGlass = false;
            this.tbn_basic_save.Location = new System.Drawing.Point(187, 810);
            this.tbn_basic_save.MouseBack = null;
            this.tbn_basic_save.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.tbn_basic_save.Name = "tbn_basic_save";
            this.tbn_basic_save.NormlBack = null;
            this.tbn_basic_save.Size = new System.Drawing.Size(75, 29);
            this.tbn_basic_save.TabIndex = 32;
            this.tbn_basic_save.Text = "保存";
            this.tbn_basic_save.UseVisualStyleBackColor = false;
            this.tbn_basic_save.Click += new System.EventHandler(this.tbn_basic_save_Click);
            // 
            // skinLabel11
            // 
            this.skinLabel11.AutoSize = true;
            this.skinLabel11.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel11.BorderColor = System.Drawing.Color.White;
            this.skinLabel11.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel11.Location = new System.Drawing.Point(20, 181);
            this.skinLabel11.Name = "skinLabel11";
            this.skinLabel11.Size = new System.Drawing.Size(43, 20);
            this.skinLabel11.TabIndex = 20;
            this.skinLabel11.Text = "性别:";
            // 
            // tb_remarks
            // 
            this.tb_remarks.BackColor = System.Drawing.Color.Transparent;
            this.tb_remarks.DownBack = null;
            this.tb_remarks.Icon = null;
            this.tb_remarks.IconIsButton = false;
            this.tb_remarks.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_remarks.IsPasswordChat = '\0';
            this.tb_remarks.IsSystemPasswordChar = false;
            this.tb_remarks.Lines = new string[0];
            this.tb_remarks.Location = new System.Drawing.Point(96, 600);
            this.tb_remarks.Margin = new System.Windows.Forms.Padding(0);
            this.tb_remarks.MaxLength = 32767;
            this.tb_remarks.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_remarks.MouseBack = null;
            this.tb_remarks.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_remarks.Multiline = true;
            this.tb_remarks.Name = "tb_remarks";
            this.tb_remarks.NormlBack = null;
            this.tb_remarks.Padding = new System.Windows.Forms.Padding(5);
            this.tb_remarks.ReadOnly = false;
            this.tb_remarks.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_remarks.Size = new System.Drawing.Size(163, 98);
            // 
            // 
            // 
            this.tb_remarks.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_remarks.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_remarks.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_remarks.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_remarks.SkinTxt.Multiline = true;
            this.tb_remarks.SkinTxt.Name = "BaseText";
            this.tb_remarks.SkinTxt.Size = new System.Drawing.Size(153, 88);
            this.tb_remarks.SkinTxt.TabIndex = 0;
            this.tb_remarks.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_remarks.SkinTxt.WaterText = "";
            this.tb_remarks.TabIndex = 21;
            this.tb_remarks.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_remarks.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_remarks.WaterText = "";
            this.tb_remarks.WordWrap = true;
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel1.Location = new System.Drawing.Point(20, 22);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(58, 20);
            this.skinLabel1.TabIndex = 0;
            this.skinLabel1.Text = "备注名:";
            // 
            // skinLabel16
            // 
            this.skinLabel16.AutoSize = true;
            this.skinLabel16.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel16.BorderColor = System.Drawing.Color.White;
            this.skinLabel16.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel16.Location = new System.Drawing.Point(21, 596);
            this.skinLabel16.Name = "skinLabel16";
            this.skinLabel16.Size = new System.Drawing.Size(43, 20);
            this.skinLabel16.TabIndex = 31;
            this.skinLabel16.Text = "备注:";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel2.Location = new System.Drawing.Point(19, 59);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(73, 20);
            this.skinLabel2.TabIndex = 1;
            this.skinLabel2.Text = "微信昵称:";
            // 
            // skinLabel15
            // 
            this.skinLabel15.AutoSize = true;
            this.skinLabel15.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel15.BorderColor = System.Drawing.Color.White;
            this.skinLabel15.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel15.Location = new System.Drawing.Point(21, 559);
            this.skinLabel15.Name = "skinLabel15";
            this.skinLabel15.Size = new System.Drawing.Size(73, 20);
            this.skinLabel15.TabIndex = 30;
            this.skinLabel15.Text = "客户分类:";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel3.Location = new System.Drawing.Point(20, 99);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(58, 20);
            this.skinLabel3.TabIndex = 2;
            this.skinLabel3.Text = "微信号:";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel4.Location = new System.Drawing.Point(19, 140);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(73, 20);
            this.skinLabel4.TabIndex = 3;
            this.skinLabel4.Text = "所属微信:";
            // 
            // tbx_remark
            // 
            this.tbx_remark.BackColor = System.Drawing.Color.Transparent;
            this.tbx_remark.DownBack = null;
            this.tbx_remark.Icon = null;
            this.tbx_remark.IconIsButton = false;
            this.tbx_remark.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_remark.IsPasswordChat = '\0';
            this.tbx_remark.IsSystemPasswordChar = false;
            this.tbx_remark.Lines = new string[0];
            this.tbx_remark.Location = new System.Drawing.Point(95, 18);
            this.tbx_remark.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_remark.MaxLength = 32767;
            this.tbx_remark.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_remark.MouseBack = null;
            this.tbx_remark.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_remark.Multiline = false;
            this.tbx_remark.Name = "tbx_remark";
            this.tbx_remark.NormlBack = null;
            this.tbx_remark.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_remark.ReadOnly = false;
            this.tbx_remark.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_remark.Size = new System.Drawing.Size(163, 28);
            // 
            // 
            // 
            this.tbx_remark.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_remark.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_remark.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_remark.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_remark.SkinTxt.Name = "BaseText";
            this.tbx_remark.SkinTxt.Size = new System.Drawing.Size(153, 18);
            this.tbx_remark.SkinTxt.TabIndex = 0;
            this.tbx_remark.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_remark.SkinTxt.WaterText = "";
            this.tbx_remark.TabIndex = 4;
            this.tbx_remark.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_remark.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_remark.WaterText = "";
            this.tbx_remark.WordWrap = true;
            // 
            // skinLabel14
            // 
            this.skinLabel14.AutoSize = true;
            this.skinLabel14.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel14.BorderColor = System.Drawing.Color.White;
            this.skinLabel14.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel14.Location = new System.Drawing.Point(20, 521);
            this.skinLabel14.Name = "skinLabel14";
            this.skinLabel14.Size = new System.Drawing.Size(73, 20);
            this.skinLabel14.TabIndex = 27;
            this.skinLabel14.Text = "所属客服:";
            // 
            // tbx_nickname
            // 
            this.tbx_nickname.BackColor = System.Drawing.Color.Transparent;
            this.tbx_nickname.DownBack = null;
            this.tbx_nickname.Icon = null;
            this.tbx_nickname.IconIsButton = false;
            this.tbx_nickname.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_nickname.IsPasswordChat = '\0';
            this.tbx_nickname.IsSystemPasswordChar = false;
            this.tbx_nickname.Lines = new string[0];
            this.tbx_nickname.Location = new System.Drawing.Point(95, 56);
            this.tbx_nickname.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_nickname.MaxLength = 32767;
            this.tbx_nickname.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_nickname.MouseBack = null;
            this.tbx_nickname.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_nickname.Multiline = false;
            this.tbx_nickname.Name = "tbx_nickname";
            this.tbx_nickname.NormlBack = null;
            this.tbx_nickname.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_nickname.ReadOnly = false;
            this.tbx_nickname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_nickname.Size = new System.Drawing.Size(163, 28);
            // 
            // 
            // 
            this.tbx_nickname.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_nickname.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_nickname.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_nickname.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_nickname.SkinTxt.Name = "BaseText";
            this.tbx_nickname.SkinTxt.Size = new System.Drawing.Size(153, 18);
            this.tbx_nickname.SkinTxt.TabIndex = 0;
            this.tbx_nickname.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_nickname.SkinTxt.WaterText = "";
            this.tbx_nickname.TabIndex = 5;
            this.tbx_nickname.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_nickname.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_nickname.WaterText = "";
            this.tbx_nickname.WordWrap = true;
            // 
            // tb_address
            // 
            this.tb_address.BackColor = System.Drawing.Color.Transparent;
            this.tb_address.DownBack = null;
            this.tb_address.Icon = null;
            this.tb_address.IconIsButton = false;
            this.tb_address.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_address.IsPasswordChat = '\0';
            this.tb_address.IsSystemPasswordChar = false;
            this.tb_address.Lines = new string[0];
            this.tb_address.Location = new System.Drawing.Point(97, 480);
            this.tb_address.Margin = new System.Windows.Forms.Padding(0);
            this.tb_address.MaxLength = 32767;
            this.tb_address.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_address.MouseBack = null;
            this.tb_address.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_address.Multiline = false;
            this.tb_address.Name = "tb_address";
            this.tb_address.NormlBack = null;
            this.tb_address.Padding = new System.Windows.Forms.Padding(5);
            this.tb_address.ReadOnly = false;
            this.tb_address.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_address.Size = new System.Drawing.Size(162, 28);
            // 
            // 
            // 
            this.tb_address.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_address.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_address.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_address.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_address.SkinTxt.Name = "BaseText";
            this.tb_address.SkinTxt.Size = new System.Drawing.Size(152, 18);
            this.tb_address.SkinTxt.TabIndex = 0;
            this.tb_address.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_address.SkinTxt.WaterText = "";
            this.tb_address.TabIndex = 20;
            this.tb_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_address.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_address.WaterText = "";
            this.tb_address.WordWrap = true;
            // 
            // tbx_wxaccount
            // 
            this.tbx_wxaccount.BackColor = System.Drawing.Color.Transparent;
            this.tbx_wxaccount.DownBack = null;
            this.tbx_wxaccount.Icon = null;
            this.tbx_wxaccount.IconIsButton = false;
            this.tbx_wxaccount.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_wxaccount.IsPasswordChat = '\0';
            this.tbx_wxaccount.IsSystemPasswordChar = false;
            this.tbx_wxaccount.Lines = new string[0];
            this.tbx_wxaccount.Location = new System.Drawing.Point(95, 96);
            this.tbx_wxaccount.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_wxaccount.MaxLength = 32767;
            this.tbx_wxaccount.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_wxaccount.MouseBack = null;
            this.tbx_wxaccount.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_wxaccount.Multiline = false;
            this.tbx_wxaccount.Name = "tbx_wxaccount";
            this.tbx_wxaccount.NormlBack = null;
            this.tbx_wxaccount.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_wxaccount.ReadOnly = false;
            this.tbx_wxaccount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_wxaccount.Size = new System.Drawing.Size(163, 28);
            // 
            // 
            // 
            this.tbx_wxaccount.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_wxaccount.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_wxaccount.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_wxaccount.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_wxaccount.SkinTxt.Name = "BaseText";
            this.tbx_wxaccount.SkinTxt.Size = new System.Drawing.Size(153, 18);
            this.tbx_wxaccount.SkinTxt.TabIndex = 0;
            this.tbx_wxaccount.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_wxaccount.SkinTxt.WaterText = "";
            this.tbx_wxaccount.TabIndex = 6;
            this.tbx_wxaccount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_wxaccount.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_wxaccount.WaterText = "";
            this.tbx_wxaccount.WordWrap = true;
            // 
            // lb_address
            // 
            this.lb_address.AutoSize = true;
            this.lb_address.BackColor = System.Drawing.Color.Transparent;
            this.lb_address.BorderColor = System.Drawing.Color.White;
            this.lb_address.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.lb_address.Location = new System.Drawing.Point(21, 484);
            this.lb_address.Name = "lb_address";
            this.lb_address.Size = new System.Drawing.Size(73, 20);
            this.lb_address.TabIndex = 26;
            this.lb_address.Text = "联系地址:";
            // 
            // tbx_belong_wx
            // 
            this.tbx_belong_wx.BackColor = System.Drawing.Color.Transparent;
            this.tbx_belong_wx.DownBack = null;
            this.tbx_belong_wx.Icon = null;
            this.tbx_belong_wx.IconIsButton = false;
            this.tbx_belong_wx.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_belong_wx.IsPasswordChat = '\0';
            this.tbx_belong_wx.IsSystemPasswordChar = false;
            this.tbx_belong_wx.Lines = new string[0];
            this.tbx_belong_wx.Location = new System.Drawing.Point(95, 137);
            this.tbx_belong_wx.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_belong_wx.MaxLength = 32767;
            this.tbx_belong_wx.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_belong_wx.MouseBack = null;
            this.tbx_belong_wx.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_belong_wx.Multiline = false;
            this.tbx_belong_wx.Name = "tbx_belong_wx";
            this.tbx_belong_wx.NormlBack = null;
            this.tbx_belong_wx.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_belong_wx.ReadOnly = false;
            this.tbx_belong_wx.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_belong_wx.Size = new System.Drawing.Size(163, 28);
            // 
            // 
            // 
            this.tbx_belong_wx.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_belong_wx.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_belong_wx.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_belong_wx.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_belong_wx.SkinTxt.Name = "BaseText";
            this.tbx_belong_wx.SkinTxt.Size = new System.Drawing.Size(153, 18);
            this.tbx_belong_wx.SkinTxt.TabIndex = 0;
            this.tbx_belong_wx.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_belong_wx.SkinTxt.WaterText = "";
            this.tbx_belong_wx.TabIndex = 7;
            this.tbx_belong_wx.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_belong_wx.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_belong_wx.WaterText = "";
            this.tbx_belong_wx.WordWrap = true;
            // 
            // tb_dgedu
            // 
            this.tb_dgedu.BackColor = System.Drawing.Color.Transparent;
            this.tb_dgedu.DownBack = null;
            this.tb_dgedu.Icon = null;
            this.tb_dgedu.IconIsButton = false;
            this.tb_dgedu.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_dgedu.IsPasswordChat = '\0';
            this.tb_dgedu.IsSystemPasswordChar = false;
            this.tb_dgedu.Lines = new string[0];
            this.tb_dgedu.Location = new System.Drawing.Point(99, 396);
            this.tb_dgedu.Margin = new System.Windows.Forms.Padding(0);
            this.tb_dgedu.MaxLength = 32767;
            this.tb_dgedu.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_dgedu.MouseBack = null;
            this.tb_dgedu.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_dgedu.Multiline = false;
            this.tb_dgedu.Name = "tb_dgedu";
            this.tb_dgedu.NormlBack = null;
            this.tb_dgedu.Padding = new System.Windows.Forms.Padding(5);
            this.tb_dgedu.ReadOnly = false;
            this.tb_dgedu.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_dgedu.Size = new System.Drawing.Size(158, 28);
            // 
            // 
            // 
            this.tb_dgedu.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_dgedu.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_dgedu.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_dgedu.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_dgedu.SkinTxt.Name = "BaseText";
            this.tb_dgedu.SkinTxt.Size = new System.Drawing.Size(148, 18);
            this.tb_dgedu.SkinTxt.TabIndex = 0;
            this.tb_dgedu.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_dgedu.SkinTxt.WaterText = "";
            this.tb_dgedu.TabIndex = 25;
            this.tb_dgedu.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_dgedu.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_dgedu.WaterText = "";
            this.tb_dgedu.WordWrap = true;
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel5.Location = new System.Drawing.Point(19, 220);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(73, 20);
            this.skinLabel5.TabIndex = 8;
            this.skinLabel5.Text = "出生日期:";
            // 
            // tb_qq
            // 
            this.tb_qq.BackColor = System.Drawing.Color.Transparent;
            this.tb_qq.DownBack = null;
            this.tb_qq.Icon = null;
            this.tb_qq.IconIsButton = false;
            this.tb_qq.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_qq.IsPasswordChat = '\0';
            this.tb_qq.IsSystemPasswordChar = false;
            this.tb_qq.Lines = new string[0];
            this.tb_qq.Location = new System.Drawing.Point(96, 305);
            this.tb_qq.Margin = new System.Windows.Forms.Padding(0);
            this.tb_qq.MaxLength = 32767;
            this.tb_qq.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_qq.MouseBack = null;
            this.tb_qq.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_qq.Multiline = false;
            this.tb_qq.Name = "tb_qq";
            this.tb_qq.NormlBack = null;
            this.tb_qq.Padding = new System.Windows.Forms.Padding(5);
            this.tb_qq.ReadOnly = false;
            this.tb_qq.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_qq.Size = new System.Drawing.Size(163, 28);
            // 
            // 
            // 
            this.tb_qq.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_qq.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_qq.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_qq.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_qq.SkinTxt.Name = "BaseText";
            this.tb_qq.SkinTxt.Size = new System.Drawing.Size(153, 18);
            this.tb_qq.SkinTxt.TabIndex = 0;
            this.tb_qq.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_qq.SkinTxt.WaterText = "";
            this.tb_qq.TabIndex = 9;
            this.tb_qq.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_qq.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_qq.WaterText = "";
            this.tb_qq.WordWrap = true;
            // 
            // skinLabel12
            // 
            this.skinLabel12.AutoSize = true;
            this.skinLabel12.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel12.BorderColor = System.Drawing.Color.White;
            this.skinLabel12.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel12.Location = new System.Drawing.Point(129, 181);
            this.skinLabel12.Name = "skinLabel12";
            this.skinLabel12.Size = new System.Drawing.Size(58, 20);
            this.skinLabel12.TabIndex = 23;
            this.skinLabel12.Text = "年龄段:";
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel6.Location = new System.Drawing.Point(19, 258);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(58, 20);
            this.skinLabel6.TabIndex = 10;
            this.skinLabel6.Text = "手机号:";
            // 
            // tb_phone
            // 
            this.tb_phone.BackColor = System.Drawing.Color.Transparent;
            this.tb_phone.DownBack = null;
            this.tb_phone.Icon = null;
            this.tb_phone.IconIsButton = false;
            this.tb_phone.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_phone.IsPasswordChat = '\0';
            this.tb_phone.IsSystemPasswordChar = false;
            this.tb_phone.Lines = new string[0];
            this.tb_phone.Location = new System.Drawing.Point(95, 253);
            this.tb_phone.Margin = new System.Windows.Forms.Padding(0);
            this.tb_phone.MaxLength = 32767;
            this.tb_phone.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_phone.MouseBack = null;
            this.tb_phone.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_phone.Multiline = false;
            this.tb_phone.Name = "tb_phone";
            this.tb_phone.NormlBack = null;
            this.tb_phone.Padding = new System.Windows.Forms.Padding(5);
            this.tb_phone.ReadOnly = false;
            this.tb_phone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_phone.Size = new System.Drawing.Size(162, 28);
            // 
            // 
            // 
            this.tb_phone.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_phone.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_phone.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_phone.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_phone.SkinTxt.Name = "BaseText";
            this.tb_phone.SkinTxt.Size = new System.Drawing.Size(152, 18);
            this.tb_phone.SkinTxt.TabIndex = 0;
            this.tb_phone.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_phone.SkinTxt.WaterText = "";
            this.tb_phone.TabIndex = 11;
            this.tb_phone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_phone.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_phone.WaterText = "";
            this.tb_phone.WordWrap = true;
            // 
            // skinLabel7
            // 
            this.skinLabel7.AutoSize = true;
            this.skinLabel7.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel7.BorderColor = System.Drawing.Color.White;
            this.skinLabel7.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel7.Location = new System.Drawing.Point(25, 310);
            this.skinLabel7.Name = "skinLabel7";
            this.skinLabel7.Size = new System.Drawing.Size(37, 20);
            this.skinLabel7.TabIndex = 12;
            this.skinLabel7.Text = "QQ:";
            // 
            // skinLabel8
            // 
            this.skinLabel8.AutoSize = true;
            this.skinLabel8.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel8.BorderColor = System.Drawing.Color.White;
            this.skinLabel8.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel8.Location = new System.Drawing.Point(25, 359);
            this.skinLabel8.Name = "skinLabel8";
            this.skinLabel8.Size = new System.Drawing.Size(43, 20);
            this.skinLabel8.TabIndex = 14;
            this.skinLabel8.Text = "职业:";
            // 
            // tb_incoleve
            // 
            this.tb_incoleve.BackColor = System.Drawing.Color.Transparent;
            this.tb_incoleve.DownBack = null;
            this.tb_incoleve.Icon = null;
            this.tb_incoleve.IconIsButton = false;
            this.tb_incoleve.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_incoleve.IsPasswordChat = '\0';
            this.tb_incoleve.IsSystemPasswordChar = false;
            this.tb_incoleve.Lines = new string[0];
            this.tb_incoleve.Location = new System.Drawing.Point(99, 439);
            this.tb_incoleve.Margin = new System.Windows.Forms.Padding(0);
            this.tb_incoleve.MaxLength = 32767;
            this.tb_incoleve.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_incoleve.MouseBack = null;
            this.tb_incoleve.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_incoleve.Multiline = false;
            this.tb_incoleve.Name = "tb_incoleve";
            this.tb_incoleve.NormlBack = null;
            this.tb_incoleve.Padding = new System.Windows.Forms.Padding(5);
            this.tb_incoleve.ReadOnly = false;
            this.tb_incoleve.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_incoleve.Size = new System.Drawing.Size(158, 28);
            // 
            // 
            // 
            this.tb_incoleve.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_incoleve.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_incoleve.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_incoleve.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_incoleve.SkinTxt.Name = "BaseText";
            this.tb_incoleve.SkinTxt.Size = new System.Drawing.Size(148, 18);
            this.tb_incoleve.SkinTxt.TabIndex = 0;
            this.tb_incoleve.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_incoleve.SkinTxt.WaterText = "";
            this.tb_incoleve.TabIndex = 19;
            this.tb_incoleve.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_incoleve.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_incoleve.WaterText = "";
            this.tb_incoleve.WordWrap = true;
            // 
            // skinLabel9
            // 
            this.skinLabel9.AutoSize = true;
            this.skinLabel9.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel9.BorderColor = System.Drawing.Color.White;
            this.skinLabel9.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel9.Location = new System.Drawing.Point(20, 401);
            this.skinLabel9.Name = "skinLabel9";
            this.skinLabel9.Size = new System.Drawing.Size(73, 20);
            this.skinLabel9.TabIndex = 16;
            this.skinLabel9.Text = "文化程度:";
            // 
            // skinLabel10
            // 
            this.skinLabel10.AutoSize = true;
            this.skinLabel10.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel10.BorderColor = System.Drawing.Color.White;
            this.skinLabel10.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel10.Location = new System.Drawing.Point(20, 448);
            this.skinLabel10.Name = "skinLabel10";
            this.skinLabel10.Size = new System.Drawing.Size(73, 20);
            this.skinLabel10.TabIndex = 18;
            this.skinLabel10.Text = "收入水平:";
            // 
            // tb_occupation
            // 
            this.tb_occupation.BackColor = System.Drawing.Color.Transparent;
            this.tb_occupation.DownBack = null;
            this.tb_occupation.Icon = null;
            this.tb_occupation.IconIsButton = false;
            this.tb_occupation.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_occupation.IsPasswordChat = '\0';
            this.tb_occupation.IsSystemPasswordChar = false;
            this.tb_occupation.Lines = new string[0];
            this.tb_occupation.Location = new System.Drawing.Point(97, 354);
            this.tb_occupation.Margin = new System.Windows.Forms.Padding(0);
            this.tb_occupation.MaxLength = 32767;
            this.tb_occupation.MinimumSize = new System.Drawing.Size(28, 28);
            this.tb_occupation.MouseBack = null;
            this.tb_occupation.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tb_occupation.Multiline = false;
            this.tb_occupation.Name = "tb_occupation";
            this.tb_occupation.NormlBack = null;
            this.tb_occupation.Padding = new System.Windows.Forms.Padding(5);
            this.tb_occupation.ReadOnly = false;
            this.tb_occupation.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_occupation.Size = new System.Drawing.Size(162, 28);
            // 
            // 
            // 
            this.tb_occupation.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_occupation.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_occupation.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tb_occupation.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tb_occupation.SkinTxt.Name = "BaseText";
            this.tb_occupation.SkinTxt.Size = new System.Drawing.Size(152, 18);
            this.tb_occupation.SkinTxt.TabIndex = 0;
            this.tb_occupation.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_occupation.SkinTxt.WaterText = "";
            this.tb_occupation.TabIndex = 17;
            this.tb_occupation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tb_occupation.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tb_occupation.WaterText = "";
            this.tb_occupation.WordWrap = true;
            // 
            // pa_detail_info
            // 
            this.pa_detail_info.BackColor = System.Drawing.Color.White;
            this.pa_detail_info.BorderColor = System.Drawing.Color.DarkGray;
            this.pa_detail_info.Controls.Add(this.panel5);
            this.pa_detail_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pa_detail_info.Location = new System.Drawing.Point(0, 36);
            this.pa_detail_info.Name = "pa_detail_info";
            this.pa_detail_info.Size = new System.Drawing.Size(290, 561);
            this.pa_detail_info.TabIndex = 2;
            this.pa_detail_info.TabItemImage = null;
            this.pa_detail_info.Text = "详细信息";
            // 
            // panel5
            // 
            this.panel5.AutoScroll = true;
            this.panel5.AutoSize = true;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.pic_sex);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.picImage);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.lblSignature);
            this.panel5.Controls.Add(this.lblNick);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.lblArea);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.lb_remarknam);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(290, 561);
            this.panel5.TabIndex = 11;
            // 
            // pic_sex
            // 
            this.pic_sex.Location = new System.Drawing.Point(111, 258);
            this.pic_sex.Name = "pic_sex";
            this.pic_sex.Size = new System.Drawing.Size(22, 19);
            this.pic_sex.TabIndex = 11;
            this.pic_sex.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 339);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "备注：";
            // 
            // picImage
            // 
            this.picImage.Location = new System.Drawing.Point(59, 57);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(166, 165);
            this.picImage.TabIndex = 0;
            this.picImage.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 256);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "性别：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 290);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "昵称：";
            // 
            // lblSignature
            // 
            this.lblSignature.AutoSize = true;
            this.lblSignature.Location = new System.Drawing.Point(106, 382);
            this.lblSignature.Name = "lblSignature";
            this.lblSignature.Size = new System.Drawing.Size(107, 20);
            this.lblSignature.TabIndex = 3;
            this.lblSignature.Text = "一切皆有可能！";
            // 
            // lblNick
            // 
            this.lblNick.AutoSize = true;
            this.lblNick.Location = new System.Drawing.Point(104, 290);
            this.lblNick.Name = "lblNick";
            this.lblNick.Size = new System.Drawing.Size(121, 20);
            this.lblNick.TabIndex = 2;
            this.lblNick.Text = "总有一天会遇见你";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 432);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "地区：";
            // 
            // lblArea
            // 
            this.lblArea.AutoSize = true;
            this.lblArea.Location = new System.Drawing.Point(106, 432);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(93, 20);
            this.lblArea.TabIndex = 4;
            this.lblArea.Text = "天津市南开区";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 382);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "签名：";
            // 
            // lb_remarknam
            // 
            this.lb_remarknam.AutoSize = true;
            this.lb_remarknam.Location = new System.Drawing.Point(107, 339);
            this.lb_remarknam.Name = "lb_remarknam";
            this.lb_remarknam.Size = new System.Drawing.Size(37, 20);
            this.lb_remarknam.TabIndex = 6;
            this.lb_remarknam.Text = "大亮";
            // 
            // Costom_analysls
            // 
            this.Costom_analysls.BackColor = System.Drawing.Color.Gray;
            this.Costom_analysls.Controls.Add(this.tbc_right_analysis);
            this.Costom_analysls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Costom_analysls.Location = new System.Drawing.Point(0, 36);
            this.Costom_analysls.Name = "Costom_analysls";
            this.Costom_analysls.Size = new System.Drawing.Size(290, 561);
            this.Costom_analysls.TabIndex = 1;
            this.Costom_analysls.TabItemImage = null;
            this.Costom_analysls.Text = "客户分析";
            // 
            // tbc_right_analysis
            // 
            this.tbc_right_analysis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbc_right_analysis.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.tbc_right_analysis.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.tbc_right_analysis.Controls.Add(this.skinTabPage1);
            this.tbc_right_analysis.Controls.Add(this.skinTabPage2);
            this.tbc_right_analysis.Controls.Add(this.skinTabPage3);
            this.tbc_right_analysis.Controls.Add(this.skinTabPage4);
            this.tbc_right_analysis.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tbc_right_analysis.HeadBack = null;
            this.tbc_right_analysis.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.tbc_right_analysis.ItemSize = new System.Drawing.Size(69, 36);
            this.tbc_right_analysis.Location = new System.Drawing.Point(1, 1);
            this.tbc_right_analysis.Name = "tbc_right_analysis";
            this.tbc_right_analysis.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("tbc_right_analysis.PageArrowDown")));
            this.tbc_right_analysis.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("tbc_right_analysis.PageArrowHover")));
            this.tbc_right_analysis.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("tbc_right_analysis.PageCloseHover")));
            this.tbc_right_analysis.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("tbc_right_analysis.PageCloseNormal")));
            this.tbc_right_analysis.PageDown = ((System.Drawing.Image)(resources.GetObject("tbc_right_analysis.PageDown")));
            this.tbc_right_analysis.PageDownTxtColor = System.Drawing.Color.White;
            this.tbc_right_analysis.PageHover = ((System.Drawing.Image)(resources.GetObject("tbc_right_analysis.PageHover")));
            this.tbc_right_analysis.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.tbc_right_analysis.PageNorml = null;
            this.tbc_right_analysis.SelectedIndex = 0;
            this.tbc_right_analysis.Size = new System.Drawing.Size(288, 563);
            this.tbc_right_analysis.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbc_right_analysis.TabIndex = 0;
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.Controls.Add(this.panel2);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(288, 527);
            this.skinTabPage1.TabIndex = 0;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "朋友圈分析";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.btn_time_lin_save);
            this.panel2.Controls.Add(this.skinLabel25);
            this.panel2.Controls.Add(this.tbx_tim_li_cost1);
            this.panel2.Controls.Add(this.tbx_tim_li_cost3);
            this.panel2.Controls.Add(this.skinLabel24);
            this.panel2.Controls.Add(this.tbx_tim_li_cost2);
            this.panel2.Controls.Add(this.skinLabel23);
            this.panel2.Controls.Add(this.skinLabel22);
            this.panel2.Controls.Add(this.tbx_tim_li_persanaly);
            this.panel2.Controls.Add(this.skinLabel21);
            this.panel2.Controls.Add(this.tbx_tim_li_acti_tim);
            this.panel2.Controls.Add(this.skinLabel13);
            this.panel2.Controls.Add(this.tbx_tim_li_like);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(288, 527);
            this.panel2.TabIndex = 1;
            // 
            // btn_time_lin_save
            // 
            this.btn_time_lin_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_time_lin_save.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_time_lin_save.BackgroundImage")));
            this.btn_time_lin_save.BaseColor = System.Drawing.Color.Transparent;
            this.btn_time_lin_save.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_time_lin_save.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_time_lin_save.DownBack = null;
            this.btn_time_lin_save.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_time_lin_save.IsDrawGlass = false;
            this.btn_time_lin_save.Location = new System.Drawing.Point(179, 835);
            this.btn_time_lin_save.MouseBack = null;
            this.btn_time_lin_save.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_time_lin_save.Name = "btn_time_lin_save";
            this.btn_time_lin_save.NormlBack = null;
            this.btn_time_lin_save.Size = new System.Drawing.Size(75, 29);
            this.btn_time_lin_save.TabIndex = 34;
            this.btn_time_lin_save.Text = "保存";
            this.btn_time_lin_save.UseVisualStyleBackColor = false;
            this.btn_time_lin_save.Click += new System.EventHandler(this.btn_time_lin_save_Click);
            // 
            // skinLabel25
            // 
            this.skinLabel25.AutoSize = true;
            this.skinLabel25.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel25.BorderColor = System.Drawing.Color.White;
            this.skinLabel25.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel25.Location = new System.Drawing.Point(13, 667);
            this.skinLabel25.Name = "skinLabel25";
            this.skinLabel25.Size = new System.Drawing.Size(67, 20);
            this.skinLabel25.TabIndex = 15;
            this.skinLabel25.Text = "自定义3:";
            // 
            // tbx_tim_li_cost1
            // 
            this.tbx_tim_li_cost1.BackColor = System.Drawing.Color.Transparent;
            this.tbx_tim_li_cost1.DownBack = null;
            this.tbx_tim_li_cost1.Icon = null;
            this.tbx_tim_li_cost1.IconIsButton = false;
            this.tbx_tim_li_cost1.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_cost1.IsPasswordChat = '\0';
            this.tbx_tim_li_cost1.IsSystemPasswordChar = false;
            this.tbx_tim_li_cost1.Lines = new string[0];
            this.tbx_tim_li_cost1.Location = new System.Drawing.Point(13, 405);
            this.tbx_tim_li_cost1.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_tim_li_cost1.MaxLength = 32767;
            this.tbx_tim_li_cost1.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_tim_li_cost1.MouseBack = null;
            this.tbx_tim_li_cost1.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_cost1.Multiline = true;
            this.tbx_tim_li_cost1.Name = "tbx_tim_li_cost1";
            this.tbx_tim_li_cost1.NormlBack = null;
            this.tbx_tim_li_cost1.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_tim_li_cost1.ReadOnly = false;
            this.tbx_tim_li_cost1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_tim_li_cost1.Size = new System.Drawing.Size(248, 75);
            // 
            // 
            // 
            this.tbx_tim_li_cost1.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_tim_li_cost1.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_tim_li_cost1.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_tim_li_cost1.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_tim_li_cost1.SkinTxt.Multiline = true;
            this.tbx_tim_li_cost1.SkinTxt.Name = "BaseText";
            this.tbx_tim_li_cost1.SkinTxt.Size = new System.Drawing.Size(238, 65);
            this.tbx_tim_li_cost1.SkinTxt.TabIndex = 0;
            this.tbx_tim_li_cost1.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_cost1.SkinTxt.WaterText = "";
            this.tbx_tim_li_cost1.TabIndex = 12;
            this.tbx_tim_li_cost1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_tim_li_cost1.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_cost1.WaterText = "";
            this.tbx_tim_li_cost1.WordWrap = true;
            // 
            // tbx_tim_li_cost3
            // 
            this.tbx_tim_li_cost3.BackColor = System.Drawing.Color.Transparent;
            this.tbx_tim_li_cost3.DownBack = null;
            this.tbx_tim_li_cost3.Icon = null;
            this.tbx_tim_li_cost3.IconIsButton = false;
            this.tbx_tim_li_cost3.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_cost3.IsPasswordChat = '\0';
            this.tbx_tim_li_cost3.IsSystemPasswordChar = false;
            this.tbx_tim_li_cost3.Lines = new string[0];
            this.tbx_tim_li_cost3.Location = new System.Drawing.Point(13, 709);
            this.tbx_tim_li_cost3.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_tim_li_cost3.MaxLength = 32767;
            this.tbx_tim_li_cost3.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_tim_li_cost3.MouseBack = null;
            this.tbx_tim_li_cost3.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_cost3.Multiline = true;
            this.tbx_tim_li_cost3.Name = "tbx_tim_li_cost3";
            this.tbx_tim_li_cost3.NormlBack = null;
            this.tbx_tim_li_cost3.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_tim_li_cost3.ReadOnly = false;
            this.tbx_tim_li_cost3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_tim_li_cost3.Size = new System.Drawing.Size(241, 104);
            // 
            // 
            // 
            this.tbx_tim_li_cost3.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_tim_li_cost3.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_tim_li_cost3.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_tim_li_cost3.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_tim_li_cost3.SkinTxt.Multiline = true;
            this.tbx_tim_li_cost3.SkinTxt.Name = "BaseText";
            this.tbx_tim_li_cost3.SkinTxt.Size = new System.Drawing.Size(231, 94);
            this.tbx_tim_li_cost3.SkinTxt.TabIndex = 0;
            this.tbx_tim_li_cost3.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_cost3.SkinTxt.WaterText = "";
            this.tbx_tim_li_cost3.TabIndex = 16;
            this.tbx_tim_li_cost3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_tim_li_cost3.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_cost3.WaterText = "";
            this.tbx_tim_li_cost3.WordWrap = true;
            // 
            // skinLabel24
            // 
            this.skinLabel24.AutoSize = true;
            this.skinLabel24.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel24.BorderColor = System.Drawing.Color.White;
            this.skinLabel24.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel24.Location = new System.Drawing.Point(13, 505);
            this.skinLabel24.Name = "skinLabel24";
            this.skinLabel24.Size = new System.Drawing.Size(67, 20);
            this.skinLabel24.TabIndex = 13;
            this.skinLabel24.Text = "自定义2:";
            // 
            // tbx_tim_li_cost2
            // 
            this.tbx_tim_li_cost2.BackColor = System.Drawing.Color.Transparent;
            this.tbx_tim_li_cost2.DownBack = null;
            this.tbx_tim_li_cost2.Icon = null;
            this.tbx_tim_li_cost2.IconIsButton = false;
            this.tbx_tim_li_cost2.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_cost2.IsPasswordChat = '\0';
            this.tbx_tim_li_cost2.IsSystemPasswordChar = false;
            this.tbx_tim_li_cost2.Lines = new string[0];
            this.tbx_tim_li_cost2.Location = new System.Drawing.Point(13, 546);
            this.tbx_tim_li_cost2.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_tim_li_cost2.MaxLength = 32767;
            this.tbx_tim_li_cost2.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_tim_li_cost2.MouseBack = null;
            this.tbx_tim_li_cost2.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_cost2.Multiline = true;
            this.tbx_tim_li_cost2.Name = "tbx_tim_li_cost2";
            this.tbx_tim_li_cost2.NormlBack = null;
            this.tbx_tim_li_cost2.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_tim_li_cost2.ReadOnly = false;
            this.tbx_tim_li_cost2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_tim_li_cost2.Size = new System.Drawing.Size(241, 89);
            // 
            // 
            // 
            this.tbx_tim_li_cost2.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_tim_li_cost2.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_tim_li_cost2.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_tim_li_cost2.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_tim_li_cost2.SkinTxt.Multiline = true;
            this.tbx_tim_li_cost2.SkinTxt.Name = "BaseText";
            this.tbx_tim_li_cost2.SkinTxt.Size = new System.Drawing.Size(231, 79);
            this.tbx_tim_li_cost2.SkinTxt.TabIndex = 0;
            this.tbx_tim_li_cost2.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_cost2.SkinTxt.WaterText = "";
            this.tbx_tim_li_cost2.TabIndex = 14;
            this.tbx_tim_li_cost2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_tim_li_cost2.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_cost2.WaterText = "";
            this.tbx_tim_li_cost2.WordWrap = true;
            // 
            // skinLabel23
            // 
            this.skinLabel23.AutoSize = true;
            this.skinLabel23.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel23.BorderColor = System.Drawing.Color.White;
            this.skinLabel23.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel23.Location = new System.Drawing.Point(13, 377);
            this.skinLabel23.Name = "skinLabel23";
            this.skinLabel23.Size = new System.Drawing.Size(67, 20);
            this.skinLabel23.TabIndex = 11;
            this.skinLabel23.Text = "自定义1:";
            // 
            // skinLabel22
            // 
            this.skinLabel22.AutoSize = true;
            this.skinLabel22.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel22.BorderColor = System.Drawing.Color.White;
            this.skinLabel22.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel22.Location = new System.Drawing.Point(13, 235);
            this.skinLabel22.Name = "skinLabel22";
            this.skinLabel22.Size = new System.Drawing.Size(103, 20);
            this.skinLabel22.TabIndex = 9;
            this.skinLabel22.Text = "个人画像分析:";
            // 
            // tbx_tim_li_persanaly
            // 
            this.tbx_tim_li_persanaly.BackColor = System.Drawing.Color.Transparent;
            this.tbx_tim_li_persanaly.DownBack = null;
            this.tbx_tim_li_persanaly.Icon = null;
            this.tbx_tim_li_persanaly.IconIsButton = false;
            this.tbx_tim_li_persanaly.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_persanaly.IsPasswordChat = '\0';
            this.tbx_tim_li_persanaly.IsSystemPasswordChar = false;
            this.tbx_tim_li_persanaly.Lines = new string[0];
            this.tbx_tim_li_persanaly.Location = new System.Drawing.Point(13, 271);
            this.tbx_tim_li_persanaly.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_tim_li_persanaly.MaxLength = 32767;
            this.tbx_tim_li_persanaly.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_tim_li_persanaly.MouseBack = null;
            this.tbx_tim_li_persanaly.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_persanaly.Multiline = true;
            this.tbx_tim_li_persanaly.Name = "tbx_tim_li_persanaly";
            this.tbx_tim_li_persanaly.NormlBack = null;
            this.tbx_tim_li_persanaly.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_tim_li_persanaly.ReadOnly = false;
            this.tbx_tim_li_persanaly.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_tim_li_persanaly.Size = new System.Drawing.Size(248, 83);
            // 
            // 
            // 
            this.tbx_tim_li_persanaly.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_tim_li_persanaly.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_tim_li_persanaly.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_tim_li_persanaly.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_tim_li_persanaly.SkinTxt.Multiline = true;
            this.tbx_tim_li_persanaly.SkinTxt.Name = "BaseText";
            this.tbx_tim_li_persanaly.SkinTxt.Size = new System.Drawing.Size(238, 73);
            this.tbx_tim_li_persanaly.SkinTxt.TabIndex = 0;
            this.tbx_tim_li_persanaly.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_persanaly.SkinTxt.WaterText = "";
            this.tbx_tim_li_persanaly.TabIndex = 10;
            this.tbx_tim_li_persanaly.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_tim_li_persanaly.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_persanaly.WaterText = "";
            this.tbx_tim_li_persanaly.WordWrap = true;
            // 
            // skinLabel21
            // 
            this.skinLabel21.AutoSize = true;
            this.skinLabel21.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel21.BorderColor = System.Drawing.Color.White;
            this.skinLabel21.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel21.Location = new System.Drawing.Point(13, 127);
            this.skinLabel21.Name = "skinLabel21";
            this.skinLabel21.Size = new System.Drawing.Size(118, 20);
            this.skinLabel21.TabIndex = 7;
            this.skinLabel21.Text = "朋友圈活跃时间:";
            // 
            // tbx_tim_li_acti_tim
            // 
            this.tbx_tim_li_acti_tim.BackColor = System.Drawing.Color.Transparent;
            this.tbx_tim_li_acti_tim.DownBack = null;
            this.tbx_tim_li_acti_tim.Icon = null;
            this.tbx_tim_li_acti_tim.IconIsButton = false;
            this.tbx_tim_li_acti_tim.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_acti_tim.IsPasswordChat = '\0';
            this.tbx_tim_li_acti_tim.IsSystemPasswordChar = false;
            this.tbx_tim_li_acti_tim.Lines = new string[0];
            this.tbx_tim_li_acti_tim.Location = new System.Drawing.Point(13, 156);
            this.tbx_tim_li_acti_tim.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_tim_li_acti_tim.MaxLength = 32767;
            this.tbx_tim_li_acti_tim.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_tim_li_acti_tim.MouseBack = null;
            this.tbx_tim_li_acti_tim.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_acti_tim.Multiline = true;
            this.tbx_tim_li_acti_tim.Name = "tbx_tim_li_acti_tim";
            this.tbx_tim_li_acti_tim.NormlBack = null;
            this.tbx_tim_li_acti_tim.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_tim_li_acti_tim.ReadOnly = false;
            this.tbx_tim_li_acti_tim.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_tim_li_acti_tim.Size = new System.Drawing.Size(249, 66);
            // 
            // 
            // 
            this.tbx_tim_li_acti_tim.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_tim_li_acti_tim.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_tim_li_acti_tim.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_tim_li_acti_tim.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_tim_li_acti_tim.SkinTxt.Multiline = true;
            this.tbx_tim_li_acti_tim.SkinTxt.Name = "BaseText";
            this.tbx_tim_li_acti_tim.SkinTxt.Size = new System.Drawing.Size(239, 56);
            this.tbx_tim_li_acti_tim.SkinTxt.TabIndex = 0;
            this.tbx_tim_li_acti_tim.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_acti_tim.SkinTxt.WaterText = "";
            this.tbx_tim_li_acti_tim.TabIndex = 8;
            this.tbx_tim_li_acti_tim.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_tim_li_acti_tim.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_acti_tim.WaterText = "";
            this.tbx_tim_li_acti_tim.WordWrap = true;
            // 
            // skinLabel13
            // 
            this.skinLabel13.AutoSize = true;
            this.skinLabel13.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel13.BorderColor = System.Drawing.Color.White;
            this.skinLabel13.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel13.Location = new System.Drawing.Point(13, 19);
            this.skinLabel13.Name = "skinLabel13";
            this.skinLabel13.Size = new System.Drawing.Size(88, 20);
            this.skinLabel13.TabIndex = 5;
            this.skinLabel13.Text = "朋友圈喜好:";
            // 
            // tbx_tim_li_like
            // 
            this.tbx_tim_li_like.BackColor = System.Drawing.Color.Transparent;
            this.tbx_tim_li_like.DownBack = null;
            this.tbx_tim_li_like.Icon = null;
            this.tbx_tim_li_like.IconIsButton = false;
            this.tbx_tim_li_like.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_like.IsPasswordChat = '\0';
            this.tbx_tim_li_like.IsSystemPasswordChar = false;
            this.tbx_tim_li_like.Lines = new string[0];
            this.tbx_tim_li_like.Location = new System.Drawing.Point(13, 44);
            this.tbx_tim_li_like.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_tim_li_like.MaxLength = 32767;
            this.tbx_tim_li_like.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_tim_li_like.MouseBack = null;
            this.tbx_tim_li_like.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_tim_li_like.Multiline = true;
            this.tbx_tim_li_like.Name = "tbx_tim_li_like";
            this.tbx_tim_li_like.NormlBack = null;
            this.tbx_tim_li_like.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_tim_li_like.ReadOnly = false;
            this.tbx_tim_li_like.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_tim_li_like.Size = new System.Drawing.Size(250, 70);
            // 
            // 
            // 
            this.tbx_tim_li_like.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_tim_li_like.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_tim_li_like.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_tim_li_like.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_tim_li_like.SkinTxt.Multiline = true;
            this.tbx_tim_li_like.SkinTxt.Name = "BaseText";
            this.tbx_tim_li_like.SkinTxt.Size = new System.Drawing.Size(240, 60);
            this.tbx_tim_li_like.SkinTxt.TabIndex = 0;
            this.tbx_tim_li_like.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_like.SkinTxt.WaterText = "";
            this.tbx_tim_li_like.TabIndex = 6;
            this.tbx_tim_li_like.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_tim_li_like.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_tim_li_like.WaterText = "";
            this.tbx_tim_li_like.WordWrap = true;
            // 
            // skinTabPage2
            // 
            this.skinTabPage2.BackColor = System.Drawing.Color.White;
            this.skinTabPage2.Controls.Add(this.panel1);
            this.skinTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage2.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage2.Name = "skinTabPage2";
            this.skinTabPage2.Size = new System.Drawing.Size(288, 527);
            this.skinTabPage2.TabIndex = 1;
            this.skinTabPage2.TabItemImage = null;
            this.skinTabPage2.Text = "需求分析";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.btn_requirement_save);
            this.panel1.Controls.Add(this.tbx_custom2);
            this.panel1.Controls.Add(this.skinLabel26);
            this.panel1.Controls.Add(this.tbx_custom3);
            this.panel1.Controls.Add(this.skinLabel27);
            this.panel1.Controls.Add(this.skinLabel28);
            this.panel1.Controls.Add(this.tbx_custom1);
            this.panel1.Controls.Add(this.skinLabel29);
            this.panel1.Controls.Add(this.tbx_potential_demand);
            this.panel1.Controls.Add(this.skinLabel30);
            this.panel1.Controls.Add(this.tbx_req_now);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 527);
            this.panel1.TabIndex = 0;
            // 
            // btn_requirement_save
            // 
            this.btn_requirement_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_requirement_save.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_requirement_save.BackgroundImage")));
            this.btn_requirement_save.BaseColor = System.Drawing.Color.Transparent;
            this.btn_requirement_save.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_requirement_save.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_requirement_save.DownBack = null;
            this.btn_requirement_save.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_requirement_save.IsDrawGlass = false;
            this.btn_requirement_save.Location = new System.Drawing.Point(182, 668);
            this.btn_requirement_save.MouseBack = null;
            this.btn_requirement_save.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_requirement_save.Name = "btn_requirement_save";
            this.btn_requirement_save.NormlBack = null;
            this.btn_requirement_save.Size = new System.Drawing.Size(75, 29);
            this.btn_requirement_save.TabIndex = 33;
            this.btn_requirement_save.Text = "保存";
            this.btn_requirement_save.UseVisualStyleBackColor = false;
            this.btn_requirement_save.Click += new System.EventHandler(this.btn_requirement_save_Click);
            // 
            // tbx_custom2
            // 
            this.tbx_custom2.BackColor = System.Drawing.Color.Transparent;
            this.tbx_custom2.DownBack = null;
            this.tbx_custom2.Icon = null;
            this.tbx_custom2.IconIsButton = false;
            this.tbx_custom2.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_custom2.IsPasswordChat = '\0';
            this.tbx_custom2.IsSystemPasswordChar = false;
            this.tbx_custom2.Lines = new string[0];
            this.tbx_custom2.Location = new System.Drawing.Point(9, 402);
            this.tbx_custom2.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_custom2.MaxLength = 32767;
            this.tbx_custom2.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_custom2.MouseBack = null;
            this.tbx_custom2.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_custom2.Multiline = true;
            this.tbx_custom2.Name = "tbx_custom2";
            this.tbx_custom2.NormlBack = null;
            this.tbx_custom2.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_custom2.ReadOnly = false;
            this.tbx_custom2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_custom2.Size = new System.Drawing.Size(248, 75);
            // 
            // 
            // 
            this.tbx_custom2.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_custom2.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_custom2.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_custom2.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_custom2.SkinTxt.Multiline = true;
            this.tbx_custom2.SkinTxt.Name = "BaseText";
            this.tbx_custom2.SkinTxt.Size = new System.Drawing.Size(238, 65);
            this.tbx_custom2.SkinTxt.TabIndex = 0;
            this.tbx_custom2.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_custom2.SkinTxt.WaterText = "";
            this.tbx_custom2.TabIndex = 22;
            this.tbx_custom2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_custom2.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_custom2.WaterText = "";
            this.tbx_custom2.WordWrap = true;
            // 
            // skinLabel26
            // 
            this.skinLabel26.AutoSize = true;
            this.skinLabel26.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel26.BorderColor = System.Drawing.Color.White;
            this.skinLabel26.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel26.Location = new System.Drawing.Point(9, 512);
            this.skinLabel26.Name = "skinLabel26";
            this.skinLabel26.Size = new System.Drawing.Size(82, 20);
            this.skinLabel26.TabIndex = 23;
            this.skinLabel26.Text = "自定义项3:";
            // 
            // tbx_custom3
            // 
            this.tbx_custom3.BackColor = System.Drawing.Color.Transparent;
            this.tbx_custom3.DownBack = null;
            this.tbx_custom3.Icon = null;
            this.tbx_custom3.IconIsButton = false;
            this.tbx_custom3.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_custom3.IsPasswordChat = '\0';
            this.tbx_custom3.IsSystemPasswordChar = false;
            this.tbx_custom3.Lines = new string[0];
            this.tbx_custom3.Location = new System.Drawing.Point(9, 553);
            this.tbx_custom3.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_custom3.MaxLength = 32767;
            this.tbx_custom3.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_custom3.MouseBack = null;
            this.tbx_custom3.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_custom3.Multiline = true;
            this.tbx_custom3.Name = "tbx_custom3";
            this.tbx_custom3.NormlBack = null;
            this.tbx_custom3.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_custom3.ReadOnly = false;
            this.tbx_custom3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_custom3.Size = new System.Drawing.Size(248, 89);
            // 
            // 
            // 
            this.tbx_custom3.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_custom3.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_custom3.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_custom3.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_custom3.SkinTxt.Multiline = true;
            this.tbx_custom3.SkinTxt.Name = "BaseText";
            this.tbx_custom3.SkinTxt.Size = new System.Drawing.Size(238, 79);
            this.tbx_custom3.SkinTxt.TabIndex = 0;
            this.tbx_custom3.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_custom3.SkinTxt.WaterText = "";
            this.tbx_custom3.TabIndex = 24;
            this.tbx_custom3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_custom3.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_custom3.WaterText = "";
            this.tbx_custom3.WordWrap = true;
            // 
            // skinLabel27
            // 
            this.skinLabel27.AutoSize = true;
            this.skinLabel27.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel27.BorderColor = System.Drawing.Color.White;
            this.skinLabel27.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel27.Location = new System.Drawing.Point(9, 374);
            this.skinLabel27.Name = "skinLabel27";
            this.skinLabel27.Size = new System.Drawing.Size(82, 20);
            this.skinLabel27.TabIndex = 21;
            this.skinLabel27.Text = "自定义项2:";
            // 
            // skinLabel28
            // 
            this.skinLabel28.AutoSize = true;
            this.skinLabel28.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel28.BorderColor = System.Drawing.Color.White;
            this.skinLabel28.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel28.Location = new System.Drawing.Point(9, 239);
            this.skinLabel28.Name = "skinLabel28";
            this.skinLabel28.Size = new System.Drawing.Size(82, 20);
            this.skinLabel28.TabIndex = 19;
            this.skinLabel28.Text = "自定义项1:";
            // 
            // tbx_custom1
            // 
            this.tbx_custom1.BackColor = System.Drawing.Color.Transparent;
            this.tbx_custom1.DownBack = null;
            this.tbx_custom1.Icon = null;
            this.tbx_custom1.IconIsButton = false;
            this.tbx_custom1.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_custom1.IsPasswordChat = '\0';
            this.tbx_custom1.IsSystemPasswordChar = false;
            this.tbx_custom1.Lines = new string[0];
            this.tbx_custom1.Location = new System.Drawing.Point(9, 272);
            this.tbx_custom1.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_custom1.MaxLength = 32767;
            this.tbx_custom1.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_custom1.MouseBack = null;
            this.tbx_custom1.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_custom1.Multiline = true;
            this.tbx_custom1.Name = "tbx_custom1";
            this.tbx_custom1.NormlBack = null;
            this.tbx_custom1.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_custom1.ReadOnly = false;
            this.tbx_custom1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_custom1.Size = new System.Drawing.Size(248, 83);
            // 
            // 
            // 
            this.tbx_custom1.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_custom1.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_custom1.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_custom1.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_custom1.SkinTxt.Multiline = true;
            this.tbx_custom1.SkinTxt.Name = "BaseText";
            this.tbx_custom1.SkinTxt.Size = new System.Drawing.Size(238, 73);
            this.tbx_custom1.SkinTxt.TabIndex = 0;
            this.tbx_custom1.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_custom1.SkinTxt.WaterText = "";
            this.tbx_custom1.TabIndex = 20;
            this.tbx_custom1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_custom1.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_custom1.WaterText = "";
            this.tbx_custom1.WordWrap = true;
            // 
            // skinLabel29
            // 
            this.skinLabel29.AutoSize = true;
            this.skinLabel29.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel29.BorderColor = System.Drawing.Color.White;
            this.skinLabel29.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel29.Location = new System.Drawing.Point(9, 131);
            this.skinLabel29.Name = "skinLabel29";
            this.skinLabel29.Size = new System.Drawing.Size(73, 20);
            this.skinLabel29.TabIndex = 17;
            this.skinLabel29.Text = "潜在需求:";
            // 
            // tbx_potential_demand
            // 
            this.tbx_potential_demand.BackColor = System.Drawing.Color.Transparent;
            this.tbx_potential_demand.DownBack = null;
            this.tbx_potential_demand.Icon = null;
            this.tbx_potential_demand.IconIsButton = false;
            this.tbx_potential_demand.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_potential_demand.IsPasswordChat = '\0';
            this.tbx_potential_demand.IsSystemPasswordChar = false;
            this.tbx_potential_demand.Lines = new string[0];
            this.tbx_potential_demand.Location = new System.Drawing.Point(9, 160);
            this.tbx_potential_demand.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_potential_demand.MaxLength = 32767;
            this.tbx_potential_demand.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_potential_demand.MouseBack = null;
            this.tbx_potential_demand.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_potential_demand.Multiline = true;
            this.tbx_potential_demand.Name = "tbx_potential_demand";
            this.tbx_potential_demand.NormlBack = null;
            this.tbx_potential_demand.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_potential_demand.ReadOnly = false;
            this.tbx_potential_demand.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_potential_demand.Size = new System.Drawing.Size(249, 66);
            // 
            // 
            // 
            this.tbx_potential_demand.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_potential_demand.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_potential_demand.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_potential_demand.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_potential_demand.SkinTxt.Multiline = true;
            this.tbx_potential_demand.SkinTxt.Name = "BaseText";
            this.tbx_potential_demand.SkinTxt.Size = new System.Drawing.Size(239, 56);
            this.tbx_potential_demand.SkinTxt.TabIndex = 0;
            this.tbx_potential_demand.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_potential_demand.SkinTxt.WaterText = "";
            this.tbx_potential_demand.TabIndex = 18;
            this.tbx_potential_demand.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_potential_demand.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_potential_demand.WaterText = "";
            this.tbx_potential_demand.WordWrap = true;
            // 
            // skinLabel30
            // 
            this.skinLabel30.AutoSize = true;
            this.skinLabel30.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel30.BorderColor = System.Drawing.Color.White;
            this.skinLabel30.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel30.Location = new System.Drawing.Point(9, 19);
            this.skinLabel30.Name = "skinLabel30";
            this.skinLabel30.Size = new System.Drawing.Size(88, 20);
            this.skinLabel30.TabIndex = 15;
            this.skinLabel30.Text = "现阶段需求:";
            // 
            // tbx_req_now
            // 
            this.tbx_req_now.BackColor = System.Drawing.Color.Transparent;
            this.tbx_req_now.DownBack = null;
            this.tbx_req_now.Icon = null;
            this.tbx_req_now.IconIsButton = false;
            this.tbx_req_now.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_req_now.IsPasswordChat = '\0';
            this.tbx_req_now.IsSystemPasswordChar = false;
            this.tbx_req_now.Lines = new string[0];
            this.tbx_req_now.Location = new System.Drawing.Point(9, 48);
            this.tbx_req_now.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_req_now.MaxLength = 32767;
            this.tbx_req_now.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_req_now.MouseBack = null;
            this.tbx_req_now.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_req_now.Multiline = true;
            this.tbx_req_now.Name = "tbx_req_now";
            this.tbx_req_now.NormlBack = null;
            this.tbx_req_now.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_req_now.ReadOnly = false;
            this.tbx_req_now.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_req_now.Size = new System.Drawing.Size(250, 70);
            // 
            // 
            // 
            this.tbx_req_now.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_req_now.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_req_now.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_req_now.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_req_now.SkinTxt.Multiline = true;
            this.tbx_req_now.SkinTxt.Name = "BaseText";
            this.tbx_req_now.SkinTxt.Size = new System.Drawing.Size(240, 60);
            this.tbx_req_now.SkinTxt.TabIndex = 0;
            this.tbx_req_now.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_req_now.SkinTxt.WaterText = "";
            this.tbx_req_now.TabIndex = 16;
            this.tbx_req_now.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_req_now.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_req_now.WaterText = "";
            this.tbx_req_now.WordWrap = true;
            // 
            // skinTabPage3
            // 
            this.skinTabPage3.BackColor = System.Drawing.Color.White;
            this.skinTabPage3.Controls.Add(this.panel3);
            this.skinTabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage3.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage3.Name = "skinTabPage3";
            this.skinTabPage3.Size = new System.Drawing.Size(288, 527);
            this.skinTabPage3.TabIndex = 2;
            this.skinTabPage3.TabItemImage = null;
            this.skinTabPage3.Text = "消费分析";
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.btn_consumption_save);
            this.panel3.Controls.Add(this.lbzh);
            this.panel3.Controls.Add(this.lb_b);
            this.panel3.Controls.Add(this.tbx_comprehensive_analy);
            this.panel3.Controls.Add(this.tbx_buy_cosmetic_now);
            this.panel3.Controls.Add(this.skinLabel39);
            this.panel3.Controls.Add(this.skinLabel40);
            this.panel3.Controls.Add(this.skinLabel41);
            this.panel3.Controls.Add(this.tbx_Ornaments_tendency);
            this.panel3.Controls.Add(this.tbx_buy_ornaments_now);
            this.panel3.Controls.Add(this.skinLabel35);
            this.panel3.Controls.Add(this.skinLabel36);
            this.panel3.Controls.Add(this.skinLabel37);
            this.panel3.Controls.Add(this.lb_cloth_tendency);
            this.panel3.Controls.Add(this.tbx_sk_care_brand_tenden);
            this.panel3.Controls.Add(this.tbx_buy_skinca_now);
            this.panel3.Controls.Add(this.tbx_cloth_tendency);
            this.panel3.Controls.Add(this.tbx_buy_clothes_now);
            this.panel3.Controls.Add(this.tgh);
            this.panel3.Controls.Add(this.skinLabel32);
            this.panel3.Controls.Add(this.skinLabel33);
            this.panel3.Controls.Add(this.tbx_standard_consump);
            this.panel3.Controls.Add(this.tbx_con_hab);
            this.panel3.Controls.Add(this.tbx_Cosmetic_tendency);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(288, 527);
            this.panel3.TabIndex = 1;
            // 
            // btn_consumption_save
            // 
            this.btn_consumption_save.BackColor = System.Drawing.Color.Transparent;
            this.btn_consumption_save.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_consumption_save.BackgroundImage")));
            this.btn_consumption_save.BaseColor = System.Drawing.Color.Transparent;
            this.btn_consumption_save.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_consumption_save.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_consumption_save.DownBack = null;
            this.btn_consumption_save.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_consumption_save.IsDrawGlass = false;
            this.btn_consumption_save.Location = new System.Drawing.Point(178, 1256);
            this.btn_consumption_save.MouseBack = null;
            this.btn_consumption_save.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_consumption_save.Name = "btn_consumption_save";
            this.btn_consumption_save.NormlBack = null;
            this.btn_consumption_save.Size = new System.Drawing.Size(75, 29);
            this.btn_consumption_save.TabIndex = 34;
            this.btn_consumption_save.Text = "保存";
            this.btn_consumption_save.UseVisualStyleBackColor = false;
            this.btn_consumption_save.Click += new System.EventHandler(this.btn_consumption_save_Click);
            // 
            // lbzh
            // 
            this.lbzh.AutoSize = true;
            this.lbzh.BackColor = System.Drawing.Color.Transparent;
            this.lbzh.BorderColor = System.Drawing.Color.White;
            this.lbzh.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.lbzh.Location = new System.Drawing.Point(19, 1094);
            this.lbzh.Name = "lbzh";
            this.lbzh.Size = new System.Drawing.Size(73, 20);
            this.lbzh.TabIndex = 31;
            this.lbzh.Text = "综合分析:";
            // 
            // lb_b
            // 
            this.lb_b.AutoSize = true;
            this.lb_b.BackColor = System.Drawing.Color.Transparent;
            this.lb_b.BorderColor = System.Drawing.Color.White;
            this.lb_b.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.lb_b.Location = new System.Drawing.Point(23, 308);
            this.lb_b.Name = "lb_b";
            this.lb_b.Size = new System.Drawing.Size(118, 20);
            this.lb_b.TabIndex = 11;
            this.lb_b.Text = "最近购买化妆品:";
            // 
            // tbx_comprehensive_analy
            // 
            this.tbx_comprehensive_analy.BackColor = System.Drawing.Color.Transparent;
            this.tbx_comprehensive_analy.DownBack = null;
            this.tbx_comprehensive_analy.Icon = null;
            this.tbx_comprehensive_analy.IconIsButton = false;
            this.tbx_comprehensive_analy.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_comprehensive_analy.IsPasswordChat = '\0';
            this.tbx_comprehensive_analy.IsSystemPasswordChar = false;
            this.tbx_comprehensive_analy.Lines = new string[0];
            this.tbx_comprehensive_analy.Location = new System.Drawing.Point(19, 1128);
            this.tbx_comprehensive_analy.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_comprehensive_analy.MaxLength = 32767;
            this.tbx_comprehensive_analy.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_comprehensive_analy.MouseBack = null;
            this.tbx_comprehensive_analy.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_comprehensive_analy.Multiline = true;
            this.tbx_comprehensive_analy.Name = "tbx_comprehensive_analy";
            this.tbx_comprehensive_analy.NormlBack = null;
            this.tbx_comprehensive_analy.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_comprehensive_analy.ReadOnly = false;
            this.tbx_comprehensive_analy.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_comprehensive_analy.Size = new System.Drawing.Size(234, 112);
            // 
            // 
            // 
            this.tbx_comprehensive_analy.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_comprehensive_analy.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_comprehensive_analy.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_comprehensive_analy.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_comprehensive_analy.SkinTxt.Multiline = true;
            this.tbx_comprehensive_analy.SkinTxt.Name = "BaseText";
            this.tbx_comprehensive_analy.SkinTxt.Size = new System.Drawing.Size(224, 102);
            this.tbx_comprehensive_analy.SkinTxt.TabIndex = 0;
            this.tbx_comprehensive_analy.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_comprehensive_analy.SkinTxt.WaterText = "";
            this.tbx_comprehensive_analy.TabIndex = 30;
            this.tbx_comprehensive_analy.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_comprehensive_analy.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_comprehensive_analy.WaterText = "";
            this.tbx_comprehensive_analy.WordWrap = true;
            // 
            // tbx_buy_cosmetic_now
            // 
            this.tbx_buy_cosmetic_now.BackColor = System.Drawing.Color.Transparent;
            this.tbx_buy_cosmetic_now.DownBack = null;
            this.tbx_buy_cosmetic_now.Icon = null;
            this.tbx_buy_cosmetic_now.IconIsButton = false;
            this.tbx_buy_cosmetic_now.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_cosmetic_now.IsPasswordChat = '\0';
            this.tbx_buy_cosmetic_now.IsSystemPasswordChar = false;
            this.tbx_buy_cosmetic_now.Lines = new string[0];
            this.tbx_buy_cosmetic_now.Location = new System.Drawing.Point(23, 338);
            this.tbx_buy_cosmetic_now.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_buy_cosmetic_now.MaxLength = 32767;
            this.tbx_buy_cosmetic_now.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_buy_cosmetic_now.MouseBack = null;
            this.tbx_buy_cosmetic_now.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_cosmetic_now.Multiline = true;
            this.tbx_buy_cosmetic_now.Name = "tbx_buy_cosmetic_now";
            this.tbx_buy_cosmetic_now.NormlBack = null;
            this.tbx_buy_cosmetic_now.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_buy_cosmetic_now.ReadOnly = false;
            this.tbx_buy_cosmetic_now.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_buy_cosmetic_now.Size = new System.Drawing.Size(233, 63);
            // 
            // 
            // 
            this.tbx_buy_cosmetic_now.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_buy_cosmetic_now.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_buy_cosmetic_now.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_buy_cosmetic_now.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_buy_cosmetic_now.SkinTxt.Multiline = true;
            this.tbx_buy_cosmetic_now.SkinTxt.Name = "BaseText";
            this.tbx_buy_cosmetic_now.SkinTxt.Size = new System.Drawing.Size(223, 53);
            this.tbx_buy_cosmetic_now.SkinTxt.TabIndex = 0;
            this.tbx_buy_cosmetic_now.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_cosmetic_now.SkinTxt.WaterText = "";
            this.tbx_buy_cosmetic_now.TabIndex = 15;
            this.tbx_buy_cosmetic_now.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_buy_cosmetic_now.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_cosmetic_now.WaterText = "";
            this.tbx_buy_cosmetic_now.WordWrap = true;
            // 
            // skinLabel39
            // 
            this.skinLabel39.AutoSize = true;
            this.skinLabel39.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel39.BorderColor = System.Drawing.Color.White;
            this.skinLabel39.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel39.Location = new System.Drawing.Point(19, 750);
            this.skinLabel39.Name = "skinLabel39";
            this.skinLabel39.Size = new System.Drawing.Size(103, 20);
            this.skinLabel39.TabIndex = 24;
            this.skinLabel39.Text = "最近购买衣服:";
            // 
            // skinLabel40
            // 
            this.skinLabel40.AutoSize = true;
            this.skinLabel40.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel40.BorderColor = System.Drawing.Color.White;
            this.skinLabel40.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel40.Location = new System.Drawing.Point(19, 882);
            this.skinLabel40.Name = "skinLabel40";
            this.skinLabel40.Size = new System.Drawing.Size(103, 20);
            this.skinLabel40.TabIndex = 25;
            this.skinLabel40.Text = "饰品品牌倾向:";
            // 
            // skinLabel41
            // 
            this.skinLabel41.AutoSize = true;
            this.skinLabel41.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel41.BorderColor = System.Drawing.Color.White;
            this.skinLabel41.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel41.Location = new System.Drawing.Point(19, 997);
            this.skinLabel41.Name = "skinLabel41";
            this.skinLabel41.Size = new System.Drawing.Size(103, 20);
            this.skinLabel41.TabIndex = 26;
            this.skinLabel41.Text = "最近购买饰品:";
            // 
            // tbx_Ornaments_tendency
            // 
            this.tbx_Ornaments_tendency.BackColor = System.Drawing.Color.Transparent;
            this.tbx_Ornaments_tendency.DownBack = null;
            this.tbx_Ornaments_tendency.Icon = null;
            this.tbx_Ornaments_tendency.IconIsButton = false;
            this.tbx_Ornaments_tendency.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_Ornaments_tendency.IsPasswordChat = '\0';
            this.tbx_Ornaments_tendency.IsSystemPasswordChar = false;
            this.tbx_Ornaments_tendency.Lines = new string[0];
            this.tbx_Ornaments_tendency.Location = new System.Drawing.Point(19, 920);
            this.tbx_Ornaments_tendency.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_Ornaments_tendency.MaxLength = 32767;
            this.tbx_Ornaments_tendency.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_Ornaments_tendency.MouseBack = null;
            this.tbx_Ornaments_tendency.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_Ornaments_tendency.Multiline = true;
            this.tbx_Ornaments_tendency.Name = "tbx_Ornaments_tendency";
            this.tbx_Ornaments_tendency.NormlBack = null;
            this.tbx_Ornaments_tendency.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_Ornaments_tendency.ReadOnly = false;
            this.tbx_Ornaments_tendency.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_Ornaments_tendency.Size = new System.Drawing.Size(234, 65);
            // 
            // 
            // 
            this.tbx_Ornaments_tendency.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_Ornaments_tendency.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_Ornaments_tendency.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_Ornaments_tendency.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_Ornaments_tendency.SkinTxt.Multiline = true;
            this.tbx_Ornaments_tendency.SkinTxt.Name = "BaseText";
            this.tbx_Ornaments_tendency.SkinTxt.Size = new System.Drawing.Size(224, 55);
            this.tbx_Ornaments_tendency.SkinTxt.TabIndex = 0;
            this.tbx_Ornaments_tendency.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_Ornaments_tendency.SkinTxt.WaterText = "";
            this.tbx_Ornaments_tendency.TabIndex = 28;
            this.tbx_Ornaments_tendency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_Ornaments_tendency.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_Ornaments_tendency.WaterText = "";
            this.tbx_Ornaments_tendency.WordWrap = true;
            // 
            // tbx_buy_ornaments_now
            // 
            this.tbx_buy_ornaments_now.BackColor = System.Drawing.Color.Transparent;
            this.tbx_buy_ornaments_now.DownBack = null;
            this.tbx_buy_ornaments_now.Icon = null;
            this.tbx_buy_ornaments_now.IconIsButton = false;
            this.tbx_buy_ornaments_now.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_ornaments_now.IsPasswordChat = '\0';
            this.tbx_buy_ornaments_now.IsSystemPasswordChar = false;
            this.tbx_buy_ornaments_now.Lines = new string[0];
            this.tbx_buy_ornaments_now.Location = new System.Drawing.Point(19, 1028);
            this.tbx_buy_ornaments_now.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_buy_ornaments_now.MaxLength = 32767;
            this.tbx_buy_ornaments_now.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_buy_ornaments_now.MouseBack = null;
            this.tbx_buy_ornaments_now.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_ornaments_now.Multiline = true;
            this.tbx_buy_ornaments_now.Name = "tbx_buy_ornaments_now";
            this.tbx_buy_ornaments_now.NormlBack = null;
            this.tbx_buy_ornaments_now.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_buy_ornaments_now.ReadOnly = false;
            this.tbx_buy_ornaments_now.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_buy_ornaments_now.Size = new System.Drawing.Size(233, 55);
            // 
            // 
            // 
            this.tbx_buy_ornaments_now.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_buy_ornaments_now.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_buy_ornaments_now.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_buy_ornaments_now.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_buy_ornaments_now.SkinTxt.Multiline = true;
            this.tbx_buy_ornaments_now.SkinTxt.Name = "BaseText";
            this.tbx_buy_ornaments_now.SkinTxt.Size = new System.Drawing.Size(223, 45);
            this.tbx_buy_ornaments_now.SkinTxt.TabIndex = 0;
            this.tbx_buy_ornaments_now.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_ornaments_now.SkinTxt.WaterText = "";
            this.tbx_buy_ornaments_now.TabIndex = 29;
            this.tbx_buy_ornaments_now.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_buy_ornaments_now.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_ornaments_now.WaterText = "";
            this.tbx_buy_ornaments_now.WordWrap = true;
            // 
            // skinLabel35
            // 
            this.skinLabel35.AutoSize = true;
            this.skinLabel35.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel35.BorderColor = System.Drawing.Color.White;
            this.skinLabel35.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel35.Location = new System.Drawing.Point(19, 418);
            this.skinLabel35.Name = "skinLabel35";
            this.skinLabel35.Size = new System.Drawing.Size(103, 20);
            this.skinLabel35.TabIndex = 16;
            this.skinLabel35.Text = "护肤品牌倾向:";
            // 
            // skinLabel36
            // 
            this.skinLabel36.AutoSize = true;
            this.skinLabel36.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel36.BorderColor = System.Drawing.Color.White;
            this.skinLabel36.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel36.Location = new System.Drawing.Point(19, 540);
            this.skinLabel36.Name = "skinLabel36";
            this.skinLabel36.Size = new System.Drawing.Size(118, 20);
            this.skinLabel36.TabIndex = 17;
            this.skinLabel36.Text = "最近购买护肤品:";
            // 
            // skinLabel37
            // 
            this.skinLabel37.AutoSize = true;
            this.skinLabel37.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel37.BorderColor = System.Drawing.Color.White;
            this.skinLabel37.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel37.Location = new System.Drawing.Point(16, 3816);
            this.skinLabel37.Name = "skinLabel37";
            this.skinLabel37.Size = new System.Drawing.Size(0, 20);
            this.skinLabel37.TabIndex = 18;
            // 
            // lb_cloth_tendency
            // 
            this.lb_cloth_tendency.AutoSize = true;
            this.lb_cloth_tendency.BackColor = System.Drawing.Color.Transparent;
            this.lb_cloth_tendency.BorderColor = System.Drawing.Color.White;
            this.lb_cloth_tendency.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.lb_cloth_tendency.Location = new System.Drawing.Point(19, 655);
            this.lb_cloth_tendency.Name = "lb_cloth_tendency";
            this.lb_cloth_tendency.Size = new System.Drawing.Size(103, 20);
            this.lb_cloth_tendency.TabIndex = 19;
            this.lb_cloth_tendency.Text = "穿衣品牌倾向:";
            // 
            // tbx_sk_care_brand_tenden
            // 
            this.tbx_sk_care_brand_tenden.BackColor = System.Drawing.Color.Transparent;
            this.tbx_sk_care_brand_tenden.DownBack = null;
            this.tbx_sk_care_brand_tenden.Icon = null;
            this.tbx_sk_care_brand_tenden.IconIsButton = false;
            this.tbx_sk_care_brand_tenden.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_sk_care_brand_tenden.IsPasswordChat = '\0';
            this.tbx_sk_care_brand_tenden.IsSystemPasswordChar = false;
            this.tbx_sk_care_brand_tenden.Lines = new string[0];
            this.tbx_sk_care_brand_tenden.Location = new System.Drawing.Point(19, 570);
            this.tbx_sk_care_brand_tenden.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_sk_care_brand_tenden.MaxLength = 32767;
            this.tbx_sk_care_brand_tenden.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_sk_care_brand_tenden.MouseBack = null;
            this.tbx_sk_care_brand_tenden.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_sk_care_brand_tenden.Multiline = true;
            this.tbx_sk_care_brand_tenden.Name = "tbx_sk_care_brand_tenden";
            this.tbx_sk_care_brand_tenden.NormlBack = null;
            this.tbx_sk_care_brand_tenden.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_sk_care_brand_tenden.ReadOnly = false;
            this.tbx_sk_care_brand_tenden.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_sk_care_brand_tenden.Size = new System.Drawing.Size(233, 80);
            // 
            // 
            // 
            this.tbx_sk_care_brand_tenden.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_sk_care_brand_tenden.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_sk_care_brand_tenden.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_sk_care_brand_tenden.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_sk_care_brand_tenden.SkinTxt.Multiline = true;
            this.tbx_sk_care_brand_tenden.SkinTxt.Name = "BaseText";
            this.tbx_sk_care_brand_tenden.SkinTxt.Size = new System.Drawing.Size(223, 70);
            this.tbx_sk_care_brand_tenden.SkinTxt.TabIndex = 0;
            this.tbx_sk_care_brand_tenden.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_sk_care_brand_tenden.SkinTxt.WaterText = "";
            this.tbx_sk_care_brand_tenden.TabIndex = 20;
            this.tbx_sk_care_brand_tenden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_sk_care_brand_tenden.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_sk_care_brand_tenden.WaterText = "";
            this.tbx_sk_care_brand_tenden.WordWrap = true;
            // 
            // tbx_buy_skinca_now
            // 
            this.tbx_buy_skinca_now.BackColor = System.Drawing.Color.Transparent;
            this.tbx_buy_skinca_now.DownBack = null;
            this.tbx_buy_skinca_now.Icon = null;
            this.tbx_buy_skinca_now.IconIsButton = false;
            this.tbx_buy_skinca_now.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_skinca_now.IsPasswordChat = '\0';
            this.tbx_buy_skinca_now.IsSystemPasswordChar = false;
            this.tbx_buy_skinca_now.Lines = new string[0];
            this.tbx_buy_skinca_now.Location = new System.Drawing.Point(19, 447);
            this.tbx_buy_skinca_now.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_buy_skinca_now.MaxLength = 32767;
            this.tbx_buy_skinca_now.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_buy_skinca_now.MouseBack = null;
            this.tbx_buy_skinca_now.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_skinca_now.Multiline = true;
            this.tbx_buy_skinca_now.Name = "tbx_buy_skinca_now";
            this.tbx_buy_skinca_now.NormlBack = null;
            this.tbx_buy_skinca_now.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_buy_skinca_now.ReadOnly = false;
            this.tbx_buy_skinca_now.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_buy_skinca_now.Size = new System.Drawing.Size(234, 81);
            // 
            // 
            // 
            this.tbx_buy_skinca_now.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_buy_skinca_now.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_buy_skinca_now.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_buy_skinca_now.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_buy_skinca_now.SkinTxt.Multiline = true;
            this.tbx_buy_skinca_now.SkinTxt.Name = "BaseText";
            this.tbx_buy_skinca_now.SkinTxt.Size = new System.Drawing.Size(224, 71);
            this.tbx_buy_skinca_now.SkinTxt.TabIndex = 0;
            this.tbx_buy_skinca_now.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_skinca_now.SkinTxt.WaterText = "";
            this.tbx_buy_skinca_now.TabIndex = 21;
            this.tbx_buy_skinca_now.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_buy_skinca_now.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_skinca_now.WaterText = "";
            this.tbx_buy_skinca_now.WordWrap = true;
            // 
            // tbx_cloth_tendency
            // 
            this.tbx_cloth_tendency.BackColor = System.Drawing.Color.Transparent;
            this.tbx_cloth_tendency.DownBack = null;
            this.tbx_cloth_tendency.Icon = null;
            this.tbx_cloth_tendency.IconIsButton = false;
            this.tbx_cloth_tendency.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_cloth_tendency.IsPasswordChat = '\0';
            this.tbx_cloth_tendency.IsSystemPasswordChar = false;
            this.tbx_cloth_tendency.Lines = new string[0];
            this.tbx_cloth_tendency.Location = new System.Drawing.Point(19, 686);
            this.tbx_cloth_tendency.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_cloth_tendency.MaxLength = 32767;
            this.tbx_cloth_tendency.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_cloth_tendency.MouseBack = null;
            this.tbx_cloth_tendency.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_cloth_tendency.Multiline = true;
            this.tbx_cloth_tendency.Name = "tbx_cloth_tendency";
            this.tbx_cloth_tendency.NormlBack = null;
            this.tbx_cloth_tendency.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_cloth_tendency.ReadOnly = false;
            this.tbx_cloth_tendency.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_cloth_tendency.Size = new System.Drawing.Size(233, 53);
            // 
            // 
            // 
            this.tbx_cloth_tendency.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_cloth_tendency.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_cloth_tendency.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_cloth_tendency.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_cloth_tendency.SkinTxt.Multiline = true;
            this.tbx_cloth_tendency.SkinTxt.Name = "BaseText";
            this.tbx_cloth_tendency.SkinTxt.Size = new System.Drawing.Size(223, 43);
            this.tbx_cloth_tendency.SkinTxt.TabIndex = 0;
            this.tbx_cloth_tendency.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_cloth_tendency.SkinTxt.WaterText = "";
            this.tbx_cloth_tendency.TabIndex = 22;
            this.tbx_cloth_tendency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_cloth_tendency.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_cloth_tendency.WaterText = "";
            this.tbx_cloth_tendency.WordWrap = true;
            // 
            // tbx_buy_clothes_now
            // 
            this.tbx_buy_clothes_now.BackColor = System.Drawing.Color.Transparent;
            this.tbx_buy_clothes_now.DownBack = null;
            this.tbx_buy_clothes_now.Icon = null;
            this.tbx_buy_clothes_now.IconIsButton = false;
            this.tbx_buy_clothes_now.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_clothes_now.IsPasswordChat = '\0';
            this.tbx_buy_clothes_now.IsSystemPasswordChar = false;
            this.tbx_buy_clothes_now.Lines = new string[0];
            this.tbx_buy_clothes_now.Location = new System.Drawing.Point(19, 786);
            this.tbx_buy_clothes_now.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_buy_clothes_now.MaxLength = 32767;
            this.tbx_buy_clothes_now.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_buy_clothes_now.MouseBack = null;
            this.tbx_buy_clothes_now.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_buy_clothes_now.Multiline = true;
            this.tbx_buy_clothes_now.Name = "tbx_buy_clothes_now";
            this.tbx_buy_clothes_now.NormlBack = null;
            this.tbx_buy_clothes_now.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_buy_clothes_now.ReadOnly = false;
            this.tbx_buy_clothes_now.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_buy_clothes_now.Size = new System.Drawing.Size(233, 75);
            // 
            // 
            // 
            this.tbx_buy_clothes_now.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_buy_clothes_now.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_buy_clothes_now.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_buy_clothes_now.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_buy_clothes_now.SkinTxt.Multiline = true;
            this.tbx_buy_clothes_now.SkinTxt.Name = "BaseText";
            this.tbx_buy_clothes_now.SkinTxt.Size = new System.Drawing.Size(223, 65);
            this.tbx_buy_clothes_now.SkinTxt.TabIndex = 0;
            this.tbx_buy_clothes_now.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_clothes_now.SkinTxt.WaterText = "";
            this.tbx_buy_clothes_now.TabIndex = 23;
            this.tbx_buy_clothes_now.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_buy_clothes_now.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_buy_clothes_now.WaterText = "";
            this.tbx_buy_clothes_now.WordWrap = true;
            // 
            // tgh
            // 
            this.tgh.AutoSize = true;
            this.tgh.BackColor = System.Drawing.Color.Transparent;
            this.tgh.BorderColor = System.Drawing.Color.White;
            this.tgh.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.tgh.Location = new System.Drawing.Point(23, 15);
            this.tgh.Name = "tgh";
            this.tgh.Size = new System.Drawing.Size(73, 20);
            this.tgh.TabIndex = 8;
            this.tgh.Text = "消费标准:";
            // 
            // skinLabel32
            // 
            this.skinLabel32.AutoSize = true;
            this.skinLabel32.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel32.BorderColor = System.Drawing.Color.White;
            this.skinLabel32.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel32.Location = new System.Drawing.Point(23, 108);
            this.skinLabel32.Name = "skinLabel32";
            this.skinLabel32.Size = new System.Drawing.Size(73, 20);
            this.skinLabel32.TabIndex = 9;
            this.skinLabel32.Text = "消费习惯:";
            // 
            // skinLabel33
            // 
            this.skinLabel33.AutoSize = true;
            this.skinLabel33.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel33.BorderColor = System.Drawing.Color.White;
            this.skinLabel33.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.skinLabel33.Location = new System.Drawing.Point(23, 202);
            this.skinLabel33.Name = "skinLabel33";
            this.skinLabel33.Size = new System.Drawing.Size(103, 20);
            this.skinLabel33.TabIndex = 10;
            this.skinLabel33.Text = "化妆品牌倾向:";
            // 
            // tbx_standard_consump
            // 
            this.tbx_standard_consump.BackColor = System.Drawing.Color.Transparent;
            this.tbx_standard_consump.DownBack = null;
            this.tbx_standard_consump.Icon = null;
            this.tbx_standard_consump.IconIsButton = false;
            this.tbx_standard_consump.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_standard_consump.IsPasswordChat = '\0';
            this.tbx_standard_consump.IsSystemPasswordChar = false;
            this.tbx_standard_consump.Lines = new string[0];
            this.tbx_standard_consump.Location = new System.Drawing.Point(23, 43);
            this.tbx_standard_consump.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_standard_consump.MaxLength = 32767;
            this.tbx_standard_consump.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_standard_consump.MouseBack = null;
            this.tbx_standard_consump.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_standard_consump.Multiline = true;
            this.tbx_standard_consump.Name = "tbx_standard_consump";
            this.tbx_standard_consump.NormlBack = null;
            this.tbx_standard_consump.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_standard_consump.ReadOnly = false;
            this.tbx_standard_consump.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_standard_consump.Size = new System.Drawing.Size(232, 58);
            // 
            // 
            // 
            this.tbx_standard_consump.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_standard_consump.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_standard_consump.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_standard_consump.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_standard_consump.SkinTxt.Multiline = true;
            this.tbx_standard_consump.SkinTxt.Name = "BaseText";
            this.tbx_standard_consump.SkinTxt.Size = new System.Drawing.Size(222, 48);
            this.tbx_standard_consump.SkinTxt.TabIndex = 0;
            this.tbx_standard_consump.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_standard_consump.SkinTxt.WaterText = "";
            this.tbx_standard_consump.TabIndex = 12;
            this.tbx_standard_consump.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_standard_consump.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_standard_consump.WaterText = "";
            this.tbx_standard_consump.WordWrap = true;
            // 
            // tbx_con_hab
            // 
            this.tbx_con_hab.BackColor = System.Drawing.Color.Transparent;
            this.tbx_con_hab.DownBack = null;
            this.tbx_con_hab.Icon = null;
            this.tbx_con_hab.IconIsButton = false;
            this.tbx_con_hab.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_con_hab.IsPasswordChat = '\0';
            this.tbx_con_hab.IsSystemPasswordChar = false;
            this.tbx_con_hab.Lines = new string[0];
            this.tbx_con_hab.Location = new System.Drawing.Point(23, 138);
            this.tbx_con_hab.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_con_hab.MaxLength = 32767;
            this.tbx_con_hab.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_con_hab.MouseBack = null;
            this.tbx_con_hab.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_con_hab.Multiline = true;
            this.tbx_con_hab.Name = "tbx_con_hab";
            this.tbx_con_hab.NormlBack = null;
            this.tbx_con_hab.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_con_hab.ReadOnly = false;
            this.tbx_con_hab.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_con_hab.Size = new System.Drawing.Size(230, 52);
            // 
            // 
            // 
            this.tbx_con_hab.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_con_hab.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_con_hab.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_con_hab.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_con_hab.SkinTxt.Multiline = true;
            this.tbx_con_hab.SkinTxt.Name = "BaseText";
            this.tbx_con_hab.SkinTxt.Size = new System.Drawing.Size(220, 42);
            this.tbx_con_hab.SkinTxt.TabIndex = 0;
            this.tbx_con_hab.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_con_hab.SkinTxt.WaterText = "";
            this.tbx_con_hab.TabIndex = 13;
            this.tbx_con_hab.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_con_hab.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_con_hab.WaterText = "";
            this.tbx_con_hab.WordWrap = true;
            // 
            // tbx_Cosmetic_tendency
            // 
            this.tbx_Cosmetic_tendency.BackColor = System.Drawing.Color.Transparent;
            this.tbx_Cosmetic_tendency.DownBack = null;
            this.tbx_Cosmetic_tendency.Icon = null;
            this.tbx_Cosmetic_tendency.IconIsButton = false;
            this.tbx_Cosmetic_tendency.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_Cosmetic_tendency.IsPasswordChat = '\0';
            this.tbx_Cosmetic_tendency.IsSystemPasswordChar = false;
            this.tbx_Cosmetic_tendency.Lines = new string[0];
            this.tbx_Cosmetic_tendency.Location = new System.Drawing.Point(23, 233);
            this.tbx_Cosmetic_tendency.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_Cosmetic_tendency.MaxLength = 32767;
            this.tbx_Cosmetic_tendency.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_Cosmetic_tendency.MouseBack = null;
            this.tbx_Cosmetic_tendency.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_Cosmetic_tendency.Multiline = true;
            this.tbx_Cosmetic_tendency.Name = "tbx_Cosmetic_tendency";
            this.tbx_Cosmetic_tendency.NormlBack = null;
            this.tbx_Cosmetic_tendency.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_Cosmetic_tendency.ReadOnly = false;
            this.tbx_Cosmetic_tendency.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_Cosmetic_tendency.Size = new System.Drawing.Size(234, 56);
            // 
            // 
            // 
            this.tbx_Cosmetic_tendency.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_Cosmetic_tendency.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_Cosmetic_tendency.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_Cosmetic_tendency.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_Cosmetic_tendency.SkinTxt.Multiline = true;
            this.tbx_Cosmetic_tendency.SkinTxt.Name = "BaseText";
            this.tbx_Cosmetic_tendency.SkinTxt.Size = new System.Drawing.Size(224, 46);
            this.tbx_Cosmetic_tendency.SkinTxt.TabIndex = 0;
            this.tbx_Cosmetic_tendency.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_Cosmetic_tendency.SkinTxt.WaterText = "";
            this.tbx_Cosmetic_tendency.TabIndex = 14;
            this.tbx_Cosmetic_tendency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_Cosmetic_tendency.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_Cosmetic_tendency.WaterText = "";
            this.tbx_Cosmetic_tendency.WordWrap = true;
            // 
            // skinTabPage4
            // 
            this.skinTabPage4.BackColor = System.Drawing.Color.White;
            this.skinTabPage4.Controls.Add(this.panel7);
            this.skinTabPage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage4.Location = new System.Drawing.Point(0, 36);
            this.skinTabPage4.Name = "skinTabPage4";
            this.skinTabPage4.Size = new System.Drawing.Size(288, 527);
            this.skinTabPage4.TabIndex = 3;
            this.skinTabPage4.TabItemImage = null;
            this.skinTabPage4.Text = "标签";
            // 
            // panel7
            // 
            this.panel7.ContextMenuStrip = this.contextMenuStrip1;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(282, 518);
            this.panel7.TabIndex = 1;
            // 
            // pa_chatlog
            // 
            this.pa_chatlog.BackColor = System.Drawing.Color.White;
            this.pa_chatlog.BorderColor = System.Drawing.Color.Gray;
            this.pa_chatlog.Controls.Add(this.richTextBox1);
            this.pa_chatlog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pa_chatlog.Location = new System.Drawing.Point(0, 36);
            this.pa_chatlog.Name = "pa_chatlog";
            this.pa_chatlog.Size = new System.Drawing.Size(290, 561);
            this.pa_chatlog.TabIndex = 3;
            this.pa_chatlog.TabItemImage = null;
            this.pa_chatlog.Text = "聊天记录";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(290, 561);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(427, 136);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(179, 227);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tabc_left
            // 
            this.tabc_left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabc_left.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.tabc_left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.tabc_left.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabc_left.BackgroundImage")));
            this.tabc_left.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.tabc_left.Controls.Add(this.chat);
            this.tabc_left.Controls.Add(this.friends);
            this.tabc_left.Font = new System.Drawing.Font("微软雅黑", 11F);
            this.tabc_left.HeadBack = null;
            this.tabc_left.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.tabc_left.ItemSize = new System.Drawing.Size(140, 48);
            this.tabc_left.Location = new System.Drawing.Point(-1, 53);
            this.tabc_left.Margin = new System.Windows.Forms.Padding(0);
            this.tabc_left.Name = "tabc_left";
            this.tabc_left.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("tabc_left.PageArrowDown")));
            this.tabc_left.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("tabc_left.PageArrowHover")));
            this.tabc_left.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("tabc_left.PageCloseHover")));
            this.tabc_left.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("tabc_left.PageCloseNormal")));
            this.tabc_left.PageDown = ((System.Drawing.Image)(resources.GetObject("tabc_left.PageDown")));
            this.tabc_left.PageDownTxtColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.tabc_left.PageHover = null;
            this.tabc_left.PageHoverTxtColor = System.Drawing.Color.White;
            this.tabc_left.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.tabc_left.PageNorml = null;
            this.tabc_left.PageNormlTxtColor = System.Drawing.Color.White;
            this.tabc_left.SelectedIndex = 0;
            this.tabc_left.Size = new System.Drawing.Size(283, 551);
            this.tabc_left.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabc_left.TabIndex = 4;
            // 
            // chat
            // 
            this.chat.BackColor = System.Drawing.Color.White;
            this.chat.Controls.Add(this.chatVScrollBar2);
            this.chat.Controls.Add(this.wChatList1);
            this.chat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chat.Location = new System.Drawing.Point(0, 48);
            this.chat.Name = "chat";
            this.chat.Size = new System.Drawing.Size(283, 503);
            this.chat.TabIndex = 0;
            this.chat.TabItemImage = null;
            this.chat.Text = "聊天";
            // 
            // friends
            // 
            this.friends.BackColor = System.Drawing.Color.White;
            this.friends.Controls.Add(this.friendscrool);
            this.friends.Controls.Add(this.wFriendsList1);
            this.friends.Dock = System.Windows.Forms.DockStyle.Fill;
            this.friends.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.friends.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.friends.Location = new System.Drawing.Point(0, 48);
            this.friends.Name = "friends";
            this.friends.Size = new System.Drawing.Size(283, 503);
            this.friends.TabIndex = 1;
            this.friends.TabItemImage = null;
            this.friends.Text = "通讯录";
            // 
            // chatVScrollBar2
            // 
            this.chatVScrollBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.chatVScrollBar2.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.chatVScrollBar2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.chatVScrollBar2.DownEnterImage = null;
            this.chatVScrollBar2.DownPressedImage = null;
            this.chatVScrollBar2.DownWakeImage = ((System.Drawing.Image)(resources.GetObject("chatVScrollBar2.DownWakeImage")));
            this.chatVScrollBar2.EnterColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chatVScrollBar2.ListBox = this.wChatList1;
            this.chatVScrollBar2.Location = new System.Drawing.Point(265, 0);
            this.chatVScrollBar2.Margin = new System.Windows.Forms.Padding(7, 5, 7, 5);
            this.chatVScrollBar2.MinSlideBarLenght = 30;
            this.chatVScrollBar2.Name = "chatVScrollBar2";
            this.chatVScrollBar2.PressedColor = System.Drawing.Color.Gray;
            this.chatVScrollBar2.Sense = 5D;
            this.chatVScrollBar2.Size = new System.Drawing.Size(18, 502);
            this.chatVScrollBar2.TabIndex = 2;
            this.chatVScrollBar2.UpEnterImage = ((System.Drawing.Image)(resources.GetObject("chatVScrollBar2.UpEnterImage")));
            this.chatVScrollBar2.UpPressedImage = null;
            this.chatVScrollBar2.UpWakeImage = ((System.Drawing.Image)(resources.GetObject("chatVScrollBar2.UpWakeImage")));
            this.chatVScrollBar2.WakedColor = System.Drawing.Color.Gray;
            // 
            // wChatList1
            // 
            this.wChatList1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.wChatList1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.wChatList1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wChatList1.ContextMenuStrip = this.wchatlist_menu;
            this.wChatList1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wChatList1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.wChatList1.FormattingEnabled = true;
            this.wChatList1.ItemHeight = 12;
            this.wChatList1.Location = new System.Drawing.Point(1, -1);
            this.wChatList1.Name = "wChatList1";
            this.wChatList1.Size = new System.Drawing.Size(282, 503);
            this.wChatList1.TabIndex = 0;
            this.wChatList1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.wChatList1_MouseUp);
            // 
            // friendscrool
            // 
            this.friendscrool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.friendscrool.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.friendscrool.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.friendscrool.DownEnterImage = ((System.Drawing.Image)(resources.GetObject("friendscrool.DownEnterImage")));
            this.friendscrool.DownPressedImage = null;
            this.friendscrool.DownWakeImage = ((System.Drawing.Image)(resources.GetObject("friendscrool.DownWakeImage")));
            this.friendscrool.EnterColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.friendscrool.ListBox = this.wFriendsList1;
            this.friendscrool.Location = new System.Drawing.Point(264, 0);
            this.friendscrool.MinSlideBarLenght = 30;
            this.friendscrool.Name = "friendscrool";
            this.friendscrool.PressedColor = System.Drawing.Color.Gray;
            this.friendscrool.Sense = 5D;
            this.friendscrool.Size = new System.Drawing.Size(20, 503);
            this.friendscrool.TabIndex = 1;
            this.friendscrool.UpEnterImage = ((System.Drawing.Image)(resources.GetObject("friendscrool.UpEnterImage")));
            this.friendscrool.UpPressedImage = null;
            this.friendscrool.UpWakeImage = ((System.Drawing.Image)(resources.GetObject("friendscrool.UpWakeImage")));
            this.friendscrool.WakedColor = System.Drawing.Color.Gray;
            // 
            // wFriendsList1
            // 
            this.wFriendsList1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.wFriendsList1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(54)))), ((int)(((byte)(63)))));
            this.wFriendsList1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wFriendsList1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wFriendsList1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.wFriendsList1.FormattingEnabled = true;
            this.wFriendsList1.IntegralHeight = false;
            this.wFriendsList1.ItemHeight = 30;
            this.wFriendsList1.Location = new System.Drawing.Point(1, 0);
            this.wFriendsList1.Name = "wFriendsList1";
            this.wFriendsList1.Size = new System.Drawing.Size(282, 502);
            this.wFriendsList1.TabIndex = 0;
            // 
            // WinXinBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbxFriendSearch);
            this.Controls.Add(this.tbc_right);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.tabc_left);
            this.Name = "WinXinBox";
            this.Size = new System.Drawing.Size(1233, 603);
            this.Load += new System.EventHandler(this.WinXinBox_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.WinXinBox_Paint);
            this.wchatlist_menu.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tbc_right.ResumeLayout(false);
            this.basic_info.ResumeLayout(false);
            this.basic_info.PerformLayout();
            this.basic_pal.ResumeLayout(false);
            this.basic_pal.PerformLayout();
            this.pa_detail_info.ResumeLayout(false);
            this.pa_detail_info.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.Costom_analysls.ResumeLayout(false);
            this.tbc_right_analysis.ResumeLayout(false);
            this.skinTabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.skinTabPage2.ResumeLayout(false);
            this.skinTabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.skinTabPage3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.skinTabPage4.ResumeLayout(false);
            this.pa_chatlog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabc_left.ResumeLayout(false);
            this.chat.ResumeLayout(false);
            this.friends.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private WChatList wChatList1;
        private WFriendsList wFriendsList1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip wchatlist_menu;
        private System.Windows.Forms.ToolStripMenuItem 删除项目ToolStripMenuItem;
        private CCWin.SkinControl.SkinTabControl tbc_right;
        private CCWin.SkinControl.SkinTabPage basic_info;
        private CCWin.SkinControl.SkinTabPage Costom_analysls;
        private CCWin.SkinControl.SkinTabPage pa_detail_info;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinTextBox tbx_belong_wx;
        private CCWin.SkinControl.SkinTextBox tbx_wxaccount;
        private CCWin.SkinControl.SkinTextBox tbx_nickname;
        private CCWin.SkinControl.SkinTextBox tbx_remark;
        private CCWin.SkinControl.SkinLabel skinLabel12;
        private CCWin.SkinControl.SkinLabel skinLabel11;
        private CCWin.SkinControl.SkinTextBox tb_incoleve;
        private CCWin.SkinControl.SkinLabel skinLabel10;
        private CCWin.SkinControl.SkinTextBox tb_occupation;
        private CCWin.SkinControl.SkinLabel skinLabel9;
        private CCWin.SkinControl.SkinLabel skinLabel8;
        private CCWin.SkinControl.SkinLabel skinLabel7;
        private CCWin.SkinControl.SkinTextBox tb_phone;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private CCWin.SkinControl.SkinTextBox tb_qq;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinTextBox tb_address;
        private CCWin.SkinControl.SkinLabel lb_address;
        private CCWin.SkinControl.SkinTextBox tb_dgedu;
        private CCWin.SkinControl.SkinLabel skinLabel14;
        private CCWin.SkinControl.SkinLabel skinLabel15;
        private CCWin.SkinControl.SkinLabel skinLabel16;
        private CCWin.SkinControl.SkinButton tbn_basic_save;
        private CCWin.SkinControl.SkinTextBox tb_remarks;
        private CCWin.SkinControl.SkinTabPage pa_chatlog;
        private CCWin.SkinControl.SkinTabControl tabc_left;
        private CCWin.SkinControl.SkinTabPage chat;
        private CCWin.SkinControl.SkinTabPage friends;
        private CustomScrollBar.ListBoxScrollBar.ListBoxVScrollBar friendscrool;
        private CustomScrollBar.ListBoxScrollBar.ListBoxVScrollBar chatVScrollBar2;
        private System.Windows.Forms.Panel basic_pal;
        private System.Windows.Forms.ComboBox cb_classification;
        private System.Windows.Forms.ComboBox cbo_age;
        private System.Windows.Forms.ComboBox cb_gender;
        private System.Windows.Forms.DateTimePicker daTiPicker_brith;
        private CCWin.SkinControl.SkinLabel skinLabel19;
        private CCWin.SkinControl.SkinLabel skinLabel17;
        private CCWin.SkinControl.SkinTextBox tb_belongkf;
        private CCWin.SkinControl.SkinTabControl tbc_right_analysis;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinTabPage skinTabPage2;
        private CCWin.SkinControl.SkinTabPage skinTabPage3;
        private CCWin.SkinControl.SkinTabPage skinTabPage4;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Label lblArea;
        private System.Windows.Forms.Label lblSignature;
        private System.Windows.Forms.Label lblNick;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lb_remarknam;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private CCWin.SkinControl.SkinLabel skinLabel25;
        private CCWin.SkinControl.SkinTextBox tbx_tim_li_cost3;
        private CCWin.SkinControl.SkinLabel skinLabel24;
        private CCWin.SkinControl.SkinTextBox tbx_tim_li_cost2;
        private CCWin.SkinControl.SkinLabel skinLabel23;
        private CCWin.SkinControl.SkinTextBox tbx_tim_li_cost1;
        private CCWin.SkinControl.SkinLabel skinLabel22;
        private CCWin.SkinControl.SkinTextBox tbx_tim_li_persanaly;
        private CCWin.SkinControl.SkinLabel skinLabel21;
        private CCWin.SkinControl.SkinTextBox tbx_tim_li_acti_tim;
        private CCWin.SkinControl.SkinLabel skinLabel13;
        private CCWin.SkinControl.SkinTextBox tbx_tim_li_like;
        private CCWin.SkinControl.SkinTextBox tbx_custom2;
        private CCWin.SkinControl.SkinLabel skinLabel26;
        private CCWin.SkinControl.SkinTextBox tbx_custom3;
        private CCWin.SkinControl.SkinLabel skinLabel27;
        private CCWin.SkinControl.SkinLabel skinLabel28;
        private CCWin.SkinControl.SkinTextBox tbx_custom1;
        private CCWin.SkinControl.SkinLabel skinLabel29;
        private CCWin.SkinControl.SkinTextBox tbx_potential_demand;
        private CCWin.SkinControl.SkinLabel skinLabel30;
        private CCWin.SkinControl.SkinTextBox tbx_req_now;
        private CCWin.SkinControl.SkinButton btn_requirement_save;
        private CCWin.SkinControl.SkinLabel skinLabel39;
        private CCWin.SkinControl.SkinLabel skinLabel40;
        private CCWin.SkinControl.SkinLabel skinLabel41;
        private CCWin.SkinControl.SkinTextBox tbx_Ornaments_tendency;
        private CCWin.SkinControl.SkinTextBox tbx_buy_ornaments_now;
        private CCWin.SkinControl.SkinTextBox tbx_comprehensive_analy;
        private CCWin.SkinControl.SkinLabel skinLabel36;
        private CCWin.SkinControl.SkinLabel skinLabel37;
        private CCWin.SkinControl.SkinLabel lb_cloth_tendency;
        private CCWin.SkinControl.SkinTextBox tbx_buy_skinca_now;
        private CCWin.SkinControl.SkinTextBox tbx_cloth_tendency;
        private CCWin.SkinControl.SkinTextBox tbx_buy_clothes_now;
        private CCWin.SkinControl.SkinLabel lbzh;
        private CCWin.SkinControl.SkinButton btn_time_lin_save;
        private CCWin.SkinControl.SkinButton btn_consumption_save;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private CCWin.SkinControl.SkinLabel lb_b;
        private CCWin.SkinControl.SkinTextBox tbx_buy_cosmetic_now;
        private CCWin.SkinControl.SkinLabel skinLabel35;
        private CCWin.SkinControl.SkinTextBox tbx_sk_care_brand_tenden;
        private CCWin.SkinControl.SkinLabel tgh;
        private CCWin.SkinControl.SkinLabel skinLabel32;
        private CCWin.SkinControl.SkinLabel skinLabel33;
        private CCWin.SkinControl.SkinTextBox tbx_standard_consump;
        private CCWin.SkinControl.SkinTextBox tbx_con_hab;
        private CCWin.SkinControl.SkinTextBox tbx_Cosmetic_tendency;
        private System.Windows.Forms.Panel panel5;
        private CCWin.SkinControl.SkinTextBox tbx_creatime;
        private CCWin.SkinControl.SkinTextBox tbx_creater;
        private System.Windows.Forms.PictureBox pic_sex;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.ListBox lbxFriendSearch;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 添加标签ToolStripMenuItem;
    }
}
