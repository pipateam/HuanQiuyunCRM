﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WChat {
    /// <summary>
    /// 
    /// </summary>
   public static class CookieDictionary {
      static  Dictionary<string, string> cookies = new Dictionary<string, string>();
        /// <summary>
        ///设置cookies
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cookie"></param>
        public static void AddCookies(string user,string cookie) {
            cookies.Add(user, cookie);
        } 
        /// <summary>
        ///获取cookies
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cookie"></param>
        public static string GetCookies(string user) {
            string coo = cookies[user];
            return coo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetCookiesByName(string user,int type) {
            string coo = cookies[user];
            string[] sArray = coo.Split('=');
            if (type==0) {    //取得wxuin
                string wxuin = sArray[1].Substring(0, sArray[1].Length - 6); //去掉后面的";"
                return wxuin;
            }else {
                string wxsid = sArray[2].Substring(0, sArray[1].Length - 11); //去掉后面的";"
                return wxsid;
            }
        }
    }
}
