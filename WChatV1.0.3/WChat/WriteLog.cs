﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace WChat {
    /// <summary>
    /// 名称：写日志，将软件执行情况写入一个txt文档中。
    /// By：李大亮 2017/4/30
    /// </summary>
  public static class WriteLog {
        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="mothod"></param>
        /// <param name="Content"></param>
        public static void Writelog(string mothod,string Content) {
            string path = WChat.path.paths + "\\log.txt";
            StreamWriter sw = new StreamWriter(path, true);
            string content = "【"+DateTime.Now.ToString("yyyyMMddHHmmss")+"】" + mothod + ":" + Content + "\r\n";
            sw.Write(content);
            sw.Close();
        }
    }
}
