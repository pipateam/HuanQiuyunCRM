﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models {
    public class Fri {
        public string nick_name { get; set; } //自己的昵称

        public string wx_name { get; set; } //好友昵称

        public string Sex { get; set; } //性别

        public string weixin { get; set; } //好友串号

        public string Province { get; set; } //地区
    }
}
