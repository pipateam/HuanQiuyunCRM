﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 朋友圈分析
    /// </summary>
    public class CircleOfFriends
    {
        public int COFId { get; set; }//主键Id
        public string FriendName { get; set; }//好友名称
        public string COFLike { get; set; }//朋友圈喜好
        public DateTime ActiveTime { get; set; }//活跃时间
        public string PPAnalysis { get; set; }//个人画像分析PersonalPortraitAnalysis
        public string Custom1 { get; set; }//自定义项
        public string Custom2 { get; set; }//自定义项
        public string Custom3 { get; set; }//自定义项
    }
}
