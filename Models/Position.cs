﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 职位表
    /// </summary>
    public class Position
    {
        public int PositionId { get; set; }//主键Id
        public string Explain { get; set; }//说明
        public string PositionName { get; set; }//职位名称
    }
}
