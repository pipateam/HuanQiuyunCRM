﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models {
    public class LoginStatus {
        public int MaritalStatusId { get; set; }//主键Id
        public string Explain { get; set; }//说明
        public string MaritalStatusName { get; set; }//状况详情
    }
}
