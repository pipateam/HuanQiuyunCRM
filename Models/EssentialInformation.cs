﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class EssentialInformation
    {
        public int EssentialInformationId { get; set; }//主键列
        public string RemarksName { get; set; }//备注名
        public string NickName { get; set; }//昵称
        public string AccountNumber { get; set; }//账号
        public string The { get; set; }//所属
        public string Sex { get; set; }//性别
        public string Old { get; set; }//年龄段
        public DateTime DateOfBirth { get; set; }//出生日期
        public string Telephone { get; set; }//手机号
        public string QQ { get; set; }
        public string Occupation { get; set; }//职业
        public string Culture { get; set; }//文化程度   ?
        public string IncomeLevel { get; set; }//收入水平    ?
        public string Address { get; set; }//联系地址
        public string TheCustomer { get; set; }//所属客服    ?
        public string Remarks { get; set; }//客户分类       ？
        public string CreatePeople { get; set; }//创建人
        public string SetTime { get; set; }//最后修改时间
    }
}
