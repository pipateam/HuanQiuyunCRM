﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 从事行业表
    /// </summary>
    public class EngagedInIndustry
    {
        public int EngagedInIndustryId { get; set; }//主键Id
        public string Explain { get; set; }//说明
        public string EngagedInIndustryName { get; set; }//行业名称
    }
}
