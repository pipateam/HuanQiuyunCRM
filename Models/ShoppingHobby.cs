﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 购物爱好表
    /// </summary>
    public class ShoppingHobby
    {
        public int ShoppingHobbyId { get; set; }//主键Id
        public string Explain { get; set; }//说明
        public string ShoppingHobbyName { get; set; }//购物爱好说明
    }
}
