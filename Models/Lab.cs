﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models {
    public class Lab
    {
        public int Id { get; set; }//主键Id
        public string LabelsText { get; set; }//标签内容
    }
}
