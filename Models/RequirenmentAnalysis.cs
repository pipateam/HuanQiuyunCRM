﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 需求分析表
    /// </summary>
    public class RequirenmentAnalysis
    {
        public int RequirenmentId { get; set; }//主键Id
        public string FriendName { get; set; }//好有名
        public string CurrentDemand { get; set; }//现阶段需求
        public string PotentialDemand { get; set; }//潜在需求
        public string Custom1 { get; set; }//自定义项
        public string Custom2 { get; set; }//自定义项
        public string Custom3 { get; set; }//自定义项
    }
}
