﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 消费分析
    /// </summary>
    public class ConsumptionAnalysis
    {
        public int CAnalysisId { get; set; }//主键Id
        public string FriendName { get; set; }//客户名
        public string CStandard { get; set; }//消费标准
        public string CHabit { get; set; }//消费习惯
        public string BrandOrientation { get; set; }//化妆品品牌倾向
        public string BrandOName { get; set; }//最近购买的化妆品
        public string SkincareBrand { get; set; }//护肤品品牌倾向
        public string BrandSName { get; set; }//最近购买的护肤品
        public string ClothingBrandTendency { get; set; }//穿衣品牌倾向
        public string RecentlyPurchasedClothes { get; set; }//最近购买的衣服
        public string JewelryBrandTendency { get; set; }//饰品品牌倾向
        public string RecentlyPurchasedJewelry { get; set; }//最近购买的饰品
        public string ComprehensiveAnalysis { get; set; }//综合分析
    }
}
