﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// 兴趣爱好表
    /// </summary>
    public class Hobby
    {
        public int HobbyId { get; set; }//主键Id
        public string Explain { get; set; }//说明
        public string HobbyName { get; set; }//兴趣爱好名称
    }
}
