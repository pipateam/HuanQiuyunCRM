﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models {
    public class HongBao {
        public DateTime Time { get; set; }
        public string FromUser { get; set; } //发送者
        public string ToUser { get; set; } //接收者
        public int Received { get; set; } //收发类型
        public int Type { get; set; } //信息类型
    }
}
