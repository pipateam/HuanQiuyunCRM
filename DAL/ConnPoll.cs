﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL { 
   public class ConnPoll {
        private const int MaxPool = 300;//最大连接数
        private const int MinPool = 0;//最小连接数
        private const bool Asyn_Process = true; //设置异步访问数据库
        private const bool Mars = true;//在单个连接上得到和管理多个、仅向前引用和只读的结果集(ADO.NET2.0)
        private const int Conn_Timeout = 15;//设置连接等待时间
        private const int Conn_Lifetime = 5;//设置连接的生命周期
        private static string ConnString = "";//连接字符串
        private static  SqlConnection SqlDrConn = null;//连接对象

        private static string GetConnString() {
            return "server=bds249611391.my3w.com;"
                + " integrated security=false;"//判断windows域名 和 uid账号 登陆的方式 false为uid登陆
                + " database=bds249611391_db;"
                + " Max Pool Size=" + MaxPool + ";"
                + " Min Pool Size=" + MinPool + ";"
                + " Connect Timeout=" + Conn_Timeout + ";"
                + " Connection Lifetime=" + Conn_Lifetime + ";"
                + " Asynchronous Processing=" + Asyn_Process + ";"//表示使用异步处理操作 true为异步处理
                + " uid=bds249611391;"
                + " MultipleActiveResultSets=" + Mars + ";"
                + " pwd=hq312453";
        }

        /// <summary>
        ///   单例模式得到连接池的一个实例
        /// </summary>
        /// <returns></returns>
        public static SqlConnection GetConn() {
            try {
                if (SqlDrConn == null) {
                    ConnString = GetConnString();
                    SqlDrConn = new SqlConnection(ConnString);
                }
            } catch (Exception) {

            }
            return SqlDrConn;
        }

        public static void Open() {
            if (SqlDrConn.State == System.Data.ConnectionState.Closed) {
                SqlDrConn.Open();
            }
        }

        public static void Close() {
            SqlDrConn.Close();
        }
    }
}
