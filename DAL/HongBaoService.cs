﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Models;
namespace DAL {
    public class HongBaoService {

        public int serachTable( string ChildName ) {
            SqlConnection conn = ConnPoll.GetConn();
          string sql = string.Format("select Count(1) from ChatLog{0}", ChildName);
            SqlCommand com = new SqlCommand(sql, conn);
            try {
                ConnPoll.Open();
                int i = Convert.ToInt32(com.ExecuteScalar());
                com.Dispose();
                ConnPoll.Close();
                return i;
            }
            catch (Exception) {
                com.Dispose();
                ConnPoll.Close();
                return -1;
            }
        }


        public List<HongBao> SearchHB( string ChildName ) {
            SqlConnection conn = ConnPoll.GetConn();
            List<HongBao> Lhb = new List<HongBao>();
            string sql = string.Format("select Time,FromUser,ToUser,Received from ChatLog{0} where Type=10000", ChildName);
            SqlDataReader dr = SQLHelper.ExecuteReader(conn, System.Data.CommandType.Text, sql);
            while (dr.Read()) {
                HongBao hb = new HongBao();
                hb.Time = Convert.ToDateTime(dr["Time"]);
                hb.FromUser = dr["FromUser"].ToString();
                hb.ToUser = dr["ToUser"].ToString();
                hb.Received = Convert.ToInt32(dr["Received"]);
                Lhb.Add(hb);
            }
            return Lhb;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="LoginName">子账号</param>
        /// <param name="ChildName">微信名</param>
        /// <returns></returns>
        public List<HongBao> NotInFriSearchHB( string LoginName, string ChildName ) {
            SqlConnection conn = ConnPoll.GetConn();
            List<HongBao> Lhb = new List<HongBao>();
            string sql = string.Format("select Count(1) as Num,ToUser from ChatLog{0} where Type =10000 and ToUser not in (select wx_name from {1}_微信好友表) group by ToUser", ChildName, LoginName);
            SqlDataReader dr = SQLHelper.ExecuteReader(conn, System.Data.CommandType.Text, sql);
            while (dr.Read()) {
                HongBao hb = new HongBao();
                hb.ToUser = dr["ToUser"].ToString().Trim();
                hb.Received = Convert.ToInt32(dr["Num"]);
                Lhb.Add(hb);
            }
            return Lhb;
        }
    }
}
