﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Models;

namespace DAL {
    public class LoginStatusService {
       
        /// <summary>
        /// 查询管理账户与子账户的对应关系
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public string SearchAdministration( string UserName ) {
            SqlConnection conn = ConnPoll.GetConn();
            string sql = string.Format("select Admin from Child_HuanQiuYun where UseAcct='{0}'", UserName);
            return SQLHelper.ExecuteScalar(conn, System.Data.CommandType.Text, sql).ToString();
        }


        /// <summary>
        /// 设置子账号登陆状态
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="LoginName"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public int SetStatusLand( string UserName, string LoginName, int i ) {
            SqlConnection conn = ConnPoll.GetConn();
            string sql = string.Format("update Admin_{0} set LoginStatus={1} where UseAcct='{2}'", UserName, i, LoginName);
            return SQLHelper.ExecuteNonQuery(conn, System.Data.CommandType.Text, sql);
        }


        /// <summary>
        /// 创建子账户登陆微信表
        /// </summary>
        /// <param name="LoginName"></param>
        public void CreateLoginChild( string LoginName ) {
            SqlConnection conn = ConnPoll.GetConn();
            string sql = string.Format("use bds249611391_db "
                + " create table {0}Child "
                + " ("
                + " Id int not null identity(1,1),"
                + " LoginName nvarchar(Max) null,"
                + " LoginStatus int null"
                + " )", LoginName);
            SQLHelper.ExecuteNonQuery(conn, System.Data.CommandType.Text, sql);
        }


        /// <summary>
        /// 插入微信账号
        /// </summary>
        /// <param name="l"></param>
        /// <param name="LoginName"></param>
        /// <returns></returns>
        public int InsertLoginChild( string LoginName, string ChildName ) {
            SqlConnection conn = ConnPoll.GetConn();
            string sql = string.Format("insert into {0}Child (LoginName,LoginStatus) values ('{1}',0)", LoginName, ChildName);
            return SQLHelper.ExecuteNonQuery(conn, System.Data.CommandType.Text, sql);
        }


        /// <summary>
        /// 设置微信账号登陆状态
        /// </summary>
        /// <param name="LoginName"></param>
        /// <param name="ChildName"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public int SetLoginChild( string LoginName, string ChildName, int i ) {
            SqlConnection conn = ConnPoll.GetConn();
            StringBuilder sb = new StringBuilder(string.Format("update {0}Child set LoginStatus={1}", LoginName, i));
            if (!ChildName.Equals(string.Empty))
                sb.Append(string.Format(" where LoginName = '{0}'", ChildName));
            return SQLHelper.ExecuteNonQuery(conn, System.Data.CommandType.Text, sb.ToString());
        }


        /// <summary>
        /// 删除微信账号信息
        /// </summary>
        /// <param name="LoginName"></param>
        /// <param name="ChildName"></param>
        /// <returns></returns>
        public int DeleteLoginChild( string LoginName, string ChildName ) {
            SqlConnection conn = ConnPoll.GetConn();
            string sql = string.Format("delete {0}Child where LoginName='{1}'", LoginName, ChildName);
            return SQLHelper.ExecuteNonQuery(conn, System.Data.CommandType.Text, sql);
        }


        /// <summary>
        /// 擦寻是否有信息
        /// </summary>
        /// <param name="LoginName"></param>
        /// <param name="ChildName">此参数为空（string.empty）时，查询所有</param>
        /// <returns>返回-1则没有此表</returns>
        public int SearchLoginChild( string LoginName, string ChildName ) {
            SqlConnection conn = ConnPoll.GetConn();
            try {
                StringBuilder sb = new StringBuilder(string.Format("select count(1) from {0}Child", LoginName));
                if (!ChildName.Equals(string.Empty))
                    sb.Append(string.Format(" where LoginName='{0}'", ChildName));
                return Convert.ToInt32(SQLHelper.ExecuteScalar(conn, System.Data.CommandType.Text, sb.ToString()));
            }
            catch (Exception) {
                return -1;
                throw;
            }
        }
    }
}
