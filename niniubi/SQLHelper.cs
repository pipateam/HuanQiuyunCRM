﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace niniubi
{
    public class SQLHelper
    {
        public static readonly string connectionString = "server=bds249611391.my3w.com;database=bds249611391_db;uid=bds249611391;pwd=hq312453";

        private static void PreparedCommand(SqlConnection conn, SqlCommand cmd, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] parms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (trans != null)
                cmd.Transaction = trans;

            cmd.CommandType = cmdType;

            if (parms != null)
            {
                foreach (SqlParameter parm in parms)
                {
                    cmd.Parameters.Add(parm);
                }
            }
        }

        public static SqlDataReader ExecuteReader(string conStr,CommandType cmdType,string cmdText,params SqlParameter[] parms)
        {
            SqlCommand cmd = new SqlCommand();

            SqlConnection conn = new SqlConnection(conStr);

            try
            {
                PreparedCommand(conn, cmd, null, cmdType, cmdText, parms);

                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
                return dr;
            }
            catch (Exception)
            {
                conn.Close();
                throw;
            }
        }

        public static int ExecuteNonQuery(string conStr, CommandType cmdType, string cmdText, params SqlParameter[] parms)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(conStr))
            {
                PreparedCommand(conn, cmd, null, cmdType, cmdText, parms);

                int val = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                return val;
            }
        }

        public static object ExecuteScalar(string conStr, CommandType cmdType, string cmdText, params SqlParameter[] parms)
        {
            SqlCommand cmd = new SqlCommand();

            using (SqlConnection conn = new SqlConnection(conStr))
            {
                PreparedCommand(conn, cmd, null, cmdType, cmdText, parms);

                object val = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                return val;
            }
        }

       

        public static int ExecuteNonQuery(SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            PreparedCommand(trans.Connection, cmd, trans, cmdType, cmdText, commandParameters);
            int val = cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            return val;
        }


        public static DataTable ExecuteDataAdapter(string conStr, CommandType cmdType, string cmdText, params SqlParameter[] parms)
        {
            SqlCommand cmd = new SqlCommand();

            DataTable dataTable = new DataTable();

            using (SqlConnection conn = new SqlConnection(conStr))
            {
                PreparedCommand(conn, cmd, null, cmdType, cmdText, parms);

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dataTable);
                    cmd.Parameters.Clear();
                    return dataTable;
                }
            }
        }
    }
}
