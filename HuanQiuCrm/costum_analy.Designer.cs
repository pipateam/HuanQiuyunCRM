﻿namespace HuanQiuCrm {
    partial class costum_analy {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(costum_analy));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv_basic_ana = new CCWin.SkinControl.SkinDataGridView();
            this.skinTabControl1 = new CCWin.SkinControl.SkinTabControl();
            this.skinTabPage1 = new CCWin.SkinControl.SkinTabPage();
            this.skinTabPage2 = new CCWin.SkinControl.SkinTabPage();
            this.dgv_cicrcle_friend = new CCWin.SkinControl.SkinDataGridView();
            this.skinTabPage3 = new CCWin.SkinControl.SkinTabPage();
            this.dgv_requirement_analy = new CCWin.SkinControl.SkinDataGridView();
            this.skinTabPage4 = new CCWin.SkinControl.SkinTabPage();
            this.dgv_consump_ana = new CCWin.SkinControl.SkinDataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_ms_chiwx = new CCWin.SkinControl.SkinListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_basic_ana)).BeginInit();
            this.skinTabControl1.SuspendLayout();
            this.skinTabPage1.SuspendLayout();
            this.skinTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cicrcle_friend)).BeginInit();
            this.skinTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_requirement_analy)).BeginInit();
            this.skinTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_consump_ana)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_basic_ana
            // 
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.dgv_basic_ana.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle49;
            this.dgv_basic_ana.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.dgv_basic_ana.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_basic_ana.ColumnFont = null;
            this.dgv_basic_ana.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle50.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_basic_ana.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle50;
            this.dgv_basic_ana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_basic_ana.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle51.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle51.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_basic_ana.DefaultCellStyle = dataGridViewCellStyle51;
            this.dgv_basic_ana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_basic_ana.EnableHeadersVisualStyles = false;
            this.dgv_basic_ana.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgv_basic_ana.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_basic_ana.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_basic_ana.Location = new System.Drawing.Point(0, 0);
            this.dgv_basic_ana.Name = "dgv_basic_ana";
            this.dgv_basic_ana.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_basic_ana.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle52.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_basic_ana.RowsDefaultCellStyle = dataGridViewCellStyle52;
            this.dgv_basic_ana.RowTemplate.Height = 23;
            this.dgv_basic_ana.Size = new System.Drawing.Size(991, 541);
            this.dgv_basic_ana.TabIndex = 2;
            this.dgv_basic_ana.TitleBack = null;
            this.dgv_basic_ana.TitleBackColorBegin = System.Drawing.Color.White;
            this.dgv_basic_ana.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // skinTabControl1
            // 
            this.skinTabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.skinTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skinTabControl1.AnimatorType = CCWin.SkinControl.AnimationType.HorizSlide;
            this.skinTabControl1.BackColor = System.Drawing.Color.Silver;
            this.skinTabControl1.CloseRect = new System.Drawing.Rectangle(2, 2, 12, 12);
            this.skinTabControl1.Controls.Add(this.skinTabPage1);
            this.skinTabControl1.Controls.Add(this.skinTabPage2);
            this.skinTabControl1.Controls.Add(this.skinTabPage3);
            this.skinTabControl1.Controls.Add(this.skinTabPage4);
            this.skinTabControl1.HeadBack = null;
            this.skinTabControl1.ImgTxtOffset = new System.Drawing.Point(0, 0);
            this.skinTabControl1.ItemSize = new System.Drawing.Size(35, 112);
            this.skinTabControl1.Location = new System.Drawing.Point(113, 72);
            this.skinTabControl1.Multiline = true;
            this.skinTabControl1.Name = "skinTabControl1";
            this.skinTabControl1.PageArrowDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowDown")));
            this.skinTabControl1.PageArrowHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageArrowHover")));
            this.skinTabControl1.PageCloseHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseHover")));
            this.skinTabControl1.PageCloseNormal = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageCloseNormal")));
            this.skinTabControl1.PageDown = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageDown")));
            this.skinTabControl1.PageDownTxtColor = System.Drawing.Color.WhiteSmoke;
            this.skinTabControl1.PageHover = ((System.Drawing.Image)(resources.GetObject("skinTabControl1.PageHover")));
            this.skinTabControl1.PageImagePosition = CCWin.SkinControl.SkinTabControl.ePageImagePosition.Left;
            this.skinTabControl1.PageNorml = null;
            this.skinTabControl1.SelectedIndex = 0;
            this.skinTabControl1.Size = new System.Drawing.Size(1103, 541);
            this.skinTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.skinTabControl1.TabIndex = 7;
            this.skinTabControl1.SelectedIndexChanged += new System.EventHandler(this.skinTabControl1_SelectedIndexChanged);
            // 
            // skinTabPage1
            // 
            this.skinTabPage1.BackColor = System.Drawing.Color.White;
            this.skinTabPage1.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.skinTabPage1.Controls.Add(this.dgv_basic_ana);
            this.skinTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage1.Location = new System.Drawing.Point(112, 0);
            this.skinTabPage1.Name = "skinTabPage1";
            this.skinTabPage1.Size = new System.Drawing.Size(991, 541);
            this.skinTabPage1.TabIndex = 0;
            this.skinTabPage1.TabItemImage = null;
            this.skinTabPage1.Text = "基本信息";
            // 
            // skinTabPage2
            // 
            this.skinTabPage2.BackColor = System.Drawing.Color.White;
            this.skinTabPage2.BorderColor = System.Drawing.Color.Silver;
            this.skinTabPage2.Controls.Add(this.dgv_cicrcle_friend);
            this.skinTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage2.Location = new System.Drawing.Point(112, 0);
            this.skinTabPage2.Name = "skinTabPage2";
            this.skinTabPage2.Size = new System.Drawing.Size(991, 541);
            this.skinTabPage2.TabIndex = 1;
            this.skinTabPage2.TabItemImage = null;
            this.skinTabPage2.Text = "朋友圈分析";
            // 
            // dgv_cicrcle_friend
            // 
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.dgv_cicrcle_friend.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle53;
            this.dgv_cicrcle_friend.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.dgv_cicrcle_friend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_cicrcle_friend.ColumnFont = null;
            this.dgv_cicrcle_friend.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle54.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_cicrcle_friend.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle54;
            this.dgv_cicrcle_friend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_cicrcle_friend.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_cicrcle_friend.DefaultCellStyle = dataGridViewCellStyle55;
            this.dgv_cicrcle_friend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_cicrcle_friend.EnableHeadersVisualStyles = false;
            this.dgv_cicrcle_friend.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgv_cicrcle_friend.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_cicrcle_friend.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_cicrcle_friend.Location = new System.Drawing.Point(0, 0);
            this.dgv_cicrcle_friend.Name = "dgv_cicrcle_friend";
            this.dgv_cicrcle_friend.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_cicrcle_friend.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle56.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_cicrcle_friend.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this.dgv_cicrcle_friend.RowTemplate.Height = 23;
            this.dgv_cicrcle_friend.Size = new System.Drawing.Size(991, 541);
            this.dgv_cicrcle_friend.TabIndex = 3;
            this.dgv_cicrcle_friend.TitleBack = null;
            this.dgv_cicrcle_friend.TitleBackColorBegin = System.Drawing.Color.White;
            this.dgv_cicrcle_friend.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // skinTabPage3
            // 
            this.skinTabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.skinTabPage3.BorderColor = System.Drawing.Color.Silver;
            this.skinTabPage3.Controls.Add(this.dgv_requirement_analy);
            this.skinTabPage3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage3.Location = new System.Drawing.Point(112, 0);
            this.skinTabPage3.Name = "skinTabPage3";
            this.skinTabPage3.Size = new System.Drawing.Size(991, 541);
            this.skinTabPage3.TabIndex = 2;
            this.skinTabPage3.TabItemImage = null;
            this.skinTabPage3.Text = "需求分析";
            // 
            // dgv_requirement_analy
            // 
            dataGridViewCellStyle57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.dgv_requirement_analy.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle57;
            this.dgv_requirement_analy.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.dgv_requirement_analy.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_requirement_analy.ColumnFont = null;
            this.dgv_requirement_analy.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle58.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle58.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_requirement_analy.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle58;
            this.dgv_requirement_analy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_requirement_analy.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle59.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle59.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle59.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle59.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle59.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle59.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_requirement_analy.DefaultCellStyle = dataGridViewCellStyle59;
            this.dgv_requirement_analy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_requirement_analy.EnableHeadersVisualStyles = false;
            this.dgv_requirement_analy.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgv_requirement_analy.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_requirement_analy.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_requirement_analy.Location = new System.Drawing.Point(0, 0);
            this.dgv_requirement_analy.Name = "dgv_requirement_analy";
            this.dgv_requirement_analy.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_requirement_analy.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle60.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle60.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_requirement_analy.RowsDefaultCellStyle = dataGridViewCellStyle60;
            this.dgv_requirement_analy.RowTemplate.Height = 23;
            this.dgv_requirement_analy.Size = new System.Drawing.Size(991, 541);
            this.dgv_requirement_analy.TabIndex = 3;
            this.dgv_requirement_analy.TitleBack = null;
            this.dgv_requirement_analy.TitleBackColorBegin = System.Drawing.Color.White;
            this.dgv_requirement_analy.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // skinTabPage4
            // 
            this.skinTabPage4.BackColor = System.Drawing.Color.White;
            this.skinTabPage4.BorderColor = System.Drawing.Color.Silver;
            this.skinTabPage4.Controls.Add(this.dgv_consump_ana);
            this.skinTabPage4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skinTabPage4.Location = new System.Drawing.Point(112, 0);
            this.skinTabPage4.Name = "skinTabPage4";
            this.skinTabPage4.Size = new System.Drawing.Size(991, 541);
            this.skinTabPage4.TabIndex = 3;
            this.skinTabPage4.TabItemImage = null;
            this.skinTabPage4.Text = "消费分析";
            // 
            // dgv_consump_ana
            // 
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.dgv_consump_ana.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle61;
            this.dgv_consump_ana.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
            this.dgv_consump_ana.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_consump_ana.ColumnFont = null;
            this.dgv_consump_ana.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle62.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle62.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle62.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_consump_ana.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle62;
            this.dgv_consump_ana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_consump_ana.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle63.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle63.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle63.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle63.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle63.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle63.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_consump_ana.DefaultCellStyle = dataGridViewCellStyle63;
            this.dgv_consump_ana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_consump_ana.EnableHeadersVisualStyles = false;
            this.dgv_consump_ana.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgv_consump_ana.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dgv_consump_ana.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_consump_ana.Location = new System.Drawing.Point(0, 0);
            this.dgv_consump_ana.Name = "dgv_consump_ana";
            this.dgv_consump_ana.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_consump_ana.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle64.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.dgv_consump_ana.RowsDefaultCellStyle = dataGridViewCellStyle64;
            this.dgv_consump_ana.RowTemplate.Height = 23;
            this.dgv_consump_ana.Size = new System.Drawing.Size(991, 541);
            this.dgv_consump_ana.TabIndex = 3;
            this.dgv_consump_ana.TitleBack = null;
            this.dgv_consump_ana.TitleBackColorBegin = System.Drawing.Color.White;
            this.dgv_consump_ana.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(5, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 19);
            this.label3.TabIndex = 30;
            this.label3.Text = "微信";
            // 
            // lb_ms_chiwx
            // 
            this.lb_ms_chiwx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_ms_chiwx.Back = null;
            this.lb_ms_chiwx.BackColor = System.Drawing.Color.LightGray;
            this.lb_ms_chiwx.BorderColor = System.Drawing.Color.Transparent;
            this.lb_ms_chiwx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb_ms_chiwx.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lb_ms_chiwx.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_ms_chiwx.FormattingEnabled = true;
            this.lb_ms_chiwx.ImageVisble = false;
            this.lb_ms_chiwx.IntegralHeight = false;
            this.lb_ms_chiwx.ItemHeight = 35;
            this.lb_ms_chiwx.Location = new System.Drawing.Point(1, 72);
            this.lb_ms_chiwx.MouseColor = System.Drawing.Color.Silver;
            this.lb_ms_chiwx.Name = "lb_ms_chiwx";
            this.lb_ms_chiwx.RowBackColor1 = System.Drawing.Color.Transparent;
            this.lb_ms_chiwx.RowBackColor2 = System.Drawing.Color.Transparent;
            this.lb_ms_chiwx.SelectedColor = System.Drawing.Color.Silver;
            this.lb_ms_chiwx.Size = new System.Drawing.Size(112, 541);
            this.lb_ms_chiwx.TabIndex = 29;
            this.lb_ms_chiwx.SelectedValueChanged += new System.EventHandler(this.lb_ms_chiwx_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(112, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 19);
            this.label1.TabIndex = 31;
            this.label1.Text = "功能";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(1113, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 19);
            this.label2.TabIndex = 32;
            this.label2.Text = "导出excel";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // costum_analy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.Transparent;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(1218, 614);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lb_ms_chiwx);
            this.Controls.Add(this.skinTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InheritBack = true;
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "costum_analy";
            this.Shadow = true;
            this.Text = "客户分析";
            this.Load += new System.EventHandler(this.costum_analy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_basic_ana)).EndInit();
            this.skinTabControl1.ResumeLayout(false);
            this.skinTabPage1.ResumeLayout(false);
            this.skinTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cicrcle_friend)).EndInit();
            this.skinTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_requirement_analy)).EndInit();
            this.skinTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_consump_ana)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinDataGridView dgv_basic_ana;
        private CCWin.SkinControl.SkinTabControl skinTabControl1;
        private CCWin.SkinControl.SkinTabPage skinTabPage1;
        private CCWin.SkinControl.SkinTabPage skinTabPage2;
        private CCWin.SkinControl.SkinTabPage skinTabPage3;
        private CCWin.SkinControl.SkinTabPage skinTabPage4;
        private CCWin.SkinControl.SkinDataGridView dgv_cicrcle_friend;
        private CCWin.SkinControl.SkinDataGridView dgv_requirement_analy;
        private CCWin.SkinControl.SkinDataGridView dgv_consump_ana;
        private System.Windows.Forms.Label label3;
        private CCWin.SkinControl.SkinListBox lb_ms_chiwx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}