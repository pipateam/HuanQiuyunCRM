﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using HuanQiuCrm.Properties;
using System.Collections;
using WChat;

namespace HuanQiuCrm {
    public partial class SttingForm : Skin_Color {
        //注册时用的数据库参数
        string tempyesno;
        private Settings st = new Settings();
        private SqlHandle sql = new SqlHandle();
        private SqlTest ss = new SqlTest();
        private string ieSetuppath = System.Environment.CurrentDirectory + "\\";
        public SttingForm() {
            InitializeComponent();
        }

        #region Label按钮
        //按钮1单击事件
        private void label1_Click(object sender, EventArgs e)
        {
            SetcontrolClickColor(label1, lableState);
        }

        private string lableState = string.Empty;//记录那个按钮被点击

        //划入效果
        private void SetControlBackColor(Label l, string s)
        {
            if (!l.Text.Equals(s))
            {
                l.BackColor = Color.Honeydew;
                l.ForeColor = Color.Purple;
                //l.Font = new Font("微软雅黑",12,FontStyle.Bold);
            }
        }
        //移出效果
        private void SetControlForeColor(Label l, string s)
        {
            if (!l.Text.Equals(s))
            {
                l.BackColor = Color.Transparent;
                l.ForeColor = Color.Black;
            }
        }
        //点击效果
        private void SetcontrolClickColor(Label l, string s)
        {
            if (!l.Text.Equals(s))
            {
                ClearColor();
                lableState = l.Text;
                l.BackColor = Color.Honeydew;
                l.ForeColor = Color.Purple;
                ShowPanel(lableState);
            }
        }
        //清除效果
        private void ClearColor()
        {
            this.label1.BackColor = this.label2.BackColor = this.label3.BackColor = Color.Transparent;
            this.label1.ForeColor = this.label2.ForeColor = this.label3.ForeColor = Color.Black;
        }
        //panel显示
        private void ShowPanel( string s)
        {
            visiblepan();
            switch (s)
            {
                case "基本设置":
                    panel1.Visible = true;
                    break;
                case "文件设置":
                    panel2.Visible = true;
                    break;
                case "敏感词":
                    Pal_Sensitive_Words.Visible = true;
                    break;
            }
        }
        //panel事件
        private void visiblepan()
        {
            panel1.Visible = panel2.Visible = Pal_Sensitive_Words.Visible = false;
        }

        //按钮1划入事件
        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            SetControlBackColor(label1, lableState);
        }
        //按钮1移出事件
        private void label1_MouseLeave(object sender, EventArgs e)
        {
            SetControlForeColor(label1, lableState);
        }
        private void label2_Click(object sender, EventArgs e)
        {
            SetcontrolClickColor(this.label2, lableState);
        }
        private void label2_MouseMove(object sender, MouseEventArgs e)
        {
            SetControlBackColor(label2, lableState);
        }
        private void label2_MouseLeave(object sender, EventArgs e)
        {
            SetControlForeColor(this.label2, lableState);
        }
        /// <summary>
        /// 铭感词
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label3_Click(object sender, EventArgs e)
        {
            this.Pal_Sensitive_Words.Visible = true;
            panel1.Visible = false;
            panel2.Visible = false;
            SetcontrolClickColor(this.label3, lableState);
        }
        private void label3_MouseMove(object sender, MouseEventArgs e)
        {
            SetControlBackColor(label3, lableState);
        }

        private void label3_MouseLeave(object sender, EventArgs e)
        {
            SetControlForeColor(label3, lableState);
        }

        #endregion

        //应用按钮
        private void btn_yingyong_Click(object sender, EventArgs e)
        {
            queding();
        }

        //应用方法
        private void queding()
        {
            try
            {
                Settings st = new Settings();
                string path = this.textBox1.Text.Trim();
                st.path = WChat.path.paths = path;
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                st.Save();
            }
            catch (Exception)
            {
                return;
            }
            if (this.cboAutoUpdate.Checked == true)
                new UpdateXML().SetYesOrNo("0");
            else
                new UpdateXML().SetYesOrNo("1");
        }

        //浏览按钮
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.ShowDialog();
                if(!fbd.SelectedPath.Equals(string.Empty))
                    this.textBox1.Text = fbd.SelectedPath;
            }
            catch (Exception)
            {
            }
        }

        //确定按钮
        private void btn_ok_Click(object sender, EventArgs e)
        {
            queding();
            this.Close();
        }

        //历史版本按钮
        private void btnSearchVer_Click(object sender, EventArgs e)
        {
            HistoryVer hv = new HistoryVer();
            hv.ShowDialog();
        }

        private void OpenSetupExe(string path)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(path);
                info.WorkingDirectory = System.IO.Path.GetDirectoryName(path);
                System.Diagnostics.Process.Start(info);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(new WebBrowser().Version.ToString().Substring(0, 1)) >= 9) {
                    MessageBox.Show("当前IE版本不需要升级！");
                    return;
                }
                if (System.Environment.Is64BitOperatingSystem) {
                    string x64 = "IE9-Windows7-x64-chs.exe";
                    
                    if (!System.IO.File.Exists(ieSetuppath + x64)) {
                        MessageBox.Show("目标文件不存在或已被移除！！");
                        return;
                    }
                    else
                        OpenSetupExe(ieSetuppath + x64);
                }
                else {
                    string x86 = "IE9-Windows7-x32-chs.exe";
                    if (!System.IO.File.Exists(ieSetuppath + x86)) {
                        MessageBox.Show("目标文件不存在或已被移除！！");
                        return;
                    }
                    else
                        OpenSetupExe(ieSetuppath + x86);
                }
            }
            catch (Exception)
            {

            }
        }

        //取消按钮
        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //此按钮为不能升级用，降低版本信息
            new UpdateXML().setVer("1.0.0.1.10");
        }

        //窗体加载事件
        private void SttingForm_Load(object sender, EventArgs e)
        {
            this.button1.Visible = false;
            this.textBox1.ReadOnly = true;
            this.label1.BackColor = Color.Honeydew;
            this.label1.ForeColor = Color.Purple;
            visiblepan();
            this.panel1.Visible = true;

            tempyesno = new UpdateXML().SearchYseOrNo();
            if (tempyesno.Equals("1"))
                this.cboAutoUpdate.Checked = false;
            else
                this.cboAutoUpdate.Checked = true;

            #region 当前路径
            this.textBox1.Text = WChat.path.paths;
            #endregion

        }

        //检测更新按钮
        private void btn_update_Click(object sender, EventArgs e)
        {
            if (new UpdateXML().IsUpdate())
            {
                DialogResult dr = MessageBox.Show("检测到新版本，是否更新", "系统提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    new SelectList().Show();
                    new UpdateXML().SetYesOrNo("0");
                }
            }
            else
                MessageBox.Show("软件已经是最新版本");
        }

    }
}
