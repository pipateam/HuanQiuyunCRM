﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
//控制文件打开或关闭的命名空间
using System.Diagnostics;
using CCWin;
using System.Xml;

namespace HuanQiuCrm
{
    public partial class SelectList : Skin_Color
    {
        
        public SelectList()
        {
            InitializeComponent();
        }
        UpdateXML ux = new UpdateXML();
        string FileName = string.Empty;
        string Suffix = string.Empty;
        string updateUrl = string.Empty;
        string savePath = WChat.path.paths;
        string url = null;
        string ss = null;
        string Ver = null;
        //搜索更新列表
        private void SelectList_Load(object sender, EventArgs e)
        {
            //获取升级文件名 和 地址
            ux.read(ref FileName,ref Suffix,ref updateUrl,ref Ver);
            url = updateUrl + FileName + Suffix;
            savePath = savePath + "\\" + FileName + Suffix;
            XmlDocument xd2 = new XmlDocument();   //创建一个读取网页版xml
            xd2.Load("http://lidaliang2016.oss-cn-qingdao.aliyuncs.com/UpdateXML.xml");    //读取网络的xml
            XmlNodeList xnl2 = xd2.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl2) {
                XmlElement xe = (XmlElement)item;
                if (xe.Name == "Info") {
                    ss = xe.GetAttribute("descri");
                    break;
                }
            }
            lb_update_desc.Text = ss;
        }
     
        //下载代码
        private void FileDown(string URL,string savePath, System.Windows.Forms.ProgressBar prog, System.Windows.Forms.Label label1)
        {
            if(File.Exists(savePath))
            {
                File.Delete(savePath);
                ////打开指定位置文件
                //Process p = Process.Start(savePath);
                
                //p.WaitForExit();

                //ux.setVer(Ver);

                Application.Exit();
                return;
            }
            float percent = 0;
            try
            {
                System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
                System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
                long totalBytes = myrp.ContentLength;
                if (prog != null)
                {
                    prog.Maximum = (int)totalBytes;
                }
                System.IO.Stream st = myrp.GetResponseStream();
                System.IO.Stream so = new System.IO.FileStream(savePath, System.IO.FileMode.Create);
                long totalDownloadedByte = 0;
                byte[] by = new byte[1024];
                int osize = st.Read(by, 0, (int)by.Length);
                while (osize > 0)
                {
                    totalDownloadedByte = osize + totalDownloadedByte;
                    System.Windows.Forms.Application.DoEvents();
                    so.Write(by, 0, osize);
                    if (prog != null)
                    {
                        prog.Value = (int)totalDownloadedByte;
                    }
                    osize = st.Read(by, 0, (int)by.Length);

                    percent = (float)totalDownloadedByte / (float)totalBytes * 100;
                    //label1.Text = "当前补丁下载进度" + Convert.ToInt32(percent).ToString() + "%";
                    label1.Text = Convert.ToInt32(percent).ToString() + "%";
                    System.Windows.Forms.Application.DoEvents(); //必须加注这句代码，否则label1将因为循环执行太快而来不及显示信息
                }
                so.Close();
                st.Close();

                //打开指定位置文件
                Process p = Process.Start(savePath);
                p.WaitForExit();
                ux.setVer(Ver);
            }
            catch (System.Exception)
            {
                throw;
            }
            Application.Exit();
        }

        private void btn_updata_Click(object sender, EventArgs e) {
            this.btn_updata.Enabled = false;
            FileDown(url, savePath, this.skinProgressBar1, this.label2);
        }
        //取消更新按钮
        private void btn_conel_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
