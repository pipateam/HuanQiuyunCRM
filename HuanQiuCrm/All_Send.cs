﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WChat;
using System.Threading;
using System.Windows.Forms;

namespace HuanQiuCrm
{
    class All_Send
    {
        public All_Send(string photopath, int type, int num, List<string> ToUserNicList, List<WXUser> sure_me, string wx_name, StringBuilder recipient, string content, string time)
        {
            this.photopath = photopath;
            this.type = type;
            this.num = num;
            this.ToUserNicList = ToUserNicList;
            this.sure_me = sure_me;
            this.wx_name = wx_name;
            this.recipient = recipient;
            this.content = content;
            this.time = time;
        }
        string photopath = null;
        int type = 0;
        int num = 0;
        List<string> ToUserNicList = null;
        List<WXUser> sure_me = null;
        string wx_name = null;
        StringBuilder recipient = null;
        string content = null;
        string time = null;
        SqlHandle sql = new SqlHandle();
        Properties.Settings st = new Properties.Settings();
        public void Send() {
            WXMsg msg = new WXMsg();
            try {
                string my_userNam = sql.searchMyUserName(wx_name, st.UseAcct);
                if (ToUserNicList != null && my_userNam != string.Empty) {
                    for (int j = 0; j < ToUserNicList.Count; j++) {
                        string send_userNam = sql.Select_Send_User(wx_name, ToUserNicList[j], st.UseAcct);
                        if (send_userNam != string.Empty) {
                            msg.Msg = content;
                            msg.From = my_userNam;
                            msg.To = send_userNam;
                            msg.Type = "1";
                            msg.Time = DateTime.Now;
                            msg.FileName = photopath;
                            WXUser user = new WXUser();
                            user.UserName = msg.To;
                            if (type == 1) {
                                sure_me[num].Syc_SendMsg(sure_me[num].NickName, msg);
                                sure_me[num].SendMsgToServer(msg);
                                sql.insert_AllSend_data(st.UseAcct, msg.Time.ToString(), wx_name, ToUserNicList[j], msg.Msg, 1);
                            } else {
                                sure_me[num].SendFiles(sure_me[num], user, msg.FileName);   //发送文件
                                sql.insert_AllSend_data(st.UseAcct, msg.Time.ToString(), wx_name, ToUserNicList[j], photopath, 3);
                            }
                            Thread.Sleep(Convert.ToInt32(int.Parse(time) * 1000));//时间间隔
                        }
                    }
                }
            } catch (Exception e) {
                WriteLog.Writelog("群发", e.ToString());
            }
        }  
    }
}
