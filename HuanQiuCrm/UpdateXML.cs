﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Web;

namespace HuanQiuCrm
{
    public class UpdateXML
    {

        private string Url = null;
        
        //查找更新日志内容
        public string SearchDescri()
        {
            string temp = string.Empty;
            string path = "http://lidaliang2016.oss-cn-qingdao.aliyuncs.com/UpdateXML.xml";
            XmlDocument xdt = new XmlDocument();
            xdt.Load(path);
            XmlNodeList xnl = xdt.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl)
            {
                XmlElement xe = (XmlElement)item;
                if (xe.Name.Equals("Info"))
                {
                    temp = xe.GetAttribute("descri");
                    break;
                }
            }
            return temp;
        }


        //查看当前版本号
        public string SearchVer()
        {
            string ver = string.Empty;
            XmlDocument xdt = new XmlDocument();
            xdt.Load(@"UpdateXML.xml");
            XmlNodeList xnl = xdt.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl)
            {
                XmlElement xet = (XmlElement)item;
                if (xet.Name.Equals("Version"))
                {
                    ver = xet.GetAttribute("ver");
                    break;
                }
            }
            return ver;
        }
        //查找用户是否想升级软件的关键字1为不想升级，0为想升级
        public string SearchYseOrNo()
        {
            string temp = string.Empty;
            XmlDocument xdt = new XmlDocument();
            xdt.Load(@"UpdateXML.xml");
            XmlNodeList xnl = xdt.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl)
            {
                XmlElement xet = (XmlElement)item;
                XmlNodeList xnl2 = xet.ChildNodes;
                foreach (XmlNode item2 in xnl2)
                {
                    XmlElement xet2 = (XmlElement)item2;
                    if (xet2.Name.Equals("YesOrNo"))
                    {
                        temp = xet2.InnerText;
                        break;
                    }
                }
            }
            return temp;
        }
        //设置用户是否想更新软件
        public void SetYesOrNo(string yesno)
        {
            XmlDocument xdt = new XmlDocument();
            xdt.Load(@"UpdateXML.xml");
            XmlNodeList xnl = xdt.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl)
            {
                XmlElement xet = (XmlElement)item;
                XmlNodeList xnl2 = xet.ChildNodes;
                foreach (XmlNode item2 in xnl2)
                {
                    XmlElement xet2 = (XmlElement)item2;
                    if(xet2.Name.Equals("YesOrNo"))
                    {
                        xet2.InnerText = yesno;
                        break;
                    }
                }
            }
            xdt.Save(@"UpdateXML.xml");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="FileName">文件名</param>
        /// <param name="Suffix">后缀名</param>
        /// <param name="UpdateUrl">升级地址</param>
        public void read(ref string FileName,ref string Suffix,ref string UpdateUrl,ref string Ver)
        {
            string tmpurl = null;
            XmlDocument xd = new XmlDocument();
            xd.Load(@"UpdateXML.xml");
            XmlNodeList xnl = xd.SelectSingleNode("File").ChildNodes;

            foreach (XmlNode item in xnl)
            {
                XmlElement xe = (XmlElement)item;
                if (xe.Name == "FileName")
                    FileName = xe.GetAttribute("name");
                if (xe.Name == "Url")
                    tmpurl = xe.GetAttribute("address");
                XmlNodeList xn = xe.ChildNodes;
                foreach (XmlNode it in xn)
                {
                    if (it.Name == "Suffix")
                        Suffix = it.InnerText;
                    if (it.Name == "UpdateUrl")
                    {
                        UpdateUrl = it.InnerText;
                        break;
                     }   
                }
            }

            XmlDocument xd2 = new XmlDocument();
            xd2.Load(tmpurl);
            XmlNodeList xdl = xd2.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xdl)
            {
                XmlElement xe = (XmlElement)item;
                if (xe.Name == "Version")
                { Ver = xe.GetAttribute("ver"); break; }
            }
        }

        //查找是否可升级
        public bool IsUpdate()
        {
            string ver = null;
            string updatever = null;
            XmlDocument xd = new XmlDocument();
            xd.Load(@"UpdateXML.xml");
            XmlNodeList xnl = xd.SelectSingleNode("File").ChildNodes;

           
            foreach (XmlNode item in xnl)
            {
                XmlElement xe = (XmlElement)item;
                if (xe.Name == "Url")
                    Url = xe.GetAttribute("address");
                if (xe.Name == "Version")
                    ver = xe.GetAttribute("ver");
            }


            XmlDocument xd2 = new XmlDocument();
            xd2.Load(Url);
            XmlNodeList xnl2 = xd2.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl2)
            {
                XmlElement xe = (XmlElement)item;
                if (xe.Name == "Version")
                {
                    updatever = xe.GetAttribute("ver");
                    break;
                }
            }
            //判断本地版本号 是否小于 网络版本号
            return Convert.ToInt32(upVer(updatever)) > Convert.ToInt32(upVer(ver)) ? true : false;
        }

        //截取版本号
        public string upVer(string ver)
        {
            string tmp = null;
            foreach (char item in ver)
            {
                if (ver.IndexOf(".") > 0)
                {
                    tmp += ver.Substring(0, ver.IndexOf("."));
                    ver = ver.Substring(ver.IndexOf(".") + 1);
                }
                else
                {
                    tmp += ver;
                    break;
                }
            }
            return tmp;
        }
        
        //设置版本号
        public void setVer(string ver)
        {
            XmlDocument xd = new XmlDocument();
            xd.Load(@"UpdateXML.xml");
            XmlNodeList xnl = xd.SelectSingleNode("File").ChildNodes;
            foreach (XmlNode item in xnl)
            {
                XmlElement xe = (XmlElement)item;
                if (xe.Name == "Version")
                {
                    xe.SetAttribute("ver", ver);
                    break;
                }
            }
            xd.Save(@"UpdateXML.xml");
        }
    }
}
