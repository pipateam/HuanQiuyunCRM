﻿namespace HuanQiuCrm {
    partial class loginwx {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loginwx));
            this.Pqrcode = new CCWin.SkinControl.SkinPictureBox();
            this.lblTip = new CCWin.SkinControl.SkinLabel();
            this.CB_phone = new CCWin.SkinControl.SkinComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lb_autos_can = new System.Windows.Forms.Label();
            this.lb_swich = new CCWin.SkinControl.SkinLabel();
            this.btn_login = new CCWin.SkinControl.SkinButton();
            ((System.ComponentModel.ISupportInitialize)(this.Pqrcode)).BeginInit();
            this.SuspendLayout();
            // 
            // Pqrcode
            // 
            this.Pqrcode.BackColor = System.Drawing.Color.Transparent;
            this.Pqrcode.Location = new System.Drawing.Point(51, 65);
            this.Pqrcode.Name = "Pqrcode";
            this.Pqrcode.Size = new System.Drawing.Size(177, 169);
            this.Pqrcode.TabIndex = 0;
            this.Pqrcode.TabStop = false;
            // 
            // lblTip
            // 
            this.lblTip.BackColor = System.Drawing.Color.Transparent;
            this.lblTip.BorderColor = System.Drawing.Color.White;
            this.lblTip.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTip.Location = new System.Drawing.Point(67, 251);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(128, 17);
            this.lblTip.TabIndex = 1;
            this.lblTip.Text = "手机微信扫一扫以登录";
            this.lblTip.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CB_phone
            // 
            this.CB_phone.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CB_phone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CB_phone.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CB_phone.FormattingEnabled = true;
            this.CB_phone.Location = new System.Drawing.Point(52, 251);
            this.CB_phone.MouseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CB_phone.MouseGradientColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CB_phone.Name = "CB_phone";
            this.CB_phone.Size = new System.Drawing.Size(177, 22);
            this.CB_phone.TabIndex = 4;
            this.CB_phone.Visible = false;
            this.CB_phone.WaterText = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(111, 356);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "test";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lb_autos_can
            // 
            this.lb_autos_can.AutoSize = true;
            this.lb_autos_can.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lb_autos_can.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.lb_autos_can.Location = new System.Drawing.Point(187, 251);
            this.lb_autos_can.Name = "lb_autos_can";
            this.lb_autos_can.Size = new System.Drawing.Size(44, 17);
            this.lb_autos_can.TabIndex = 7;
            this.lb_autos_can.Text = "自动扫";
            this.lb_autos_can.Click += new System.EventHandler(this.lb_autos_can_Click);
            // 
            // lb_swich
            // 
            this.lb_swich.BackColor = System.Drawing.Color.Transparent;
            this.lb_swich.BorderColor = System.Drawing.Color.White;
            this.lb_swich.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_swich.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.lb_swich.Location = new System.Drawing.Point(80, 328);
            this.lb_swich.Name = "lb_swich";
            this.lb_swich.Size = new System.Drawing.Size(115, 17);
            this.lb_swich.TabIndex = 2;
            this.lb_swich.Text = "切换账号";
            this.lb_swich.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_swich.Click += new System.EventHandler(this.skinLabel1_Click);
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_login.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_login.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_login.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_login.DownBack = null;
            this.btn_login.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(186)))), ((int)(((byte)(20)))));
            this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_login.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_login.ForeColor = System.Drawing.Color.White;
            this.btn_login.GlowColor = System.Drawing.Color.Transparent;
            this.btn_login.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_login.IsDrawBorder = false;
            this.btn_login.IsDrawGlass = false;
            this.btn_login.Location = new System.Drawing.Point(39, 285);
            this.btn_login.MouseBack = null;
            this.btn_login.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_login.Name = "btn_login";
            this.btn_login.NormlBack = null;
            this.btn_login.Size = new System.Drawing.Size(207, 31);
            this.btn_login.TabIndex = 5;
            this.btn_login.Text = "登录";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // loginwx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CanResize = false;
            this.ClientSize = new System.Drawing.Size(279, 386);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.CB_phone);
            this.Controls.Add(this.lb_swich);
            this.Controls.Add(this.lblTip);
            this.Controls.Add(this.Pqrcode);
            this.Controls.Add(this.lb_autos_can);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MdiBorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MinimizeBox = false;
            this.Name = "loginwx";
            this.Shadow = true;
            this.ShadowWidth = 9;
            this.ShowBorder = false;
            this.ShowDrawIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "微信";
            this.TitleCenter = false;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.loginwx_FormClosed);
            this.Load += new System.EventHandler(this.loginwx_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Pqrcode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinPictureBox Pqrcode;
        private CCWin.SkinControl.SkinLabel lblTip;
        private CCWin.SkinControl.SkinComboBox CB_phone;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lb_autos_can;
        private CCWin.SkinControl.SkinLabel lb_swich;
        private CCWin.SkinControl.SkinButton btn_login;
    }
}