﻿namespace HuanQiuCrm {
    partial class Form1 {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.skinButton1 = new CCWin.SkinControl.SkinButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Consult = new CCWin.SkinControl.SkinButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_costum_analysls = new CCWin.SkinControl.SkinButton();
            this.btn_statistic_anal = new CCWin.SkinControl.SkinButton();
            this.btn_message_manege = new CCWin.SkinControl.SkinButton();
            this.tbn_AllSend = new CCWin.SkinControl.SkinButton();
            this.skinButton8 = new CCWin.SkinControl.SkinButton();
            this.btn_about = new CCWin.SkinControl.SkinButton();
            this.write_off = new CCWin.SkinControl.SkinButton();
            this.label10 = new System.Windows.Forms.Label();
            this.robot_reply = new CCWin.SkinControl.SkinButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.show_msg = new System.Windows.Forms.NotifyIcon(this.components);
            this.cms_right_msg = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于环球云ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出软件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wxTabContral1 = new WChat.WXTabContral();
            this.label11 = new System.Windows.Forms.Label();
            this.btn_group = new CCWin.SkinControl.SkinButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.cms_right_msg.SuspendLayout();
            this.SuspendLayout();
            // 
            // skinButton1
            // 
            this.skinButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skinButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinButton1.BaseColor = System.Drawing.Color.Transparent;
            this.skinButton1.BorderColor = System.Drawing.Color.Transparent;
            this.skinButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton1.DownBack = null;
            this.skinButton1.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.skinButton1.GlowColor = System.Drawing.Color.Transparent;
            this.skinButton1.Image = ((System.Drawing.Image)(resources.GetObject("skinButton1.Image")));
            this.skinButton1.ImageSize = new System.Drawing.Size(25, 25);
            this.skinButton1.InnerBorderColor = System.Drawing.Color.Transparent;
            this.skinButton1.IsDrawBorder = false;
            this.skinButton1.IsDrawGlass = false;
            this.skinButton1.Location = new System.Drawing.Point(513, 40);
            this.skinButton1.MouseBack = null;
            this.skinButton1.MouseBaseColor = System.Drawing.Color.Transparent;
            this.skinButton1.Name = "skinButton1";
            this.skinButton1.NormlBack = null;
            this.skinButton1.Size = new System.Drawing.Size(25, 25);
            this.skinButton1.TabIndex = 2;
            this.skinButton1.UseVisualStyleBackColor = false;
            this.skinButton1.Click += new System.EventHandler(this.skinButton1_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(38, 38);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "7004122719814189192small.jpg");
            this.imageList2.Images.SetKeyName(1, "2.jpg");
            // 
            // skinLabel1
            // 
            this.skinLabel1.ArtTextStyle = CCWin.SkinControl.ArtTextStyle.None;
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.ForeColor = System.Drawing.Color.White;
            this.skinLabel1.Location = new System.Drawing.Point(80, 36);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(221, 25);
            this.skinLabel1.TabIndex = 7;
            this.skinLabel1.Text = "欢迎使用环球云客服系统";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1133, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "注销";
            // 
            // btn_Consult
            // 
            this.btn_Consult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Consult.BackColor = System.Drawing.Color.Transparent;
            this.btn_Consult.BaseColor = System.Drawing.Color.Transparent;
            this.btn_Consult.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Consult.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Consult.DownBack = null;
            this.btn_Consult.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_Consult.GlowColor = System.Drawing.Color.Transparent;
            this.btn_Consult.Image = ((System.Drawing.Image)(resources.GetObject("btn_Consult.Image")));
            this.btn_Consult.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_Consult.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_Consult.IsDrawBorder = false;
            this.btn_Consult.IsDrawGlass = false;
            this.btn_Consult.Location = new System.Drawing.Point(683, 34);
            this.btn_Consult.MouseBack = null;
            this.btn_Consult.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_Consult.Name = "btn_Consult";
            this.btn_Consult.NormlBack = null;
            this.btn_Consult.Size = new System.Drawing.Size(39, 36);
            this.btn_Consult.TabIndex = 10;
            this.btn_Consult.UseVisualStyleBackColor = false;
            this.btn_Consult.Click += new System.EventHandler(this.btn_Consult_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(1071, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "关于";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(1007, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "设置";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(868, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "消息管理";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(934, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "群发助手";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(802, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "统计分析";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(739, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "客户分析";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(683, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "敏感词";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(618, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 17);
            this.label9.TabIndex = 18;
            this.label9.Text = "机器人";
            // 
            // btn_costum_analysls
            // 
            this.btn_costum_analysls.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_costum_analysls.BackColor = System.Drawing.Color.Transparent;
            this.btn_costum_analysls.BaseColor = System.Drawing.Color.Transparent;
            this.btn_costum_analysls.BorderColor = System.Drawing.Color.Transparent;
            this.btn_costum_analysls.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_costum_analysls.DownBack = null;
            this.btn_costum_analysls.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_costum_analysls.GlowColor = System.Drawing.Color.Transparent;
            this.btn_costum_analysls.Image = ((System.Drawing.Image)(resources.GetObject("btn_costum_analysls.Image")));
            this.btn_costum_analysls.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_costum_analysls.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_costum_analysls.IsDrawBorder = false;
            this.btn_costum_analysls.IsDrawGlass = false;
            this.btn_costum_analysls.Location = new System.Drawing.Point(747, 34);
            this.btn_costum_analysls.MouseBack = null;
            this.btn_costum_analysls.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_costum_analysls.Name = "btn_costum_analysls";
            this.btn_costum_analysls.NormlBack = null;
            this.btn_costum_analysls.Size = new System.Drawing.Size(39, 36);
            this.btn_costum_analysls.TabIndex = 19;
            this.btn_costum_analysls.UseVisualStyleBackColor = false;
            this.btn_costum_analysls.Click += new System.EventHandler(this.btn_costum_analysls_Click);
            // 
            // btn_statistic_anal
            // 
            this.btn_statistic_anal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_statistic_anal.BackColor = System.Drawing.Color.Transparent;
            this.btn_statistic_anal.BaseColor = System.Drawing.Color.Transparent;
            this.btn_statistic_anal.BorderColor = System.Drawing.Color.Transparent;
            this.btn_statistic_anal.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_statistic_anal.DownBack = null;
            this.btn_statistic_anal.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_statistic_anal.GlowColor = System.Drawing.Color.Transparent;
            this.btn_statistic_anal.Image = ((System.Drawing.Image)(resources.GetObject("btn_statistic_anal.Image")));
            this.btn_statistic_anal.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_statistic_anal.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_statistic_anal.IsDrawBorder = false;
            this.btn_statistic_anal.IsDrawGlass = false;
            this.btn_statistic_anal.Location = new System.Drawing.Point(811, 34);
            this.btn_statistic_anal.MouseBack = null;
            this.btn_statistic_anal.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_statistic_anal.Name = "btn_statistic_anal";
            this.btn_statistic_anal.NormlBack = null;
            this.btn_statistic_anal.Size = new System.Drawing.Size(39, 36);
            this.btn_statistic_anal.TabIndex = 20;
            this.btn_statistic_anal.UseVisualStyleBackColor = false;
            this.btn_statistic_anal.Click += new System.EventHandler(this.btn_statistic_anal_Click);
            // 
            // btn_message_manege
            // 
            this.btn_message_manege.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_message_manege.BackColor = System.Drawing.Color.Transparent;
            this.btn_message_manege.BaseColor = System.Drawing.Color.Transparent;
            this.btn_message_manege.BorderColor = System.Drawing.Color.Transparent;
            this.btn_message_manege.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_message_manege.DownBack = null;
            this.btn_message_manege.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_message_manege.GlowColor = System.Drawing.Color.Transparent;
            this.btn_message_manege.Image = ((System.Drawing.Image)(resources.GetObject("btn_message_manege.Image")));
            this.btn_message_manege.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_message_manege.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_message_manege.IsDrawBorder = false;
            this.btn_message_manege.IsDrawGlass = false;
            this.btn_message_manege.Location = new System.Drawing.Point(875, 34);
            this.btn_message_manege.MouseBack = null;
            this.btn_message_manege.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_message_manege.Name = "btn_message_manege";
            this.btn_message_manege.NormlBack = null;
            this.btn_message_manege.Size = new System.Drawing.Size(39, 36);
            this.btn_message_manege.TabIndex = 21;
            this.btn_message_manege.UseVisualStyleBackColor = false;
            this.btn_message_manege.Click += new System.EventHandler(this.btn_message_manege_Click);
            // 
            // tbn_AllSend
            // 
            this.tbn_AllSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbn_AllSend.BackColor = System.Drawing.Color.Transparent;
            this.tbn_AllSend.BaseColor = System.Drawing.Color.Transparent;
            this.tbn_AllSend.BorderColor = System.Drawing.Color.Transparent;
            this.tbn_AllSend.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.tbn_AllSend.DownBack = null;
            this.tbn_AllSend.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.tbn_AllSend.GlowColor = System.Drawing.Color.Transparent;
            this.tbn_AllSend.Image = ((System.Drawing.Image)(resources.GetObject("tbn_AllSend.Image")));
            this.tbn_AllSend.ImageSize = new System.Drawing.Size(25, 25);
            this.tbn_AllSend.InnerBorderColor = System.Drawing.Color.Transparent;
            this.tbn_AllSend.IsDrawBorder = false;
            this.tbn_AllSend.IsDrawGlass = false;
            this.tbn_AllSend.Location = new System.Drawing.Point(939, 34);
            this.tbn_AllSend.MouseBack = null;
            this.tbn_AllSend.MouseBaseColor = System.Drawing.Color.Transparent;
            this.tbn_AllSend.Name = "tbn_AllSend";
            this.tbn_AllSend.NormlBack = null;
            this.tbn_AllSend.Size = new System.Drawing.Size(39, 36);
            this.tbn_AllSend.TabIndex = 22;
            this.tbn_AllSend.UseVisualStyleBackColor = false;
            this.tbn_AllSend.Click += new System.EventHandler(this.tbn_AllSend_Click);
            // 
            // skinButton8
            // 
            this.skinButton8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.skinButton8.BackColor = System.Drawing.Color.Transparent;
            this.skinButton8.BaseColor = System.Drawing.Color.Transparent;
            this.skinButton8.BorderColor = System.Drawing.Color.Transparent;
            this.skinButton8.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton8.DownBack = null;
            this.skinButton8.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.skinButton8.GlowColor = System.Drawing.Color.Transparent;
            this.skinButton8.Image = ((System.Drawing.Image)(resources.GetObject("skinButton8.Image")));
            this.skinButton8.ImageSize = new System.Drawing.Size(25, 25);
            this.skinButton8.InnerBorderColor = System.Drawing.Color.Transparent;
            this.skinButton8.IsDrawBorder = false;
            this.skinButton8.IsDrawGlass = false;
            this.skinButton8.Location = new System.Drawing.Point(1003, 33);
            this.skinButton8.MouseBack = null;
            this.skinButton8.MouseBaseColor = System.Drawing.Color.Transparent;
            this.skinButton8.Name = "skinButton8";
            this.skinButton8.NormlBack = null;
            this.skinButton8.Size = new System.Drawing.Size(39, 36);
            this.skinButton8.TabIndex = 23;
            this.skinButton8.UseVisualStyleBackColor = false;
            this.skinButton8.Click += new System.EventHandler(this.skinButton2_Click);
            // 
            // btn_about
            // 
            this.btn_about.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_about.BackColor = System.Drawing.Color.Transparent;
            this.btn_about.BaseColor = System.Drawing.Color.Transparent;
            this.btn_about.BorderColor = System.Drawing.Color.Transparent;
            this.btn_about.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_about.DownBack = null;
            this.btn_about.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_about.GlowColor = System.Drawing.Color.Transparent;
            this.btn_about.Image = ((System.Drawing.Image)(resources.GetObject("btn_about.Image")));
            this.btn_about.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_about.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_about.IsDrawBorder = false;
            this.btn_about.IsDrawGlass = false;
            this.btn_about.Location = new System.Drawing.Point(1067, 34);
            this.btn_about.MouseBack = null;
            this.btn_about.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_about.Name = "btn_about";
            this.btn_about.NormlBack = null;
            this.btn_about.Size = new System.Drawing.Size(39, 35);
            this.btn_about.TabIndex = 24;
            this.btn_about.UseVisualStyleBackColor = false;
            this.btn_about.Click += new System.EventHandler(this.btn_about_Click);
            // 
            // write_off
            // 
            this.write_off.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.write_off.BackColor = System.Drawing.Color.Transparent;
            this.write_off.BaseColor = System.Drawing.Color.Transparent;
            this.write_off.BorderColor = System.Drawing.Color.Transparent;
            this.write_off.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.write_off.DownBack = null;
            this.write_off.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.write_off.GlowColor = System.Drawing.Color.Transparent;
            this.write_off.Image = ((System.Drawing.Image)(resources.GetObject("write_off.Image")));
            this.write_off.ImageSize = new System.Drawing.Size(25, 25);
            this.write_off.InnerBorderColor = System.Drawing.Color.Transparent;
            this.write_off.IsDrawBorder = false;
            this.write_off.IsDrawGlass = false;
            this.write_off.Location = new System.Drawing.Point(1131, 34);
            this.write_off.MouseBack = null;
            this.write_off.MouseBaseColor = System.Drawing.Color.Transparent;
            this.write_off.Name = "write_off";
            this.write_off.NormlBack = null;
            this.write_off.Size = new System.Drawing.Size(39, 36);
            this.write_off.TabIndex = 25;
            this.write_off.UseVisualStyleBackColor = false;
            this.write_off.Click += new System.EventHandler(this.write_off_Click);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(497, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "新增客服";
            // 
            // robot_reply
            // 
            this.robot_reply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.robot_reply.BackColor = System.Drawing.Color.Transparent;
            this.robot_reply.BaseColor = System.Drawing.Color.Transparent;
            this.robot_reply.BorderColor = System.Drawing.Color.Transparent;
            this.robot_reply.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.robot_reply.DownBack = null;
            this.robot_reply.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.robot_reply.GlowColor = System.Drawing.Color.Transparent;
            this.robot_reply.Image = ((System.Drawing.Image)(resources.GetObject("robot_reply.Image")));
            this.robot_reply.ImageSize = new System.Drawing.Size(25, 25);
            this.robot_reply.InnerBorderColor = System.Drawing.Color.Transparent;
            this.robot_reply.IsDrawBorder = false;
            this.robot_reply.IsDrawGlass = false;
            this.robot_reply.Location = new System.Drawing.Point(617, 35);
            this.robot_reply.MouseBack = null;
            this.robot_reply.MouseBaseColor = System.Drawing.Color.Transparent;
            this.robot_reply.Name = "robot_reply";
            this.robot_reply.NormlBack = null;
            this.robot_reply.Size = new System.Drawing.Size(39, 35);
            this.robot_reply.TabIndex = 27;
            this.robot_reply.UseVisualStyleBackColor = false;
            this.robot_reply.Click += new System.EventHandler(this.robot_reply_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(36, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(45, 62);
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // show_msg
            // 
            this.show_msg.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.show_msg.ContextMenuStrip = this.cms_right_msg;
            this.show_msg.Icon = ((System.Drawing.Icon)(resources.GetObject("show_msg.Icon")));
            this.show_msg.Text = "环球云客服系统";
            this.show_msg.Visible = true;
            this.show_msg.Click += new System.EventHandler(this.show_msg_Click);
            // 
            // cms_right_msg
            // 
            this.cms_right_msg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置ToolStripMenuItem,
            this.关于环球云ToolStripMenuItem,
            this.退出软件ToolStripMenuItem});
            this.cms_right_msg.Name = "cms_right_msg";
            this.cms_right_msg.Size = new System.Drawing.Size(137, 70);
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.设置ToolStripMenuItem.Text = "设置";
            this.设置ToolStripMenuItem.Click += new System.EventHandler(this.设置ToolStripMenuItem_Click);
            // 
            // 关于环球云ToolStripMenuItem
            // 
            this.关于环球云ToolStripMenuItem.Name = "关于环球云ToolStripMenuItem";
            this.关于环球云ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.关于环球云ToolStripMenuItem.Text = "关于环球云";
            this.关于环球云ToolStripMenuItem.Click += new System.EventHandler(this.关于环球云ToolStripMenuItem_Click);
            // 
            // 退出软件ToolStripMenuItem
            // 
            this.退出软件ToolStripMenuItem.Name = "退出软件ToolStripMenuItem";
            this.退出软件ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.退出软件ToolStripMenuItem.Text = "退出软件";
            this.退出软件ToolStripMenuItem.Click += new System.EventHandler(this.退出软件ToolStripMenuItem_Click);
            // 
            // wxTabContral1
            // 
            this.wxTabContral1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wxTabContral1.HaveCloseButton = true;
            this.wxTabContral1.ImageList = this.imageList1;
            this.wxTabContral1.ItemSize = new System.Drawing.Size(95, 60);
            this.wxTabContral1.Location = new System.Drawing.Point(0, 92);
            this.wxTabContral1.Name = "wxTabContral1";
            this.wxTabContral1.Padding = new System.Drawing.Point(9, 4);
            this.wxTabContral1.SelectedIndex = 0;
            this.wxTabContral1.ShowDrawTipText = true;
            this.wxTabContral1.Size = new System.Drawing.Size(1201, 600);
            this.wxTabContral1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.wxTabContral1.TabIndex = 6;
            this.wxTabContral1.tabClose += new WChat.WXTabContral.tabCloseEventHandle(this.wxTabContral1_tabClose);
            this.wxTabContral1.SelectedIndexChanged += new System.EventHandler(this.wxTabContral1_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(562, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 30;
            this.label11.Text = "群管家";
            // 
            // btn_group
            // 
            this.btn_group.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_group.BackColor = System.Drawing.Color.Transparent;
            this.btn_group.BaseColor = System.Drawing.Color.Transparent;
            this.btn_group.BorderColor = System.Drawing.Color.Transparent;
            this.btn_group.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_group.DownBack = null;
            this.btn_group.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_group.GlowColor = System.Drawing.Color.Transparent;
            this.btn_group.Image = global::HuanQiuCrm.Properties.Resources.groIcon;
            this.btn_group.ImageSize = new System.Drawing.Size(28, 28);
            this.btn_group.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_group.IsDrawBorder = false;
            this.btn_group.IsDrawGlass = false;
            this.btn_group.Location = new System.Drawing.Point(568, 38);
            this.btn_group.MouseBack = null;
            this.btn_group.MouseBaseColor = System.Drawing.Color.Transparent;
            this.btn_group.Name = "btn_group";
            this.btn_group.NormlBack = null;
            this.btn_group.Size = new System.Drawing.Size(28, 28);
            this.btn_group.TabIndex = 29;
            this.btn_group.UseVisualStyleBackColor = false;
            this.btn_group.Click += new System.EventHandler(this.btn_group_Click);
            // 
            // Form1
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.IpAddress;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Back = ((System.Drawing.Image)(resources.GetObject("$this.Back")));
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BorderColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1201, 688);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btn_group);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.robot_reply);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.write_off);
            this.Controls.Add(this.btn_about);
            this.Controls.Add(this.skinButton8);
            this.Controls.Add(this.tbn_AllSend);
            this.Controls.Add(this.btn_message_manege);
            this.Controls.Add(this.btn_statistic_anal);
            this.Controls.Add(this.btn_costum_analysls);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_Consult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.wxTabContral1);
            this.Controls.Add(this.skinButton1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MdiStretchImage = true;
            this.Name = "Form1";
            this.Shadow = true;
            this.ShadowColor = System.Drawing.Color.DodgerBlue;
            this.ShowBorder = false;
            this.ShowDrawIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.cms_right_msg.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinButton skinButton1;
        private System.Windows.Forms.ImageList imageList1;
        private WChat.WXTabContral wxTabContral1;
        private System.Windows.Forms.ImageList imageList2;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private System.Windows.Forms.Label label1;
        private CCWin.SkinControl.SkinButton btn_Consult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CCWin.SkinControl.SkinButton btn_costum_analysls;
        private CCWin.SkinControl.SkinButton btn_statistic_anal;
        private CCWin.SkinControl.SkinButton btn_message_manege;
        private CCWin.SkinControl.SkinButton tbn_AllSend;
        private CCWin.SkinControl.SkinButton skinButton8;
        private CCWin.SkinControl.SkinButton btn_about;
        private CCWin.SkinControl.SkinButton write_off;
        private System.Windows.Forms.Label label10;
        private CCWin.SkinControl.SkinButton robot_reply;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.NotifyIcon show_msg;
        private System.Windows.Forms.ContextMenuStrip cms_right_msg;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于环球云ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出软件ToolStripMenuItem;
        private System.Windows.Forms.Label label11;
        private CCWin.SkinControl.SkinButton btn_group;
    }
}

