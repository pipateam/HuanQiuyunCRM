﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using HuanQiuCrm.Properties;
using WChat;
namespace HuanQiuCrm {
    public partial class GroupAssistant : Skin_Color {
        private int friend_num = 0;
        SqlHandle sql = new SqlHandle();
        private Settings st = new Settings();
        private List<string> arr = new List<string>();
        private string user_Name;
        public List<WXUser> meUser_list = new List<WXUser>(); 
        private List<string> UserList = new List<string>();
        private List<string> MyFriList = new List<string>();  //将数据库查出的数据暂时存起来，以用于查询
        private List<string> TempUserList = new List<string>();
        private string mywx;
        public GroupAssistant() {
            InitializeComponent();
        }
       private void show_my_wx() {
            string acco = st.UseAcct;
            List<string> ss = new List<string>();
            ss = sql.Select_Num_Wx(acco);
            for (int i = 0; i < ss.Count; i++) {
                if (ss[i].Trim() != "") {
                    lb_ms_chiwx.Items.Add(new CCWin.SkinControl.SkinListBoxItem(ss[i].Trim()));
                }
            }
        }

        private void GroupAssistant_Load( object sender, EventArgs e ) {
            show_my_wx();
        }

        private void lb_ms_chiwx_SelectedIndexChanged( object sender, EventArgs e ) {
            if (lb_ms_chiwx.SelectedItem!=null) {
                TempUserList.Clear();
                listBox1.Items.Clear();
                MyFriList.Clear();
                 mywx = lb_ms_chiwx.SelectedItem.ToString().Trim();
                arr = sql.Select_send_man(mywx, WinXinBox._setting);   //获取群发好友
                if (arr.Count()!=0) {
                    string[] Wx_Name = arr.Distinct().ToArray();
                    for (int i = 0; i < Wx_Name.Length; i++) {
                        listBox1.Items.Add(Wx_Name[i].Trim());   //列表添加群发好友
                        MyFriList.Add(Wx_Name[i].Trim());
                    }
                }
            }
        }

        private void listBox1_SelectedIndexChanged( object sender, EventArgs e ) {
            lbx_seledfris.Items.Clear();
            if (listBox1.SelectedItem!=null) {
                string seledfris = listBox1.SelectedItem.ToString();
                if (!TempUserList.Contains(seledfris)) {  //没有了在进行添加，防止重复
                    TempUserList.Add(seledfris);
                }
            }
            foreach (var item in TempUserList) {
                friend_num = friend_num + 1;
                lbx_seledfris.Items.Add(item);
            }
            friend_num = 0;
            friend_num = TempUserList.Count();
            lb_fri_num.Text = friend_num.ToString();
            tbx_searchs.Text = "";
        }

        private void btn_done_Click( object sender, EventArgs e ) {
            if (tbx_topic.Text==string.Empty) {
                MessageBox.Show("请输入群名!");
            }
            else {
                for (int i = 0; i < TempUserList.Count; i++) {
                    user_Name = sql.Select_Send_User(mywx,TempUserList[i], WinXinBox._setting);
                    UserList.Add(user_Name);
                }
                string my_user_nam = sql.searchMyUserName(mywx, st.UseAcct);
                foreach (var user in meUser_list) {
                    if (user.NickName==mywx) {
                        if (user.Create_ChatRoom(UserList.Count, UserList, tbx_topic.Text)) {
                            MessageBox.Show("建群成功!");
                        } else {
                            MessageBox.Show("建群失败!");
                        }
                        break;
                    }
                }
                this.Close();
            }
        }

        private void btn_search_Click( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            foreach (var item in MyFriList) {
                if (item.Contains(tbx_searchs.Text)) {
                    listBox1.Items.Add(item);
                }
            }
        }

        private void tbx_searchs_TextChanged( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            foreach (var item in MyFriList) {
                if (item.Contains(tbx_searchs.Text)) {
                    listBox1.Items.Add(item);
                }
            }
        }
        /// <summary>
        /// 单击移除选择项
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbx_seledfris_SelectedIndexChanged( object sender, EventArgs e ) {
            if (lbx_seledfris.SelectedItem!=null) {
                string fri = lbx_seledfris.SelectedItem.ToString();
                lbx_seledfris.Items.Remove(fri);
                TempUserList.Remove(fri);
                friend_num = 0;
                friend_num = TempUserList.Count();
                lb_fri_num.Text = friend_num.ToString();
            }
        }
    }
}
