﻿using HuanQiuCrm.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HuanQiuCrm {
    public partial class welcom : Form {

        public welcom() {
            InitializeComponent();
            this.TopMost = true;
            this.ShowInTaskbar = false;
            this.BackColor = System.Drawing.Color.LightGray;
            this.TransparencyKey = System.Drawing.Color.LightGray;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

        }
        private void FormComeOut() {
            this.Opacity = 0;
            this.Show();
            while (this.Opacity < 1) {
                System.Threading.Thread.Sleep(50);
                this.Opacity += 0.05;
            }

        }
        private void FormDispear() {
            while (this.Opacity > 0) {
                System.Threading.Thread.Sleep(50);
                this.Opacity -= 0.05;
            }
        }
       
        private void isFirst() {
            Settings st = new Settings();
            if (st.UseAcct == "") {
                register re = new register();
                re.Show();
            }
            else {
                Form1 main = new Form1();
                main.Show();
            }
        }

        private void welcom_Load_1(object sender, EventArgs e) {
            Opacity = 0.0;
            FormComeOut();
            System.Threading.Thread.Sleep(2000);
            FormDispear();
            isFirst();
        }
    }
}
