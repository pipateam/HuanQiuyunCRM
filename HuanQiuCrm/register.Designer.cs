﻿namespace HuanQiuCrm {
    partial class register {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(register));
            this.d = new CCWin.SkinControl.SkinLabel();
            this.tbx_name = new CCWin.SkinControl.SkinTextBox();
            this.tbx_pwd = new CCWin.SkinControl.SkinTextBox();
            this.btn_login = new CCWin.SkinControl.SkinButton();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.tbx_code = new CCWin.SkinControl.SkinTextBox();
            this.pictureBox1 = new CCWin.SkinControl.SkinPictureBox();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // d
            // 
            this.d.AutoSize = true;
            this.d.BackColor = System.Drawing.Color.Transparent;
            this.d.BorderColor = System.Drawing.Color.Transparent;
            this.d.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.d.ForeColor = System.Drawing.Color.White;
            this.d.Location = new System.Drawing.Point(59, 63);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(232, 27);
            this.d.TabIndex = 1;
            this.d.Text = "欢迎使用环球云客服系统";
            // 
            // tbx_name
            // 
            this.tbx_name.BackColor = System.Drawing.Color.Transparent;
            this.tbx_name.DownBack = null;
            this.tbx_name.Icon = null;
            this.tbx_name.IconIsButton = false;
            this.tbx_name.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_name.IsPasswordChat = '\0';
            this.tbx_name.IsSystemPasswordChar = false;
            this.tbx_name.Lines = new string[0];
            this.tbx_name.Location = new System.Drawing.Point(112, 123);
            this.tbx_name.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_name.MaxLength = 32767;
            this.tbx_name.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_name.MouseBack = null;
            this.tbx_name.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_name.Multiline = false;
            this.tbx_name.Name = "tbx_name";
            this.tbx_name.NormlBack = null;
            this.tbx_name.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_name.ReadOnly = false;
            this.tbx_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_name.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_name.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_name.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_name.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_name.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_name.SkinTxt.Name = "BaseText";
            this.tbx_name.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_name.SkinTxt.TabIndex = 0;
            this.tbx_name.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_name.SkinTxt.WaterText = "";
            this.tbx_name.TabIndex = 4;
            this.tbx_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_name.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_name.WaterText = "";
            this.tbx_name.WordWrap = true;
            // 
            // tbx_pwd
            // 
            this.tbx_pwd.BackColor = System.Drawing.Color.Transparent;
            this.tbx_pwd.DownBack = null;
            this.tbx_pwd.Icon = null;
            this.tbx_pwd.IconIsButton = false;
            this.tbx_pwd.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_pwd.IsPasswordChat = '\0';
            this.tbx_pwd.IsSystemPasswordChar = false;
            this.tbx_pwd.Lines = new string[0];
            this.tbx_pwd.Location = new System.Drawing.Point(112, 168);
            this.tbx_pwd.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_pwd.MaxLength = 32767;
            this.tbx_pwd.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_pwd.MouseBack = null;
            this.tbx_pwd.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_pwd.Multiline = false;
            this.tbx_pwd.Name = "tbx_pwd";
            this.tbx_pwd.NormlBack = null;
            this.tbx_pwd.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_pwd.ReadOnly = false;
            this.tbx_pwd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_pwd.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_pwd.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_pwd.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_pwd.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_pwd.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_pwd.SkinTxt.Name = "BaseText";
            this.tbx_pwd.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_pwd.SkinTxt.TabIndex = 0;
            this.tbx_pwd.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_pwd.SkinTxt.WaterText = "";
            this.tbx_pwd.TabIndex = 5;
            this.tbx_pwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_pwd.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_pwd.WaterText = "";
            this.tbx_pwd.WordWrap = true;
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.Transparent;
            this.btn_login.BaseColor = System.Drawing.Color.White;
            this.btn_login.BorderColor = System.Drawing.Color.White;
            this.btn_login.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_login.DownBack = null;
            this.btn_login.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_login.Location = new System.Drawing.Point(44, 321);
            this.btn_login.MouseBack = null;
            this.btn_login.Name = "btn_login";
            this.btn_login.NormlBack = null;
            this.btn_login.Size = new System.Drawing.Size(253, 37);
            this.btn_login.TabIndex = 7;
            this.btn_login.Text = "登陆";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.BorderSize = 0;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.ForeColor = System.Drawing.Color.White;
            this.skinLabel1.Location = new System.Drawing.Point(40, 223);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(83, 19);
            this.skinLabel1.TabIndex = 13;
            this.skinLabel1.Text = "输入验证码:";
            this.skinLabel1.Visible = false;
            // 
            // tbx_code
            // 
            this.tbx_code.BackColor = System.Drawing.Color.Transparent;
            this.tbx_code.DownBack = null;
            this.tbx_code.Icon = null;
            this.tbx_code.IconIsButton = false;
            this.tbx_code.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_code.IsPasswordChat = '\0';
            this.tbx_code.IsSystemPasswordChar = false;
            this.tbx_code.Lines = new string[0];
            this.tbx_code.Location = new System.Drawing.Point(44, 249);
            this.tbx_code.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_code.MaxLength = 32767;
            this.tbx_code.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_code.MouseBack = null;
            this.tbx_code.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_code.Multiline = false;
            this.tbx_code.Name = "tbx_code";
            this.tbx_code.NormlBack = null;
            this.tbx_code.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_code.ReadOnly = false;
            this.tbx_code.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_code.Size = new System.Drawing.Size(126, 28);
            // 
            // 
            // 
            this.tbx_code.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_code.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_code.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_code.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_code.SkinTxt.Name = "BaseText";
            this.tbx_code.SkinTxt.Size = new System.Drawing.Size(116, 18);
            this.tbx_code.SkinTxt.TabIndex = 0;
            this.tbx_code.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_code.SkinTxt.WaterText = "";
            this.tbx_code.SkinTxt.WordWrap = false;
            this.tbx_code.TabIndex = 12;
            this.tbx_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_code.Visible = false;
            this.tbx_code.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_code.WaterText = "";
            this.tbx_code.WordWrap = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(189, 249);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(117, 28);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.ForeColor = System.Drawing.Color.White;
            this.skinLabel4.Location = new System.Drawing.Point(39, 125);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(69, 26);
            this.skinLabel4.TabIndex = 14;
            this.skinLabel4.Text = "账户：";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.ForeColor = System.Drawing.Color.White;
            this.skinLabel2.Location = new System.Drawing.Point(39, 168);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(69, 26);
            this.skinLabel2.TabIndex = 15;
            this.skinLabel2.Text = "密码：";
            // 
            // register
            // 
            this.AcceptButton = this.btn_login;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BorderColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(359, 412);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinLabel4);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.tbx_code);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.tbx_pwd);
            this.Controls.Add(this.tbx_name);
            this.Controls.Add(this.d);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "register";
            this.Shadow = true;
            this.ShadowWidth = 5;
            this.ShowDrawIcon = false;
            this.Text = "登陆";
            this.TitleCenter = false;
            this.TitleColor = System.Drawing.Color.White;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private CCWin.SkinControl.SkinLabel d;
        private CCWin.SkinControl.SkinTextBox tbx_name;
        private CCWin.SkinControl.SkinTextBox tbx_pwd;
        private CCWin.SkinControl.SkinButton btn_login;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinTextBox tbx_code;
        private CCWin.SkinControl.SkinPictureBox pictureBox1;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinLabel skinLabel2;
    }
}