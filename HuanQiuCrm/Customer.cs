﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using WChat;
using System.Threading;
using HuanQiuCrm.Properties;
using System.Windows.Forms.DataVisualization.Charting;
using Models;
using DAL;
namespace HuanQiuCrm
{
    public partial class Customer : Skin_Color
    {
        SqlHandle sql = new SqlHandle();
        Settings st = new Settings();
        FriService fs = new FriService();
        HongBaoService hbs = new HongBaoService();
        string ChildName = string.Empty;
        List<string> WchatL = new List<string>();                  //登录微信的集合

        public Customer(string ChildName)
        {
            this.ChildName = ChildName;
            InitializeComponent();
        }

        //显示控件的宽度
        private void listwidth()
        {
            int i = Convert.ToInt32(listView1.Width / 100 * 16.5);
            listView1.Columns[0].Width = i;
            listView1.Columns[1].Width = i;
            listView1.Columns[2].Width = i;
            listView1.Columns[3].Width = i;
            listView1.Columns[4].Width = i;
            listView1.Columns[5].Width = i;

            int j = Convert.ToInt32(listView2.Width/100*16.5);
            listView2.Columns[0].Width = j;
            listView2.Columns[1].Width = j;
            listView2.Columns[2].Width = j;
            listView2.Columns[3].Width = j;
            listView2.Columns[4].Width = j;
            listView2.Columns[5].Width = j;

            int l = Convert.ToInt32(listView3.Width / 100 * 25);
            listView3.Columns[0].Width = l;
            listView3.Columns[1].Width = l;
            listView3.Columns[2].Width = l;
            listView3.Columns[3].Width = l;

            listView4.Columns[0].Width = l;
            listView4.Columns[1].Width = l;
            listView4.Columns[2].Width = l;
            listView4.Columns[3].Width = l;
            //界面的分割线
            string str = string.Empty;
            for (int k = 0; k < skinTabPage1.Width; k++)
            {
                str += "—";
            }
            label2.Text = str;
        }
        //将微信集合添加到下拉框
        private void BindCbo() {
            foreach (var item in Form1.login_wxb.Values) {
                comboBox1.Items.Add(item._me.NickName);
            }
            comboBox1.SelectedIndex = 0;
        }
        //查询所有信息
        private void AllFriend() {
            int All = 0;
            int allMan = 0;
            int allWoman = 0;
            int AllFa = 0;
            int AllShou = 0;
            int NewFriend = 0;

            foreach (var item in Form1.login_wxb.Values) {
                string Child = item._me.NickName;
                List<Fri> L = fs.SearchAll(st.UseAcct, Child);
                List<HongBao> Lhb = hbs.SearchHB(Child);
                #region 好友
                foreach (Fri f in L) {
                    switch (f.Sex.Trim()) {
                    case "1":
                    allMan++;
                    break;
                    case "2":
                    allWoman++;
                    break;
                    }
                }
                #endregion
                #region 红包
                foreach (HongBao h in Lhb) {
                    if (h.Received == 0)
                        AllShou++;
                    else
                        AllFa++;
                }
                #endregion
                NewFriend += Convert.ToInt32(fs.SearchNewFriend(Child));
                All += L.Count;
            }

            ListViewItem lv = new ListViewItem();
            lv.Text = All.ToString();
            lv.SubItems.Add(NewFriend.ToString());
            lv.SubItems.Add(allMan.ToString());
            lv.SubItems.Add(allWoman.ToString());
            lv.SubItems.Add(AllShou.ToString());
            lv.SubItems.Add(AllFa.ToString());
            listView1.Items.Add(lv);
        }
        

        private void Customer_Load(object sender, EventArgs e)
        {
            BindCbo();
            listwidth();
            AllFriend();
            skinTabControl1.SelectedIndex = 0;
            
            if (ChildName == string.Empty)
                return;

            ShowShuJu(st.UseAcct,comboBox1.SelectedItem.ToString());

            if (skinTabPage4.Controls.Count > 0)
                skinTabPage4.Controls.Clear();
            PieChart(new Title("好友地区统计图"), st.UseAcct); //设置统计图
        }
        //统计分析数据显示 分
        private void ShowShuJu(string ZiZhangHu,string WChatname)
        {
            listView2.Items.Clear();
            listView2.Items.Clear();
            listView3.Items.Clear();
            listView4.Items.Clear();
            List<Fri> frilist = fs.SearchAll(ZiZhangHu, WChatname);
            List<HongBao> Lhb = hbs.SearchHB(WChatname);
            List<HongBao> Lhb2 = hbs.NotInFriSearchHB(ZiZhangHu, WChatname);
            int Man = 0;
            int Woman = 0;
            int Shou = 0;
            int Fa = 0;
            int HB = 0;
            //红包
            foreach (HongBao item in Lhb)
            {
                if (item.Received == 0)
                    Shou++;
                else
                    Fa++;
            }
            //好友信息
            foreach (Fri item in frilist)
            {
                ListViewItem lvi = new ListViewItem();
                switch (item.Sex.Trim())
                {
                    case "1":
                        Man++;
                        lvi.Text = item.wx_name;
                        lvi.SubItems.Add("男");
                        lvi.SubItems.Add(item.Province);
                        break;
                    case "2":
                        Woman++;
                        lvi.Text = item.wx_name;
                        lvi.SubItems.Add("女");
                        lvi.SubItems.Add(item.Province);
                        break;
                    default:
                        lvi.Text = item.wx_name;
                        lvi.SubItems.Add("未知");
                        lvi.SubItems.Add(item.Province);
                        break;
                }
                //个人红包记录
                foreach (HongBao it in Lhb)
                {
                    if (item.wx_name == it.ToUser)
                        HB++;
                }
                lvi.SubItems.Add(HB.ToString());
                listView3.Items.Add(lvi);
                HB = 0;
            }
            //未在当前好友中的记录
            foreach (HongBao item in Lhb2)
            {
                ListViewItem lvs = new ListViewItem();
                lvs.Text = item.ToUser;
                lvs.SubItems.Add("");
                lvs.SubItems.Add("");
                lvs.SubItems.Add(item.Received.ToString());
                listView4.Items.Add(lvs);
            }
            //显示
            ListViewItem lv = new ListViewItem();
            lv.Text = frilist.Count.ToString();                                                         //好友总数
            lv.SubItems.Add(fs.SearchNewFriend(ChildName));                                 //新增好友数量
            lv.SubItems.Add(Man.ToString());                                                        //男性好友
            lv.SubItems.Add(Woman.ToString());                                                   //女性好友
            lv.SubItems.Add(Shou.ToString());                                                       //收红包
            lv.SubItems.Add(Fa.ToString());                                                          //发红包
            listView2.Items.Add(lv);
        }

        //创建统计分析图
        private void PieChart( Title title, string LoginName ) {
            Chart chart = new Chart();
            chart.ChartAreas.Add("ChartArea");
            chart.Legends.Add("Legends");
            chart.Series.Add("Series");
            chart.Width = skinTabPage4.Width/100*90;
            chart.Height = skinTabPage4.Height;
            chart.Anchor = (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left);
            //Title title = new Title();
            //title.Text = title.ToString();
            title.Alignment = ContentAlignment.MiddleCenter;
            title.Font = new System.Drawing.Font("Trebuchet MS", 14F, FontStyle.Bold);
            chart.Titles.Add(title);
            chart.ChartAreas[0].AxisX.Interval = 1; //设置轴的间距
            chart.Legends["Legends"].BackHatchStyle = ChartHatchStyle.DarkDownwardDiagonal;
            chart.Legends["Legends"].BorderWidth = 1;
            chart.Legends["Legends"].BorderColor = Color.FromArgb(200, 200, 200);
            chart.Series["Series"].ChartType = SeriesChartType.Pie;
            chart.Series["Series"].ChartType = SeriesChartType.Doughnut;
            chart.Series["Series"].LegendText = "#VALX:    [ #PERCENT{P1} ]";
            chart.Series["Series"].Label = "#VALX";
            chart.Series["Series"].Font = new System.Drawing.Font("Trebuchet MS", 10, System.Drawing.FontStyle.Bold);
            chart.Series["Series"].BorderColor = Color.FromArgb(255, 101, 101, 101);
            chart.Series["Series"]["PieLabelStyle"] = "Outside";
            chart.Series["Series"]["PieDrawingStyle"] = "Default";
            skinTabPage4.Controls.Add(chart);

            Dictionary<string, int> dic = new DAL.FriService().SearchFriendProvince(LoginName);
            foreach (var item in dic) {
                chart.Series[0].Points.AddXY(item.Key, item.Value);
            }
        }
        
        /// <summary>
        /// 好友地域分析
        /// </summary>
        private void Customer_SizeChanged( object sender, EventArgs e ) {
            listwidth();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowShuJu(st.UseAcct, comboBox1.SelectedItem.ToString());
        }
    }
}