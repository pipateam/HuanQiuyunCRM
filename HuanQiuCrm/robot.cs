﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using WChat;
using HuanQiuCrm.Properties;
using System.Data.SqlClient;

namespace HuanQiuCrm
{
    public partial class robot : Skin_Color {
        Settings st = new Settings();
        SqlHandle sql = new SqlHandle();
        //机器人回复
        Auto_Reply_EventHandler auto_reply = new Auto_Reply_EventHandler();

        //图灵机器人聊天
        public delegate void RobotReplyEventHandler(bool asd,List<string> arr);
        public event RobotReplyEventHandler RobotReply;

        //智能回复
        public delegate void AotumaticReplyEventHandler(bool asd,List<string> arr);
        public event AotumaticReplyEventHandler AotumaticReply;

        List<string> arr = new List<string> { };

        public robot()
        {
            InitializeComponent();

        }

        /// <summary>
        /// 机器人回复
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_Click(object sender, EventArgs e)
        {
            arr.Clear();
            for (int i = 0; i < listBox1.SelectedItems.Count; i++) {
                arr.Add(listBox1.SelectedItems[i].ToString());
            }
            if (arr.Count>0) {
                if (RobotReply != null) {
                    if (checkBox1.Checked) {
                        st.robotOpen = true;
                        st.Save();
                        RobotReply(true,arr);
                    }
                    else {
                        st.robotOpen = false;
                        st.Save();
                        RobotReply(false,arr);
                    }
                    MessageBox.Show("设置成功!");
                }
            }else {
                MessageBox.Show("请先在列表选择要回复的客服，再来点击这里");
                if (st.robotOpen == true) {
                    checkBox1.Checked = true;
                }
                else {
                    checkBox1.Checked = false;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 显示智能回复 添加过的内容
        /// </summary>
        private void read_data()
        {
            try
            {
                String strConn = "Data Source=bds249611391.my3w.com;Initial Catalog=bds249611391_db;Persist Security Info=True;User ID=bds249611391;Password=hq312453";
                SqlConnection conn = new SqlConnection(strConn);
                String sqlId = "select Time,Keyword,Contents from "+st.UseAcct+"_智能回复表";
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                da.Fill(ds, "智能回复");

                skinDataGridView1.DataSource = ds;
                skinDataGridView1.DataMember = "智能回复";
                conn.Close();

                skinDataGridView1.Columns[0].HeaderText = "添加时间";
                skinDataGridView1.Columns[0].Width = 130;
                skinDataGridView1.Columns[1].HeaderText = "关键词";
                skinDataGridView1.Columns[2].HeaderText = "回复内容";
                skinDataGridView1.Columns[2].Width = 165;
            }
            catch
            {
            }

        }

        /// <summary>
        /// 显示当前登录的微信号
        /// </summary>
        private void WeiXin() {
            string[] Arr;
            List<string> arr = new List<string> { };
            arr = sql.Select_send_devices(st.UseAcct);
            Arr = arr.Distinct().ToArray();
            for (int i = 0; i < Arr.Length; i++) {
                listBox1.Items.Add(Arr[i].Trim());
            }
        }

        private void robot_Load(object sender, EventArgs e)
        {
            if (st.robotOpen == true)
            {
                checkBox1.Checked = true;
            }
            else
            {
                checkBox1.Checked = false;
            }

            if (st.AotuOpen == true)
            {
                checkbox2.Checked = true;
            }
            else
            {
                checkbox2.Checked = false;
            }
            read_data();
            //显示登录的微信号
            WeiXin();
        }

        //添加智能回复数据
        private void btn_add_Click(object sender, EventArgs e)
        {
            string time = DateTime.Now.ToString();
            string keyword = tbx_keyword.Text;
            string reply = tbx_content.Text;
            if (keyword != "" && reply != "")
            {
                sql.insert_reply_data(st.UseAcct,time, keyword, reply);
            }
            else
            {
                MessageBox.Show("关键词或内容不能为空");
            }
            tbx_content.Text = "";
            tbx_keyword.Text = "";
            read_data();
        }

        /// <summary>
        /// 关键词回复开关
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkbox2_Click( object sender, EventArgs e ) {
            arr.Clear();
            for (int i = 0; i < listBox1.SelectedItems.Count; i++) {
                arr.Add(listBox1.SelectedItems[i].ToString());
            }
            if (arr.Count>0) {
                if (AotumaticReply != null) {
                    if (checkbox2.Checked) {
                        st.AotuOpen = true;
                        st.Save();
                        AotumaticReply(true,arr);
                    }
                    else {
                        st.AotuOpen = false;
                        st.Save();
                        AotumaticReply(false,arr);
                    }
                    MessageBox.Show("设置成功!");
                }
            }else {
                MessageBox.Show("请先在列表选择要回复的客服，再来点击这里");
                if (st.AotuOpen == true) {
                    checkbox2.Checked = true;
                }
                else {
                    checkbox2.Checked = false;
                }
            }
          

        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string num = skinDataGridView1.SelectedRows[0].Cells[0].EditedFormattedValue.ToString();
            sql.Delete_reply_data(st.UseAcct,num);
            read_data();
        }
    }
}
