﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using WChat;
using HuanQiuCrm.Properties;
using System.Data.SqlClient;
using System.Threading;

namespace HuanQiuCrm {
    public partial class Mess_Manage : Skin_Color {
        SqlHandle sql = new SqlHandle();
        Settings st = new Settings();
        Dictionary<string, List<string>> dic = new Dictionary<string, List<string>>();
        private string WXname;
        private string selfri;
        private bool is_search = false;
        private string FilePath;
        public delegate void MyInvoke();
        private string search_conte;
        private string selwx;
        public Mess_Manage() {
            InitializeComponent();
            btnPageUp.MouseMove += BtnPageUp_MouseMove;
            btnPageUp.MouseLeave += BtnPageUp_MouseLeave;
            btnPageDown.MouseMove += BtnPageDown_MouseMove;
            btnPageDown.MouseLeave += BtnPageDown_MouseLeave;
            btnPageDown.Click += BtnPageDown_Click;
            btnPageUp.Click += BtnPageUp_Click;
        }
        #region 分页
        private void BtnPageUp_MouseMove( object sender, MouseEventArgs e ) {
            btnPageUp.Font = new Font(btnPageUp.Font, FontStyle.Bold);
        }
        private void BtnPageUp_MouseLeave( object sender, EventArgs e ) {
            btnPageUp.Font = new Font(btnPageUp.Font, FontStyle.Regular);
        }
        private void BtnPageDown_MouseMove( object sender, MouseEventArgs e ) {
            btnPageDown.Font = new Font(btnPageDown.Font, FontStyle.Bold);
        }
        private void BtnPageDown_MouseLeave( object sender, EventArgs e ) {
            btnPageDown.Font = new Font(btnPageDown.Font, FontStyle.Regular);
        }
        //下一页
        private void BtnPageDown_Click( object sender, EventArgs e ) {
            int i = Convert.ToInt32(txtPage.Text);
            if (i >= Convert.ToInt32(lblPageCount.Text)) return;
            if (selwx == "" && search_conte == "") return;
            chatLogBox1.Clear();
            txtPage.Text = (++i).ToString();
            if (is_search) {
                if (SearchTextStr == "好友") {
                    SearchChatLogFromFriend(page_current - i + 1, search_conte);
                }
                else if (SearchTextStr == "聊天记录") {
                    SearchChatLogFangFa(page_current - i + 1);
                }
            }
            else {
                SearchChatLogFromFriend(page_current - i + 1, selfri);
            }
        }
        //上一页
        private void BtnPageUp_Click( object sender, EventArgs e ) {
            int i = Convert.ToInt32(txtPage.Text);
            if (i <= 1) return;
            if (selwx == "" && search_conte == "") return;
            chatLogBox1.Clear();        //清空之前加载的数据
            txtPage.Text = (--i).ToString();
            if (is_search) {
                if (SearchTextStr == "好友") {
                    SearchChatLogFromFriend(page_current - i + 1, search_conte);
                }
                else if (SearchTextStr == "聊天记录") {
                    SearchChatLogFangFa(page_current - i + 1);
                }
            }
            else {
                SearchChatLogFromFriend(page_current - i + 1, selfri);
            }
        }
        #endregion

        //comboBox赋值
        private void BindComboBox() {
            comboBox1.Items.Add("好友");
            comboBox1.Items.Add("聊天记录");
            comboBox1.SelectedIndex = 0;

            comboBox2.Items.Add("全部");
            comboBox2.Items.Add("近一个月");
            comboBox2.Items.Add("近三个月");
            comboBox2.Items.Add("近一年");
            comboBox2.SelectedIndex = 0;

            SearchTextStr = comboBox1.Text;
            dt = Convert.ToDateTime("1753/1/1 0:00:00");
        }

        private void Mess_Manage_Load(object sender, EventArgs e) {
            BindComboBox();
            panel1.Visible = false;
            show_my_child_account();
            chatLogBox1.load();
        }
        /// <summary>
        /// 查所有微信
        /// </summary>
        private void show_my_child_account()
        {
            List<string> ss = new List<string>();
            ss = sql.Select_Num_Wx(st.UseAcct);
            this.lb_ms_chiwx.Items.Clear();
            foreach (string item in ss) {
                if(item != string.Empty)
                    this.lb_ms_chiwx.Items.Add(item);
            }
        }

        /// <summary>
        /// 聊天记录查询方法
        /// </summary>
        /// <param name="tabNa"></param>
        /// <param name="fri"></param>
        private void showchLog(string WXname, string fri)
        {
            chatLogBox1.Clear();
            List<WXMsg> msg = sql.GetChatLog(WXname, fri);
            for (int i = msg.Count - 1; i >= 0; i--)
            {
                chatLogBox1.RecoveryChatLog(msg[i]);               
            }
        }
      
        private void chiwx_get_ms_fried()
        {
            List<string> chlofri = new List<string>();
            if (selwx != "")
            {
                chlofri = sql.GetUsrNaFrChlo(selwx);
                string[] Arr = chlofri.Distinct().ToArray();
                lb_ms_myfri.Items.Clear();
                for (int s = 0; s < Arr.Length; s++)
                {
                    if (Arr[s].Trim() != "")
                    {    //不为空了在进行添加
                        lb_ms_myfri.Items.Add(Arr[s].Trim());
                    }
                }
            }
        }

        //选择微信号事件
        private void lb_ms_chiwx_SelectedIndexChanged_1(object sender, EventArgs e) {
            if (lb_ms_chiwx.SelectedItem!=null) {
                this.lb_ms_myfri.Items.Clear();
                selwx = lb_ms_chiwx.SelectedItem.ToString();
                chiwx_get_ms_fried();
            }
        }

        /// <summary>
        /// 点击好友事件
        /// </summary>
        private void lb_ms_myfri_SelectedValueChanged(object sender, EventArgs e) {
            txtPage.Text = "1";
            lblPageCount.Text = "1";
            is_search = false;
            try {
                 selfri = lb_ms_myfri.SelectedItem.ToString();
                if (selfri != "" && selwx != "" ) { 
                    int i = sql.SearchChatLogCount(selwx, selfri, dt);
                    if (i < 1)
                        return;
                    page_current = i % pageshow_count == 0 ? i / pageshow_count : i / pageshow_count + 1;
                    lblPageCount.Text = page_current.ToString();
                    txtPage.Text = 1.ToString();
                    SearchChatLogFromFriend(page_current, selfri);
                    showchLog(selwx, selfri);
                }
            }
            catch (Exception) {
            }
        }

        string SearchTextStr = "";
        DateTime dt;
        int pageshow_count = 20;
        int page_current = 1;
      
        private void btn_search_Click( object sender, EventArgs e ) {
            is_search = true;
            search_conte = this.tbx_search.Text.Trim();

            if (search_conte == "" || selwx == "") { if (selwx == "") MessageBox.Show("请选择微信！"); return; }
            if (panel1.Visible == true) panel1.Visible = false;

            chatLogBox1.Clear();        //清空之前加载的数据
            switch (SearchTextStr) {
            case "好友":
            int i = sql.SearchChatLogCount(selwx, search_conte, dt);
            if (i < 1)
                return;
            page_current = i % pageshow_count == 0 ? i / pageshow_count : i / pageshow_count + 1;
            lblPageCount.Text = page_current.ToString();
            txtPage.Text = 1.ToString();
            SearchChatLogFromFriend(page_current, search_conte);

            break;

            case "聊天记录":
            int j = sql.GetChatLogPageCount(selwx, search_conte, dt);
            if (j < 1)
                return;
            page_current = j % pageshow_count == 0 ? j / pageshow_count : j / pageshow_count + 1;
            lblPageCount.Text = page_current.ToString();
            txtPage.Text = 1.ToString();
            SearchChatLogFangFa(page_current);
            break;
            }
        }

        List<WXMsg> L = new List<WXMsg>();
        void SearchChatLogFromFriend( int page, string friend ) {
            L = sql.SearchNumChatLog(selwx, friend, pageshow_count, page, dt);
            foreach (WXMsg item in L) {
                chatLogBox1.RecoveryChatLog(item);
            }
        }

        void SearchChatLogFangFa( int j ) {
            L = sql.GetChatLogNumPage(selwx, search_conte, pageshow_count, j, dt);
            for (int i = 0; i < L.Count; i++) {
                chatLogBox1.RecoveryChatLog(L[i]);
                if (i == 0)
                    Thread.Sleep(5);
            }
        }

        /// <summary>
        /// 打开日期选择器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_seltim_Click_1( object sender, EventArgs e ) {
            monthCalendar1.Visible = true;
            List<DateTime> l = sql.GetTimFroToUseAndTabNaInChaLog(selwx, selfri);
            DateTime[] dt = new DateTime[l.Count];
            for (int i = 0; i < l.Count; i++) {
                dt[i] = l[i];
            }
            monthCalendar1.BoldedDates = dt;
        }

        private void monthCalendar1_DateSelected_1( object sender, DateRangeEventArgs e ) {
            chatLogBox1.Clear();   //清空之前加载的数据
            string sel_time = monthCalendar1.SelectionStart.ToString();
            string tim = sel_time.Substring(0, sel_time.Length - 7);
            List<WXMsg> li_msg = sql.GetCLogFroToUseAndTimNaInChaLog(selwx, selfri, tim.Trim());
            for (int i = 0; i < li_msg.Count; i++) {
                chatLogBox1.RecoveryChatLog(li_msg[i]);
            }
            monthCalendar1.Visible = false;
            lb_tim.Text = tim;
        }

        private void button1_Click( object sender, EventArgs e ) {
            if (panel1.Visible == true)
                panel1.Visible = false;
            else
                panel1.Visible = true;
        }

        private void button3_Click( object sender, EventArgs e ) {
            panel1.Visible = false;
        }

        private void button2_Click( object sender, EventArgs e ) {
            if (comboBox1.Text == "") return;
            SearchTextStr = comboBox1.Text;
            int i = 0;
            switch (comboBox2.Text) {
            case "近一个月":
            i = -1;
            break;
            case "近三个月":
            i = -3;
            break;
            case "近一年":
            i = -12;
            break;
            case "全部":
            i = 0;
            break;
            }
            if (i == 0) dt = Convert.ToDateTime("1753/1/1 0:00:00");
            else dt = DateTime.Now.AddMonths(i);
            panel1.Visible = false;
        }

        private void export() {
           ChatLogExport ce = new ChatLogExport();
           if (FilePath!=string.Empty) {
              ce.ChatLog_Export(FilePath, selwx, selfri);
           }
        }
        private void 导出消息记录ToolStripMenuItem_Click( object sender, EventArgs e ) {
            SaveFileDialog kk = new SaveFileDialog();
            kk.Title = "保存消息记录(html)文件";
            kk.Filter = "html文件(*.html)|*.html|所有文件(*.*) |*.*";
            kk.FilterIndex = 1;
            kk.FileName = selfri + ".html";
            if (kk.ShowDialog() == DialogResult.OK) {
                FilePath = kk.FileName;
                Thread ss = new Thread(new ThreadStart(export));
                ss.Start();
            }
        }
    }
}
