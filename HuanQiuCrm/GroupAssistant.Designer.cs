﻿namespace HuanQiuCrm {
    partial class GroupAssistant {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroupAssistant));
            this.label3 = new System.Windows.Forms.Label();
            this.lb_ms_chiwx = new CCWin.SkinControl.SkinListBox();
            this.btn_done = new CCWin.SkinControl.SkinButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbx_topic = new CCWin.SkinControl.SkinTextBox();
            this.lbx_seledfris = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lb_fri_num = new System.Windows.Forms.Label();
            this.btn_search = new CCWin.SkinControl.SkinButton();
            this.tbx_searchs = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 19);
            this.label3.TabIndex = 32;
            this.label3.Text = "一，微信";
            // 
            // lb_ms_chiwx
            // 
            this.lb_ms_chiwx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_ms_chiwx.Back = null;
            this.lb_ms_chiwx.BackColor = System.Drawing.Color.LightGray;
            this.lb_ms_chiwx.BorderColor = System.Drawing.Color.Transparent;
            this.lb_ms_chiwx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb_ms_chiwx.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lb_ms_chiwx.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_ms_chiwx.FormattingEnabled = true;
            this.lb_ms_chiwx.ImageVisble = false;
            this.lb_ms_chiwx.IntegralHeight = false;
            this.lb_ms_chiwx.ItemHeight = 35;
            this.lb_ms_chiwx.Location = new System.Drawing.Point(0, 69);
            this.lb_ms_chiwx.MouseColor = System.Drawing.Color.Silver;
            this.lb_ms_chiwx.Name = "lb_ms_chiwx";
            this.lb_ms_chiwx.RowBackColor1 = System.Drawing.Color.Transparent;
            this.lb_ms_chiwx.RowBackColor2 = System.Drawing.Color.Transparent;
            this.lb_ms_chiwx.SelectedColor = System.Drawing.Color.Silver;
            this.lb_ms_chiwx.Size = new System.Drawing.Size(164, 444);
            this.lb_ms_chiwx.TabIndex = 31;
            this.lb_ms_chiwx.SelectedIndexChanged += new System.EventHandler(this.lb_ms_chiwx_SelectedIndexChanged);
            // 
            // btn_done
            // 
            this.btn_done.BackColor = System.Drawing.Color.Transparent;
            this.btn_done.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_done.DownBack = null;
            this.btn_done.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.ForeColor = System.Drawing.Color.Black;
            this.btn_done.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_done.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_done.Location = new System.Drawing.Point(866, 464);
            this.btn_done.MouseBack = null;
            this.btn_done.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_done.Name = "btn_done";
            this.btn_done.NormlBack = null;
            this.btn_done.Size = new System.Drawing.Size(79, 27);
            this.btn_done.TabIndex = 34;
            this.btn_done.Text = "完成";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.IntegralHeight = false;
            this.listBox1.ItemHeight = 14;
            this.listBox1.Location = new System.Drawing.Point(165, 96);
            this.listBox1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(259, 417);
            this.listBox1.Sorted = true;
            this.listBox1.TabIndex = 33;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(168, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 19);
            this.label1.TabIndex = 35;
            this.label1.Text = "二，选择好友";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(689, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 19);
            this.label2.TabIndex = 36;
            this.label2.Text = "四，输入群名";
            // 
            // tbx_topic
            // 
            this.tbx_topic.BackColor = System.Drawing.Color.Transparent;
            this.tbx_topic.DownBack = null;
            this.tbx_topic.Icon = null;
            this.tbx_topic.IconIsButton = false;
            this.tbx_topic.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_topic.IsPasswordChat = '\0';
            this.tbx_topic.IsSystemPasswordChar = false;
            this.tbx_topic.Lines = new string[0];
            this.tbx_topic.Location = new System.Drawing.Point(730, 220);
            this.tbx_topic.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_topic.MaxLength = 32767;
            this.tbx_topic.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_topic.MouseBack = null;
            this.tbx_topic.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_topic.Multiline = false;
            this.tbx_topic.Name = "tbx_topic";
            this.tbx_topic.NormlBack = null;
            this.tbx_topic.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_topic.ReadOnly = false;
            this.tbx_topic.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_topic.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_topic.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_topic.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_topic.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_topic.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_topic.SkinTxt.Name = "BaseText";
            this.tbx_topic.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_topic.SkinTxt.TabIndex = 0;
            this.tbx_topic.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_topic.SkinTxt.WaterText = "";
            this.tbx_topic.TabIndex = 37;
            this.tbx_topic.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_topic.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_topic.WaterText = "";
            this.tbx_topic.WordWrap = true;
            // 
            // lbx_seledfris
            // 
            this.lbx_seledfris.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lbx_seledfris.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbx_seledfris.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbx_seledfris.FormattingEnabled = true;
            this.lbx_seledfris.IntegralHeight = false;
            this.lbx_seledfris.ItemHeight = 14;
            this.lbx_seledfris.Location = new System.Drawing.Point(424, 69);
            this.lbx_seledfris.Name = "lbx_seledfris";
            this.lbx_seledfris.Size = new System.Drawing.Size(258, 444);
            this.lbx_seledfris.Sorted = true;
            this.lbx_seledfris.TabIndex = 38;
            this.lbx_seledfris.SelectedIndexChanged += new System.EventHandler(this.lbx_seledfris_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(421, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 19);
            this.label4.TabIndex = 39;
            this.label4.Text = "三，已选择好友";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.label5.Location = new System.Drawing.Point(734, 276);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 12);
            this.label5.TabIndex = 40;
            this.label5.Text = "共选择好友个数：";
            // 
            // lb_fri_num
            // 
            this.lb_fri_num.AutoSize = true;
            this.lb_fri_num.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.lb_fri_num.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.lb_fri_num.Location = new System.Drawing.Point(848, 276);
            this.lb_fri_num.Name = "lb_fri_num";
            this.lb_fri_num.Size = new System.Drawing.Size(12, 12);
            this.lb_fri_num.TabIndex = 41;
            this.lb_fri_num.Text = "0";
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.Transparent;
            this.btn_search.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_search.DownBack = null;
            this.btn_search.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.ForeColor = System.Drawing.Color.Black;
            this.btn_search.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_search.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_search.Location = new System.Drawing.Point(363, 69);
            this.btn_search.MouseBack = null;
            this.btn_search.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.Name = "btn_search";
            this.btn_search.NormlBack = null;
            this.btn_search.Size = new System.Drawing.Size(61, 24);
            this.btn_search.TabIndex = 42;
            this.btn_search.Text = "搜索";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // tbx_searchs
            // 
            this.tbx_searchs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_searchs.Font = new System.Drawing.Font("微软雅黑", 13.6F);
            this.tbx_searchs.Location = new System.Drawing.Point(165, 69);
            this.tbx_searchs.Name = "tbx_searchs";
            this.tbx_searchs.Size = new System.Drawing.Size(196, 24);
            this.tbx_searchs.TabIndex = 43;
            this.tbx_searchs.TextChanged += new System.EventHandler(this.tbx_searchs_TextChanged);
            // 
            // GroupAssistant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackShade = false;
            this.BackToColor = false;
            this.BorderColor = System.Drawing.Color.Transparent;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(975, 513);
            this.Controls.Add(this.tbx_searchs);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.lb_fri_num);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbx_seledfris);
            this.Controls.Add(this.tbx_topic);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lb_ms_chiwx);
            this.EffectWidth = 1;
            this.ForeColor = System.Drawing.Color.Transparent;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "GroupAssistant";
            this.Radius = 0;
            this.Shadow = true;
            this.ShadowWidth = 10;
            this.ShowBorder = false;
            this.Text = "群聊助手";
            this.Load += new System.EventHandler(this.GroupAssistant_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private CCWin.SkinControl.SkinListBox lb_ms_chiwx;
        private CCWin.SkinControl.SkinButton btn_done;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private CCWin.SkinControl.SkinTextBox tbx_topic;
        private System.Windows.Forms.ListBox lbx_seledfris;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lb_fri_num;
        private CCWin.SkinControl.SkinButton btn_search;
        private System.Windows.Forms.TextBox tbx_searchs;
    }
}