﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;

namespace HuanQiuCrm
{
    public partial class HistoryVer : Skin_Color
    {
        public HistoryVer()
        {
            InitializeComponent();
        }

        private List<VersionTable> l = new List<VersionTable>();
        VerShow vs;
        bool isOpen = false;


        private void HistoryVer_Load(object sender, EventArgs e)
        {
            VersionServices vs = new VersionServices();
            l = vs.searchVersion();

            for (int i = 0; i < l.Count; i++)
            {
                Button b = new Button();
                b.Font = new System.Drawing.Font("微软雅黑",12F,FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                b.AutoSize = false;
                b.BackColor = Color.White;
                b.ForeColor = Color.Black;
                b.Text = l[i].Version;
                if (l.Count > 8)
                    b.Width = this.Width - 23;
                else
                    b.Width = this.Width - 8;
                b.Height = 50;
                b.FlatStyle = FlatStyle.Flat;
                b.FlatAppearance.BorderColor = Color.White;
                b.Location = new Point(0,i*b.Height);
                b.MouseLeave += new EventHandler(ClearShow);
                b.MouseHover += new EventHandler(SearchShow);
                panel1.Controls.Add(b);
            }
        }

        private void SearchShow(object sender, EventArgs e)
        {
            if(isOpen == false)
            {
                vs = new VerShow();
                foreach (VersionTable item in l)
                {
                    if (((Button)sender).Text.Equals(item.Version))
                    {
                        vs.vt = item;
                        vs.StartPosition = new FormStartPosition();
                        vs.Location = new Point(MousePosition.X+5, MousePosition.Y);
                        vs.Show();
                        isOpen = true;
                    }
                }
            }
        }


        private void ClearShow(object sender,EventArgs e)
        {
            if (isOpen == true)
            {
                vs.Close();
                isOpen = false;
            }
        }

    }
}
