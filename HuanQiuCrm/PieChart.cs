﻿using HuanQiuCrm.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WChat;

namespace PieChart
{
   public class PieChart
    {
        SqlHandle sql = new SqlHandle();
        Settings st = new Settings();
        string[] arr;  //保存好友所在地区（不重复）

        Chart chart = new Chart();
        public PieChart(Control control, Title title)
        {
            chart.ChartAreas.Add("ChartArea");
            chart.Legends.Add("Legends");
            chart.Series.Add("Series");
            chart.Width = 900;
            chart.Height = 600;
            chart.Anchor = (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left);   
            //Title title = new Title();
            //title.Text = title.ToString();
            title.Alignment = ContentAlignment.MiddleCenter;
            title.Font = new System.Drawing.Font("Trebuchet MS", 14F, FontStyle.Bold);
            chart.Titles.Add(title);
            chart.ChartAreas[0].AxisX.Interval = 1;
            chart.Legends["Legends"].BackHatchStyle = ChartHatchStyle.DarkDownwardDiagonal;
            chart.Legends["Legends"].BorderWidth = 1;
            chart.Legends["Legends"].BorderColor = Color.FromArgb(200, 200, 200);
            chart.Series["Series"].ChartType = SeriesChartType.Pie;
            chart.Series["Series"].ChartType = SeriesChartType.Doughnut;
            chart.Series["Series"].LegendText = "#VALX:    [ #PERCENT{P1} ]";
            chart.Series["Series"].Label = "#VALX";
            chart.Series["Series"].Font = new System.Drawing.Font("Trebuchet MS", 10, System.Drawing.FontStyle.Bold);
            chart.Series["Series"].BorderColor = Color.FromArgb(255, 101, 101, 101);
            chart.Series["Series"]["PieLabelStyle"] = "Outside";
            chart.Series["Series"]["PieDrawingStyle"] = "Default";
            control.Controls.Add(chart);
        }
        public void showData ()
        {
            arr = sql.Select_friend_pro(st.UseAcct).Distinct().ToArray();
            for (int i = 0; i < arr.Length; i++) {
                int num = sql.SearchProNum(arr[i], st.UseAcct);
                if (arr[i] == "") {
                    chart.Series[0].Points.AddXY("未知地区", num);
                }
                else {
                    chart.Series[0].Points.AddXY(arr[i], num);
                }
            }

        }
    }
}
