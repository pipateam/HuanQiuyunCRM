﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using DAL;
using System.Data.SqlClient;
using HuanQiuCrm.Properties;
using WChat;
using System.IO;

namespace HuanQiuCrm {
    public partial class costum_analy : Skin_Color {
        SqlHandle sql = new SqlHandle();
        private DataGridView sel_data_grid_view;
        private string selwx;
        public costum_analy() {
            InitializeComponent();
        }

        private void read_basic_info(string tabna) { 
            //string tbna = "RequirenmentAnalysis" + tabna;
            try {
                String strConn = "Data Source=bds249611391.my3w.com;Initial Catalog=bds249611391_db;Persist Security Info=True;User ID=bds249611391;Password=hq312453";
                SqlConnection conn = new SqlConnection(strConn);
                String sqlId = "select * from EssentialInformation" + tabna;
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                da.Fill(ds, "EssentialInformation" + tabna );

                dgv_basic_ana.DataSource = ds;
                dgv_basic_ana.DataMember = "EssentialInformation" + tabna;
                conn.Close();

                dgv_basic_ana.Columns[0].HeaderText = "序号";
                dgv_basic_ana.Columns[1].HeaderText = "备注名";
                dgv_basic_ana.Columns[2].HeaderText = "昵称";
                dgv_basic_ana.Columns[3].HeaderText = "账号";
                dgv_basic_ana.Columns[4].HeaderText = "所属";
                dgv_basic_ana.Columns[5].HeaderText = "性别";
                dgv_basic_ana.Columns[6].HeaderText = "年龄段";
                dgv_basic_ana.Columns[7].HeaderText = "出生日期";
                dgv_basic_ana.Columns[8].HeaderText = "手机号";
                dgv_basic_ana.Columns[9].HeaderText = "QQ";
                dgv_basic_ana.Columns[10].HeaderText = "职业";
                dgv_basic_ana.Columns[11].HeaderText = "收入水平";
                dgv_basic_ana.Columns[12].HeaderText = "联系地址";
                dgv_basic_ana.Columns[13].HeaderText = "所属客服";
                dgv_basic_ana.Columns[14].HeaderText = "客户分类";
                dgv_basic_ana.Columns[15].HeaderText = "创建人";
                dgv_basic_ana.Columns[16].HeaderText = "修改人";
                dgv_basic_ana.Columns[17].HeaderText = "最后修改时间";
            }
            catch {

            }

        }

        private void read_circle_friends(string tabna) { 
            //string tbna = "RequirenmentAnalysis" + tabna;
            try {
                String strConn = "Data Source=bds249611391.my3w.com;Initial Catalog=bds249611391_db;Persist Security Info=True;User ID=bds249611391;Password=hq312453";
                SqlConnection conn = new SqlConnection(strConn);
                String sqlId = "select * from CircleOfFriends" + tabna;
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                da.Fill(ds, "CircleOfFriends" + tabna);

                dgv_cicrcle_friend.DataSource = ds;
                dgv_cicrcle_friend.DataMember = "CircleOfFriends" + tabna;
                conn.Close();

                dgv_cicrcle_friend.Columns[0].HeaderText = "序号";
                dgv_cicrcle_friend.Columns[1].HeaderText = "客户名";
                dgv_cicrcle_friend.Columns[2].HeaderText = "朋友圈喜好";
                dgv_cicrcle_friend.Columns[3].HeaderText = "朋友圈活跃时间";
                dgv_cicrcle_friend.Columns[4].HeaderText = "个人画像分析";
                dgv_cicrcle_friend.Columns[5].HeaderText = "自定义项1";
                dgv_cicrcle_friend.Columns[6].HeaderText = "自定义项2";
                dgv_cicrcle_friend.Columns[7].HeaderText = "自定义项3";

            }
            catch {

            }

        }

        private void read_requirement_analy(string tabna) {
            //string tbna = "RequirenmentAnalysis" + tabna;
            try {
                String strConn = "Data Source=bds249611391.my3w.com;Initial Catalog=bds249611391_db;Persist Security Info=True;User ID=bds249611391;Password=hq312453";
                SqlConnection conn = new SqlConnection(strConn);
                String sqlId = "select * from RequirenmentAnalysis" + tabna;
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                da.Fill(ds, "RequirenmentAnalysis" + tabna);

                dgv_requirement_analy.DataSource = ds;
                dgv_requirement_analy.DataMember = "RequirenmentAnalysis" + tabna;
                conn.Close();

                dgv_requirement_analy.Columns[0].HeaderText = "序号";
                dgv_requirement_analy.Columns[1].HeaderText = "客户昵称";
                dgv_requirement_analy.Columns[2].HeaderText = "现阶段需求";
                dgv_requirement_analy.Columns[3].HeaderText = "潜在需求";
                dgv_requirement_analy.Columns[4].HeaderText = "自定义项1";
                dgv_requirement_analy.Columns[5].HeaderText = "自定义项2";
                dgv_requirement_analy.Columns[6].HeaderText = "自定义项3";
            }
            catch {

            }

        }

        private void read_consumption_analy(string tabna) {
            //string tbna = "RequirenmentAnalysis" + tabna;
            try {
                String strConn = "Data Source=bds249611391.my3w.com;Initial Catalog=bds249611391_db;Persist Security Info=True;User ID=bds249611391;Password=hq312453";
                SqlConnection conn = new SqlConnection(strConn);
                String sqlId = "select * from ConsumptionAnalysis" + tabna;
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                da.Fill(ds, "ConsumptionAnalysis" + tabna);

                dgv_consump_ana.DataSource = ds;
                dgv_consump_ana.DataMember = "ConsumptionAnalysis" + tabna;
                conn.Close();

                dgv_consump_ana.Columns[0].HeaderText = "序号";
                dgv_consump_ana.Columns[1].HeaderText = "客户昵称";
                dgv_consump_ana.Columns[2].HeaderText = "消费标准";
                dgv_consump_ana.Columns[3].HeaderText = "消费习惯";
                dgv_consump_ana.Columns[4].HeaderText = "化妆品品牌倾向";
                dgv_consump_ana.Columns[5].HeaderText = "最近购买化妆品";
                dgv_consump_ana.Columns[6].HeaderText = "护肤品牌倾向";
                dgv_consump_ana.Columns[7].HeaderText = "最近购买护肤品";
                dgv_consump_ana.Columns[8].HeaderText = "穿衣品牌倾向";
                dgv_consump_ana.Columns[9].HeaderText = "最近购买衣服";
                dgv_consump_ana.Columns[10].HeaderText = "饰品品牌倾向";
                dgv_consump_ana.Columns[11].HeaderText = "最近购买饰品";
                dgv_consump_ana.Columns[12].HeaderText = "综合分析";

            }
            catch {

            }

        }

        private void costum_analy_Load(object sender, EventArgs e) {
           // read_basic_info("总有一天会遇见你");
           // read_circle_friends("总有一天会遇见你");
           // read_requirement_analy("总有一天会遇见你");
           // read_consumption_analy("总有一天会遇见你");
            show_cos_ana();

           // RequirenmentAnalysisService ras = new RequirenmentAnalysisService();
           //// MessageBox.Show(ras.SearchAll("空白").CurrentDemand);
           // //dgv_cus_ana.Columns[0].HeaderText = ras.SearchAll("空白").CurrentDemand;
        }

        private void show_cos_ana() {
            Settings st = new Settings();
            string acco = st.UseAcct;
            List<string> ss = new List<string>();
            ss = sql.Select_Num_Wx(acco);
            for (int i = 0; i < ss.Count; i++) {
                if (ss[i].Trim()!="") {
                    lb_ms_chiwx.Items.Add(new CCWin.SkinControl.SkinListBoxItem(ss[i].Trim()));
                }
            }
        }

        private void lb_ms_chiwx_SelectedValueChanged(object sender, EventArgs e) {
            if (lb_ms_chiwx.SelectedItem!=null) {
                selwx = lb_ms_chiwx.SelectedItem.ToString();
                try {
                    dgv_basic_ana.DataSource = null;    //清空数据
                    dgv_cicrcle_friend.DataSource = null;   //清空数据
                    dgv_requirement_analy.DataSource = null;     //清空数据
                    dgv_consump_ana.DataSource = null;      //清空数据
                    read_basic_info(selwx);
                    read_circle_friends(selwx);
                    read_requirement_analy(selwx);
                    read_consumption_analy(selwx);
                }
                catch {

                }
            }
        }

        public void DataToExcel( DataGridView m_DataView )
         {
             SaveFileDialog kk = new SaveFileDialog(); 
             kk.Title = "保存EXECL文件"; 
             kk.Filter = "EXECL文件(*.xls) |*.xls |所有文件(*.*) |*.*"; 
             kk.FilterIndex = 1;
             if (kk.ShowDialog() == DialogResult.OK) 
             { 
                 string FileName = kk.FileName; // + ".xls"
                if (File.Exists(FileName))
                    File.Delete(FileName);
                FileStream objFileStream; 
                StreamWriter objStreamWriter; 
                string strLine = ""; 
               objFileStream = new FileStream( FileName, FileMode.OpenOrCreate, FileAccess.Write); 
                objStreamWriter = new StreamWriter( objFileStream, System.Text.Encoding.Unicode);
                for (int i = 0; i<m_DataView.Columns.Count; i++) 
                { 
                    if (m_DataView.Columns[i].Visible == true) 
                    { 
                        strLine = strLine + m_DataView.Columns[i].HeaderText.ToString() + Convert.ToChar(9); 
                    } 
                } 
                objStreamWriter.WriteLine(strLine); 
                strLine = ""; 

                for (int i = 0; i<m_DataView.Rows.Count; i++) 
                { 
                    if (m_DataView.Columns[0].Visible == true) 
                    { 
                        if (m_DataView.Rows[i].Cells[0].Value == null) 
                            strLine = strLine + " " + Convert.ToChar(9); 
                        else 
                            strLine = strLine + m_DataView.Rows[i].Cells[0].Value.ToString() + Convert.ToChar(9); 
                    } 
                    for (int j = 1; j<m_DataView.Columns.Count; j++) 
                    { 
                        if (m_DataView.Columns[j].Visible == true) 
                        { 
                            if (m_DataView.Rows[i].Cells[j].Value == null) 
                                strLine = strLine + " " + Convert.ToChar(9); 
                            else 
                            { 
                                string rowstr = ""; 
                                rowstr = m_DataView.Rows[i].Cells[j].Value.ToString(); 
                                if (rowstr.IndexOf("\r\n") >  0) 
                                    rowstr = rowstr.Replace("\r\n", " "); 
                                if (rowstr.IndexOf("\t") >  0) 
                                    rowstr = rowstr.Replace("\t", " "); 
                                strLine = strLine + rowstr + Convert.ToChar(9); 
                            } 
                        } 
                    } 
                    objStreamWriter.WriteLine(strLine); 
                    strLine = ""; 
                } 
                objStreamWriter.Close(); 
                objFileStream.Close();
                MessageBox.Show(this,"保存EXCEL成功","提示", MessageBoxButtons.OK, MessageBoxIcon.Information); 
            }
        }

        private void label2_Click( object sender, EventArgs e ) {
            if (sel_data_grid_view!=null) {
                DataToExcel(sel_data_grid_view);
            }
        }

        private void skinTabControl1_SelectedIndexChanged( object sender, EventArgs e ) {
            string pag_name = skinTabControl1.SelectedTab.Text.ToString();
            if (pag_name!="") {
                switch (pag_name) {
                case "基本信息":
                sel_data_grid_view = dgv_basic_ana;
                break;
                case "朋友圈分析":
                sel_data_grid_view = dgv_cicrcle_friend;
                break;
                case "需求分析":
                sel_data_grid_view = dgv_requirement_analy;
                break;
                case "消费分析":
                sel_data_grid_view = dgv_consump_ana;
                break;
                }
            }
        }
    }
}
