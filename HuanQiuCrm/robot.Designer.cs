﻿namespace HuanQiuCrm
{
    partial class robot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(robot));
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.checkBox1 = new CCWin.SkinControl.SkinCheckBox();
            this.checkbox2 = new CCWin.SkinControl.SkinCheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.btn_add = new CCWin.SkinControl.SkinButton();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.tbx_content = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.tbx_keyword = new CCWin.SkinControl.SkinTextBox();
            this.skinDataGridView1 = new CCWin.SkinControl.SkinDataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.skinPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinDataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.skinLabel1.Location = new System.Drawing.Point(19, 207);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(149, 20);
            this.skinLabel1.TabIndex = 0;
            this.skinLabel1.Text = "请选择一项回复功能：";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.BackColor = System.Drawing.Color.Transparent;
            this.checkBox1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.checkBox1.DownBack = null;
            this.checkBox1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox1.Location = new System.Drawing.Point(180, 206);
            this.checkBox1.MouseBack = null;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.NormlBack = null;
            this.checkBox1.SelectedDownBack = null;
            this.checkBox1.SelectedMouseBack = null;
            this.checkBox1.SelectedNormlBack = null;
            this.checkBox1.Size = new System.Drawing.Size(87, 21);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "机器人回复";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // checkbox2
            // 
            this.checkbox2.AutoSize = true;
            this.checkbox2.BackColor = System.Drawing.Color.Transparent;
            this.checkbox2.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.checkbox2.DownBack = null;
            this.checkbox2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkbox2.Location = new System.Drawing.Point(282, 206);
            this.checkbox2.MouseBack = null;
            this.checkbox2.Name = "checkbox2";
            this.checkbox2.NormlBack = null;
            this.checkbox2.SelectedDownBack = null;
            this.checkbox2.SelectedMouseBack = null;
            this.checkbox2.SelectedNormlBack = null;
            this.checkbox2.Size = new System.Drawing.Size(87, 21);
            this.checkbox2.TabIndex = 2;
            this.checkbox2.Text = "关键词回复";
            this.checkbox2.UseVisualStyleBackColor = false;
            this.checkbox2.Click += new System.EventHandler(this.checkbox2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.button1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.button1.Location = new System.Drawing.Point(387, 206);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 29);
            this.button1.TabIndex = 3;
            this.button1.Text = "确定";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.skinPanel1.Controls.Add(this.btn_add);
            this.skinPanel1.Controls.Add(this.tbx_content);
            this.skinPanel1.Controls.Add(this.skinLabel3);
            this.skinPanel1.Controls.Add(this.skinLabel2);
            this.skinPanel1.Controls.Add(this.tbx_keyword);
            this.skinPanel1.Controls.Add(this.skinDataGridView1);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(-2, 261);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(498, 320);
            this.skinPanel1.TabIndex = 4;
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Transparent;
            this.btn_add.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(202)))), ((int)(((byte)(10)))));
            this.btn_add.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(202)))), ((int)(((byte)(10)))));
            this.btn_add.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_add.DownBack = null;
            this.btn_add.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btn_add.Location = new System.Drawing.Point(389, 32);
            this.btn_add.MouseBack = null;
            this.btn_add.Name = "btn_add";
            this.btn_add.NormlBack = null;
            this.btn_add.Size = new System.Drawing.Size(74, 28);
            this.btn_add.TabIndex = 17;
            this.btn_add.Text = "添加";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.skinLabel4.ForeColor = System.Drawing.Color.Red;
            this.skinLabel4.Location = new System.Drawing.Point(7, 240);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(83, 16);
            this.skinLabel4.TabIndex = 16;
            this.skinLabel4.Text = "设定一个关键词*";
            // 
            // tbx_content
            // 
            this.tbx_content.BackColor = System.Drawing.Color.Transparent;
            this.tbx_content.DownBack = null;
            this.tbx_content.Icon = null;
            this.tbx_content.IconIsButton = false;
            this.tbx_content.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_content.IsPasswordChat = '\0';
            this.tbx_content.IsSystemPasswordChar = false;
            this.tbx_content.Lines = new string[0];
            this.tbx_content.Location = new System.Drawing.Point(179, 32);
            this.tbx_content.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_content.MaxLength = 32767;
            this.tbx_content.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_content.MouseBack = null;
            this.tbx_content.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_content.Multiline = false;
            this.tbx_content.Name = "tbx_content";
            this.tbx_content.NormlBack = null;
            this.tbx_content.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_content.ReadOnly = false;
            this.tbx_content.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_content.Size = new System.Drawing.Size(197, 28);
            // 
            // 
            // 
            this.tbx_content.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_content.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_content.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_content.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_content.SkinTxt.Name = "BaseText";
            this.tbx_content.SkinTxt.Size = new System.Drawing.Size(187, 18);
            this.tbx_content.SkinTxt.TabIndex = 0;
            this.tbx_content.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_content.SkinTxt.WaterText = "";
            this.tbx_content.TabIndex = 15;
            this.tbx_content.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_content.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_content.WaterText = "";
            this.tbx_content.WordWrap = true;
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(176, 9);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(59, 17);
            this.skinLabel3.TabIndex = 14;
            this.skinLabel3.Text = "回复内容:";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(18, 7);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(47, 17);
            this.skinLabel2.TabIndex = 13;
            this.skinLabel2.Text = "关键词:";
            // 
            // tbx_keyword
            // 
            this.tbx_keyword.BackColor = System.Drawing.Color.Transparent;
            this.tbx_keyword.DownBack = null;
            this.tbx_keyword.Icon = null;
            this.tbx_keyword.IconIsButton = false;
            this.tbx_keyword.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_keyword.IsPasswordChat = '\0';
            this.tbx_keyword.IsSystemPasswordChar = false;
            this.tbx_keyword.Lines = new string[0];
            this.tbx_keyword.Location = new System.Drawing.Point(21, 32);
            this.tbx_keyword.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_keyword.MaxLength = 32767;
            this.tbx_keyword.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_keyword.MouseBack = null;
            this.tbx_keyword.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_keyword.Multiline = false;
            this.tbx_keyword.Name = "tbx_keyword";
            this.tbx_keyword.NormlBack = null;
            this.tbx_keyword.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_keyword.ReadOnly = false;
            this.tbx_keyword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_keyword.Size = new System.Drawing.Size(139, 28);
            // 
            // 
            // 
            this.tbx_keyword.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_keyword.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_keyword.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_keyword.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_keyword.SkinTxt.Name = "BaseText";
            this.tbx_keyword.SkinTxt.Size = new System.Drawing.Size(129, 18);
            this.tbx_keyword.SkinTxt.TabIndex = 0;
            this.tbx_keyword.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_keyword.SkinTxt.WaterText = "";
            this.tbx_keyword.TabIndex = 12;
            this.tbx_keyword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_keyword.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_keyword.WaterText = "";
            this.tbx_keyword.WordWrap = true;
            // 
            // skinDataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(246)))), ((int)(((byte)(253)))));
            this.skinDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.skinDataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.skinDataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.skinDataGridView1.ColumnFont = null;
            this.skinDataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.skinDataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.skinDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.skinDataGridView1.ColumnSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(188)))), ((int)(((byte)(240)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.skinDataGridView1.DefaultCellStyle = dataGridViewCellStyle3;
            this.skinDataGridView1.EnableHeadersVisualStyles = false;
            this.skinDataGridView1.GridColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.skinDataGridView1.HeadFont = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinDataGridView1.HeadSelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridView1.Location = new System.Drawing.Point(22, 64);
            this.skinDataGridView1.MouseCellBackColor = System.Drawing.Color.White;
            this.skinDataGridView1.Name = "skinDataGridView1";
            this.skinDataGridView1.ReadOnly = true;
            this.skinDataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.skinDataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.skinDataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.skinDataGridView1.RowTemplate.Height = 23;
            this.skinDataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.skinDataGridView1.Size = new System.Drawing.Size(442, 233);
            this.skinDataGridView1.TabIndex = 11;
            this.skinDataGridView1.TitleBack = null;
            this.skinDataGridView1.TitleBackColorBegin = System.Drawing.Color.White;
            this.skinDataGridView1.TitleBackColorEnd = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(196)))), ((int)(((byte)(242)))));
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 26);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.label1.Location = new System.Drawing.Point(21, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "请选择自动回复的微信号：";
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 19;
            this.listBox1.Location = new System.Drawing.Point(204, 52);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(170, 137);
            this.listBox1.TabIndex = 6;
            // 
            // robot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.Transparent;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(495, 583);
            this.Controls.Add(this.skinLabel4);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.skinPanel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkbox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.skinLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "robot";
            this.Shadow = true;
            this.ShowDrawIcon = false;
            this.Text = "智能机器人";
            this.Load += new System.EventHandler(this.robot_Load);
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skinDataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinCheckBox checkBox1;
        private CCWin.SkinControl.SkinCheckBox checkbox2;
        private System.Windows.Forms.Button button1;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinButton btn_add;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinTextBox tbx_content;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinTextBox tbx_keyword;
        private CCWin.SkinControl.SkinDataGridView skinDataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox1;
    }
}