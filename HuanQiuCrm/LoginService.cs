﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WChat;
namespace HuanQiuCrm {
    /// <summary>
    /// 微信登录服务类
    /// </summary>
    public class LoginService
    {
    /*********************************************************************************************************/
        public string Pass_Ticket = "";    //静态全局变量，到处都可以使用
        public string SKey = "";     //静态全局变量，到处都可以使用
        public string Wxuin = "";     //静态全局变量，到处都可以使用
        public string Wxsid = "";     //静态全局变量，到处都可以使用 
        private string _session_id = null;    //静态全局变量，到处都可以使用
        private BaseService lbs = new BaseService();
        private string _qrcode_url = "https://login.weixin.qq.com/qrcode/"; //后面增加会话id
        //判断二维码扫描情况   200表示扫描登录  201表示已扫描未登录  其它表示未扫描
        private string _login_check_url = "https://login.weixin.qq.com/cgi-bin/mmwebwx-bin/login?loginicon=true&uuid="; //后面增加会话id

        /// <summary>
        /// 获取登录二维码
        /// </summary>
        /// <returns></returns>
        public Image GetQRCode() {
            string _session_id_url = "https://login.weixin.qq.com/jslogin?appid=wx782c26e4c19acffb";
            // MessageBox.Show("下面是获取session_id的请求：");
            byte[] bytes = lbs.SendGetRequest(_session_id_url);    //获取session_id【1】
            if (bytes == null) return null;
            else if (bytes.Count() == 0) return null;
            else {
                _session_id = Encoding.UTF8.GetString(bytes).Split(new string[] { "\"" }, StringSplitOptions.None)[1];
                // MessageBox.Show("下面是获取二维码的请求：");
                bytes = lbs.SendGetRequest(_qrcode_url + _session_id);    //获取二维码的请求【2】
                if (bytes == null) return null;
                else if (bytes.Count() == 0) return null;
                else {
                    return Image.FromStream(new MemoryStream(bytes));
                }
            }
            return null;
        }

        /// <summary>
        /// 登录扫描检测
        /// </summary>
        /// <returns></returns>
        public object LoginCheck() {
            if (_session_id == null) {
                return null;
            }
            // MessageBox.Show("下面检查是否登陆成功的的请求，也是设置cookies的get请求：");
            byte[] bytes = lbs.SendGetRequest(_login_check_url + _session_id);    //检查是否登陆成功的的请求，也是设置cookies的get请求【3】
            if (bytes == null) {
                return null;
            }
            else if (bytes.Count() == 0) return null;
            else {
                string login_result = Encoding.UTF8.GetString(bytes);
                if (login_result.Contains("=201")) //已扫描 未登录
                {
                    string base64_image = login_result.Split(new string[] { "\'" }, StringSplitOptions.None)[1].Split(',')[1];
                    byte[] base64_image_bytes = Convert.FromBase64String(base64_image);
                    MemoryStream memoryStream = new MemoryStream(base64_image_bytes, 0, base64_image_bytes.Length);
                    memoryStream.Write(base64_image_bytes, 0, base64_image_bytes.Length);
                    ////转成图片
                    return Image.FromStream(memoryStream);
                }
                else if (login_result.Contains("=200"))  //已扫描 已登录
                {
                    string login_redirect_url = login_result.Split(new string[] { "\"" }, StringSplitOptions.None)[1];
                    return login_redirect_url;
                }
                else {
                    return null;
                }
            }
        }

        public object ClickLoginCheck(string uuid ) {
            if (uuid == string.Empty) {
                return null;
            }
            byte[] bytes = lbs.SendGetRequest(_login_check_url + uuid);    //检查是否登陆成功的的请求，也是设置cookies的get请求【3】
            if (bytes == null) {
                return null;
            }
            else if (bytes.Count() == 0) return null;
            else {
                string login_result = Encoding.UTF8.GetString(bytes);
                if (login_result.Contains("=201")) //已扫描 未登录
                {
                    string base64_image = login_result.Split(new string[] { "\'" }, StringSplitOptions.None)[1].Split(',')[1];
                    byte[] base64_image_bytes = Convert.FromBase64String(base64_image);
                    MemoryStream memoryStream = new MemoryStream(base64_image_bytes, 0, base64_image_bytes.Length);
                    memoryStream.Write(base64_image_bytes, 0, base64_image_bytes.Length);
                    ////转成图片
                    return Image.FromStream(memoryStream);
                }
                else if (login_result.Contains("=200"))  //已扫描 已登录
                {
                    string login_redirect_url = login_result.Split(new string[] { "\"" }, StringSplitOptions.None)[1];
                    return login_redirect_url;
                }
                else {
                    return null;
                }
            }
        }
        /// <summary>
        /// 获取sid uid结果存放在cookies中
        /// </summary>
        public void GetSidUid(string login_redirect) {
            byte[] bytes = null;
            bytes = lbs.SendGetRequest(login_redirect + "&fun=new&version=v2&lang=zh_CN");    //获取sid，uid的请求【4】 
            if (bytes == null) return;
            else if (bytes.Count() == 0) return;
            else {
                string pass_ticket = Encoding.UTF8.GetString(bytes);
                Pass_Ticket = pass_ticket.Split(new string[] { "pass_ticket" }, StringSplitOptions.None)[1].TrimStart('>').TrimEnd('<', '/');
                SKey = pass_ticket.Split(new string[] { "skey" }, StringSplitOptions.None)[1].TrimStart('>').TrimEnd('<', '/');
                Wxuin = pass_ticket.Split(new string[] { "wxuin" }, StringSplitOptions.None)[1].TrimStart('>').TrimEnd('<', '/');
                Wxsid = pass_ticket.Split(new string[] { "wxsid" }, StringSplitOptions.None)[1].TrimStart('>').TrimEnd('<', '/');

                CookieDictionary.AddCookies(login_redirect + "SKY", SKey);
                CookieDictionary.AddCookies(login_redirect + "Ticket", Pass_Ticket);
                CookieDictionary.AddCookies(login_redirect + "Wxuin", Wxuin);
                CookieDictionary.AddCookies(login_redirect + "Wxsid", Wxsid);
            }
        }
    }
}
