﻿namespace HuanQiuCrm {
    partial class Mess_Manage {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mess_Manage));
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_ms_chiwx = new System.Windows.Forms.ListBox();
            this.lb_ms_myfri = new System.Windows.Forms.ListBox();
            this.tbx_search = new System.Windows.Forms.TextBox();
            this.btn_search = new CCWin.SkinControl.SkinButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblPageCount = new System.Windows.Forms.Label();
            this.txtPage = new System.Windows.Forms.TextBox();
            this.btnPageDown = new System.Windows.Forms.Button();
            this.btnPageUp = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lb_tim = new System.Windows.Forms.Label();
            this.btn_seltim = new CCWin.SkinControl.SkinButton();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chatLogBox1 = new WChat.ChatLogBox();
            this.cms_selfri = new CCWin.SkinControl.SkinContextMenuStrip();
            this.导出消息记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除消息记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.cms_selfri.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(264, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 19);
            this.label5.TabIndex = 30;
            this.label5.Text = "最近聊天记录";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(134, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 19);
            this.label4.TabIndex = 29;
            this.label4.Text = "好友";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 19);
            this.label3.TabIndex = 28;
            this.label3.Text = "微信";
            // 
            // lb_ms_chiwx
            // 
            this.lb_ms_chiwx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_ms_chiwx.BackColor = System.Drawing.Color.Silver;
            this.lb_ms_chiwx.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb_ms_chiwx.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lb_ms_chiwx.FormattingEnabled = true;
            this.lb_ms_chiwx.IntegralHeight = false;
            this.lb_ms_chiwx.ItemHeight = 17;
            this.lb_ms_chiwx.Items.AddRange(new object[] {
            "1111",
            "22222",
            "3333"});
            this.lb_ms_chiwx.Location = new System.Drawing.Point(0, 75);
            this.lb_ms_chiwx.Name = "lb_ms_chiwx";
            this.lb_ms_chiwx.Size = new System.Drawing.Size(128, 443);
            this.lb_ms_chiwx.Sorted = true;
            this.lb_ms_chiwx.TabIndex = 33;
            this.lb_ms_chiwx.SelectedIndexChanged += new System.EventHandler(this.lb_ms_chiwx_SelectedIndexChanged_1);
            // 
            // lb_ms_myfri
            // 
            this.lb_ms_myfri.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lb_ms_myfri.BackColor = System.Drawing.Color.LightGray;
            this.lb_ms_myfri.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb_ms_myfri.ContextMenuStrip = this.cms_selfri;
            this.lb_ms_myfri.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lb_ms_myfri.FormattingEnabled = true;
            this.lb_ms_myfri.IntegralHeight = false;
            this.lb_ms_myfri.ItemHeight = 17;
            this.lb_ms_myfri.Location = new System.Drawing.Point(128, 75);
            this.lb_ms_myfri.Name = "lb_ms_myfri";
            this.lb_ms_myfri.Size = new System.Drawing.Size(136, 443);
            this.lb_ms_myfri.Sorted = true;
            this.lb_ms_myfri.TabIndex = 34;
            this.lb_ms_myfri.SelectedValueChanged += new System.EventHandler(this.lb_ms_myfri_SelectedValueChanged);
            // 
            // tbx_search
            // 
            this.tbx_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbx_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.tbx_search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbx_search.Location = new System.Drawing.Point(796, 45);
            this.tbx_search.Multiline = true;
            this.tbx_search.Name = "tbx_search";
            this.tbx_search.Size = new System.Drawing.Size(100, 24);
            this.tbx_search.TabIndex = 38;
            // 
            // btn_search
            // 
            this.btn_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.btn_search.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_search.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_search.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_search.DownBack = null;
            this.btn_search.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_search.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_search.InnerBorderColor = System.Drawing.Color.DimGray;
            this.btn_search.IsDrawGlass = false;
            this.btn_search.Location = new System.Drawing.Point(912, 44);
            this.btn_search.MouseBack = null;
            this.btn_search.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.Name = "btn_search";
            this.btn_search.NormlBack = null;
            this.btn_search.Size = new System.Drawing.Size(52, 25);
            this.btn_search.TabIndex = 39;
            this.btn_search.Text = "搜索";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.lblPageCount);
            this.panel3.Controls.Add(this.txtPage);
            this.panel3.Controls.Add(this.btnPageDown);
            this.panel3.Controls.Add(this.btnPageUp);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.lb_tim);
            this.panel3.Controls.Add(this.btn_seltim);
            this.panel3.Location = new System.Drawing.Point(264, 487);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(724, 30);
            this.panel3.TabIndex = 40;
            // 
            // lblPageCount
            // 
            this.lblPageCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPageCount.AutoSize = true;
            this.lblPageCount.Location = new System.Drawing.Point(658, 8);
            this.lblPageCount.Name = "lblPageCount";
            this.lblPageCount.Size = new System.Drawing.Size(11, 12);
            this.lblPageCount.TabIndex = 45;
            this.lblPageCount.Text = "0";
            // 
            // txtPage
            // 
            this.txtPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPage.Location = new System.Drawing.Point(614, 8);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(38, 14);
            this.txtPage.TabIndex = 48;
            this.txtPage.Text = "0";
            this.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnPageDown
            // 
            this.btnPageDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPageDown.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPageDown.FlatAppearance.BorderSize = 0;
            this.btnPageDown.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPageDown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnPageDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPageDown.Location = new System.Drawing.Point(696, 4);
            this.btnPageDown.Name = "btnPageDown";
            this.btnPageDown.Size = new System.Drawing.Size(21, 23);
            this.btnPageDown.TabIndex = 47;
            this.btnPageDown.Text = ">";
            this.btnPageDown.UseVisualStyleBackColor = true;
            // 
            // btnPageUp
            // 
            this.btnPageUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPageUp.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPageUp.FlatAppearance.BorderSize = 0;
            this.btnPageUp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnPageUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPageUp.Location = new System.Drawing.Point(594, 4);
            this.btnPageUp.Name = "btnPageUp";
            this.btnPageUp.Size = new System.Drawing.Size(21, 23);
            this.btnPageUp.TabIndex = 46;
            this.btnPageUp.Text = "<";
            this.btnPageUp.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(650, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 12);
            this.label6.TabIndex = 49;
            this.label6.Text = "|";
            // 
            // lb_tim
            // 
            this.lb_tim.AutoSize = true;
            this.lb_tim.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_tim.Location = new System.Drawing.Point(48, 7);
            this.lb_tim.Name = "lb_tim";
            this.lb_tim.Size = new System.Drawing.Size(41, 17);
            this.lb_tim.TabIndex = 2;
            this.lb_tim.Text = "4月20";
            // 
            // btn_seltim
            // 
            this.btn_seltim.BackColor = System.Drawing.Color.Transparent;
            this.btn_seltim.BaseColor = System.Drawing.Color.Transparent;
            this.btn_seltim.BorderColor = System.Drawing.Color.Transparent;
            this.btn_seltim.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_seltim.DownBack = null;
            this.btn_seltim.DownBaseColor = System.Drawing.Color.Lime;
            this.btn_seltim.Image = ((System.Drawing.Image)(resources.GetObject("btn_seltim.Image")));
            this.btn_seltim.ImageSize = new System.Drawing.Size(24, 20);
            this.btn_seltim.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_seltim.IsDrawGlass = false;
            this.btn_seltim.Location = new System.Drawing.Point(9, 4);
            this.btn_seltim.Margin = new System.Windows.Forms.Padding(0);
            this.btn_seltim.MouseBack = null;
            this.btn_seltim.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_seltim.Name = "btn_seltim";
            this.btn_seltim.NormlBack = null;
            this.btn_seltim.Size = new System.Drawing.Size(29, 23);
            this.btn_seltim.TabIndex = 1;
            this.btn_seltim.UseVisualStyleBackColor = false;
            this.btn_seltim.Click += new System.EventHandler(this.btn_seltim_Click_1);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.monthCalendar1.ForeColor = System.Drawing.Color.Lime;
            this.monthCalendar1.Location = new System.Drawing.Point(269, 302);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 41;
            this.monthCalendar1.Visible = false;
            this.monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected_1);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(962, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 23);
            this.button1.TabIndex = 42;
            this.button1.Text = "V";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(694, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(292, 85);
            this.panel1.TabIndex = 43;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(227, 45);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(51, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "取消";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(160, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(51, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "确定";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(182, 10);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(96, 20);
            this.comboBox2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "时间";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(49, 10);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(92, 20);
            this.comboBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "类型";
            // 
            // chatLogBox1
            // 
            this.chatLogBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chatLogBox1.BackColor = System.Drawing.Color.DimGray;
            this.chatLogBox1.Location = new System.Drawing.Point(266, 75);
            this.chatLogBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.chatLogBox1.Name = "chatLogBox1";
            this.chatLogBox1.Size = new System.Drawing.Size(721, 411);
            this.chatLogBox1.TabIndex = 21;
            // 
            // cms_selfri
            // 
            this.cms_selfri.Arrow = System.Drawing.Color.Black;
            this.cms_selfri.Back = System.Drawing.Color.White;
            this.cms_selfri.BackRadius = 4;
            this.cms_selfri.Base = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(200)))), ((int)(((byte)(254)))));
            this.cms_selfri.DropDownImageSeparator = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(197)))), ((int)(((byte)(197)))));
            this.cms_selfri.Fore = System.Drawing.Color.Black;
            this.cms_selfri.HoverFore = System.Drawing.Color.White;
            this.cms_selfri.ItemAnamorphosis = true;
            this.cms_selfri.ItemBorder = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.cms_selfri.ItemBorderShow = true;
            this.cms_selfri.ItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.cms_selfri.ItemPressed = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.cms_selfri.ItemRadius = 4;
            this.cms_selfri.ItemRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.cms_selfri.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.导出消息记录ToolStripMenuItem,
            this.删除消息记录ToolStripMenuItem});
            this.cms_selfri.ItemSplitter = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(148)))), ((int)(((byte)(212)))));
            this.cms_selfri.Name = "cms_selfri";
            this.cms_selfri.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.cms_selfri.Size = new System.Drawing.Size(153, 70);
            this.cms_selfri.SkinAllColor = true;
            this.cms_selfri.TitleAnamorphosis = true;
            this.cms_selfri.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(228)))), ((int)(((byte)(236)))));
            this.cms_selfri.TitleRadius = 4;
            this.cms_selfri.TitleRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            // 
            // 导出消息记录ToolStripMenuItem
            // 
            this.导出消息记录ToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.导出消息记录ToolStripMenuItem.Name = "导出消息记录ToolStripMenuItem";
            this.导出消息记录ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.导出消息记录ToolStripMenuItem.Text = "导出消息记录";
            this.导出消息记录ToolStripMenuItem.Click += new System.EventHandler(this.导出消息记录ToolStripMenuItem_Click);
            // 
            // 删除消息记录ToolStripMenuItem
            // 
            this.删除消息记录ToolStripMenuItem.Name = "删除消息记录ToolStripMenuItem";
            this.删除消息记录ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.删除消息记录ToolStripMenuItem.Text = "删除消息记录";
            // 
            // Mess_Manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(989, 518);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.tbx_search);
            this.Controls.Add(this.lb_ms_myfri);
            this.Controls.Add(this.lb_ms_chiwx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chatLogBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "Mess_Manage";
            this.Shadow = true;
            this.ShadowWidth = 5;
            this.ShowBorder = false;
            this.Text = "消息管理";
            this.TitleColor = System.Drawing.Color.Black;
            this.Load += new System.EventHandler(this.Mess_Manage_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.cms_selfri.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private WChat.ChatLogBox chatLogBox1;
        private System.Windows.Forms.ListBox lb_ms_chiwx;
        private System.Windows.Forms.ListBox lb_ms_myfri;
        private System.Windows.Forms.TextBox tbx_search;
        private CCWin.SkinControl.SkinButton btn_search;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lb_tim;
        private CCWin.SkinControl.SkinButton btn_seltim;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPageCount;
        private System.Windows.Forms.TextBox txtPage;
        private System.Windows.Forms.Button btnPageDown;
        private System.Windows.Forms.Button btnPageUp;
        private System.Windows.Forms.Label label6;
        private CCWin.SkinControl.SkinContextMenuStrip cms_selfri;
        private System.Windows.Forms.ToolStripMenuItem 导出消息记录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除消息记录ToolStripMenuItem;
    }
}