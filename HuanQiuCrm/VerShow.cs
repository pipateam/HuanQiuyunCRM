﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
namespace HuanQiuCrm
{
    public partial class VerShow : Form
    {
        public VerShow()
        {
            InitializeComponent();
        }

        public VersionTable vt = new VersionTable();
        int i = 0;
        int height = 40;


        private void VerShow_Load(object sender, EventArgs e)
        {
            addcontrols("环球云" + vt.Version + "版");
            cut(vt.VersionDescription);
            addcontrols("发布时间：" + vt.Time.ToShortDateString());
            this.Height = (i + 1) * height;
            this.panel1.Height = (i + 1) * height;
        }


        private void addcontrols(string s)
        {
            i++;
            Label l = new Label();
            l.Text = s;
            l.Font = new Font("微软雅黑", 12F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(134)));
            l.BackColor = Color.White;
            l.ForeColor = Color.Black;
            l.Width = this.Width - 4;
            l.Height = height;
            l.AutoSize = false;
            l.FlatStyle = FlatStyle.Flat;
            l.BorderStyle = BorderStyle.None;
            l.Location = new Point(0, i * l.Height);
            l.TextAlign = ContentAlignment.MiddleLeft;
            this.panel1.Controls.Add(l);
        }


        private void cut(string s)
        {
            int i = -1;
            string temp = null;
            while (true)
            {
                if (temp == null)
                {
                    i = s.IndexOf("\\");
                    if (i > -1)
                    {
                        temp = s.Substring(i + 1);
                        addcontrols(s.Substring(0, i));
                    }
                    else
                    {
                        addcontrols(s);
                        break;
                    }
                }
                else
                {
                    i = temp.IndexOf("\\");
                    if (i > -1)
                    {
                        string b = temp;
                        temp = b.Substring(i + 1);
                        addcontrols(b.Substring(0, i));
                    }
                    else
                    {
                        addcontrols(temp);
                        break;
                    }
                }
            }
        }



    }
}
