﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace HuanQiuCrm
{
    public class VersionServices
    {
        private string connStr = "server=bds249611391.my3w.com;database=bds249611391_db;uid=bds249611391;pwd=hq312453";

        public List<VersionTable> searchVersion()
        {
            try
            {
                List<VersionTable> l = new List<VersionTable>();
                string sql = "select Id,Version,VersionDescription,Time from VersionTable order by Time desc";
                SqlConnection conn = new SqlConnection(connStr);
                conn.Open();
                SqlCommand com = new SqlCommand(sql, conn);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                {
                    VersionTable vt = new VersionTable();
                    vt.Id = Convert.ToInt32(dr["Id"]);
                    vt.Version = dr["Version"].ToString();
                    vt.VersionDescription = dr["VersionDescription"].ToString();
                    vt.Time = Convert.ToDateTime(dr["Time"]);
                    l.Add(vt);
                }
                dr.Close();
                conn.Close();
                return l;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
