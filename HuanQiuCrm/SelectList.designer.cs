﻿namespace HuanQiuCrm
{
    partial class SelectList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectList));
            this.label2 = new System.Windows.Forms.Label();
            this.skinProgressBar1 = new CCWin.SkinControl.SkinProgressBar();
            this.btn_updata = new CCWin.SkinControl.SkinButton();
            this.btn_conel = new CCWin.SkinControl.SkinButton();
            this.lb_update_desc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(502, 243);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "0%";
            // 
            // skinProgressBar1
            // 
            this.skinProgressBar1.Back = null;
            this.skinProgressBar1.BackColor = System.Drawing.Color.Transparent;
            this.skinProgressBar1.BarBack = null;
            this.skinProgressBar1.BarRadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinProgressBar1.Border = System.Drawing.Color.Transparent;
            this.skinProgressBar1.ForeColor = System.Drawing.Color.Lime;
            this.skinProgressBar1.InnerBorder = System.Drawing.Color.Transparent;
            this.skinProgressBar1.Location = new System.Drawing.Point(25, 237);
            this.skinProgressBar1.Name = "skinProgressBar1";
            this.skinProgressBar1.RadiusStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinProgressBar1.Size = new System.Drawing.Size(467, 23);
            this.skinProgressBar1.TabIndex = 12;
            this.skinProgressBar1.TrackBack = System.Drawing.Color.White;
            // 
            // btn_updata
            // 
            this.btn_updata.BackColor = System.Drawing.Color.Transparent;
            this.btn_updata.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_updata.BorderColor = System.Drawing.Color.Transparent;
            this.btn_updata.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_updata.DownBack = null;
            this.btn_updata.Location = new System.Drawing.Point(48, 288);
            this.btn_updata.MouseBack = null;
            this.btn_updata.Name = "btn_updata";
            this.btn_updata.NormlBack = null;
            this.btn_updata.Size = new System.Drawing.Size(193, 32);
            this.btn_updata.TabIndex = 13;
            this.btn_updata.Text = "更新";
            this.btn_updata.UseVisualStyleBackColor = false;
            this.btn_updata.Click += new System.EventHandler(this.btn_updata_Click);
            // 
            // btn_conel
            // 
            this.btn_conel.BackColor = System.Drawing.Color.Transparent;
            this.btn_conel.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_conel.BorderColor = System.Drawing.Color.Transparent;
            this.btn_conel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_conel.DownBack = null;
            this.btn_conel.DownBaseColor = System.Drawing.Color.Lime;
            this.btn_conel.Location = new System.Drawing.Point(293, 288);
            this.btn_conel.MouseBack = null;
            this.btn_conel.MouseBaseColor = System.Drawing.Color.Lime;
            this.btn_conel.Name = "btn_conel";
            this.btn_conel.NormlBack = null;
            this.btn_conel.Size = new System.Drawing.Size(193, 32);
            this.btn_conel.TabIndex = 14;
            this.btn_conel.Text = "取消更新";
            this.btn_conel.UseVisualStyleBackColor = false;
            this.btn_conel.Click += new System.EventHandler(this.btn_conel_Click);
            // 
            // lb_update_desc
            // 
            this.lb_update_desc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb_update_desc.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_update_desc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.lb_update_desc.Location = new System.Drawing.Point(25, 66);
            this.lb_update_desc.Multiline = true;
            this.lb_update_desc.Name = "lb_update_desc";
            this.lb_update_desc.Size = new System.Drawing.Size(494, 143);
            this.lb_update_desc.TabIndex = 15;
            // 
            // SelectList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.Transparent;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(547, 368);
            this.Controls.Add(this.lb_update_desc);
            this.Controls.Add(this.btn_conel);
            this.Controls.Add(this.btn_updata);
            this.Controls.Add(this.skinProgressBar1);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.MaximizeBox = false;
            this.Name = "SelectList";
            this.Shadow = true;
            this.ShowIcon = false;
            this.Text = "环球云升级";
            this.Load += new System.EventHandler(this.SelectList_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private CCWin.SkinControl.SkinProgressBar skinProgressBar1;
        private CCWin.SkinControl.SkinButton btn_updata;
        private CCWin.SkinControl.SkinButton btn_conel;
        private System.Windows.Forms.TextBox lb_update_desc;
    }
}