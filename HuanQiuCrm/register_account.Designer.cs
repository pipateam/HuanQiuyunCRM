﻿namespace HuanQiuCrm
{
    partial class register_account
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(register_account));
            this.btn_register = new CCWin.SkinControl.SkinButton();
            this.tbx_contact = new CCWin.SkinControl.SkinTextBox();
            this.tbx_userName = new CCWin.SkinControl.SkinTextBox();
            this.tbx_depart = new CCWin.SkinControl.SkinTextBox();
            this.tbx_pwd = new CCWin.SkinControl.SkinTextBox();
            this.tbx_name = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.SuspendLayout();
            // 
            // btn_register
            // 
            this.btn_register.BackColor = System.Drawing.Color.Transparent;
            this.btn_register.BaseColor = System.Drawing.Color.White;
            this.btn_register.BorderColor = System.Drawing.Color.White;
            this.btn_register.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_register.DownBack = null;
            this.btn_register.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btn_register.Location = new System.Drawing.Point(114, 362);
            this.btn_register.MouseBack = null;
            this.btn_register.Name = "btn_register";
            this.btn_register.NormlBack = null;
            this.btn_register.Size = new System.Drawing.Size(148, 42);
            this.btn_register.TabIndex = 18;
            this.btn_register.Text = "立即注册";
            this.btn_register.UseVisualStyleBackColor = false;
            this.btn_register.Click += new System.EventHandler(this.btn_register_Click);
            // 
            // tbx_contact
            // 
            this.tbx_contact.BackColor = System.Drawing.Color.Transparent;
            this.tbx_contact.DownBack = null;
            this.tbx_contact.Icon = null;
            this.tbx_contact.IconIsButton = false;
            this.tbx_contact.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_contact.IsPasswordChat = '\0';
            this.tbx_contact.IsSystemPasswordChar = false;
            this.tbx_contact.Lines = new string[0];
            this.tbx_contact.Location = new System.Drawing.Point(41, 306);
            this.tbx_contact.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_contact.MaxLength = 32767;
            this.tbx_contact.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_contact.MouseBack = null;
            this.tbx_contact.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_contact.Multiline = false;
            this.tbx_contact.Name = "tbx_contact";
            this.tbx_contact.NormlBack = null;
            this.tbx_contact.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_contact.ReadOnly = false;
            this.tbx_contact.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_contact.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_contact.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_contact.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_contact.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_contact.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_contact.SkinTxt.Name = "BaseText";
            this.tbx_contact.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_contact.SkinTxt.TabIndex = 0;
            this.tbx_contact.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_contact.SkinTxt.WaterText = "";
            this.tbx_contact.TabIndex = 17;
            this.tbx_contact.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_contact.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_contact.WaterText = "";
            this.tbx_contact.WordWrap = true;
            // 
            // tbx_userName
            // 
            this.tbx_userName.BackColor = System.Drawing.Color.Transparent;
            this.tbx_userName.DownBack = null;
            this.tbx_userName.Icon = null;
            this.tbx_userName.IconIsButton = false;
            this.tbx_userName.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_userName.IsPasswordChat = '\0';
            this.tbx_userName.IsSystemPasswordChar = false;
            this.tbx_userName.Lines = new string[0];
            this.tbx_userName.Location = new System.Drawing.Point(41, 253);
            this.tbx_userName.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_userName.MaxLength = 32767;
            this.tbx_userName.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_userName.MouseBack = null;
            this.tbx_userName.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_userName.Multiline = false;
            this.tbx_userName.Name = "tbx_userName";
            this.tbx_userName.NormlBack = null;
            this.tbx_userName.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_userName.ReadOnly = false;
            this.tbx_userName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_userName.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_userName.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_userName.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_userName.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_userName.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_userName.SkinTxt.Name = "BaseText";
            this.tbx_userName.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_userName.SkinTxt.TabIndex = 0;
            this.tbx_userName.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_userName.SkinTxt.WaterText = "";
            this.tbx_userName.TabIndex = 16;
            this.tbx_userName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_userName.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_userName.WaterText = "";
            this.tbx_userName.WordWrap = true;
            // 
            // tbx_depart
            // 
            this.tbx_depart.BackColor = System.Drawing.Color.Transparent;
            this.tbx_depart.DownBack = null;
            this.tbx_depart.Icon = null;
            this.tbx_depart.IconIsButton = false;
            this.tbx_depart.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_depart.IsPasswordChat = '\0';
            this.tbx_depart.IsSystemPasswordChar = false;
            this.tbx_depart.Lines = new string[0];
            this.tbx_depart.Location = new System.Drawing.Point(41, 199);
            this.tbx_depart.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_depart.MaxLength = 32767;
            this.tbx_depart.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_depart.MouseBack = null;
            this.tbx_depart.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_depart.Multiline = false;
            this.tbx_depart.Name = "tbx_depart";
            this.tbx_depart.NormlBack = null;
            this.tbx_depart.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_depart.ReadOnly = false;
            this.tbx_depart.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_depart.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_depart.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_depart.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_depart.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_depart.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_depart.SkinTxt.Name = "BaseText";
            this.tbx_depart.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_depart.SkinTxt.TabIndex = 0;
            this.tbx_depart.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_depart.SkinTxt.WaterText = "";
            this.tbx_depart.TabIndex = 14;
            this.tbx_depart.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_depart.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_depart.WaterText = "";
            this.tbx_depart.WordWrap = true;
            // 
            // tbx_pwd
            // 
            this.tbx_pwd.BackColor = System.Drawing.Color.Transparent;
            this.tbx_pwd.DownBack = null;
            this.tbx_pwd.Icon = null;
            this.tbx_pwd.IconIsButton = false;
            this.tbx_pwd.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_pwd.IsPasswordChat = '\0';
            this.tbx_pwd.IsSystemPasswordChar = false;
            this.tbx_pwd.Lines = new string[0];
            this.tbx_pwd.Location = new System.Drawing.Point(41, 150);
            this.tbx_pwd.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_pwd.MaxLength = 32767;
            this.tbx_pwd.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_pwd.MouseBack = null;
            this.tbx_pwd.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_pwd.Multiline = false;
            this.tbx_pwd.Name = "tbx_pwd";
            this.tbx_pwd.NormlBack = null;
            this.tbx_pwd.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_pwd.ReadOnly = false;
            this.tbx_pwd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_pwd.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_pwd.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_pwd.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_pwd.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_pwd.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_pwd.SkinTxt.Name = "BaseText";
            this.tbx_pwd.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_pwd.SkinTxt.TabIndex = 0;
            this.tbx_pwd.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_pwd.SkinTxt.WaterText = "";
            this.tbx_pwd.TabIndex = 15;
            this.tbx_pwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_pwd.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_pwd.WaterText = "";
            this.tbx_pwd.WordWrap = true;
            this.tbx_pwd.MouseEnter += new System.EventHandler(this.tbx_pwd_MouseEnter);
            // 
            // tbx_name
            // 
            this.tbx_name.BackColor = System.Drawing.Color.Transparent;
            this.tbx_name.DownBack = null;
            this.tbx_name.Icon = null;
            this.tbx_name.IconIsButton = false;
            this.tbx_name.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_name.IsPasswordChat = '\0';
            this.tbx_name.IsSystemPasswordChar = false;
            this.tbx_name.Lines = new string[0];
            this.tbx_name.Location = new System.Drawing.Point(41, 95);
            this.tbx_name.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_name.MaxLength = 32767;
            this.tbx_name.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_name.MouseBack = null;
            this.tbx_name.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_name.Multiline = false;
            this.tbx_name.Name = "tbx_name";
            this.tbx_name.NormlBack = null;
            this.tbx_name.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_name.ReadOnly = false;
            this.tbx_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_name.Size = new System.Drawing.Size(185, 28);
            // 
            // 
            // 
            this.tbx_name.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_name.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_name.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_name.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_name.SkinTxt.Name = "BaseText";
            this.tbx_name.SkinTxt.Size = new System.Drawing.Size(175, 18);
            this.tbx_name.SkinTxt.TabIndex = 0;
            this.tbx_name.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_name.SkinTxt.WaterText = "";
            this.tbx_name.TabIndex = 13;
            this.tbx_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_name.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_name.WaterText = "";
            this.tbx_name.WordWrap = true;
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel5.ForeColor = System.Drawing.Color.White;
            this.skinLabel5.Location = new System.Drawing.Point(264, 309);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(74, 22);
            this.skinLabel5.TabIndex = 8;
            this.skinLabel5.Text = "联系方式";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel4.ForeColor = System.Drawing.Color.White;
            this.skinLabel4.Location = new System.Drawing.Point(264, 258);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(74, 22);
            this.skinLabel4.TabIndex = 9;
            this.skinLabel4.Text = "用户姓名";
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel6.ForeColor = System.Drawing.Color.White;
            this.skinLabel6.Location = new System.Drawing.Point(264, 204);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(74, 22);
            this.skinLabel6.TabIndex = 10;
            this.skinLabel6.Text = "所属部门";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel3.ForeColor = System.Drawing.Color.White;
            this.skinLabel3.Location = new System.Drawing.Point(264, 154);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(74, 22);
            this.skinLabel3.TabIndex = 11;
            this.skinLabel3.Text = "输入密码";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel2.ForeColor = System.Drawing.Color.White;
            this.skinLabel2.Location = new System.Drawing.Point(264, 99);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(74, 22);
            this.skinLabel2.TabIndex = 12;
            this.skinLabel2.Text = "输入账户";
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.Transparent;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold);
            this.skinLabel1.ForeColor = System.Drawing.Color.White;
            this.skinLabel1.Location = new System.Drawing.Point(98, 43);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(186, 22);
            this.skinLabel1.TabIndex = 7;
            this.skinLabel1.Text = "欢迎使用环球云客服系统";
            // 
            // register_account
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.CaptionFont = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ClientSize = new System.Drawing.Size(375, 434);
            this.Controls.Add(this.btn_register);
            this.Controls.Add(this.tbx_contact);
            this.Controls.Add(this.tbx_userName);
            this.Controls.Add(this.tbx_depart);
            this.Controls.Add(this.tbx_pwd);
            this.Controls.Add(this.tbx_name);
            this.Controls.Add(this.skinLabel5);
            this.Controls.Add(this.skinLabel4);
            this.Controls.Add(this.skinLabel6);
            this.Controls.Add(this.skinLabel3);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "register_account";
            this.Shadow = true;
            this.ShowDrawIcon = false;
            this.Text = "注 册";
            this.TitleCenter = false;
            this.TitleColor = System.Drawing.Color.White;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinButton btn_register;
        private CCWin.SkinControl.SkinTextBox tbx_contact;
        private CCWin.SkinControl.SkinTextBox tbx_userName;
        private CCWin.SkinControl.SkinTextBox tbx_depart;
        private CCWin.SkinControl.SkinTextBox tbx_pwd;
        private CCWin.SkinControl.SkinTextBox tbx_name;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinLabel skinLabel1;
    }
}