﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HuanQiuCrm {
    static class Program {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (new UpdateXML().IsUpdate())
            {
                if (new UpdateXML().SearchYseOrNo().Equals("0"))
                {
                    DialogResult dr = MessageBox.Show("有新版本，是否更新", "系统提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                        Application.Run(new SelectList());
                    else
                    {
                        new UpdateXML().SetYesOrNo("1");
                        Application.Run(new welcom());
                    }
                }
                else
                    Application.Run(new welcom());
            }
            else
                Application.Run(new welcom());

        }
    }
}
