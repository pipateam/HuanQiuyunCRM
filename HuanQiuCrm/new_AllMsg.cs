﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using WChat;
using System.Threading;
using HuanQiuCrm.Properties;
using DAL;

namespace HuanQiuCrm
{
    public partial class new_AllMsg : Skin_Color
    {
        SqlHandle sql = new SqlHandle();
        List<string> arr = new List<string>() { };
        Settings st = new Settings();
        private List<string> MyFriList = new List<string>();  //将数据库查出的数据暂时存起来，以用于查询
        private List<string> TempUserList = new List<string>();
        private List<string> SeledList = new List<string>();
        LabSer ls = new LabSer();
        private int friend_num = 0;
        private int Seled_friend_Num = 0;
        public List<WXUser> sure_me;
        public WXUser sure_user;

        private int type = 1;
        string wx_name;
        int num;  //获取索引值
        public new_AllMsg()
        {
            InitializeComponent();
            arr = sql.Select_send_devices(st.UseAcct);
            string[] Arr = arr.Distinct().ToArray();
            for (int i=0;i< Arr.Length;i++)
            {
                if (Arr[i] != "")
                {
                    cm_wx_acoount.Items.Add(Arr[i].ToString().Trim());  //添加当前登录的账户
                }               
            }
            //标签下拉列表选择更改方法
            skinCBOLabel.SelectedIndexChanged += SkinCBOLabel_SelectedIndexChanged;
        }
        /// <summary>
        /// 标签下拉列表选择更改方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SkinCBOLabel_SelectedIndexChanged( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            List<string> L = ls.SearchFriendNick(wx_name, skinCBOLabel.SelectedItem.ToString());
            foreach (string item in L) {
                listBox1.Items.Add(item);
            }
        }

        string[] phoPath = { };
        string PhoName;
        string photopath;
        /// <summary>
        /// 标签选择（下拉列表）
        /// </summary>
        /// <param name="str"></param>
        private void BindCbo( string str ) {
            if (str != "") {
                skinCBOLabel.Items.Clear();
                List<string> L = ls.SearchFriend(str);
                foreach (string item in L) {
                    skinCBOLabel.Items.Add(item);
                }
            }
        }
        private void send_img_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "选择要发送的图片";
            dlg.Filter = "Image files (*.Jpg;*.Jpeg;*.Bmp;*.Gif;*.Png)|*.jpg*.jpeg;*.gif;*.bmp|AllFiles (*.*)|*.*";
            
            if (dlg.ShowDialog() == DialogResult.OK)
            {                
                //获取图片名称
                photopath = dlg.FileName.ToString();
                phoPath = photopath.Split('\\');
                PhoName = phoPath[phoPath.Length - 1];
                tbx_text.Text = PhoName;
                tbx_text.Enabled = false;

                //消息类型判断
                if (send_img.Checked) {
                    type = 3;
                }
            } else {
                send_img.Checked = false;
                send_text.Checked = true;
            }
        }
        private void new_AllMsg_Load(object sender, EventArgs e)
        {
            //发送消息类型默认为文本类型
            send_text.Checked = true;
        }
        public void Pro_User(List<WXUser> _me,WXUser to)
        {
            sure_me = _me;
            sure_user = to;
        }
        private void drop_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            wx_name = cm_wx_acoount.SelectedItem.ToString();
            if(ls.SearchTable(wx_name) > 0)
                BindCbo(wx_name);          //标签
            num = cm_wx_acoount.SelectedIndex;
            List<string> arr = new List<string> { };
            arr = sql.Select_send_man(wx_name,st.UseAcct);   //获取群发好友
            string[] Wx_Name = arr.Distinct().ToArray();
            for (int i=0;i<Wx_Name.Length;i++)
            {
                listBox1.Items.Add(Wx_Name[i].Trim());   //列表添加群发好友
                MyFriList.Add(Wx_Name[i].Trim());
            }
            friend_num = Wx_Name.Count();
            lb_friend_num.Text = friend_num.ToString();
            friend_num = 0;   // 复位
        }
        /// <summary>
        /// 发送按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void skinButton1_Click(object sender, EventArgs e)
        {
            StringBuilder recipient = new StringBuilder();
            if (TempUserList.Count!=0) {
                string content = tbx_text.Text;
                string time = Time.Value.ToString();
                //发送完成 关闭
                All_Send s = new All_Send(photopath, type, num, TempUserList, sure_me, wx_name, recipient, content, time);
                new Thread(new ThreadStart(s.Send)).Start();
                this.Close();
            } else {
                MessageBox.Show("请选择要发送的联系人");
            }
        }
        /// <summary>
        /// 群发好友全选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_All_Sel_Click(object sender, EventArgs e)
        {
            for (int i=0; i<listBox1.Items.Count;i++)
            {
                listBox1.SelectedIndex = i;
            }
        }
        /// <summary>
        /// 群发好友反选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Anti_Sel_Click(object sender, EventArgs e)
        {
            if (TempUserList !=null) {
                foreach (string item in TempUserList) {
                    SeledList.Add(item);    //把之前选择的保存起来
                    lbx_selected.Items.Remove(item);
                }
                foreach (string item in listBox1.Items) {
                    if (!TempUserList.Contains(item)) {
                        TempUserList.Add(item);
                        lbx_selected.Items.Add(item);
                    }
                }
                foreach (string user in SeledList) {
                    TempUserList.Remove(user);
                }

                Seled_friend_Num = TempUserList.Count();
                lb_fri_num.Text = Seled_friend_Num.ToString();
                SeledList.Clear();
            }else {
                btn_All_Sel_Click(null, null);
            }
        }
        /// <summary>
        /// 群发好友全不选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_No_Sel_Click(object sender, EventArgs e)
        {
            TotallyOptional();
        }

        /// <summary>
        /// 全不选
        /// </summary>
        /// <param name="ListBox"></param>
        /// <param name="b"></param>
        private void TotallyOptional() 
        {
            TempUserList.Clear();
            lbx_selected.Items.Clear();
         
            Seled_friend_Num = TempUserList.Count();
            lb_fri_num.Text = Seled_friend_Num.ToString();
        }

        #region 搜索
        private void btn_search_Click( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            foreach (var item in MyFriList) {
                if (item.Contains(tbx_searchs.Text)) {
                    listBox1.Items.Add(item);
                }
            }
        }

        private void tbx_searchs_TextChanged( object sender, EventArgs e ) {
            listBox1.Items.Clear();
            foreach (var item in MyFriList) {
                if (item.Contains(tbx_searchs.Text)) {
                    listBox1.Items.Add(item);
                }
            }
        }
        #endregion

        /// <summary>
        /// 单击移除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbx_selected_SelectedIndexChanged( object sender, EventArgs e ) {
            if (lbx_selected.SelectedItem != null) {
                string fri = lbx_selected.SelectedItem.ToString();
                lbx_selected.Items.Remove(fri);
                TempUserList.Remove(fri);
                Seled_friend_Num = 0;
                Seled_friend_Num = TempUserList.Count();
                lb_fri_num.Text = Seled_friend_Num.ToString();
            }
        }
        /// <summary>
        /// 好友列表点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged( object sender, EventArgs e ) {
            lbx_selected.Items.Clear();
            if (listBox1.SelectedItem != null) {
                string seledfris = listBox1.SelectedItem.ToString();
                if (!TempUserList.Contains(seledfris)) {  //没有了在进行添加，防止重复
                    TempUserList.Add(seledfris);
                }
            }
            foreach (var item in TempUserList) {
                lbx_selected.Items.Add(item);
            }
            Seled_friend_Num = 0;
            Seled_friend_Num = TempUserList.Count();
            lb_fri_num.Text = Seled_friend_Num.ToString();
            tbx_searchs.Text = "";
        }
    }
}
