﻿namespace HuanQiuCrm
{
    partial class new_AllMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(new_AllMsg));
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.cm_wx_acoount = new CCWin.SkinControl.SkinComboBox();
            this.send_text = new CCWin.SkinControl.SkinRadioButton();
            this.send_img = new CCWin.SkinControl.SkinRadioButton();
            this.tbx_text = new CCWin.SkinControl.SkinTextBox();
            this.skinButton1 = new CCWin.SkinControl.SkinButton();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Time = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.群发测试BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btn_All_Sel = new CCWin.SkinControl.SkinButton();
            this.btn_Anti_Sel = new CCWin.SkinControl.SkinButton();
            this.btn_No_Sel = new CCWin.SkinControl.SkinButton();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_friend_num = new System.Windows.Forms.Label();
            this.skinCBOLabel = new CCWin.SkinControl.SkinComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbx_selected = new System.Windows.Forms.ListBox();
            this.lb_fri_num = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbx_searchs = new System.Windows.Forms.TextBox();
            this.btn_search = new CCWin.SkinControl.SkinButton();
            ((System.ComponentModel.ISupportInitialize)(this.Time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.群发测试BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(20, 50);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(96, 20);
            this.skinLabel1.TabIndex = 0;
            this.skinLabel1.Text = "请选择微信号:";
            // 
            // cm_wx_acoount
            // 
            this.cm_wx_acoount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cm_wx_acoount.FormattingEnabled = true;
            this.cm_wx_acoount.Location = new System.Drawing.Point(129, 49);
            this.cm_wx_acoount.Name = "cm_wx_acoount";
            this.cm_wx_acoount.Size = new System.Drawing.Size(159, 22);
            this.cm_wx_acoount.TabIndex = 1;
            this.cm_wx_acoount.WaterText = "";
            this.cm_wx_acoount.SelectedIndexChanged += new System.EventHandler(this.drop_list_SelectedIndexChanged);
            // 
            // send_text
            // 
            this.send_text.AutoSize = true;
            this.send_text.BackColor = System.Drawing.Color.Transparent;
            this.send_text.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.send_text.DownBack = null;
            this.send_text.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.send_text.Location = new System.Drawing.Point(130, 509);
            this.send_text.MouseBack = null;
            this.send_text.Name = "send_text";
            this.send_text.NormlBack = null;
            this.send_text.SelectedDownBack = null;
            this.send_text.SelectedMouseBack = null;
            this.send_text.SelectedNormlBack = null;
            this.send_text.Size = new System.Drawing.Size(74, 21);
            this.send_text.TabIndex = 2;
            this.send_text.TabStop = true;
            this.send_text.Text = "文本消息";
            this.send_text.UseVisualStyleBackColor = false;
            // 
            // send_img
            // 
            this.send_img.AutoSize = true;
            this.send_img.BackColor = System.Drawing.Color.Transparent;
            this.send_img.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.send_img.DownBack = null;
            this.send_img.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.send_img.Location = new System.Drawing.Point(215, 508);
            this.send_img.MouseBack = null;
            this.send_img.Name = "send_img";
            this.send_img.NormlBack = null;
            this.send_img.SelectedDownBack = null;
            this.send_img.SelectedMouseBack = null;
            this.send_img.SelectedNormlBack = null;
            this.send_img.Size = new System.Drawing.Size(74, 21);
            this.send_img.TabIndex = 3;
            this.send_img.TabStop = true;
            this.send_img.Text = "发送图片";
            this.send_img.UseVisualStyleBackColor = false;
            this.send_img.Click += new System.EventHandler(this.send_img_Click);
            // 
            // tbx_text
            // 
            this.tbx_text.BackColor = System.Drawing.Color.Transparent;
            this.tbx_text.DownBack = null;
            this.tbx_text.Icon = null;
            this.tbx_text.IconIsButton = false;
            this.tbx_text.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_text.IsPasswordChat = '\0';
            this.tbx_text.IsSystemPasswordChar = false;
            this.tbx_text.Lines = new string[0];
            this.tbx_text.Location = new System.Drawing.Point(21, 544);
            this.tbx_text.Margin = new System.Windows.Forms.Padding(0);
            this.tbx_text.MaxLength = 32767;
            this.tbx_text.MinimumSize = new System.Drawing.Size(28, 28);
            this.tbx_text.MouseBack = null;
            this.tbx_text.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.tbx_text.Multiline = true;
            this.tbx_text.Name = "tbx_text";
            this.tbx_text.NormlBack = null;
            this.tbx_text.Padding = new System.Windows.Forms.Padding(5);
            this.tbx_text.ReadOnly = false;
            this.tbx_text.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbx_text.Size = new System.Drawing.Size(480, 75);
            // 
            // 
            // 
            this.tbx_text.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_text.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbx_text.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.tbx_text.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.tbx_text.SkinTxt.Multiline = true;
            this.tbx_text.SkinTxt.Name = "BaseText";
            this.tbx_text.SkinTxt.Size = new System.Drawing.Size(470, 65);
            this.tbx_text.SkinTxt.TabIndex = 0;
            this.tbx_text.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_text.SkinTxt.WaterText = "";
            this.tbx_text.TabIndex = 4;
            this.tbx_text.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbx_text.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.tbx_text.WaterText = "";
            this.tbx_text.WordWrap = true;
            // 
            // skinButton1
            // 
            this.skinButton1.BackColor = System.Drawing.Color.Transparent;
            this.skinButton1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinButton1.DownBack = null;
            this.skinButton1.InnerBorderColor = System.Drawing.Color.Transparent;
            this.skinButton1.Location = new System.Drawing.Point(425, 631);
            this.skinButton1.MouseBack = null;
            this.skinButton1.Name = "skinButton1";
            this.skinButton1.NormlBack = null;
            this.skinButton1.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.skinButton1.Size = new System.Drawing.Size(75, 23);
            this.skinButton1.TabIndex = 5;
            this.skinButton1.Text = "发送消息";
            this.skinButton1.UseVisualStyleBackColor = false;
            this.skinButton1.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(24, 511);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(92, 17);
            this.skinLabel2.TabIndex = 8;
            this.skinLabel2.Text = "请选择接收人：";
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.listBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.listBox1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listBox1.IntegralHeight = false;
            this.listBox1.ItemHeight = 21;
            this.listBox1.Location = new System.Drawing.Point(21, 113);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(230, 340);
            this.listBox1.Sorted = true;
            this.listBox1.TabIndex = 9;
            this.listBox1.UseTabStops = false;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Time
            // 
            this.Time.Location = new System.Drawing.Point(116, 633);
            this.Time.Name = "Time";
            this.Time.Size = new System.Drawing.Size(61, 21);
            this.Time.TabIndex = 10;
            this.Time.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(22, 634);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "消息发送间隔：";
            // 
            // 群发测试BindingSource
            // 
            this.群发测试BindingSource.DataMember = "群发测试";
            // 
            // btn_All_Sel
            // 
            this.btn_All_Sel.BackColor = System.Drawing.Color.Transparent;
            this.btn_All_Sel.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_All_Sel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_All_Sel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_All_Sel.DownBack = null;
            this.btn_All_Sel.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.btn_All_Sel.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_All_Sel.Location = new System.Drawing.Point(302, 466);
            this.btn_All_Sel.MouseBack = null;
            this.btn_All_Sel.Name = "btn_All_Sel";
            this.btn_All_Sel.NormlBack = null;
            this.btn_All_Sel.Radius = 12;
            this.btn_All_Sel.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.btn_All_Sel.Size = new System.Drawing.Size(60, 24);
            this.btn_All_Sel.TabIndex = 12;
            this.btn_All_Sel.Text = "全 选";
            this.btn_All_Sel.UseVisualStyleBackColor = false;
            this.btn_All_Sel.Click += new System.EventHandler(this.btn_All_Sel_Click);
            // 
            // btn_Anti_Sel
            // 
            this.btn_Anti_Sel.BackColor = System.Drawing.Color.Transparent;
            this.btn_Anti_Sel.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Anti_Sel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_Anti_Sel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_Anti_Sel.DownBack = null;
            this.btn_Anti_Sel.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.btn_Anti_Sel.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_Anti_Sel.Location = new System.Drawing.Point(368, 466);
            this.btn_Anti_Sel.MouseBack = null;
            this.btn_Anti_Sel.Name = "btn_Anti_Sel";
            this.btn_Anti_Sel.NormlBack = null;
            this.btn_Anti_Sel.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.btn_Anti_Sel.Size = new System.Drawing.Size(58, 24);
            this.btn_Anti_Sel.TabIndex = 12;
            this.btn_Anti_Sel.Text = "反 选";
            this.btn_Anti_Sel.UseVisualStyleBackColor = false;
            this.btn_Anti_Sel.Click += new System.EventHandler(this.btn_Anti_Sel_Click);
            // 
            // btn_No_Sel
            // 
            this.btn_No_Sel.BackColor = System.Drawing.Color.Transparent;
            this.btn_No_Sel.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_No_Sel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btn_No_Sel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_No_Sel.DownBack = null;
            this.btn_No_Sel.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.btn_No_Sel.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_No_Sel.Location = new System.Drawing.Point(432, 466);
            this.btn_No_Sel.MouseBack = null;
            this.btn_No_Sel.Name = "btn_No_Sel";
            this.btn_No_Sel.NormlBack = null;
            this.btn_No_Sel.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.btn_No_Sel.Size = new System.Drawing.Size(60, 24);
            this.btn_No_Sel.TabIndex = 12;
            this.btn_No_Sel.Text = "全不选";
            this.btn_No_Sel.UseVisualStyleBackColor = false;
            this.btn_No_Sel.Click += new System.EventHandler(this.btn_No_Sel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.label2.Location = new System.Drawing.Point(18, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 20);
            this.label2.TabIndex = 13;
            this.label2.Text = "共有好友数量：";
            // 
            // lb_friend_num
            // 
            this.lb_friend_num.AutoSize = true;
            this.lb_friend_num.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_friend_num.Location = new System.Drawing.Point(127, 87);
            this.lb_friend_num.Name = "lb_friend_num";
            this.lb_friend_num.Size = new System.Drawing.Size(15, 17);
            this.lb_friend_num.TabIndex = 14;
            this.lb_friend_num.Text = "0";
            // 
            // skinCBOLabel
            // 
            this.skinCBOLabel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.skinCBOLabel.FormattingEnabled = true;
            this.skinCBOLabel.Location = new System.Drawing.Point(367, 49);
            this.skinCBOLabel.Name = "skinCBOLabel";
            this.skinCBOLabel.Size = new System.Drawing.Size(134, 22);
            this.skinCBOLabel.TabIndex = 18;
            this.skinCBOLabel.WaterText = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.label3.Location = new System.Drawing.Point(294, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "选择标签:";
            // 
            // lbx_selected
            // 
            this.lbx_selected.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lbx_selected.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.lbx_selected.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lbx_selected.FormattingEnabled = true;
            this.lbx_selected.ItemHeight = 21;
            this.lbx_selected.Location = new System.Drawing.Point(257, 113);
            this.lbx_selected.Name = "lbx_selected";
            this.lbx_selected.Size = new System.Drawing.Size(244, 340);
            this.lbx_selected.Sorted = true;
            this.lbx_selected.TabIndex = 19;
            this.lbx_selected.SelectedIndexChanged += new System.EventHandler(this.lbx_selected_SelectedIndexChanged);
            // 
            // lb_fri_num
            // 
            this.lb_fri_num.AutoSize = true;
            this.lb_fri_num.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lb_fri_num.ForeColor = System.Drawing.Color.Black;
            this.lb_fri_num.Location = new System.Drawing.Point(364, 87);
            this.lb_fri_num.Name = "lb_fri_num";
            this.lb_fri_num.Size = new System.Drawing.Size(15, 17);
            this.lb_fri_num.TabIndex = 21;
            this.lb_fri_num.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(249, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "已选择好友数量：";
            // 
            // tbx_searchs
            // 
            this.tbx_searchs.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tbx_searchs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbx_searchs.Font = new System.Drawing.Font("微软雅黑", 13.6F);
            this.tbx_searchs.Location = new System.Drawing.Point(21, 467);
            this.tbx_searchs.Name = "tbx_searchs";
            this.tbx_searchs.Size = new System.Drawing.Size(199, 24);
            this.tbx_searchs.TabIndex = 45;
            this.tbx_searchs.TextChanged += new System.EventHandler(this.tbx_searchs_TextChanged);
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.Transparent;
            this.btn_search.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.btn_search.DownBack = null;
            this.btn_search.DownBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.ForeColor = System.Drawing.Color.Black;
            this.btn_search.GlowColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btn_search.InnerBorderColor = System.Drawing.Color.Transparent;
            this.btn_search.Location = new System.Drawing.Point(239, 466);
            this.btn_search.MouseBack = null;
            this.btn_search.MouseBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.btn_search.Name = "btn_search";
            this.btn_search.NormlBack = null;
            this.btn_search.RoundStyle = CCWin.SkinClass.RoundStyle.All;
            this.btn_search.Size = new System.Drawing.Size(49, 24);
            this.btn_search.TabIndex = 44;
            this.btn_search.Text = "搜索";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // new_AllMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderColor = System.Drawing.Color.Transparent;
            this.CaptionBackColorBottom = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionBackColorTop = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(196)))), ((int)(((byte)(31)))));
            this.CaptionHeight = 30;
            this.ClientSize = new System.Drawing.Size(513, 662);
            this.Controls.Add(this.tbx_searchs);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.lb_fri_num);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbx_selected);
            this.Controls.Add(this.skinCBOLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lb_friend_num);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_No_Sel);
            this.Controls.Add(this.btn_Anti_Sel);
            this.Controls.Add(this.btn_All_Sel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Time);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinButton1);
            this.Controls.Add(this.tbx_text);
            this.Controls.Add(this.send_img);
            this.Controls.Add(this.send_text);
            this.Controls.Add(this.cm_wx_acoount);
            this.Controls.Add(this.skinLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.InnerBorderColor = System.Drawing.Color.Transparent;
            this.Name = "new_AllMsg";
            this.Shadow = true;
            this.Text = "新建群发";
            this.Load += new System.EventHandler(this.new_AllMsg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.群发测试BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinComboBox cm_wx_acoount;
        private CCWin.SkinControl.SkinRadioButton send_text;
        private CCWin.SkinControl.SkinRadioButton send_img;
        private CCWin.SkinControl.SkinTextBox tbx_text;
        private CCWin.SkinControl.SkinButton skinButton1;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.NumericUpDown Time;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource 群发测试BindingSource;
        private CCWin.SkinControl.SkinButton btn_All_Sel;
        private CCWin.SkinControl.SkinButton btn_Anti_Sel;
        private CCWin.SkinControl.SkinButton btn_No_Sel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lb_friend_num;
        private CCWin.SkinControl.SkinComboBox skinCBOLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lbx_selected;
        private System.Windows.Forms.Label lb_fri_num;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbx_searchs;
        private CCWin.SkinControl.SkinButton btn_search;
    }
}