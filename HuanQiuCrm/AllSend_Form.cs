﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using WChat;
using System.Data.SqlClient;
using HuanQiuCrm.Properties;

namespace HuanQiuCrm
{
    public partial class AllSend_Form : Skin_Color
    {
        public List<WXUser> _me;
        public WXUser _user;
        Settings st = new Settings();
        SqlHandle sql = new SqlHandle();
        public AllSend_Form()
        {
            InitializeComponent();
        }
        public void Get_Name(List<WXUser> a, WXUser b)
        {
            _me = a;
            _user = b;
        }
        /// <summary>
        /// 新建群发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_new_msg_Click(object sender, EventArgs e)
        {
            new_AllMsg na = new new_AllMsg();
            na.Pro_User(_me, _user);
            na.Show();
        }

        /// <summary>
        /// 显示群发消息
        /// </summary>
        private void read_data()
        {
            try {
                String strConn = "Data Source=bds249611391.my3w.com;Initial Catalog=bds249611391_db;Persist Security Info=True;User ID=bds249611391;Password=hq312453";
                SqlConnection conn = new SqlConnection(strConn);
                String sqlId = "select SendTime,name,wxName,recipient from " + st.UseAcct + "_群发记录表";
                conn.Open();
                SqlCommand cmd = new SqlCommand(sqlId, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataSet ds = new DataSet();
                da.Fill(ds, "" + st.UseAcct + "_群发记录表");

                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "" + st.UseAcct + "_群发记录表";
                conn.Close();

                dataGridView1.Columns[0].HeaderText = "发送时间";
                dataGridView1.Columns[0].Width = 130;
                dataGridView1.Columns[1].HeaderText = "客服名称";
                dataGridView1.Columns[2].HeaderText = "微信号";
                dataGridView1.Columns[3].HeaderText = "接收人";
                dataGridView1.Columns[3].Width = 180;
            } catch(Exception e) {
                WriteLog.Writelog("read_data", e.ToString());
            }
        }
        /// <summary>
        /// 刷新按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void skinButton1_Click(object sender, EventArgs e){
            read_data();
        }
        private void AllSend_Form_Load(object sender, EventArgs e){
            read_data();
        }

        /// <summary>
        /// 点击项目显示数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            RichTextBox.Text = "";
            string context = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            //MessageBox.Show(context);
            label_rec.Text = context.Length > 20 ? context.Substring(0, 21) + "...." : context;
            string time = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            if (sql.Select_AllSend_Type(st.UseAcct, time)[0].Trim() == "1")
            {
                RichTextBox.WordWrap = true;
                RichTextBox.Text = sql.Select_AllSend_Text(st.UseAcct, time)[0];
            }
            else
            {
                RichTextBox.WordWrap = false;
                string img = sql.Select_AllSend_Text(st.UseAcct, time)[0].Trim();
                // MessageBox.Show(img);
                try {
                    Image image = Image.FromFile(img);
                    Clipboard.SetImage(image);
                    RichTextBox.SelectionStart = 0;
                    RichTextBox.Paste();
                    Clipboard.Clear();
                   } catch { }
              
            }

        }
    }
}