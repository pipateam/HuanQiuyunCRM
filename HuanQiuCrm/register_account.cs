﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CCWin;
using WChat;

namespace HuanQiuCrm
{
    public partial class register_account : Skin_DevExpress
    {
        public register_account()
        {
            InitializeComponent();
        }
        SqlHandle sql = new SqlHandle();
       
        private void tbx_pwd_MouseEnter(object sender, EventArgs e)
        {
            ToolTip p = new ToolTip();
            p.ShowAlways = true;
            p.SetToolTip(this.tbx_pwd, "请注意：密码长度不能小于六位");
        }

        private void btn_register_Click(object sender, EventArgs e) {
            //此为只注册经理级账户 所以权限全开 开通状态不开
            string account = tbx_name.Text;
            string password = tbx_pwd.Text;
            string username = tbx_userName.Text;
            string contact = tbx_contact.Text;

            if (account == "" || password == "") {
                MessageBox.Show("请输入账户或密码");
            }
            else if (username == "" || contact == "") {
                MessageBox.Show("用户姓名与联系方式为必填项");
            }
            else {
                bool exists = false;
                if (exists) {
                    MessageBox.Show("该用户名已被占用，请重新输入");
                }
                else {
                    //不存在((IList)sql.selectAccount()).Contains("" + tbx_name.Text + "")
                    if (tbx_pwd.Text.Length >= 6) {
                        sql.create_AllSend_Tab(tbx_name.Text);
                        sql.create_wx_Tab(tbx_name.Text);
                        sql.createNewTab(tbx_name.Text);
                        sql.CreateNumOfWxTab(tbx_name.Text);  //创建微信表

                        sql.Create_reply_tab(tbx_name.Text);
                        sql.insertOnedata(tbx_name.Text, tbx_pwd.Text, 1, null, tbx_depart.Text, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, username, contact, 0);
                        MessageBox.Show("恭喜注册成功！");
                        this.Close();
                    }
                    else {
                        MessageBox.Show("密码长度不小于六位！");
                    }
                }
            }
        }
    }
}
